<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ukarts_beta');

/** MySQL database username */
define('DB_USER', 'ukarts_beta');

/** MySQL database password */
define('DB_PASSWORD', 'C$aU6Y!pw@bp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_MEMORY_LIMIT', '128M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2J&v~/GB4}OLXvVnyvwe}sv:^%vHOBam8YE@%m+1KhVGHK[4W{<kO3nnj^ELEw|S');
define('SECURE_AUTH_KEY',  'oB+?&SCO/KrSQS!<0Z}+[D@Sl,L[8wlD5Ok?4?tA%j;)/o&Ji}x//BBIdU80qJ43');
define('LOGGED_IN_KEY',    'nLj4_}`Upkz3--4JPIUoj7Pr^3Y;^%:w170`d%1:,}!ca#p`w;zxfSS*X-7_#%+]');
define('NONCE_KEY',        'wB0o@%``6$O-xAiAN.:@3X?pFC7p5v%S@cr.<m)EUzVyb~:F0/CSnS0]?nMkQ+_@');
define('AUTH_SALT',        'fytqH^~~4wRA=4-V>3xSS+rO,68/|M!>iN&EGz1uDsXxK7*;.wP#y^k_CC17kdw@');
define('SECURE_AUTH_SALT', '(}ge(Jx=uZ&x!K;]+iAPrPG~D_nYn8JVoiAB|M61Y{*;M_>d2LZ<Es.|^n g#osu');
define('LOGGED_IN_SALT',   'es;y_@_Hx6g|vV*O76 [xSld@W-`s.Zgb_koNU_-tOE0UKJn^L,^O~(+: 4 x1KM');
define('NONCE_SALT',       'ba>Qaq=g[j) [*c8B/m[c]Zf8-g/A%=HleH|:u%LmVmr%P9b^j Dhb?N3[oRvmq;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ua_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
