<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
	
	<div class="pagination-class">Found <?php echo $query->found_posts; ?> Results<br />
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?></div><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?> 
		<div class="filter-custome-class">

			<div class="hover-images-class"></div>
	<? $get_the_id= get_the_id(); 
			 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) {   }  else {  ?><span  class="onsale-product">Sale</span><?php  }  ?>
  <div class="main-tbl-blf-dd">
		<div class="hover-img"><a href="<?php echo the_permalink(); ?>">View Detail</a></div>
			<?php 
				if ( has_post_thumbnail() ) { 
					echo '<p class="thumbnail_custome">';
					the_post_thumbnail("small");
					echo '</p>';
				}
			?>
		</div>
			<div class="basic-info-product">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h2>
			<p>By <a href="<?php echo site_url(); ?>/artist-main/?user_id=<?php echo the_author_id(); ?>"><?php  the_author(); ?></a></p>
			
			
<p class="price_symbol"><?php  $get_the_id= get_the_id(); 

 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) { ?>£ <?  echo $price = get_post_meta($get_the_id, 'edd_price', true); }  else {  ?>  <strong class="item-price"><span><del>£ <? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price= get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php  }  
      ?>
        
	
	
      </p>
	</div> 
	  <div class="comlex-info-product">
	  <?php //dynamic_sidebar('reviews'); ?>
		
		
		
		
		<form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">

	<a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
	
	
	
		<div class="edd_purchase_submit_wrapper">
			<input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="€ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>
						
		</div><!--end .edd_purchase_submit_wrapper-->

		
	</form>
		
		
			<div class="star-shape-custome">
		 <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content<?php echo $get_the_id; ?> {
        display: none;
        position: fixed;
        top: 25%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        /*height: 50%;*/
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: white;
        z-index:9999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content<?php echo $get_the_id; ?> > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
background: #70C0EF;
border-radius: 100px;}   

      .white_content<?php echo $get_the_id; ?> input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

      .white_content<?php echo $get_the_id; ?> input[type="submit"]
      {position: absolute; right:0; top: 0; height: 50px;}

      .white_content<?php echo $get_the_id; ?> form
      {position: relative;}
</style>
 <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='block';document.getElementById('fade<?php echo $get_the_id; ?>').style.display='block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
	<div id="light<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
	<?php
    $current_user = wp_get_current_user();
       $current_user_id= $current_user->ID;
?>

	  <form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
            <input type="text" name="review_title" placeholder="Review Title" />
            <select name="review_starts">
                <option name="0">Select Review star:</option>
                <option name="1">1</option>
                <option name="2">2</option>
                <option name="3">3</option>
                <option name="4">4</option>
                <option name="5">5</option>
              </select>
              <input type="text" name="review_description" placeholder="Review Description" />
			   <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
			   <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
             <input type="submit" name="submit" value="submit" />

       </form>
 <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='none';document.getElementById('fade<?php echo $get_the_id; ?>').style.display='none'">X</a>

		
		
		</div>
		</div>
		
	
		
		
		<a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>
			
		
		
		</div> 
		</div> 
		
		<?php
	}
	?>
	<div class="pagination-class">Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?></div><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>
<style>
.checkout-custome-button::before {
    display: inline-block;
    font-family: Ionicons;
    speak: none;
    font-style: normal;
    font-weight: 400;
    font-variant: normal;
    text-transform: none;
    text-rendering: auto;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-size: 16px;
    content: "";
    margin-right: 5px;
    margin-top: -3px;
vertical-align: middle; }
.filter-custome-class {
  float: left;
  height: 30%;
  width: 31.3%;
}
.attachment-aaaa.size-aaaa {
margin: 2px 0px 0px -2px; }
p.thumbnail_custome {
    width: 97%;
    height: 190px;
    overflow: hidden;
}
.edd-wl-heading {
    display: none;
}
.edd-wish-list {
    display: none;
}
.label {
    color: black;
}
.six-featured-section {
    width: 10%;
}
</style>