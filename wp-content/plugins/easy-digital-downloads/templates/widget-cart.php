<?php
/**
 * This template is used to display the Downloads cart widget.
 */
$cart_items    = edd_get_cart_contents();
$cart_quantity = edd_get_cart_quantity();
$display       = $cart_quantity > 0 ? '' : ' style="display:none;"';
?>
<p class="edd-cart-number-of-items"<?php echo $display; ?>><?php _e( 'Number of items in cart', 'easy-digital-downloads' ); ?>: <span class="edd-cart-quantity"><?php echo $cart_quantity; ?></span>
</p>

<ul class="edd-cart">
<?php if( $cart_items ) : ?>

	<?php foreach( $cart_items as $key => $item ) : ?>

		<?php echo edd_get_cart_item_template( $key, $item, false ); ?>
		<td class="edd_cart_item_name">
						<?php
							if ( current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail( $item['id'] ) ) {
								echo '<div class="edd_cart_item_image">';
									echo get_the_post_thumbnail( $item['id'], apply_filters( 'edd_checkout_image_size', array( 25,25 ) ) );
								echo '</div>';
							}  
						//	$item_title = edd_get_cart_item_name( $item );
						//	echo '<span class="edd_checkout_cart_item_title">' . esc_html( $item_title ) . '</span>';   

							/**
							 * Runs after the item in cart's title is echoed
							 * @since 2.6
							 *
							 * @param array $item Cart Item
							 * @param int $key Cart key
							 */
							do_action( 'edd_checkout_cart_item_title_after', $item, $key );
						?>
					</td>
		
	<?php endforeach; ?>

	<?php edd_get_template_part( 'widget', 'cart-checkout' ); ?>

<?php else : ?>

	<?php edd_get_template_part( 'widget', 'cart-empty' ); ?>

<?php endif; ?>
</ul>


	
