<?php 
global $wpdb;
$result = $wpdb->get_results("select * from  ua_commission_payment");	

?>	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="scripts/jQuery.sortpaginate.js"></script>	
  <script>
  	$("table").sortpaginate();
  	$("table").sortpaginate({pageSize: 5});
  </script>
<div class="container">
	 <table class="table" id="example">
    <thead>
     <tr><th>S.no </th>
     	<th> Ref ID</th>
     	<th> Buyer Name</th>
     	<th>Seller Name</th>
     	<th>Amount Agreed</th>
     	<th>Escrow Amount</th>
     	<th>Pending Amount</th>
     	<th>release Amount</th></tr>
     </thead>
<tbody>
     <?php 
     $i=1;
      foreach($result as $results){
      	?>
      	<tr>
      		<td><?php echo $i; ?></td>
      		<td><?php echo $results->refrence_id; 	?></td>
      		<td><?php echo toshow_user_name($results->sender_id); 	?></td>
      		<td><?php echo toshow_user_name($results->reciever_id); 	?></td>
      		<td><?php echo $results->total_payment; 	?></td>
      		<td><?php echo $results->escrow_money; 	?></td>
      		<td><?php echo $results->pending_money; 	?></td>
      		<td><?php echo $results->release_amount; 	?></td>
      		
           


      	</tr>
 



      <?php ++$i; }

     ?>
</tbody>

	 </table>

<?php
function toshow_user_name($userid){
	$user=get_user_by('ID',$userid);
	return $user->display_name;

}


 ?>
 <style>
.sp_wrapper .sp_table table { table-layout: fixed; }

.sp_wrapper .sp_table thead th {
  cursor: pointer;
  background-color: #fff;
}

.sp_wrapper .sp_table .sp_sorted_asc {
  background-image: url("down-arrow.png");
  background-position: right center;
  background-repeat: no-repeat;
}

.sp_wrapper .sp_table .sp_sorted_desc {
  background-image: url("up-arrow.png");
  background-position: right center;
  background-repeat: no-repeat;
}

.sp_wrapper .sp_navigator .sp_next, .sp_wrapper .sp_navigator .sp_previous {
  margin-left: 5px;
  margin-right: 5px;
}
</style>
<script type="text/javascript">
  
  $(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

</div>