<?php
/*
Plugin Name: commission plugin
Description: A commission plugin show the commission amount .
Author: saurav thakur 
Version: 0.1
*/
add_action('admin_menu', 'test_plugin_setup_menu');
 
function test_plugin_setup_menu(){
        add_menu_page( 'commission', 'commission', 'manage_options', 'commission', 'test_init' );
}
 
function test_init(){
      	require_once(dirname(__FILE__) . '/commission.php');
}
 
 add_action('wp_enqueue_scripts','ava_test_init');

function ava_test_init() {
    wp_enqueue_script( 'jQuery.sortpaginate', plugins_url( '/scripts/jQuery.sortpaginate.js', __FILE__ ));
}
?>