#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YoBro Pro\n"
"POT-Creation-Date: 2017-12-20 16:01+0600\n"
"PO-Revision-Date: 2017-12-20 16:00+0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: yobro.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: admin/class-yobro-admin-menu.php:12 admin/class-yobro-admin-menu.php:13
msgid "Yo Bro"
msgstr ""

#: admin/class-yobro-admin-menu.php:23
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: admin/templates/yobro-settings.php:15
msgid "Yo Bro Settings"
msgstr ""

#: admin/templates/yobro-settings.php:17
msgid "Enable Files in Chat"
msgstr ""

#: admin/templates/yobro-settings.php:26
msgid "AWS Access Key ID"
msgstr ""

#: admin/templates/yobro-settings.php:32
msgid "AWS Secret Access Key"
msgstr ""

#: admin/templates/yobro-settings.php:38
msgid "AWS Bucket Name"
msgstr ""

#: admin/templates/yobro-settings.php:44
msgid "AWS Bucket Region"
msgstr ""

#: admin/templates/yobro-settings.php:50
msgid "Maximum File Size in MB"
msgstr ""

#: admin/templates/yobro-settings.php:56
msgid "Maximum Number of Files in Single Upload"
msgstr ""

#: admin/templates/yobro-settings.php:62
msgid "Enable Multiple Files"
msgstr ""

#: admin/templates/yobro-settings.php:71
msgid "Enable Avatar"
msgstr ""

#: admin/templates/yobro-settings.php:80
msgid "Chat Page Url"
msgstr ""

#: admin/templates/yobro-settings.php:86
msgid "Number of Conversation in Notification"
msgstr ""

#: admin/templates/yobro-settings.php:92
msgid "Number of Message in MessageBox"
msgstr ""

#: admin/templates/yobro-settings.php:101
msgid "Chat Button Name"
msgstr ""

#: admin/templates/yobro-settings.php:108
msgid "Save Changes"
msgstr ""

#: app/class-bbpress-buddypress.php:16
msgid "<div id=\"yobro-notification\" data-bbp=\"true\"></div>"
msgstr ""

#: app/class-bbpress-buddypress.php:25
msgid "<div id=\"yobro-new-message\" class=\"buddypressCustom\" data-id=\""
msgstr ""

#: app/class-yobro-install.php:32
msgid "Welcome to YoBro – You‘re almost ready to start :)"
msgstr ""

#: app/yobro-visual-composer.php:6
msgid "Yo Bro Chat Box"
msgstr ""

#: app/yobro-visual-composer.php:9 app/yobro-visual-composer.php:19
msgid "Pages"
msgstr ""

#: app/yobro-visual-composer.php:16
msgid "Yo Bro Chat Notification"
msgstr ""

#: yobro.php:25
msgid ""
"Sorry this plugin use some <b>PHP VERSION 5.4</b> functionality. If you want "
"to use this plugin please update your server <b>PHP VERSION 5.4</b> or "
"higher."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "YoBro Pro"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://redqteam.com/demo/plugins/userplace"
msgstr ""

#. Description of the plugin/theme
msgid "A simple WordPress messaging / chat solution"
msgstr ""

#. Author of the plugin/theme
msgid "redqteam"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.redqteam.com"
msgstr ""
