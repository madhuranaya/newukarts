
(function($){$(document).ready(function(){$('#post-gallery .tabs .tab-panel').hide();$('#post-gallery .tabs .tab-panel.active').show();$('#post-gallery .tabs .tab-actions li').each(function(){$(this).click(function(e){e.preventDefault()
$('#post-gallery .tabs .tab-panel').hide();$('#post-gallery .tabs .tab-panel.active').removeClass('active');$('#post-gallery .tabs .tab-actions .active').removeClass('active');$('#post-gallery .tabs '+$(this).data('tab')).show();$('#post-gallery .tabs '+$(this).data('tab')).addClass('active');$(this).addClass('active');});});$('#post-gallery .spectrum-colorpicker').each(function(){$(this).spectrum({color:$(this).val()?$(this).val():'#fff',showInput:true,allowEmpty:true,showAlpha:true,preferredFormat:"hex",});});$('#post-gallery .input-trigger').each(function(){$(this).change(function(){$('#post-gallery .if-input-'+$(this).attr('id')).hide();$('#post-gallery .if-input-'+$(this).attr('id')+'.if-value-'+$(this).val()).show();});$('#post-gallery .if-input-'+$(this).attr('id')).hide();$('#post-gallery .if-input-'+$(this).attr('id')+'.if-value-'+$(this).val()).show();});});})(jQuery);(function($){$.fn.slideEditor=function(options)
{var $self=$(this);$self.settings=$.extend({id:undefined,galleryId:undefined,$loader:$self.find('.frame-loader'),$content:$self.find('.frame-content'),$toolbar:$self.find('.frame-toolbar'),},options)
$self.open=function(id,galleryId)
{$self.settings.id=id;$self.settings.galleryId=galleryId;$self.show();$self.load();}
$self.close=function()
{$self.hide();}
$self.load=function()
{$self.settings.$loader.show();$self.settings.$content.hide();$self.settings.$toolbar.hide();$.post($self.settings.url,{action:'gallery_attachment_get',id:$self.settings.id,gallery_id:$self.settings.galleryId,},$self.onLoad);}
$self.onLoad=function(response)
{$self.find('select').val(undefined);$self.find(':input').val(undefined);$self.find(':input').attr('value',undefined);$self.find(':input[type="checkbox"]').attr('value',1);$self.find(':input[type="radio"]').attr('value',1);$self.find(':input[type="checkbox"]').val(1);$self.find(':input[type="radio"]').val(1);$self.find(':input[type="checkbox"]').prop('checked',true);$self.find(':input[type="radio"]').prop('checked',true);$self.find('input#format_show_overlay').prop('checked',false);if(response.error!==undefined&&response.error===false&&response.data.gallery_settings){for(var key in response.data.gallery_settings){var $el=$self.find('input[name="'+key+'"]');if($el&&$el.length>0){switch($el.attr('type')){case'text':case'number':case'url':case'email':$el.val(response.data.gallery_settings[key]);$el.attr('value',response.data.gallery_settings[key]);break
case'checkbox':case'radio':$el.prop('checked',response.data.gallery_settings[key]);break}
if($el.hasClass('spectrum-colorpicker'))
$el.spectrum('set',$el.val());}
$el=$self.find('select[name="'+key+'"]');if($el&&$el.length>0){$el.val(response.data.gallery_settings[key]);}
$el=$self.find('textarea[name="'+key+'"]');if($el&&$el.length>0){$el.val(response.data.gallery_settings[key]);}}}
$self.settings.$loader.hide();$self.settings.$content.show();$self.settings.$toolbar.show();}
$self.update=function()
{$self.settings.$loader.show();$self.settings.$content.hide();$self.settings.$toolbar.hide();$.post($self.settings.url,$.param({action:'gallery_attachment_update',id:$self.settings.id,gallery_id:$self.settings.galleryId,},true)
+'&'
+$self.find(':input').serialize(),$self.onUpdate);}
$self.onUpdate=function(response)
{if(response.error!==undefined&&response.error===false){}
$self.settings.$loader.hide();$self.settings.$content.show();$self.settings.$toolbar.show();}
$(document).on('click','.slide-editor-caller',function(e){e.preventDefault();$self.open($(this).data('id'),$(this).data('gallery'));});$self.find('button.media-modal-close').click(function(e){$self.close();});$self.find('button.modal-button').click(function(e){$self.update();});}
$('#slide-editor').each(function(){$(this).slideEditor({url:$(this).data('url')});});})(jQuery);