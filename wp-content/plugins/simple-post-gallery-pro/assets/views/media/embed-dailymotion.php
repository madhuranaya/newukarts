<?php
/**
 * Responsive embed dailymotion videos.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.0
 */ 
?>
<div class="video-frame <?php if ( empty( $height ) ) : ?>video-responsive<?php endif ?>"
    <?php if ( $height && $height > 0 ) : ?>
        style="height: <?= $height ? $height : 375 ?>px;"
    <?php else : ?>
        style="position: relative; overflow: hidden; min-height: 150px;"
    <?php endif ?>
>
    <iframe src="//www.dailymotion.com/embed/video/<?= $attachment->video_id ?>"
        <?php if ( empty( $height ) ) : ?>
            width="640" height="368"
        <?php endif ?>
        frameborder="0"
        webkitallowfullscreen mozallowfullscreen allowfullscreen
        class="attachment-<?= $attachment->ID ?>"
        img="<?= $attachment->edit_url ?>"
        style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;"
    ></iframe>
</div>