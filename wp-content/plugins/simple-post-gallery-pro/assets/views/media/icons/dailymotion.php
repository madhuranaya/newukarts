<?php
/**
 * Dailymotion icon
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.0
 */ 
?><img title="Dailymotion" src="<?= assets_url( 'svgs/dailymotion.svg', __FILE__ ) ?> ?>" style="max-height:15px;width:auto;max-width:165px"/>