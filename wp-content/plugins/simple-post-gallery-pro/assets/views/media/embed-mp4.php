<?php
/**
 * Responsive video canvas (MP4).
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGallery
 * @version 1.3.0
 */ 
?>
<div class="video-frame <?php if ( empty( $height ) ) : ?>video-responsive<?php endif ?>"
    <?php if ( $height && $height > 0 ) : ?>
        style="height: <?= $height ? $height : 375 ?>px;"
    <?php else : ?>
        style="position: relative; overflow: hidden; min-height: 150px;"
    <?php endif ?>
>
    <video controls
        class="attachment-<?= $attachment->ID ?>"
        style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;"
        <?php if ( $height === null ) : ?>
            width="640" height="368"
        <?php endif ?>
    >
        <source src="<?= $attachment->url ?>" type="<?= $attachment->mime ?>">
        Your browser does not support the video tag.
    </video>
</div>