<?php
/**
 * input view
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */
?> <input id="format_<?= isset( $name ) ? $name : uniqid() ?>" type="<?= isset( $type ) ? $type : 'text' ?>" class="<?= isset( $class ) ? $class : ( $type === 'checkbox' ? 'checkbox' : 'regular-text') ?>" <?php if ( !isset( $fname ) && isset( $name ) ) : ?> name="format_data[<?= $name ?>]" <?php endif ?> <?php if ( isset( $fname ) ) : ?> name="<?= $fname ?>" <?php endif ?> <?php if ( isset( $type ) && $type === 'checkbox' ) : ?> value="1" <?php if ( isset( $data[$name] ) && ! empty( $data[$name] ) ) : ?> checked="checked" <?php endif ?> <?php else : ?> value="<?= isset( $data[$name] ) ? $data[$name] : null ?>" <?php endif ?> > <?php if ( isset( $desc ) ) : ?> <?php if ( isset( $hint ) && $hint === true ) : ?> <?php else : ?> <br><span class="description"><?= $desc ?></span> <?php endif ?> <?php endif ?>