<?php
/**
 * Settings pro view.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */ 
?> <section id="cache" <?php if ( $tab != 'pro' ) : ?> style="display: none;" <?php endif ?> ><h1 id="top"><?php _e( 'Pro', 'simple-post-gallery-pro' ) ?></h1><p><?php _e( 'Extended settings provided by <strong>Post Gallery Pro</strong>.', 'simple-post-gallery-pro' ) ?></p><h2> <?php _e( 'Default Appearance', 'simple-post-gallery-pro' ) ?> </h2><table class="form-table"><tr valign="top"><th scope="row"><?php _e( 'Arrow color', 'simple-post-gallery-pro' ) ?></th><td><input type="text" class="spectrum-colorpicker" name="default_arrow_color" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_arrow_color'] ) ) : ?> value="<?= $postGallery->extra['default_arrow_color'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default arrow icon color for galleries with formats that include a slider.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr valign="top"><th scope="row"><?php _e( 'Arrow background color', 'simple-post-gallery-pro' ) ?></th><td><input type="text" class="spectrum-colorpicker" name="default_arrow_background_color" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_arrow_background_color'] ) ) : ?> value="<?= $postGallery->extra['default_arrow_background_color'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default background color (of the circle surrounding the icon) for galleries with formats that include a slider.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr valign="top"><th scope="row"><?php _e( 'Arrow size', 'simple-post-gallery-pro' ) ?></th><td><input type="number" name="default_arrow_size" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_arrow_size'] ) ) : ?> value="<?= $postGallery->extra['default_arrow_size'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default arrow size in pixels.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr valign="top"><th scope="row"><?php _e( 'Video play color', 'simple-post-gallery-pro' ) ?></th><td><input type="text" class="spectrum-colorpicker" name="default_play_color" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_play_color'] ) ) : ?> value="<?= $postGallery->extra['default_play_color'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default video play icon color.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr valign="top"><th scope="row"><?php _e( 'Video play background color', 'simple-post-gallery-pro' ) ?></th><td><input type="text" class="spectrum-colorpicker" name="default_play_background_color" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_play_background_color'] ) ) : ?> value="<?= $postGallery->extra['default_play_background_color'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default video play background color (of the circle surrounding the icon).', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr valign="top"><th scope="row"><?php _e( 'Video play size', 'simple-post-gallery-pro' ) ?></th><td><input type="number" class="regular-text" name="default_play_size" <?php if ( $postGallery->extra && isset( $postGallery->extra['default_play_size'] ) ) : ?> value="<?= $postGallery->extra['default_play_size'] ?>" <?php endif ?> ><br><span class="description"> <?php _e( 'Default video play icon size in pixels.', 'simple-post-gallery-pro' ) ?> </span></td></tr></table><script type="text/javascript">(function($) { $(document).ready(function() {
        /**
         * Enable color picker.
         * @since 1.0.0
         * @see https://bgrins.github.io/spectrum/
         */
        $('.spectrum-colorpicker').each(function() {
            $(this).spectrum({
                color: $(this).val() ? $(this).val() : '#fff',
                showInput: true,
                allowEmpty: true,
                showAlpha: true,
                preferredFormat: "hex",
            });
        });     
    }); })(jQuery);</script> <?php do_action( 'postgallery_settings_pro_view' ) ?> </section>