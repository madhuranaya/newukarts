<?php
/**
 * thumb-slider.form view.
 * Generated with ayuco.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
global $postgallerypro;
?> <div class="form if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><div class="thumb-slider heading"> <?php _e( 'Format options', 'simple-post-gallery-pro' ) ?> </div><div class="options body"><div class="tabs"><ul class="tab-actions"><li class="active" data-tab="#slides"><span class="dashicons dashicons-format-video"></span> <?php _e( 'Slides', 'simple-post-gallery-pro' ) ?></li><li data-tab="#appearance"><span class="dashicons dashicons-admin-appearance"></span> <?php _e( 'Appearance', 'simple-post-gallery-pro' ) ?></li><li data-tab="#images"><span class="dashicons dashicons-format-gallery"></span> <?php _e( 'Images', 'simple-post-gallery-pro' ) ?></li><li data-tab="#videos"><span class="dashicons dashicons-video-alt2"></span> <?php _e( 'Videos', 'simple-post-gallery-pro' ) ?></li></ul><div id="slides" class="tab-panel active"><table class="form-table" style="margin:0"><tbody><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_slides_to_show"><?php _e( 'Slides to show', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'slides_to_show',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> <br class="if-reactive if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><span class="description if-reactive if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"> <?php _e( 'For the thumbnail slider below.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_slides_to_scroll"><?php _e( 'Slides to scroll', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'slides_to_scroll',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> <br class="if-reactive if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><span class="description if-reactive if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"> <?php _e( 'For the thumbnail slider below.', 'simple-post-gallery-pro' ) ?> </span></td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_slides_speed"><?php _e( 'Speed', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'slides_speed',
                                    'type'  => 'number',
                                    'data'  => $data,
                                    'desc'  => __( 'Speed in milliseconds needed to move to the next slide.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_is_infinite"><?php _e( 'Infinite', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'is_infinite',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Make slides scroll infinitely in a loop.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_is_autoplay"><?php _e( 'Auto play', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'is_autoplay',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Make slides slide automatically.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_autoplay_speed"><?php _e( 'Auto play Speed', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'autoplay_speed',
                                    'type'  => 'number',
                                    'data'  => $data,
                                    'desc'  => __( 'Time in milliseconds in which autoplay will keep the current slide visible prior to moving to the next one.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_is_rtl"><?php _e( 'Right to left', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'is_rtl',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Slide from right to left.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr></tbody></table></div><!--.tab-panel--><div id="appearance" class="tab-panel"><table class="form-table" style="margin:0"><tbody><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_show_arrows"><?php _e( 'Show arrows', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'show_arrows',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Display navigation arrows on slider sides.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_arrow_color"><?php _e( 'Arrow color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                    'name'  => 'arrow_color',
                                    'data'  => $data,
                                    'desc'  => __( 'Arrow icon color.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_arrow_background_color"><?php _e( 'Arrow back color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                    'name'  => 'arrow_background_color',
                                    'data'  => $data,
                                    'desc'  => __( 'Background color of the circle surrounding the icon.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_arrow_size"><?php _e( 'Arrow size', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'arrow_size',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_show_dots"><?php _e( 'Show dots', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'show_dots',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Display navigation dots below the slider.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_is_center_mode"><?php _e( 'Center mode', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'is_center_mode',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'class' => 'checkbox input-trigger',
                                    'desc'  => __( 'Highlight the thumbnail displayed in the center of the slider.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_is_center_mode if-value-1 if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_center_padding"><?php _e( 'Center mode padding', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'center_padding',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_fade"><?php _e( 'Fade', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'fade',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Fade effect between slides.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_has_adaptive_height"><?php _e( 'Adaptive', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'has_adaptive_height',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Adapt to each slide\'s height.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-slider if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_on_ready"><?php _e( 'Show on ready', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'on_ready',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Hides gallery until web page has been loaded by the browser.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr></tbody></table></div><!--.tab-panel--><div id="images" class="tab-panel"><table class="form-table" style="margin:0"><tbody><tr class="if-reactive if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_image_size"><?php _e( 'Image size', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.select', [
                                    'name'      => 'image_size',
                                    'data'      => $data,
                                    'options'   => $image_sizes,
                                    'class'     => 'select input-trigger',
                                    'desc'      => __( 'Sizes can be changed in Wordpress settings. Default: <strong>Large</strong>', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_image_size if-value-custom if-value-custom if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_image_width"><?php _e( 'Image width', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'image_width',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_image_size if-value-custom if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_image_height"><?php _e( 'Image height', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'image_height',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_image_size if-value-custom if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_image_crop"><?php _e( 'Image crop', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'image_crop',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_thumb_size"><?php _e( 'Thumbnail size', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.select', [
                                    'name'      => 'thumb_size',
                                    'data'      => $data,
                                    'options'   => $image_sizes,
                                    'class'     => 'select input-trigger',
                                    'desc'      => __( 'Sizes can be changed in Wordpress settings. Default: <strong>Thumb</strong>', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_thumb_size if-value-custom if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_thumb_width"><?php _e( 'Thumb width', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'thumb_width',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_thumb_size if-value-custom if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_thumb_height"><?php _e( 'Thumb height', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'thumb_height',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr><tr class="if-reactive if-input-format_thumb_size if-value-custom if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_thumb_crop"><?php _e( 'Thumb crop', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'thumb_crop',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                ] ) ?> </td></tr></tbody></table></div><!--.tab-panel--><div id="videos" class="tab-panel"><table class="form-table" style="margin:0"><tbody><tr class="if-reactive if-slider if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_video_same_height"><?php _e( 'Same height', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'video_same_height',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Make videos\' height same as the slides.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_show_play"><?php _e( 'Show play', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'show_play',
                                    'type'  => 'checkbox',
                                    'data'  => $data,
                                    'desc'  => __( 'Display video play icon over video\'s thumbnails.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_play_color"><?php _e( 'Play color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                    'name'  => 'play_color',
                                    'data'  => $data,
                                    'desc'  => __( 'Video play icon color.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_play_background_color"><?php _e( 'Play back color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                    'name'  => 'play_background_color',
                                    'data'  => $data,
                                    'desc'  => __( 'Background color of the circle surrounding the video play icon.', 'simple-post-gallery-pro' ),
                                ] ) ?> </td></tr><tr class="if-reactive if-thumb_slider if-vertical if-slider_sync if-slider_sync_inverted if-vertical_left if-vertical_right"><th><label for="format_play_size"><?php _e( 'Play size', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                    'name'  => 'play_size',
                                    'type'  => 'number',
                                    'data'  => $data,
                                ] ) ?> </td></tr></tbody></table></div><!--.tab-panel--></div><!--.tabs--></div><!--.body--></div><!--.form-->