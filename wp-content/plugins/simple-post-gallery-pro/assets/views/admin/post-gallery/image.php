<?php
/**
 * image view
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */
?> <input id="format_<?= isset( $name ) ? $name : uniqid() ?>" type="text" class="<?= isset( $class ) ? $class : '' ?>spectrum-colorpicker color" <?php if ( !isset( $fname ) && isset( $name ) ) : ?> name="format_data[<?= $name ?>]" <?php endif ?> <?php if ( isset( $fname ) ) : ?> name="<?= $fname ?>" <?php endif ?> value="<?= isset( $data[$name] ) ? $data[$name] : null ?>"> <?php if ( isset( $desc ) ) : ?> <?php if ( isset( $hint ) && $hint === true ) : ?> <?php else : ?> <br><span class="description"><?= $desc ?></span> <?php endif ?> <?php endif ?>