<?php
/**
 * Edit slide button on attachment.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */
?> <span class="action edit-slide slide-editor-caller" data-id="<?= !empty( $attachment ) ? $attachment->ID : '{{id}}' ?>" data-gallery="<?= $post->ID ?>"><i class="fa fa-pencil" aria-hidden="true"></i></span>