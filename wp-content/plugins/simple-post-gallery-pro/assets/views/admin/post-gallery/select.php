<?php
/**
 * select view
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */
?> <select id="format_<?= isset( $name ) ? $name : uniqid() ?>" class="<?= isset( $class ) ? $class : 'select' ?>" <?php if ( !isset( $fname ) && isset( $name ) ) : ?> name="format_data[<?= $name ?>]" <?php endif ?> <?php if ( isset( $fname ) ) : ?> name="<?= $fname ?>" <?php endif ?> > <?php foreach ( $options as $key => $value ) : ?> <option value="<?= $key ?>" <?php if ( isset( $data[$name] ) && $data[$name] === $key ) : ?> selected="selected" <?php endif ?> ><?= $value ?></option> <?php endforeach ?> </select> <?php if ( isset( $desc ) ) : ?> <?php if ( isset( $hint ) && $hint === true ) : ?> <?php else : ?> <br><span class="description"><?= $desc ?></span> <?php endif ?> <?php endif ?>