<?php
/**
 * textarea view
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */
?> <textarea id="format_<?= isset( $name ) ? $name : uniqid() ?>" class="<?= isset( $class ) ? $class : 'regular-text' ?>" rows="<?= isset( $rows ) ? $rows : '4' ?>" <?php if ( !isset( $fname ) && isset( $name ) ) : ?> name="format_data[<?= $name ?>]" <?php endif ?> <?php if ( isset( $fname ) ) : ?> name="<?= $fname ?>" <?php endif ?> ><?= isset( $data[$name] ) ? $data[$name] : null ?></textarea> <?php if ( isset( $desc ) ) : ?> <?php if ( isset( $hint ) && $hint === true ) : ?> <?php else : ?> <br><span class="description"><?= $desc ?></span> <?php endif ?> <?php endif ?>