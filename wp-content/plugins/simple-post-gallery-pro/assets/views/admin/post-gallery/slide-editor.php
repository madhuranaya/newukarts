<?php
/**
 * admin.post-gallery.slide-editor view.
 * Slide editor.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
global $postgallerypro;
?> <div id="slide-editor" data-url="<?= admin_url( '/admin-ajax.php' ) ?>" style="display: none"><div class="media-modal wp-core-ui"><button type="button" class="media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text"><?php _e( 'Close' ) ?></span></span></button><div class="media-modal-content"><div class="media-frame"><div class="frame-title"><h1><?php _e( 'Edit Gallery Media', 'simple-post-gallery-pro' ) ?></h1></div><div class="frame-content"><h2><span class="dashicons dashicons-admin-links"></span> <?php _e( 'Click behaviour', 'simple-post-gallery-pro' ) ?></h2><div id="behaviour"><table class="form-table" style="margin:0"><tbody><tr><th><label for="format_use_lightbox"><?php _e( 'Use lightbox', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'use_lightbox',
                                            'name'  => 'use_lightbox',
                                            'type'  => 'checkbox',
                                            'data'  => [],
                                            'desc'  => __( 'Disable lightbox to make custom url functional.', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr><tr><th><label for="format_url"><?php _e( 'Custom url', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'custom_url',
                                            'name'  => 'custom_url',
                                            'type'  => 'url',
                                            'data'  => [],
                                            'desc'  => __( 'Disable lightbox to make this url functional.', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr></tbody></table></div><h2><span class="dashicons dashicons-admin-page"></span> <?php _e( 'Overlay', 'simple-post-gallery-pro' ) ?></h2><div id="overlay"><table class="form-table" style="margin:0"><tbody><tr><th><label for="format_use_lightbox"><?php _e( 'Display overlay', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'show_overlay',
                                            'name'  => 'show_overlay',
                                            'type'  => 'checkbox',
                                            'data'  => ['show_overlay' => 0],
                                            'desc'  => __( 'Displays an overlay box with text over the media.', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_position"><?php _e( 'Position', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.select', [
                                            'fname'     => 'overlay_position',
                                            'name'      => 'overlay_position',
                                            'data'      => [],
                                            'options'   => apply_filters( 'postgallery_overlay_positions', [
                                                            'topleft'       => __( 'Top left', 'simple-post-gallery-pro' ),
                                                            'topcenter'     => __( 'Top center', 'simple-post-gallery-pro' ),
                                                            'topright'      => __( 'Top right', 'simple-post-gallery-pro' ),
                                                            'midleft'       => __( 'Middle left', 'simple-post-gallery-pro' ),
                                                            'midcenter'     => __( 'Middle center', 'simple-post-gallery-pro' ),
                                                            'midright'      => __( 'Middle right', 'simple-post-gallery-pro' ),
                                                            'bottomleft'    => __( 'Bottom left', 'simple-post-gallery-pro' ),
                                                            'bottomcenter'  => __( 'Bottom center', 'simple-post-gallery-pro' ),
                                                            'bottomright'   => __( 'Bottom right', 'simple-post-gallery-pro' ),
                                                        ] ),
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_background_color"><?php _e( 'Back color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                            'fname' => 'overlay_background_color',
                                            'name'  => 'overlay_background_color',
                                            'data'      => [],
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_color"><?php _e( 'Text color', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.image', [
                                            'fname' => 'overlay_color',
                                            'name'  => 'overlay_color',
                                            'data'      => [],
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_text"><?php _e( 'Text', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.textarea', [
                                            'fname' => 'overlay_text',
                                            'name'  => 'overlay_text',
                                            'data'  => [],
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_text_size"><?php _e( 'Text size', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'overlay_text_size',
                                            'name'  => 'overlay_text_size',
                                            'type'  => 'number',
                                            'data'  => [],
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_width"><?php _e( 'Width', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'overlay_width',
                                            'name'  => 'overlay_width',
                                            'type'  => 'text',
                                            'data'  => [],
                                            'desc'  => __( 'Type <b>auto</b> to auto adjust based on text, otherwise type a value in pixels (i.e. 150px) or in percentage (i.e. 50%).', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_margin"><?php _e( 'Margin', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'overlay_margin',
                                            'name'  => 'overlay_margin',
                                            'type'  => 'number',
                                            'data'  => [],
                                            'desc'  => __( 'Margin value in pixels.', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr><tr><th><label for="format_overlay_padding"><?php _e( 'Padding', 'simple-post-gallery-pro' ) ?></label></th><td> <?php $postgallerypro->view( 'admin.post-gallery.input', [
                                            'fname' => 'overlay_padding',
                                            'name'  => 'overlay_padding',
                                            'type'  => 'number',
                                            'data'  => [],
                                            'desc'  => __( 'Padding value in pixels.', 'simple-post-gallery-pro' ),
                                        ] ) ?> </td></tr></tbody></table></div></div><!--.media-frame-content--><div class="frame-loader" style="display: none"><i class="fa fa-spinner fa-spin"></i> <?php _e( 'Loading...', 'simple-post-gallery-pro' ) ?> </div><div class="frame-toolbar"><button type="button" class="button modal-button button-primary button-large"> <?php _e( 'Update' ) ?> </button></div></div><!--.media-frame--></div><!--.media-modal-content--></div><!--.media-modal--></div><!--#slide-editor-->