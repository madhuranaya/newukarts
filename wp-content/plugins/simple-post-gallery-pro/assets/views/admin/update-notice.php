<?php
/**
 * Available update notice.
 *
 * @author Cami Mostajo
 * @package WPMVC\Addons\LicenseKey
 * @license MIT
 * @version 1.2.0
 */
?> <div class="notice notice-warning"><div class="content" style="overflow: hidden;"><img src="<?= assets_url( 'svgs/icon.svg', __FILE__ ) ?>" width="125" style="float: left; margin-right: 20px;"><h3 style="margin-bottom: 5px;"><?php _e( 'Update Available!' ) ?></h3><p style="margin-top: 0;"><span><?php _e( 'New update available for <strong>Post Gallery PRO</strong>', 'simple-post-gallery-pro' ) ?>.</span></p> <?php $main->addon_updater_button(
            $license_key->data->downloadable->url,
            'button-primary'
        ) ?> <small class="description">&lt;- <?php _e( 'Click to begin, then wait until update is completed.', 'simple-post-gallery-pro' ) ?> </small></div></div>