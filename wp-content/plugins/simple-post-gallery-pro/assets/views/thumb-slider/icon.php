<?php
/**
 * thumb-slider.icon view.
 * Generated with ayuco.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
?>
<img src="<?= assets_url( 'svgs/thumb-slider.svg', __FILE__ ) ?>"
    alt="thumb_slider"
    class="img-responsive"
    title="<?php _e( 'Thumbnails Slider', 'simple-post-gallery-pro' ) ?>"
/>