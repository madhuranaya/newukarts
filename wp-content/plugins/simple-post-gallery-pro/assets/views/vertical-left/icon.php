<?php
/**
 * slider-sync.icon view.
 * Generated with ayuco.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
?>
<img src="<?= assets_url( 'svgs/vertical-left.svg', __FILE__ ) ?>"
    alt="vertical_left"
    class="img-responsive"
    title="<?php _e( 'Vertical Left', 'simple-post-gallery-pro' ) ?>"
/>