<?php
/**
 * Gallery view/template.
 * Format: default
 *
 * COPY THIS FILE IN YOUR THEME FOR CUSTOMIZATIONS. LOCATION:
 * [theme-folder]/views/plugins/post-gallery/gallery.php
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.0
 */ 
?>
<div class="post-gallery post-gallery-wrapper default">
    <div id="post-gallery-<?= $post->ID ?>" class="thumbs-container">
        <?php foreach ( $post->gallery as $attachment ) : ?>
            <?php $attachment->get_gallery_settings( $post->ID ) ?>
            <a class="item <?php if ( $attachment->is_video ) : ?>is-video<?php endif ?> <?php if ( $attachment->custom_url ) : ?>custom-url<?php elseif ( $attachment->is_lightbox ) : ?>swipebox<?php endif ?>"
                <?php if ( $attachment->custom_url ) : ?>
                    href="<?= $attachment->custom_url ?>"
                <?php else : ?>
                    href="<?php echo $attachment->is_video && !$attachment->is_uploaded ? $attachment->video_url : $attachment->url ?>"
                    rel="post-gallery-<?php echo $post->ID ?>"
                    title="<?php echo $attachment->caption ?>"
                <?php endif ?>
            >
                <?php if ( $attachment->is_video ) : ?>
                    <?= $play ?>
                <?php endif ?>
                <img src="<?php echo $attachment->thumb_url ? $attachment->thumb_url : $attachment->no_thumb_url ?>"
                    alt="<?= $attachment->alt ?>"
                />
            </a>
        <?php endforeach ?>
    </div>
</div>