<?php
/**
 * Gallery view/template.
 * Format: vertical
 *
 * COPY THIS FILE IN YOUR THEME FOR CUSTOMIZATIONS. LOCATION:
 * [theme-folder]/views/plugins/post-gallery/gallery-vertical.php
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.0
 */ 
?>
<div class="post-gallery-wrapper">
    <div id="post-gallery-<?= $post->ID ?>"
        class="post-gallery format-vertical thumbs-container"
        data-autoplay="<?= $post->format_data['is_autoplay'] ? 1 : 0 ?>"
        data-infinite="<?= $post->format_data['is_infinite'] ? 1 : 0 ?>"
        data-dots="<?= $post->format_data['show_dots'] ? 1 : 0 ?>"
        data-arrows="<?= $post->format_data['show_arrows'] ? 1 : 0 ?>"
        data-centermode="<?= $post->format_data['is_center_mode'] ? 1 : 0 ?>"
        data-rtl="<?= $post->format_data['is_rtl'] ? 1 : 0 ?>"
        data-speed="<?= $post->format_data['slides_speed'] ?>"
        data-autoloadspeed="<?= $post->format_data['autoplay_speed'] ?>"
        data-toshow="<?= $post->format_data['slides_to_show'] ?>"
        data-toscroll="<?= $post->format_data['slides_to_scroll'] ?>"
        data-centerpadding="<?= $post->format_data['center_padding'] ?>"
        <?php if ( $post->format_data['on_ready'] ) : ?>style="display: none"<?php endif ?>
    >
        <?php foreach ( $post->gallery as $attachment ) : ?>
            <?php $attachment->get_gallery_settings( $post->ID ) ?>
            <a class="item <?php if ( $attachment->is_video ) : ?>is-video<?php endif ?> <?php if ( $attachment->custom_url ) : ?>custom-url<?php else : ?>swipebox<?php endif ?>"
                <?php if ( $attachment->custom_url ) : ?>
                    href="<?= $attachment->custom_url ?>"
                <?php else : ?>
                    href="<?php echo $attachment->is_video && !$attachment->is_uploaded ? $attachment->video_url : $attachment->url ?>"
                    rel="post-gallery-<?php echo $post->ID ?>"
                    title="<?php echo $attachment->caption ?>"
                <?php endif ?>
            >
                <?php if ( $attachment->is_video && isset( $post->format_data['show_play'] ) && $post->format_data['show_play'] ) : ?>
                    <?= $play ?>
                <?php endif ?>
                <?php if ( ! isset( $post->format_data['thumb_size'] )
                    || empty( $post->format_data['thumb_size'] )
                    || $post->format_data['thumb_size'] === 'default'
                    || $post->format_data['thumb_size'] === 'thumb'
                ) : ?>
                    <img src="<?php echo $attachment->thumb_url ? $attachment->thumb_url : $attachment->no_thumb_url ?>"
                        alt="<?= $attachment->alt ?>"
                    />
                <?php elseif ( $post->format_data['thumb_size'] === 'medium' ) : ?>
                    <img src="<?php echo $attachment->medium_url ? $attachment->medium_url : $attachment->no_medium_url ?>"
                        alt="<?= $attachment->alt ?>"
                    />
                <?php elseif ( $post->format_data['thumb_size'] === 'large' ) : ?>
                    <img src="<?php echo $attachment->large_url ? $attachment->large_url : $attachment->no_large_url ?>"
                        alt="<?= $attachment->alt ?>"
                    />
                <?php elseif ( $post->format_data['thumb_size'] === 'custom' ) : ?>
                    <img src="<?= $attachment->get_res( $post->format_data['thumb_width'], $post->format_data['thumb_height'], $post->format_data['thumb_crop'] ) ?>"
                        alt="<?= $attachment->alt ?>"
                    />
                <?php endif ?>
            </a>
        <?php endforeach ?>
    </div>
    <div class="arrows" <?php if ( ! $post->format_data['show_arrows'] ) : ?>style="display:none"<?php endif ?>>
        <?= $arrow_prev ?>
        <?= $arrow_next ?>
    </div>
</div>