<?php
/**
 * Gallery view/template.
 * Format: vertical_right
 *
 * COPY THIS FILE IN YOUR THEME FOR CUSTOMIZATIONS. LOCATION:
 * [theme-folder]/views/plugins/post-gallery/gallery-vertical-right.php
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.0
 */ 
?>
<div class="post-gallery-wrapper">
    <div id="post-gallery-<?= $post->ID ?>"
        class="post-gallery format-vertical-side format-vertical-left"
        data-autoplay="<?= $post->format_data['is_autoplay'] ? 1 : 0 ?>"
        data-infinite="<?= $post->format_data['is_infinite'] ? 1 : 0 ?>"
        data-dots="<?= $post->format_data['show_dots'] ? 1 : 0 ?>"
        data-arrows="<?= $post->format_data['show_arrows'] ? 1 : 0 ?>"
        data-centermode="<?= $post->format_data['is_center_mode'] ? 1 : 0 ?>"
        data-rtl="<?= $post->format_data['is_rtl'] ? 1 : 0 ?>"
        data-fade="<?= $post->format_data['fade'] ? 1 : 0 ?>"
        data-adaptive="<?= $post->format_data['has_adaptive_height'] ? 1 : 0 ?>"
        data-speed="<?= $post->format_data['slides_speed'] ?>"
        data-autoloadspeed="<?= $post->format_data['autoplay_speed'] ?>"
        data-toshow="<?= $post->format_data['slides_to_show'] ?>"
        data-toscroll="<?= $post->format_data['slides_to_scroll'] ?>"
        data-centerpadding="<?= $post->format_data['center_padding'] ?>"
        <?php if ( $post->format_data['on_ready'] ) : ?>style="display: none"<?php endif ?>
    >
        <div class="slider-for" style="width: calc(100% - <?= $side_width ?>px)">
            <?php foreach ( $post->gallery as $attachment ) : ?>
                <?php $attachment->get_gallery_settings( $post->ID ) ?>
                <a class="item <?php if ( $attachment->show_overlay ) : ?>has-overlay<?php endif ?> <?php if ( $attachment->is_video ) : ?>is-video<?php endif ?> <?php if ( $attachment->custom_url ) : ?>custom-url<?php else : ?>swipebox<?php endif ?>"
                    <?php if ( $attachment->custom_url ) : ?>
                        href="<?= $attachment->custom_url ?>"
                    <?php else : ?>
                        href="<?php echo $attachment->is_video && !$attachment->is_uploaded ? $attachment->video_url : $attachment->url ?>"
                        rel="post-gallery-<?php echo $post->ID ?>"
                        title="<?php echo $attachment->caption ?>"
                    <?php endif ?>
                >
                    <?php if ( $attachment->is_video ) : ?>
                        <?= apply_filters( 'postgallery_pro_video_preview', null, $attachment, $post ) ?>
                    <?php else : ?>
                        <?php if ( ! isset( $post->format_data['image_size'] )
                            || empty( $post->format_data['image_size'] )
                            || $post->format_data['image_size'] === 'default'
                            || $post->format_data['image_size'] === 'large'
                        ) : ?>
                            <img src="<?php echo $attachment->large_url ? $attachment->large_url : $attachment->no_large_url ?>"
                                alt="<?= $attachment->alt ?>"
                            />
                        <?php elseif ( $post->format_data['image_size'] === 'medium' ) : ?>
                            <img src="<?php echo $attachment->medium_url ? $attachment->medium_url : $attachment->no_medium_url ?>"
                                alt="<?= $attachment->alt ?>"
                            />
                        <?php elseif ( $post->format_data['image_size'] === 'thumb' ) : ?>
                            <img src="<?php echo $attachment->thumb_url ? $attachment->thumb_url : $attachment->no_thumb_url ?>"
                                alt="<?= $attachment->alt ?>"
                            />
                        <?php elseif ( $post->format_data['image_size'] === 'custom' ) : ?>
                            <img src="<?= $attachment->get_res( $post->format_data['image_width'], $post->format_data['image_height'], $post->format_data['image_crop'] ) ?>"
                                alt="<?= $attachment->alt ?>"
                            />
                        <?php endif ?>
                        <?php if ( $attachment->show_overlay ) : ?>
                            <?php $view->show( 'common.overlay', ['post' => $post, 'attachment' => $attachment] ) ?>
                        <?php endif ?>
                    <?php endif ?>
                </a>
            <?php endforeach ?>
        </div>
        <div class="slider-nav thumbs-container" style="width: <?= $side_width ?>px">
            <?php foreach ( $post->gallery as $attachment ) : ?>
                <a class="item-nav">
                    <?php if ( $attachment->is_video && isset( $post->format_data['show_play'] ) && $post->format_data['show_play'] ) : ?>
                        <?= $play ?>
                    <?php endif ?>
                    <?php if ( ! isset( $post->format_data['thumb_size'] )
                        || empty( $post->format_data['thumb_size'] )
                        || $post->format_data['thumb_size'] === 'default'
                        || $post->format_data['thumb_size'] === 'thumb'
                    ) : ?>
                        <img src="<?php echo $attachment->thumb_url ? $attachment->thumb_url : $attachment->no_thumb_url ?>"
                            alt="<?= $attachment->alt ?>"
                        />
                    <?php elseif ( $post->format_data['thumb_size'] === 'medium' ) : ?>
                        <img src="<?php echo $attachment->medium_url ? $attachment->medium_url : $attachment->no_medium_url ?>"
                            alt="<?= $attachment->alt ?>"
                        />
                    <?php elseif ( $post->format_data['thumb_size'] === 'large' ) : ?>
                        <img src="<?php echo $attachment->large_url ? $attachment->large_url : $attachment->no_large_url ?>"
                            alt="<?= $attachment->alt ?>"
                        />
                    <?php elseif ( $post->format_data['thumb_size'] === 'custom' ) : ?>
                        <img src="<?= $attachment->get_res( $post->format_data['thumb_width'], $post->format_data['thumb_height'], $post->format_data['thumb_crop'] ) ?>"
                            alt="<?= $attachment->alt ?>"
                        />
                    <?php endif ?>
                </a>
            <?php endforeach ?>
        </div>
    </div>
    <div class="arrows" <?php if ( ! $post->format_data['show_arrows'] ) : ?>style="display:none"<?php endif ?>>
        <?= $arrow_prev ?>
        <?= $arrow_next ?>
    </div>
</div>