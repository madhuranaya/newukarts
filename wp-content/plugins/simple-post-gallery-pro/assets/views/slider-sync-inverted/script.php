/**
 * Init script
 * Format: Slider Sync
 * 
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.1
 */
(function($) { $(document).ready(function() {
    $('#post-gallery-<?= $post->ID ?>').each(function() {
        $(this).find('.slider-for').slick({
            dots: false,
            arrows: false,
            adaptiveHeight: $(this).data('adaptive') === 1,
            fade: $(this).data('fade') === 1,
            cssEase: $(this).data('fade') === 1 ? 'linear' : undefined,
            speed: $(this).data('speed'),
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: $(this).find('.slider-nav'),
        });
        $(this).find('.slider-nav').slick({
            infinite: $(this).data('infinite') === 1,
            dots: $(this).data('dots') === 1,
            arrows: $(this).data('arrows') === 1,
            autoplay: $(this).data('autoplay') === 1,
            centerMode: $(this).data('centermode') === 1,
            centerPadding: $(this).data('centerpadding') !== undefined
                ? $(this).data('centerpadding')+'px'
                : undefined,
            speed: $(this).data('speed'),
            autoplaySpeed: $(this).data('autoloadspeed'),
            slidesToShow: $(this).data('toshow'),
            slidesToScroll: $(this).data('toscroll'),
            focusOnSelect: true,
            asNavFor: $(this).find('.slider-for'),
            prevArrow: $(this).parent().find('.arrow.prev'),
            nextArrow: $(this).parent().find('.arrow.next'),
        });
        $(this).show();
    });
}); })(jQuery);