<?php
/**
 * slider-sync.icon view.
 * Generated with ayuco.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
?>
<img src="<?= assets_url( 'svgs/slider-sync-inverted.svg', __FILE__ ) ?>"
    alt="slider_sync_inverted"
    class="img-responsive"
    title="<?php _e( 'Slider Sync Inverted', 'simple-post-gallery-pro' ) ?>"
/>