/**
 * Init script
 * Format: Slider
 * 
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.3.1
 */
(function($) { $(document).ready(function() {
    $('#post-gallery-<?= $post->ID ?>').each(function() {

        $(this).slick({
            infinite: $(this).data('infinite') === 1,
            dots: $(this).data('dots') === 1,
            arrows: $(this).data('arrows') === 1,
            autoplay: $(this).data('autoplay') === 1,
            adaptiveHeight: $(this).data('adaptive') === 1,
            fade: $(this).data('fade') === 1,
            cssEase: $(this).data('fade') === 1 ? 'linear' : undefined,
            speed: $(this).data('speed'),
            autoplaySpeed: $(this).data('autoloadspeed'),
            prevArrow: $(this).parent().find('.arrow.prev'),
            nextArrow: $(this).parent().find('.arrow.next'),
            slidesToShow: 1,
        });
        $(this).show();
    });
}); })(jQuery);