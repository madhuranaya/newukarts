<?php
/**
 * slider.icon view.
 * Generated with ayuco.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
?>
<img src="<?= assets_url( 'svgs/slider.svg', __FILE__ ) ?>"
    alt="slider"
    class="img-responsive"
    title="<?php _e( 'Slider', 'simple-post-gallery-pro' ) ?>"
/>