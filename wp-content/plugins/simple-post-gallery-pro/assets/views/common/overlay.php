<?php
/**
 * Overlay to be displayed over media.
 * Format: slider
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.0.0
 */ 
?>
<div id="g-<?= $post->ID ?>-a-<?= $attachment->ID ?>"
    class="overlay-box <?= $attachment->overlay_position ?>"
>
    <div class="overlay-text">
        <?= $attachment->overlay_text ?>
    </div>
</div>