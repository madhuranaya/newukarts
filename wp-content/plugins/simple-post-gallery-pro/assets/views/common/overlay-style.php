.post-gallery .has-overlay {
    position: relative;
    overflow: hidden;
}
.post-gallery .overlay-box {
    position: absolute;
    top: 0;
    left: 0;
    width: calc(100%);
    height: calc(100%);
    display: flex;
}
.post-gallery .overlay-box.topleft {
    justify-content: flex-start;
    align-items: flex-start;
}
.post-gallery .overlay-box.topcenter {
    justify-content: center;
    align-items: flex-start;
}
.post-gallery .overlay-box.topright {
    justify-content: flex-end;
    align-items: flex-start;
}
.post-gallery .overlay-box.midleft {
    justify-content: flex-start;
    align-items: center;
}
.post-gallery .overlay-box.midcenter {
    justify-content: center;
    align-items: center;
}
.post-gallery .overlay-box.midright {
    justify-content: flex-end;
    align-items: center;
}
.post-gallery .overlay-box.bottomleft {
    justify-content: flex-start;
    align-items: flex-end;
}
.post-gallery .overlay-box.bottomcenter {
    justify-content: center;
    align-items: flex-end;
}
.post-gallery .overlay-box.bottomright {
    justify-content: flex-end;
    align-items: flex-end;
}
<?php foreach ( $post->gallery as $attachment ) : ?>
    <?php $attachment->get_gallery_settings( $post->ID ) ?>
    <?php if ( $attachment->show_overlay ) : ?>
        .post-gallery #g-<?= $post->ID ?>-a-<?= $attachment->ID ?>.overlay-box {
            <?php if ( $attachment->overlay_margin ) : ?>
                padding: <?= $attachment->overlay_margin ?>px;
            <?php endif ?>
        }
        .post-gallery #g-<?= $post->ID ?>-a-<?= $attachment->ID ?> .overlay-text {
            <?php if ( $attachment->overlay_padding ) : ?>
                padding: <?= $attachment->overlay_padding ?>px;
            <?php endif ?>
            <?php if ( $attachment->overlay_background_color ) : ?>
                background-color: <?= $attachment->overlay_background_color ?>;
            <?php endif ?>
            <?php if ( $attachment->overlay_color ) : ?>
                color: <?= $attachment->overlay_color ?>;
            <?php endif ?>
            <?php if ( $attachment->overlay_width ) : ?>
                width: <?= $attachment->overlay_width ?>;
            <?php endif ?>
            <?php if ( $attachment->overlay_text_size ) : ?>
                font-size: <?= $attachment->overlay_text_size ?>px;
            <?php endif ?>
        }
    <?php endif ?>
<?php endforeach ?>