<?php $width = isset( $post->format_data['play_size'] )
    && !empty( $post->format_data['play_size'] )
    && is_numeric( $post->format_data['play_size'] )
        ? $post->format_data['play_size']
        : 100
?>
.post-gallery a.item, .post-gallery a.item-nav {
    position: relative;
}
.post-gallery .video-play-icon {
    position: absolute;
    width: <?= $width ?>px;
    left: calc(50% - <?= round($width/2) ?>px);
    opacity: 0.8;
    align-self: center;
}
.post-gallery a.item:hover .video-play-icon,
.post-gallery a.item-nav:hover .video-play-icon {
    opacity: 1
}
.post-gallery.default .thumbs-container a {
    display: inline-block;
}
.post-gallery .video-play-container {
    position: absolute;
    left: 0;
    top: 0;
    display: inline-flex;
    width: calc(100%);
    height: calc(100%);
}