<?php
/**
 * arrow view.
 * Arrow button for sliders.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.2.0
 */
?>
<button type="button" class="post-gallery arrow <?php if ( isset( $class ) ) : ?><?= $class ?><?php endif ?>">
    <svg xmlns:svg="http://www.w3.org/2000/svg"
        xmlns="http://www.w3.org/2000/svg"
        width="<?= isset( $post->format_data['arrow_size'] ) && $post->format_data['arrow_size'] ? $post->format_data['arrow_size'] : 20 ?>"
        height="<?= isset( $post->format_data['arrow_size'] ) && $post->format_data['arrow_size']  ? $post->format_data['arrow_size'] : 20 ?>"
        viewBox="0 0 20 20"
        class="arrow "
    >
        <g transform="translate(0,-1032.3622)">
            <circle class="background"
                style="fill:<?php if ( isset( $post->format_data['arrow_background_color'] ) ) : ?><?= $post->format_data['arrow_background_color'] ?><?php else : ?>#ffffff<?php endif ?>;"
                cx="10"
                cy="1042.3622"
                r="10"
            />
            <path class="icon"
                style="fill:<?php if ( isset( $post->format_data['arrow_background_color'] ) ) : ?><?= $post->format_data['arrow_background_color'] ?><?php else : ?>#ffffff<?php endif ?>;stroke:<?php if ( isset( $post->format_data['arrow_color'] ) ) : ?><?= $post->format_data['arrow_color'] ?><?php else : ?>#848484<?php endif ?>;"
                d="m 13.017857,1037.3622 -5.9642856,5.0446 5.9374996,4.9554"
            />
        </g>
    </svg>
</button>
