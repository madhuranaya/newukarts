<?php
/**
 * play view.
 * Video's play icon.
 *
 * @author Cami Mostajo
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGalleryPro
 * @version 1.1.2
 */
?>
<div class="video-play-container" <?php if ( isset( $side_width ) ) : ?>style="max-width:<?= $side_width ?>px"<?php endif ?>>
    <svg class="video-play-icon" xmlns:svg="http://www.w3.org/2000/svg"
        xmlns="http://www.w3.org/2000/svg"
        width="100"
        height="100"
        viewBox="0 0 100 100"
    >
        <g transform="translate(0,-952.36223)">
            <circle class="background"
                style="fill:<?php if ( isset( $post->format_data['play_background_color'] ) ) : ?><?= $post->format_data['play_background_color'] ?><?php else : ?>#fff<?php endif ?>"
                cx="50"
                cy="1002.3622"
                r="50"
            />
            <path class="icon"
                style="fill:<?php if ( isset( $post->format_data['play_color'] ) ) : ?><?= $post->format_data['play_color'] ?><?php else : ?>#000<?php endif ?>"
                d="m 30,972.36223 0,59.99997 50,-30 z"
            />
        </g>
    </svg>
</div>