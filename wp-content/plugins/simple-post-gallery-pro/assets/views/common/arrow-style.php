.post-gallery-wrapper {
    position: relative;
}
.post-gallery.arrow {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: 20px;
    height: 20px;
    padding: 0;
    cursor: pointer;
    color: transparent;
    border: none;
    outline: none;
    background: transparent;
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    opacity: 0.8;
    stroke-opacity: 0.8;
    z-index: 9;
}
.post-gallery.arrow.prev {
    left: -25px;
}
.post-gallery.arrow.next {
    right: -25px;
    transform: rotate(180deg) translate(0, +50%);
    -webkit-transform: rotate(180deg) translate(0, +50%);
    -ms-transform: rotate(180deg) translate(0, +50%);
}
.post-gallery.arrow:hover {
    opacity: 1;
    stroke-opacity: 1;
}
.post-gallery.arrow svg .background {
    stroke-width:0;
}
.post-gallery.arrow svg .icon {
    fill-rule:evenodd;
    stroke-width:2;
}
@media screen and (max-width: 770px) {
    .post-gallery.arrow.prev {
        left: 0;
    }
    .post-gallery.arrow.next {
        right: 0;
    }
}