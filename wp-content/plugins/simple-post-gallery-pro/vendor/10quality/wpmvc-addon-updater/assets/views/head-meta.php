<?php
/**
 * Adds meta information on head.
 *
 * @author Cami Mostajo
 * @package WPMVC\Addons\Updater
 * @license MIT
 * @version 1.0.0
 */
?>
<meta id="wpmvc-ajaxurl" value="<?= admin_url( '/admin-ajax.php' ) ?>">
<meta id="wpmvc-updaterurl" value="<?= home_url( '/wpmvc-updater.php' ) ?>">