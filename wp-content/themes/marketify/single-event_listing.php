<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Marketify
 */

get_header(); ?>

    <?php do_action( 'marketify_entry_before' ); ?>
    <div class="titl-dat-mn">
<h2 class="title-cont-clf"><?php echo the_title(); ?></h2>
<span><?php echo the_date(); ?></span>
</div>
    <div class="container"></div>
        <div id="content" class="site-content row">

            <div role="main" id="primary" class="col-xs-12 col-md-12 <?php echo ! is_active_sidebar( 'sidebar-1' ) ? 'col-md-offset-2' : '' ?>">
                <main id="main" class="site-main" role="main">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'content', 'single' ); ?>
                        <?php// comments_template(); ?>

                    <?php endwhile; ?>

                <?php else : ?>

                    <?php get_template_part( 'no-results', 'index' ); ?>

                <?php endif; ?>

                </main><!-- #main -->
            </div><!-- #primary -->

            <?php //get_sidebar(); ?>

        </div>
    
       <?php do_action( 'marketify_single_download_after' ); ?>

<?php get_footer(); ?>
