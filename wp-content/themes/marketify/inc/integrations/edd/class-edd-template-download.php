<?php

class Marketify_EDD_Template_Download {

    public function __construct() {
        add_action('wp_head', array($this, 'featured_area'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

        add_action('marketify_entry_before', array($this, 'download_title'), 5);
        add_action('marketify_entry_before', array($this, 'featured_area_header_actions'), 5);


        add_action('marketify_download_info', array($this, 'download_price'), 5);
        add_action('marketify_download_actions', array($this, 'demo_link'));

        add_action('marketify_download_entry_title_before_audio', array($this, 'featured_audio'));

        add_filter('post_class', array($this, 'post_class'), 10, 3);
        add_filter('body_class', array($this, 'body_class'));
    }

    public function post_class($classes, $class, $post_id) {
        if (!$post_id || get_post_type($post_id) !== 'download' || is_admin()) {
            return $classes;
        }

        if ('on' == esc_attr(get_theme_mod('downloads-archives-truncate-title', 'on'))) {
            $classes[] = 'edd-download--truncated-title';
        }


        return $classes;
    }

    public function body_class($classes) {
        $format = $this->get_post_format();
        $setting = esc_attr(get_theme_mod("download-{$format}-feature-area"), 'top');

        $classes[] = 'feature-location-' . $setting;

        return $classes;
    }

    public function enqueue_scripts() {
        wp_enqueue_script('marketify-download', get_template_directory_uri() . '/js/download/download.js', array('marketify'));
    }

    public function download_price() {
        global $post;
        ?>
        <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <span itemprop="price" class="edd_price">
        <?php edd_price($post->ID); ?>
            </span>
        </span>
        <?php
    }

    function demo_link($download_id = null) {
        global $post, $edd_options;

        if ('download' != get_post_type()) {
            return;
        }

        if (!$download_id) {
            $download_id = $post->ID;
        }

        $field = apply_filters('marketify_demo_field', 'demo');
        $demo = get_post_meta($download_id, $field, true);

        if (!$demo) {
            return;
        }

        $label = apply_filters('marketify_demo_button_label', __('Demo', 'marketify'));

        if ($post->_edd_cp_custom_pricing) {
            echo '<br /><br />';
        }

        $class = 'button';

        if (!did_action('marketify_single_download_content_before'
                )) {
            $class .= ' button--color-white';
        }

        echo apply_filters('marketify_demo_link', sprintf('<a href="%s" class="%s" target="_blank">%s</a>', esc_url($demo), $class, $label));
    }

    public function get_featured_images() {
        global $post;

        $images = array();
        $_images = get_post_meta($post->ID, 'preview_images', true);

        if (is_array($_images) && !empty($_images)) {
            foreach ($_images as $image) {
                $images[] = get_post($image);
            }
        } else {
            $images = get_attached_media('image', $post->ID);
        }

        return apply_filters('marketify_download_get_featured_images', $images, $post);
    }

    public function featured_area() {
        global $post;

        if (!$post || !is_singular('download')) {
            return;
        }

        $format = get_post_format();

        if ('' == $format) {
            $format = 'standard';
        }

        if ($this->is_format_location('top')) {
            add_action('marketify_entry_before', array($this, "featured_{$format}"), 5);

            if ('standard' != $format && $this->is_format_style('inline')) {
                add_action('marketify_entry_before', array($this, 'featured_standard'), 6);
            }
        } else {
            add_action('marketify_single_download_content_before_content', array($this, 'featured_' . $format), 5);

            if (method_exists($this, 'featured_' . $format . '_navigation')) {
                add_action('marketify_single_download_content_before_content', array($this, 'featured_' . $format . '_navigation'), 7);
            }

            if ('standard' != $format && $this->is_format_style('inline')) {
                add_action('marketify_single_download_content_before_content', array($this, 'featured_standard'), 6);
                add_action('marketify_single_download_content_before_content', array($this, 'featured_standard_navigation'), 7);
            }
        }
    }

    private function get_post_format() {
        global $post;

        if (!$post) {
            return false;
        }

        $format = get_post_format();

        if ('' == $format) {
            $format = 'standard';
        }

        return $format;
    }

    public function is_format_location($location) {
        if (!is_array($location)) {
            $location = array($location);
        }

        $format = $this->get_post_format();
        $setting = esc_attr(get_theme_mod("download-{$format}-feature-area", 'top'));

        if (in_array($setting, $location)) {
            return true;
        }

        return false;
    }

    public function is_format_style($style) {
        if (!is_array($style)) {
            $style = array($style);
        }

        $format = $this->get_post_format();
        $setting = esc_attr(get_theme_mod("downloads-{$format}-feature-image", 'background'));

        if (in_array($setting, $style)) {
            return true;
        }

        return false;
    }

    public function download_title() {
        if (!is_singular('download')) {
            return;
        }

        the_post();
        ?>
        <div class="custome-title-single-page">
            <div class="breadcrumb-custome-class">artwork / <? echo the_terms($get_the_id, 'download_category', '', ' / ', ''); ?> / <?php the_author(); ?> / <? echo the_title(); ?></div></div>


        <div class="custome-title-single-page container">
            <div class="download-title"><? echo the_title(); ?></div>

            <?php
            $get_the_id = get_the_author_meta('ID');
            $site_url = site_url();
            printf(
                    __('<span class="byline dddgggggg">by %1$s</span>', 'marketify'), sprintf('<span class="author vcard"><a class="url fn n" href="' . $site_url . '/artist-main/?user_id=' . $get_the_id . '" title="%2$s">%3$s %4$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'marketify'), get_the_author())), get_avatar(get_the_author_meta('ID'), 50, apply_filters('marketify_default_avatar', null)), esc_html(get_the_author_meta('display_name'))
                    )
            );
            ?>
        <!--div class="the_author">by<span><a href="<?php //echo site_url(); ?>//"><?php // the_author(); ?></a><span>
        </div-->
        </div>

        <div class="newdiv container">
            <div class="six-featured-section" ><?php echo the_post_thumbnail(array(1200, 1200)); ?>
                <img id="main_img" src="" style="display:none;">
        <?php
        global $wpdb;
        $get_the_id = get_the_id();

        $qry = $wpdb->get_results("SELECT * FROM `ua_postmeta` WHERE `meta_key` = 'file_upload' AND post_id =" . $get_the_id);
        foreach ($qry as $attachment) {
            $b = unserialize($attachment->meta_value);
            //echo"<pre>";
            //print_r($b);
            ?>
                    <?php if (wp_get_attachment_url($b[0]) != '') { ?>
                        <a ><img id="img_1"z src="<?php echo wp_get_attachment_url($b[0]); ?>"></a>

                    <?php }
                    if (wp_get_attachment_url($b[1]) != '') {
                        ?>
                        <a ><img id="img_2" src="<?php echo wp_get_attachment_url($b[1]); ?>"></a>

            <?php }
            if (wp_get_attachment_url($b[2]) != '') {
                ?>
                        <a ><img id="img_3" src="<?php echo wp_get_attachment_url($b[2]); ?>"></a>
            <?php }
            if (wp_get_attachment_url($b[3]) != '') {
                ?>
                        <a ><img id="img_4" src="<?php echo wp_get_attachment_url($b[3]); ?>"></a>
                    <?php }
                    if (wp_get_attachment_url($b[4]) != '') {
                        ?>
                        <a ><img id="img_5" src="<?php echo wp_get_attachment_url($b[4]); ?>"></a>
                    <?php } ?>



        <?php } ?>
                <script>
                    $(document).ready(function () {
                        $("#img_1").click(function () {
                            var img1 = $(this).attr('src');
                            var img2 = $('.size-1200x1200').attr('src');
                            $("#img_1").attr("src", img2);
                            $('.size-1200x1200').attr("src", img1);

                            $('.size-1200x1200').removeAttr('srcset');
                        });

                        $("#img_2").click(function () {
                            var img1 = $("#img_2").attr('src');
                            //alert(img1);
                            var img2 = $('.size-1200x1200').attr('src');
                            $("#img_2").attr("src", img2);
                            $('.size-1200x1200').attr("src", img1);

                            $('.size-1200x1200').removeAttr('srcset');
                        });
                        $("#img_3").click(function () {
                            var img1 = $("#img_3").attr('src');
                            //alert(img1);
                            var img2 = $('.size-1200x1200').attr('src');
                            $("#img_3").attr("src", img2);
                            $('.size-1200x1200').attr("src", img1);

                            $('.size-1200x1200').removeAttr('srcset');
                        });
                        $("#img_4").click(function () {
                            var img1 = $("#img_4").attr('src');
                            //alert(img1);
                            var img2 = $('.size-1200x1200').attr('src');
                            $("#img_4").attr("src", img2);
                            $('.size-1200x1200').attr("src", img1);

                            $('.size-1200x1200').removeAttr('srcset');
                        });
                        $("#img_5").click(function () {
                            var img1 = $("#img_5").attr('src');
                            //alert(img1);
                            var img2 = $('.size-1200x1200').attr('src');
                            $("#img_5").attr("src", img2);
                            $('.size-1200x1200').attr("src", img1);

                            $('.size-1200x1200').removeAttr('srcset');
                        });
                    });

                </script>
                <!--div class="six-featured-section" style= "width: 10%;float: left;" >
        <?php $get_the_id = get_the_id(); ?>
        <?php $gall = the_gallery(); ?>  
                <?php foreach ($gall as $attachment) : ?>
                 <a href="<?php echo $attachment->large_url ?>"
                    alt="<?php echo $attachment->alt ?>"> <img src="<?php echo $attachment->large_url ?>"
                    alt="<?php echo $attachment->alt ?>"/></a>
               

        <?php endforeach ?>
        <?php echo do_shortcode('[post_gallery]'); ?>
            </div-->
                <div class="single-content-section"><h2>Description by the artist</h2><?php the_content(); ?></div>	
            </div> 


            <div class="page-header page-header--download download-header container">
              <!--  <h1 class="page-titlxcdvce fdfg"><?php // the_title();  ?></h1>-->
        <?php
        if (!empty(get_post_meta(get_the_ID(), 'sale_status', true)) && get_post_meta(get_the_ID(), 'sale_status', true) == "Sold") {
            //echo "<div class='solddd'>Sold Out </div>";
        }
        ?>

                <?php
                $get_the_id = get_the_id();
                $categories = get_the_terms($get_the_id, 'download_category', 'download');
                //echo count($categories);
//print_r($categories);
//print_r(count($categories));
//if(count($categories)>1){
                $term_id_select = '';
                foreach ($categories as $category) {
                    $term_id_select = $category->term_id;
                }
//}
                //$term_id_select= $category->term_id;
                if ($term_id_select == "71" || $term_id_select == "72" || $term_id_select == "70" || $term_id_select == "74" || $term_id_select == "59" || $term_id_select == "118" || $term_id_select == "119" || $term_id_select == "120" || $term_id_select == "121" || $term_id_select == "122" || $term_id_select == "123" || $term_id_select == "124" || $term_id_select == "125" || $term_id_select == "142" || $term_id_select == "137" || $term_id_select == "144" || $term_id_select == "138" || $term_id_select == "136" || $term_id_select == "141" || $term_id_select == "139" || $term_id_select == "143" || $term_id_select == "134" || $term_id_select == "135" || $term_id_select == "145" || $term_id_select == "95" || $term_id_select == "91" || $term_id_select == "86" || $term_id_select == "96" || $term_id_select == "88" || $term_id_select == "93" || $term_id_select == "92" || $term_id_select == "89" || $term_id_select == "94" || $term_id_select == "93" || $term_id_select == "97" || $term_id_select == "80" || $term_id_select == "75" || $term_id_select == "87" || $term_id_select == "85" || $term_id_select == "79" || $term_id_select == "78" || $term_id_select == "76" || $term_id_select == "84" || $term_id_select == "81" || $term_id_select == "83" || $term_id_select == "82" || $term_id_select == "116" || $term_id_select == "110" || $term_id_select == "114" || $term_id_select == "108" || $term_id_select == "112" || $term_id_select == "109" || $term_id_select == "107" || $term_id_select == "111" || $term_id_select == "115" || $term_id_select == "113") {
                    ?> <div class="Original vv active">Original</div> 

                    <?php
                    $value = get_field("how_many_prints_are_available", $user_id);

                    if (!empty($value)):
                        ?>
                        <div class="Print vv" >Prints</div>
                        <?
                    endif;

                    //echo "$value";  
                } else {
                    ?>
                    <div class="single-original"><div class="Original vv active">Original</div></div>

                    <?php
                }
                //echo $term_id_select = $category->term_id; 
                ?> 

                <div class="product-information bnbn Originalbb"><!--/product-information
                    <img alt="" class="newarrival" src="<?php //echo site_url(); ?>/images/product-details/new.jpg">-->
                <?php //  the_excerpt(); ?>


        <?php
        $get_the_id = get_the_id();
        echo "</br>";
        // display download categories
//      echo the_terms( $get_the_id, 'medium', 'Media: ', ', ', '' );    echo "</br>";
//      echo the_terms( $get_the_id, 'size', 'Size: ', ', ', '' );    echo "</br>";
        //       echo the_terms( $get_the_id, 'download_category', 'Style: ', ', ', '' );   echo "</br>";
        //       echo the_terms( $get_the_id, 'subject', 'Subject: ', ', ', '' );    echo "</br>";



        $terms = get_the_terms($post->id, 'medium');
        if (!is_wp_error($terms)) {
            $skills_links = wp_list_pluck($terms, 'name');

            $skills_yo = implode(", ", $skills_links);
            ?>
                        <div class="media-top-size">
                            <div class="guyg"> 
                        <?php
                        if ($skills_yo == '') {
                            //echo "hello";
                        } else {
                            ?>
                                    <span> Media: <?php echo $skills_yo; ?></span><br>
                                <?php }
                                ?>


                            <?php } ?>
        <?php
        $terms = get_the_terms($post->id, 'size');
        if (!is_wp_error($terms)) {
            $skills_links = wp_list_pluck($terms, 'name');

            if (!empty($skills_links)) {
                $skills_yo = implode(", ", $skills_links);
            }
            ?>
                                <?php
                                if ($skills_yo == '') {

//echo "hello";
                                } else {
                                    ?>
                                    <span>Size: <?php echo $skills_yo; ?></span>
                                <?php }
                                ?>

                            </div>






            <?php /*  if( ! get_post_meta( $get_the_id, 'Height', true ) ) {  }  else { ?> <?php echo get_post_meta( $get_the_id, 'Height', true ); ?> H <? }  ?>
              <?php  if( ! get_post_meta( $get_the_id, 'Width', true ) ) {  }  else {  ?> X <? echo get_post_meta( $get_the_id, 'Width', true ); ?> W <? }  ?>
              <?php  if( ! get_post_meta( $get_the_id, 'Depth', true ) ) { ?>X 0 D  inches <?  }  else {  ?> X <?php echo get_post_meta( $get_the_id, 'Depth', true ); ?> D  inches <?php } */ ?>

                        <?php } ?>

                        <?
                        $terms = get_the_terms($post->id, 'download_category');
                        if (!is_wp_error($terms)) {
                            ?>

                            <?php
                            $skills_links = wp_list_pluck($terms, 'name');

                            $skills_yo = implode(", ", $skills_links);

                            if ($skills_yo == '') {

//echo "hello";
                            } else {
                                ?>
                                <span>Style: <?php echo $skills_yo; ?></span></br>
                            <?php }
                            ?>

                        <?php
                        }

                        $terms = get_the_terms($post->id, 'subject');
                        if (!is_wp_error($terms)) {
                            ?>

                            <?php
                            $skills_links = wp_list_pluck($terms, 'name');

                            $skills_yo = implode(", ", $skills_links);
                            if ($skills_yo == '') {

//echo "hello";
                            } else {
                                ?>
                                <span>Subject: <?php echo $skills_yo; ?></span></br>
                            <?php }
                            ?>


                        <?php } ?>



        <!-- <a href=""><img alt="" class="share img-responsive" src="<?php // echo site_url();  ?>/images/product-details/share.png"></a-->
                    </div><!--/product-information-->

                    <div class="product-information bnbn printsbn"><!--/product-information
        <img alt="" class="newarrival" src="<?php //echo site_url();  ?>/images/product-details/new.jpg">-->  
                        <?php //  the_excerpt();  ?>

                        <?php
                        $get_the_id = get_the_id();
                        echo "</br>";
                        // display download categories
//      echo the_terms( $get_the_id, 'medium', 'Media: ', ', ', '' );    echo "</br>";
//      echo the_terms( $get_the_id, 'size', 'Size: ', ', ', '' );    echo "</br>";
                        //       echo the_terms( $get_the_id, 'download_category', 'Style: ', ', ', '' );   echo "</br>";
                        //       echo the_terms( $get_the_id, 'subject', 'Subject: ', ', ', '' );    echo "</br>";
                        ?>


                        <span>Prints available : <?php
                            print_r($value = get_field("how_many_prints_are_available", $user_id));
                            /*  if(get_post_meta( $get_the_id, 'how_many_prints', true ) ) { echo get_post_meta( $get_the_id, 'how_many_prints', true ); }  else {  }  */
                            ?> </span></br>

                        <span><?php if (get_post_meta($get_the_id, 'are_the_prints_numbered_and_signed', true)) {
                        echo get_post_meta($get_the_id, 'are_the_prints_numbered_and_signed', true);
                    } else {
                        
                    } ?></span></br>

                        <span>Printed on:  <?php
                    if (get_post_meta($get_the_id, 'what_media_are_the_prints_printed_on', true) == 'Other') {
                        echo get_post_meta($get_the_id, 'explain_others', true);
                    } else {
                        echo get_post_meta($get_the_id, 'what_media_are_the_prints_printed_on', true);
                    }
                    ?></span></br>

                        <span>Sizes available:   </span></br>  



                    </div><!--/product-information-->
                    <script>

                        jQuery(document).ready(function () {

                            jQuery(".Original").click(function () {

                                jQuery(".Originalbb").show();

                                jQuery(".printsbn").hide();
                                jQuery(".Original").addClass("active");
                                jQuery(".Print").removeClass("active");

                            });
                            jQuery(".Print").click(function () {

                                jQuery(".Originalbb").hide();

                                jQuery(".printsbn").show();
                                jQuery(".Print").addClass("active");
                                jQuery(".Original").removeClass("active");
                            });
                        });
                    </script>
                    <style> .product-information.bnbn.printsbn {
                            display: none;
                        }</style>

                </div> 


                <div class="product-information bnbn printsbn">
                    <div class="media-top-size"><!--/product-information
                                                    <img alt="" class="newarrival" src="<?php //echo site_url(); ?>/images/product-details/new.jpg">-->  
                        <?php //  the_excerpt(); ?>

                        <?php $get_the_id = get_the_id();
                        ?>
                        <?php
                        global $wpdb;
                        $postid = get_the_ID();
                        $result = $wpdb->get_results("SELECT * FROM `ua_postmeta` WHERE `meta_key` = '_edd_payment_meta'");
                        $sum = 0;
                        foreach ($result as $key => $res) {
                            //echo "<pre>";      
                            // print_r($res); 
                            $ser = unserialize($res->meta_value);
                            // echo "<pre>";
                            // print_r($ser['downloads'][0]['quantity']);

                            if ($postid == $ser['downloads'][0]['id']) {
                                $print = $ser['downloads'][0]['quantity'];
                                $sum += $print;
                                $tot = get_field("how_many_prints_are_available", $get_the_id);
                                // print_r($sum);
                                ?>
                                <?php
                                $class = ( $key !== count($result) - 1 ) ? " class='not-last'" : " class='last'";
                                $valuedd1 = $tot - $sum;
                                echo "<div{$class}>";
                                echo "<span class='printav'>Prints available : $valuedd1" . " From " . "$tot" . "";
                                echo "</span></div>";
                                //print_r($valuedd);
                                //$print1 = $valuedd - $print;
                                //print_r  ($valuedd1);
                                ?><?php //print_r  ($valueddd = get_field( "how_many_prints_are_available", $get_the_id));
                                /*  if(get_post_meta( $get_the_id, 'how_many_prints', true ) ) { echo get_post_meta( $get_the_id, 'how_many_prints', true ); }  else {  }  */
                                ?>
                                <style>
                                    .print_avail{
                                        display:none;
                                    }


                                </style>
                                <?php }
                            }
                            ?>
                        <style>


                        </style>
                        <span class="print_avail">Prints available : <?php
                            print_r($valuedd = get_field("how_many_prints_are_available", $get_the_id));
                            /*  if(get_post_meta( $get_the_id, 'how_many_prints', true ) ) { echo get_post_meta( $get_the_id, 'how_many_prints', true ); }  else {  }  */
                            ?> </span></br>
                        <span>Printed on:  <?php
                        if (get_post_meta($get_the_id, 'what_media_are_the_prints_printed_on', true) == 'Other') {
                            echo get_post_meta($get_the_id, 'explain_others', true);
                        } else {
                            echo get_post_meta($get_the_id, 'what_media_are_the_prints_printed_on', true);
                        }
                        ?></span>
                    </div>
                    <form id="edd_purchase_<?php echo $get_the_id; ?>-3" class="edd_download_purchase_form edd_purchase_<?php echo $get_the_id; ?>" method="post">

        <?php
        $get_the_id = get_the_id();




        $terms = get_the_terms($post->id, 'medium');
        if (!is_wp_error($terms)) {
            $skills_links = wp_list_pluck($terms, 'name');

            $skills_yo = implode(", ", $skills_links);
            ?>
                            <div class="media-top-size">
                               <div class="guyg"> <!--span> Media: <?php echo $skills_yo; ?></span--> <br>
                                <?php } ?>
        <?php
        $terms = get_the_terms($post->id, 'size');
        if (!is_wp_error($terms)) {
            $skills_links = wp_list_pluck($terms, 'name');

            if (!empty($skills_links)) {
                $skills_yo = implode(", ", $skills_links);
            }
            ?>

               <!--span>Size: <?php echo $skills_yo; ?></span-->
                                </div>
                                <?php $get_the_id = get_the_id(); ?>
            <?php
            $value = get_field("Avalable_size", $user_id);
            $va = get_post_meta($get_the_id, 'edd_variable_prices', true);
            //print_r($va);
            ?>
                                        <?php
                                        if (count($va) > 0) {
                                            ?>


                                    <div class="edd_price_options" style="display:block">
                                        <select name="edd_options[price_id][]" id="sizze" class="user-success" style="float:none;">
                                            <option value="" data-val="">select size</option>
                                            <?php
                                            $i = 1;
                                            foreach ($va as $val) {
                                                ?>
                                                <option data-ship="<?php echo $val['shiping']; ?>" for="edd_price_option_<?php echo $get_the_id; ?>_<?php echo $i; ?>" name="edd_options[price_id][]" id="edd_price_option_<?php echo $get_the_id; ?>_<?php echo $i; ?>" class="edd_price_option_<?php echo $get_the_id; ?>" value="<?php echo $i; ?>" data-val="<?php echo $val['amount']; ?>" ><?php echo $val['down_height'] . 'H X ' . $val['down_width'] . ' W X ' . $val['down_depth'] . 'D inches ,', '  £' . $val['amount'] . ' price <br> '; ?></option> 
                                        <?php
                                        ++$i;
                                    }
                                    ?>
                                        </select>

                                    </div>

            <?php } ?>



                                <script>
                                    $("#sizze").on('change', function () {
                                        var tset = $('option:selected', this).attr('data-val');
                                        var dataship = $('option:selected', this).attr('data-ship');
                                        //alert($(this).val()); 
                                        //alert(tset);
                                        $(".edd-add-to-cart").attr('data-price', $(this).val());
                                        if (tset == "") {
                                            $(".item-price").html('£ <?php echo get_post_meta($get_the_id, 'edd_sale_price', true); ?>');
                                        } else {
                                            $(".item-price").html('£' + tset + ' (included shipping cost £' + dataship + ')');
                                        }

                                    });
                                </script>

                            <?php } ?>

                            <?
                            $terms = get_the_terms($post->id, 'download_category');
                            if (!is_wp_error($terms)) {
                                ?>

                                <?php
                                $skills_links = wp_list_pluck($terms, 'name');

                                $skills_yo = implode(", ", $skills_links);
                                ?>

                <!--span>Style: <?php echo $skills_yo; ?></span></br-->
                            <?php
                            }

                            $terms = get_the_terms($post->id, 'subject');
                            if (!is_wp_error($terms)) {
                                ?>

            <?php
            $skills_links = wp_list_pluck($terms, 'name');

            $skills_yo = implode(", ", $skills_links);
            ?>

                <!--span>Subject: <?php echo $skills_yo; ?></span></br-->
                            <?php } ?>

                        </div><!--/product-information-->

                        <div class="product-information bnbn printsbn">  
        <?php //  the_excerpt();  ?>

        <?php $get_the_id = get_the_id();
        echo "</br>";
        ?> 



                        </div><!--/product-information-->
                        <script>

                            jQuery(document).ready(function () {

                                jQuery(".Original").click(function () {

                                    jQuery(".Originalbb").show();

                                    jQuery(".printsbn").hide();
                                    jQuery(".Original").addClass("active");
                                    jQuery(".Print").removeClass("active");

                                });
                                jQuery(".Print").click(function () {

                                    jQuery(".Originalbb").hide();

                                    jQuery(".printsbn").show();
                                    jQuery(".Print").addClass("active");
                                    jQuery(".Original").removeClass("active");
                                });
                            });
                        </script>
                        <style> .product-information.bnbn.printsbn {
                                display: none;
                            }</style>

                        <div class="price_addtocart_button">

                            <p class="price_symbol"><?php $get_the_id = get_the_id();

        if (!get_post_meta($get_the_id, 'edd_sale_price', true)) {
            ?> £<?php echo $price = get_post_meta($get_the_id, 'edd_price', true);
                    } else { ?>  <strong class="item-price"><span><del>£<? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price = get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php }
        ?>
                            </br><p class="make-an-offer"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/offer-icon.png" /></span> <a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display = 'block';
                                    document.getElementById('fade').style.display = 'block'">Make an Offer </a> </p>
                            </p>

                            <div class="download-header__info download-header__info--actions">

                                <?php
                                // jagdeep code start 
                                $user_id = get_current_user_id();
                                $user_meta = get_userdata($user_id);
                                $user_roles = $user_meta->roles;
                                $rol = $user_roles[0];
                                $postid = get_the_ID();
                                // jagdeep code end 

                                global $wpdb;
                                $results = $wpdb->get_results("SELECT * FROM `ua_postmeta` WHERE `meta_key` = '_edd_payment_meta'");
                                // echo "<pre>";
                                // print_r($results); 

                                $auth = get_post($get_the_id);
                                //do_action( 'marketify_download_actions' );
                                if (get_current_user_id() != $auth->post_author) {
                                    //echo get_post_meta( get_the_ID(), 'product_status', true );
                                    if (!empty(get_post_meta(get_the_ID(), 'product_status', true)) && get_post_meta(get_the_ID(), 'product_status', true) == "Deactive") {
                                        ?>  
                                        <button class="edd-add-to-cart button  edd-submit edd-has-js" >Add to Cart</button>
                                        <?php
                                    } else {
                                        if (!empty(get_post_meta(get_the_ID(), 'sale_status', true)) && get_post_meta(get_the_ID(), 'sale_status', true) == "Sold") {
                                            ?>

                                            <button class="edd-add-to-cart button edd-submit edd-has-js" disabled>Add to Cart </button>

                                        <?php } else {
                                            if (is_user_logged_in() && $rol == shop_vendor) {
                                                ?>
                                                <a href="http://beta.uk-arts.com/checkout?edd_action=add_to_cart&download_id=<?php echo $postid = get_the_ID(); ?>" class="edd-add-to-cart button  edd-submit edd-has-js"><span class="edd-add-to-cart-label">Purchase</span> <span class="edd-loading" aria-label="Loading"></span></a>
                                                <!--button class="edd-add-to-cart button  edd-submit edd-has-js" disabled>Add to Cart </button-->

                    <?php
                    } else {
                        foreach ($results as $res) {
                            // echo "<pre>";
                            // print_r($res); 
                            $ser = unserialize($res->meta_value);

                            if ($user_id == $ser['user_info']['id'] && $postid == $ser['downloads'][0]['id']) {
                                //echo "hello";
                                ?>
                                                        <button class="edd-add-to-cart button  edd-submit edd-has-js" disabled>Add to Cart </button>
                                                        <style>
                                                            .edd_purchase_submit_wrapper{
                                                                display:none;
                                                            }
                                                        </style>
                            <?php
                            }
                        }
                        if (count($va) > 0) {
                            ?>

                                                    <div class="edd_purchase_submit_wrapper">
                                                        <a href="http://beta.uk-arts.com/checkout?edd_action=add_to_cart&download_id=<?php echo $postid = get_the_ID(); ?>" class="edd-add-to-cart button  edd-submit edd-has-js"><span class="edd-add-to-cart-label">Purchase</span> <span class="edd-loading" aria-label="Loading"></span></a><input type="submit" class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo $get_the_id; ?>" data-variable-price="yes" data-price-mode="single" style="display: none;"><a href="http://beta.uk-arts.com/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;">Checkout</a>
                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                <svg class="edd-icon edd-icon-check" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" aria-hidden="true">
                                                                <path d="M26.11 8.844c0 .39-.157.78-.44 1.062L12.234 23.344c-.28.28-.672.438-1.062.438s-.78-.156-1.06-.438l-7.782-7.78c-.28-.282-.438-.673-.438-1.063s.156-.78.438-1.06l2.125-2.126c.28-.28.672-.438 1.062-.438s.78.156 1.062.438l4.594 4.61L21.42 5.656c.282-.28.673-.438 1.063-.438s.78.155 1.062.437l2.125 2.125c.28.28.438.672.438 1.062z"></path>
                                                                </svg>
                                                                Added to cart					</span>
                                                        </span>
                                                    </div>

                        <?php
                        } else {
                            do_action('marketify_download_actions');
                        }
                    }
                }
            }
        } else {
            ?>
                                    <button class="edd-add-to-cart button  edd-submit edd-has-js" >Add to Cart</button>
                        <?php
                    }
                    ?>
                            </div>

                        </div>
                    </form>
                    <?
                    $get_the_id = get_the_id();
                    global $wpdb;
                    $curentuser_contact = $wpdb->get_results("SELECT AVG(artistid) as cvcvcx FROM artistreviews where product_id= '$get_the_id'");

                    $curentuser_contact_count = $wpdb->get_results("SELECT count(artistid) as edstar FROM artistreviews where product_id= '$get_the_id'");
                    //  print_r($curentuser_contact_count);
                    foreach ($curentuser_contact_count as $curentuser_contact_counted) {
                        $star_counted = $curentuser_contact_counted->edstar;
                    }

                    // print_r($curentuser_contact);
                    foreach ($curentuser_contact as $curentuser_contacted) {
                        $current_first = $curentuser_contacted->cvcvcx;
                    }
                    $current_first;
                    $current_f_user = round($current_first);
                    if ($current_f_user == "1") { //echo "1"; 
                        ?> <div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" />
                            ( <?php echo $star_counted; ?> )
                        </div><?
                        } elseif ($current_f_user == "2") { //echo "2";
                            ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /> ( <?php echo $star_counted; ?> )</div><?
                        } elseif ($current_f_user == "3") { //echo "3";
                            ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /> ( <?php echo $star_counted; ?> )</div><?
                        } elseif ($current_f_user == "4") { // echo "4"; 
                            ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /> ( <?php echo $star_counted; ?> )</div><?
            } elseif ($current_f_user == "5") { //echo "5";
                ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /> ( <?php echo $star_counted; ?> )</div> <?
            }
            ?>
                </div>  




                <?php
                rewind_posts();
            }

            public function featured_area_header_actions() {
                if (!is_singular('download')) {
                    return;
                }
                ?>


        <?php // dynamic_sidebar('reviews');  ?>

                <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">
        <?php if (!empty(get_post_meta(get_the_ID(), 'product_status', true)) && get_post_meta(get_the_ID(), 'product_status', true) == "Deactive") { ?>

                        <a href="<?php echo site_url(); ?>/uk-artists-collection/" class="edd-wl-button  444 before edd-wl-action edd-wl-open-modal glyph-left "><i class="glyphicon glyphicon-heart" style="margin: 2%;"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span>Follow this Artist</a>
        <?php } elseif (is_user_logged_in()) { ?>
                        <a href="" class="edd-wl-button  444 before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart" style="margin: 2%;"></i></i>
                            <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span>Follow this Artist</a>
        <?php } else { ?> 
                        <a href="<?php echo site_url(); ?>/uk-artists-collection/" class="edd-wl-button-btns"><i class="glyphicon glyphicon-heart" style="margin: 2%;"></i></i>
                            <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span>Follow this Artist</a>
        <?php } ?>  

                    <div class="download-header__info">
        <?php //do_action( 'marketify_download_info' );  ?>
                    </div>

                    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">

                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Shipping <i id="shipping_tab1" class="material-icons more-less glyphicon glyphicon-plus" style="font-size:22px"></i>
                                        <i id="shipping_tab2" class="material-icons more-less glyphicon glyphicon-minus" style="font-size:22px;display:none "></i>

                                    </a>  

                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <?php
                                    $get_the_id = get_the_id();
                                    $my_meta = get_post_meta($get_the_id, '_my_meta', TRUE);
                                    // $shipping_text= $my_meta['shipping_text'];
                                    if ($my_meta == "") { //echo "khali";
                                    } else {
                                        echo $my_meta = $my_meta['shipping_text'];
                                    }
                                    ?>
                                    <!--Anim pariatur-->
                                </div>
                            </div>
                        </div>
                        <script>
                                $("#shipping_tab1").click(function () {
                                    $("#collapseOne").show();
                                    $(this).hide();
                                    $("#shipping_tab3").show();
                                    $("#shipping_tab2").show();
                                    $("#collapseTwo").hide();
                                    $("#shipping_tab4").hide();

                                });
                                $("#shipping_tab2").click(function () {
                                    $("#collapseOne").hide();
                                    $(this).hide();
                                    $("#shipping_tab3").show();
                                    $("#shipping_tab1").show();
                                    $("#collapseTwo").hide();
                                    $("#shipping_tab4").hide();
                                });
                        </script>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Returns & Refunds  <i id="shipping_tab3" class="material-icons more-less glyphicon glyphicon-plus" style="font-size:22px"></i>
                                        <i id="shipping_tab4" class="material-icons more-less glyphicon glyphicon-minus" style="font-size:22px;display:none "></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
        <?php
        $get_the_id = get_the_id();
        $my_meta = get_post_meta($get_the_id, '_my_meta', TRUE);
        // $shipping_text= $my_meta['shipping_text'];
        if ($my_meta == "") { // echo "khali";
        } else {
            echo $my_meta = $my_meta['return_refund'];
        }
        ?>
                                    <!--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.-->
                                </div>
                            </div>
                        </div>
                        <script>
                            $("#shipping_tab3").click(function () {
                                $("#collapseTwo").show();
                                $(this).hide();
                                $("#shipping_tab4").show();
                                $("#collapseOne").hide();
                                $("#shipping_tab2").hide();
                                $("#shipping_tab1").show();
                            });
                            $("#shipping_tab4").click(function () {
                                $("#collapseTwo").hide();
                                $(this).hide();
                                $("#shipping_tab2").hide();
                                $("#shipping_tab3").show();
                                $("#collapseOne").hide();
                                $("#shipping_tab1").show();
                            });
                        </script>
                        <div class="panel panel-default">
                            <!--div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Pay by instalments
                                                             <i class="material-icons more-less glyphicon glyphicon-plus" style="font-size:22px"></i>   
                
                                    </a>
                                </h4>
                            </div-->
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
        <?php
        $get_the_id = get_the_id();
        $my_meta = get_post_meta($get_the_id, '_my_meta', TRUE);
        // $shipping_text= $my_meta['shipping_text'];
        if ($my_meta == "") { // echo "khali";
        } else {
            echo $my_meta = $my_meta['payper_instal'];
        }
        ?>
                                    <!-- Anim pariatur cliche reprehenderit, -->
                                </div>
                            </div>




                        </div><!-- panel-group -->


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">


        <?php $get_the_id = get_the_id(); ?>
        <?php $get_permalink = get_permalink(); ?>

                                <!--i class="more-less glyphicon glyphicon-plus"></i-->
                                <div class="share-icon"><div class="s-h">Share</div> <?php //dynamic_sidebar('socialicon');  ?>


                                    <aside id="text-4" class="footer-section-first">           <div class="textwidget"><div class="footer-social">




                                                <a href="http://twitter.com/home?status=<?php echo $get_permalink; ?>" target="_blank" title="Click to share this post on Twitter">
                                                    <span class="screen-reader-text">Twitter</span></a>

                                                <a href="https://www.instagram.com/" title="Click to share this post on instagram" onclick="return fbs_click()" target="_blank"><span class="screen-reader-text">Instagram</span></a>

                                                <a href="http://www.facebook.com/share.php?u=<?php echo $get_permalink; ?>" onclick="return fbs_click()"  target="_blank"> <span class="screen-reader-text">Facebook</span> </a>

                                                <a href="http://pinterest.com/pin/create/button/?url=<?php echo $get_permalink; ?>/&description=products" target="_blank" title="Click to share this post on Pintrest" class="pin-it-button" count-layout="horizontal"><span class="screen-reader-text">Pinterest</span></a>

                                            </div>
                                        </div>
                                    </aside>                    


                                </div>
                            </div>
                            <div id="headingFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">

                                </div>
                            </div>

                        </div><!-- panel-group -->

                        <div class="artist-contact-button">
                            <style>
                                .black_overlay{
                                    display: none;
                                    position: fixed;
                                    top: 0;
                                    left:0;
                                    width: 100%;
                                    height: 100%;z-index:9998;
                                    background: rgba(0,0,0,0.8);

                                }
                                .white_content {
                                    display: none;
                                    position: fixed;
                                    top: 25%;
                                    left: 50%;
                                    margin-left: -400px;
                                    width: 800px;
                                    /*height: 50%;*/
                                    padding: 20px;
                                    /*border: 16px solid orange;*/
                                    background-color: white;
                                    z-index:99999;

                                    box-shadow: 0 0 10px rgba(0,0,0,0.3);
                                } .white_content > a
                                {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                                 font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
                                 background: #70C0EF;
                                 border-radius: 100px;}   

                                .white_content input[type="text"]
                                {height:50px; width: 100%; border:1px solid #ccc;}

                                .white_content form
                                {position: relative;}

                                .outer-div {
                                    width: 48%;
                                    float: left;
                                    margin-left: 2%;
                                    margin-bottom: 2%; }
                                input {

                                    width: 100%;

                                }
                                a.signup_button {
                                    position: relative;
                                    right: auto;
                                    display: inline;
                                    top: auto;
                                    background: transparent;
                                    color: #DEDC00;
                                }


                            </style>


                            <div id="vbvbvb">
                                <!-- Popup Div Starts Here -->                
                                <div id="popupContact">             
                                    <!-- Contact Us Form -->
                                    <form action="#" id="form" method="post" name="form">
                                        <div id="close" onclick ="div_hide()">X</div>     
                                        <h2>Contact Artist Name </h2>
                                    <?php
                                    $auth = get_post($post_ID); // gets author from post

                                    $authid = $auth->post_author;
                                    $user_email = get_the_author_meta('user_email', $authid);
                                    ?>
                                        <input id="name" name="ftext" placeholder="Name" value="<?php echo get_the_author_meta('display_name'); ?>" type="text" required />
                                        <input id="email" name="ltext"
                                               placeholder="Email" value="<?php //echo $user_email ;?>"type="text" required />
                                        <textarea id="msg" name="ymessage" placeholder="Message"></textarea>
                                        <!--a href="javascript:%20check_empty()" id="submit">Send</a-->
                                        <input class="sub-btn" type="submit" name="submit" value="Send" />
                                        <p class="buyer">You can only contact the artist if you have a buyer account. If you don't, please <span class="signup"><a href="<?php echo site_url(); ?>/terms-conditions/">signup for free</a></span> here.</p>
                                    </form>

                                    <?php
                                    if (isset($_POST["submit"])) {
                                        //echo "Yes";
                                        echo $firstname = $_POST['ftext'];
                                        echo $surname = $_POST['ltext'];
                                        echo $message = $_POST['ymessage'];
                                        echo $currentuser = $_POST['currentuser'];
                                        echo $authoruser = $_POST['authoruser'];

                                        global $wpdb;
                                        //$wpdb->query($sql);
                                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin,author) values (%s,%s,%s,%s,%s)", $firstname, $surname, $message, $currentuser, $authoruser);
                                        $wpdb->query($sql);

                                        $to = "ukarts@471031.vps-10.com";

                                        $txt = "Hello world!";
                                        $headers = "From: shanmand01@gmail.com";

                                        mail($to, $message, $firstname, $headers);
                                        ?>
                                        <?php echo the_author(); ?>
                                        <?php echo get_the_author_id(); ?>
                                        <meta http-equiv="refresh" content="1;url=<?php echo site_url(); ?>/chat/" />
                                        <?php
                                        $cookie_name = "author";
                                        $cookie_value = get_the_author_id();
                                        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

                                        /* if(!isset($_COOKIE[$cookie_name])) {
                                          echo "Cookie named '" . $cookie_name . "' is not set!";
                                          } else {
                                          echo "Cookie '" . $cookie_name . "' is set!<br>";
                                          echo "Value is: " . $_COOKIE[$cookie_name];
                                          } */
                                    } else {
                                        //  echo "N0";
                                    }
                                    ?>






                                </div>
                            </div>

                            <?php
                            if (is_user_logged_in()) {
                                // $author_id= get_the_author_id(); 
                                //if( !empty(get_post_meta( get_the_ID(), 'product_status', true )) && get_post_meta( get_the_ID(), 'product_status', true )=="Active"){
                                //   if(!empty(get_post_meta( get_the_ID(), 'sale_status', true )) && get_post_meta( get_the_ID(), 'sale_status', true ) !="Sold" ){
                                ?>

                                <button id="popup" onclick="div_show()">Contact Artist Name </button>

            <?php
//}}
        } else {
            ?>

                                <!--p class="flex-caption custome-contact-button"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p-->  

                                <!--p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display='block';document.getElementById('fade').style.display='block'">Commission the Artist </a></p-->




                                <div id="light" class="white_content contacttheartistwithoutlogin"> You can only contact the artist if you have a buyer account. If you don’t, please  for free here. <div class="for-free-button"><a class="signup_button_contact" href="<?php echo site_url(); ?>/uk-artists-collection/">signup</a></div><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none'">X</a></div>

                                <div id="fade" class="black_overlay"></div>

        <?php }
        ?>

                            <script>
                                function div_show() {
                                    document.getElementById('vbvbvb').style.display = "block";
                                }
                                function div_hide() {
                                    document.getElementById('vbvbvb').style.display = "none";
                                }
                            </script>




                            <style>

                                form#popupContact {    background-color: white; }
                                #vbvbvb { width:100%; height:100%; top:0;left:0; display:none;position:fixed; background-color:#313131;
                                          overflow:auto; z-index: 99999;}
                                img#close {
                                    position:absolute;right:-14px;
                                    top:-14px;cursor:pointer
                                }
                                div#popupContact {
                                    background: #fff none repeat scroll 0 0;
                                    font-family: "Raleway",sans-serif;
                                    left: 20%;
                                    margin-left: 0;
                                    padding-bottom: 30px;
                                    padding-top: 40px;
                                    position: absolute;
                                    top: 17%;
                                    width: 60%;
                                }
                                #popupContact {
                                    min-height: 410px;
                                    padding: 20px;
                                }
                                #popupContact h2 {
                                    font-size: 24px;
                                    font-weight: bold;
                                    margin-bottom: 20px;
                                    text-align: center;
                                }
                                #popupContact input {
                                    background: #f2f2f2 none repeat scroll 0 0;
                                    border: medium none;
                                    display: inline-block;
                                    float: left;
                                    height: 40px;
                                    margin-left: 10px;
                                    margin-right: 10px;
                                    padding-left: 10px;
                                    width: 46%;
                                }
                                #popupContact > img {
                                    background: #fff none repeat scroll 0 0;
                                    border-radius: 50%;
                                    margin-right: 0;
                                    margin-top: 6px;
                                    padding: 5px;
                                    width: 40px;
                                }
                                .buyer {
                                    float: left;
                                    margin-left: 20px;
                                    margin-top: 30px;
                                    text-align: left;
                                    width: 60%;
                                }
                                #popupContact textarea {
                                    background: #eee none repeat scroll 0 0;
                                    border: medium none;
                                    float: left;
                                    height: 140px;
                                    margin-left: 10px;
                                    margin-top: 40px;
                                    padding: 10px;
                                    width: 95%;
                                }
                                .signup {
                                    color: #e0de0d;
                                }
                                .sub-btn {
                                    background: #dedc00 none repeat scroll 0 0 !important;
                                    height: 40px !important;
                                    margin-top: 30px;
                                    width: 200px !important;
                                    float: left;
                                }
                            </style>



                            <!--p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p-->
                            <div id="light" class="white_content">
                                    <?php
                                    if (is_user_logged_in()) {
                                        $author_id = get_the_author_id();
                                        ?></p>

            <? // echo do_shortcode('[front-end-pm]');
            //echo do_shortcode('[yobro_chatbox]');  
            ?>


                                    <form name="contactusartist" method="post">
                                        <input type="text" name="ftext" placeholder="First name" required />
                                        <input type="text" name="ltext" placeholder="Sure" required />
                                        <textarea rows="4" cols="50" name="ymessage" placeholder="Your message" required>
                                        </textarea> 
                                    <?php $current_user = wp_get_current_user(); ?>
                                    <?php $current_user->user_login; ?>


                                        <input type="hidden" name="currentuser" value="<?php $current_user->user_login; ?>" />
                                        <input type="hidden" name="authoruser" value="<?php $author_id = get_the_author(); ?>" />
                                        <input type="submit" name="submit" value="Send" />
                                        <p>You can only contact the artist,If you have a buyer account,If you don't,please <a href="<?php echo site_url(); ?>/uk-artists-collection/">signup</a> here </p>
                                    </form>
                                    <?php
                                    if (isset($_POST["submit"])) {
                                        //echo "Yes";
                                        $firstname = $_POST['ftext'];
                                        $surname = $_POST['ltext'];
                                        $message = $_POST['ymessage'];
                                        $currentuser = $_POST['currentuser'];
                                        $authoruser = $_POST['authoruser'];

                                        global $wpdb;
                                        //$wpdb->query($sql);
                                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin,author) values (%s,%s,%s,%s,%s)", $firstname, $surname, $message, $currentuser, $authoruser);
                                        $wpdb->query($sql);
                                    } else {
                                        //  echo "N0";
                                    }
                                } else {
                                    ?>You can only contact the artist if you have a buyer account. If you don’t, please <a href="<?php echo site_url(); ?>/uk-artists-collection/">signup</a> for free here.<?php
                                }
                                ?>

                                <?php
                                if (isset($_POST["submit"])) {
                                    $fname = $_POST['fname'];
                                    $lname = $_POST['lname'];
                                    $email = $_POST['email'];
                                    $subject = "Contact of Artist";
                                    $message = $_POST['message'];
                                    $to = "ukarts@471031.vps-10.com";
// mail($to,$subject,$email,$message);
                                } else {
                                    //echo "N0"; 
                                }
                                ?>
                                <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none'">X</a></div>
                            <div id="fade" class="black_overlay"></div>

        <?php
        $user = wp_get_current_user();

        //if(!in_array( 'shop_vendor', (array) $user->roles )){
        //if( !empty(get_post_meta( get_the_ID(), 'product_status', true )) && get_post_meta( get_the_ID(), 'product_status', true )=="Active"){
        //     if(!empty(get_post_meta( get_the_ID(), 'sale_status', true )) && get_post_meta( get_the_ID(), 'sale_status', true ) !="Sold" ){
        ?>


                            <!---a href="#">Commission the Artist</a-->
                            <p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display = 'block';
                                    document.getElementById('fade').style.display = 'block'">Commission the Artist </a></p>
                            <?php
                            // } 
                            //	 }
                            //  } 
                            //jag code start
                            $user_id = get_current_user_id();
                            $user_meta = get_userdata($user_id);

                            $user_roles = $user_meta->roles;
                            $rol = $user_roles[0];
                            // print_r ($user_roles); 
                            /* if(is_user_logged_in() && $rol==administrator  ){
                              ?>
                              <p class="flex-caption custome-contact-button"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p>


                              <p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display='block';document.getElementById('fade').style.display='block'">Commission the Artist </a></p>

                              <?php } */

                            if (!is_user_logged_in()) {
                                ?>
                                <p class="flex-caption custome-contact-button"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'block';
                                        document.getElementById('fade').style.display = 'block'">Contact the Artist</a></p>
        <?php
        }
        /*  if(is_user_logged_in() && $rol==contributor   ){
          ?>
          <p class="flex-caption custome-contact-button"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p>

          <p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display='block';document.getElementById('fade').style.display='block'">Commission the Artist </a></p>
          <?php } */
        ?> 



                            <div id="lighted" class="white_content pop_comission_main contactANWlogin">

                                <div class="popup-comission"><?php //echo do_shortcode('[contact-form-7 id="1103" title="commision-pop-up"]'); ?>
                                    <?php
                                    global $wpdb;

//print_r((array) $user->roles);
                                    if (is_user_logged_in()) {
                                        $sender_id = get_current_user_id();
                                        $post_id_m = get_the_id();

                                        $user_count = $wpdb->get_row("SELECT * FROM ua_commission where post_id=" . $post_id_m . " and sender_id=" . $sender_id);

                                        if (count($user_count) >= 1) {
                                            echo "You already applied this artists. your referance id is : " . $user_count->ref_id;
                                        } else {
                                            ?>

                                            <form action="<?php echo site_url(); ?>/commmission-chat" method="get" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">

                                                <div style="display: none;">
                <?php
                $auth = get_post($post_ID); // gets author from post

                $authid = $auth->post_author;
                $user_email = get_the_author_meta('user_email', $authid);
                ?> 
                                                    <input type="hidden" name="post_id" value="<?php echo get_the_id(); ?>">
                                                    <input type="hidden" name="email" value="<?php echo $user_email; ?>">
                                                    <input type="hidden" name="author_id" value="<?php echo $authid; ?>">
                                                    <input type="hidden" name="commsion_chat" value="commsion_chat">
                                                    <input type="hidden" name="_wpcf7_container_post" value="1079">
                                                    <!--<input type="hidden" name="ref_id" value="<?php echo get_the_id() . $authid; ?>"> -->
                                                </div>
                                                <div class="title_head">
                                                    <h2>Commission Artist Name</h2>
                                                    <span class="small_text">How does it work </span>
                                                </div>
                                                <p><span class="wpcf7-form-control-wrap firstname">
                                                        <input type="text" name="firstname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="First name*"></span>
                                                </p>
                                                <p><span class="wpcf7-form-control-wrap Surname"><input type="text" name="Surname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Surname*"></span></p>
                                                <p><span class="wpcf7-form-control-wrap Budget"><input type="number" name="Budget" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" aria-invalid="false" placeholder="£ Budget*"></span></p>
                                                <p><span class="wpcf7-form-control-wrap PreferredCompletionDate"><input type="date" name="PreferredCompletionDate" value="" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-required wpcf7-validates-as-date" aria-required="true" aria-invalid="false" placeholder="Preferred Completion Date*"></span></p>

                                                <p><span class="wpcf7-form-control-wrap Email"><input type="email" name="Email" value="<?php //echo $user_email; ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span></p>
                                                <p><span class="wpcf7-form-control-wrap description"><textarea name="description" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Description*"></textarea></span></p>
                                                <div class="left-s">
                                                    <p class="prefersize"><span class="wpcf7-form-control-wrap preferredsize"><input type="text" name="preferredsize" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Preferred size*"></span></p>
                                                    <p class="terms"><span class="wpcf7-form-control-wrap checkbox-156"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="checkbox-156[]" value="commission agree"><span class="wpcf7-list-item-label">commission agree</span></span></span></span> I agree to the UK Arts <a class="terms" href="<?php echo site_url() . '/terms-conditions/'; ?>"> Terms &amp; Conditions for Commissions</a></p>  
                                                    <p><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span>
                                                    </p></div>
                                                <div class="right-s">
                                                    <p class="choose">Proof of Citizenship* <label for="user_certificate" class="btn">Choose files </label><input class="text-input" required="" type="file" style="visibility:hidden;" name="UploadInspiration" id="user_certificate"></p>
                                                    <!--span>Upload Inspiration</span> <span class="wpcf7-form-control-wrap UploadInspiration"><input type="file" name="UploadInspiration" size="40" class="wpcf7-form-control wpcf7-file" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv" aria-invalid="false"></span--><p></p>
                                                    <div class="commit">
                                                        <p>
                                                            You are not commiting to progress with the commission by submitting this form.</p>
                                                        <p>You can only contact the artist if you have a buyer account.if you don,t please <a href="#"> Signup for free</a> here
                                                        </p></div>

                                                </div>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div></form>
            <?php
            }
        } else {
            ?>

                                        <p class="comting">You are not committing to progress with the commission by submitting this form.<br>
                                            You can only contact the artist if you have a buyer account. if you don't than please signup for free here.

                                        </p><div class="for-free-button"><a class="signup_button_contact" href="<?php echo site_url(); ?>/uk-artists-collection/">Signup here</a></div>

            <?php } ?>     

                                </div>


                                <a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none'">X</a></div>
                            <div id="fade" class="black_overlay"></div>

                        </div>
                    </div>

            </div>

            <?php
        }

        public function featured_standard() {

            //echo get_the_ID();
            // echo get_the_post_thumbnail( get_the_ID(), $size );
            $images = $this->get_featured_images();
            $before = '<div class="download-gallery">';

            $after = '</div>';

            $size = apply_filters('marketify_featured_standard_image_size', 'large');

            echo $before;

            //    if ( empty( $images ) && has_post_thumbnail( get_the_ID() ) ) {
            ?>

            <?
            echo $after;
            return;
            // } else {
            foreach ($images as $image) :
                ?>
                <div class="download-gallery__image"><a href="<?php echo esc_url(wp_get_attachment_url($image->ID)); ?>"><?php echo wp_get_attachment_image($image->ID, $size); ?></a></div>
            <?php endforeach; ?>
            <?php
            //   }
//
            echo $after;
        }

        public function featured_standard_navigation() {
            $images = $this->get_featured_images();

            if (empty($images)) {
                return;
            }

            $before = '<div class="download-gallery-navigation ' . ( count($images) > 6 ? 'has-dots' : '' ) . '">';
            $after = '</div>';

            $size = apply_filters('marketify_featured_standard_image_size_navigation', 'thumbnail');

            if (count($images) == 1 || ( empty($images) && has_post_thumbnail(get_the_ID()) )) {
                return;
            }

            echo $before;

            foreach ($images as $image) {
                ?>
                <div class="download-gallery-navigation__image"><?php echo wp_get_attachment_image($image->ID, $size); ?></div>
                <?php
            }

            echo $after;
        }

        /**
         * Output featured audio.
         *
         * Depending on the found audio it will be oEmbedded or create a
         * WordPress playlist from the found tracks.
         *
         * @since 2.0.0
         *
         * @return mixed
         */
        public function featured_audio() {
            $audio = $this->_get_audio();

            // if we are using a URL try to embed it (only on single download)
            if (!is_array($audio) && is_singular('download') && !did_action('marketify_single_download_content_after')) {
                $audio = wp_oembed_get($audio);
            } elseif ($audio) {
                // grid preview only needs one
                if (!is_singular('download')) {
                    $audio = array_splice($audio, 0, 1);
                }

                if (!empty($audio)) {
                    $audio = wp_playlist_shortcode(array(
                        'id' => get_post()->ID,
                        'ids' => $audio,
                        'images' => false,
                        'tracklist' => is_singular('download')
                            ));
                }
            }

            $audio = apply_filters('marketify_get_featured_audio', $audio);

            if ($audio) {
                echo '<div class="download-audio">' . $audio . '</div>';
            }
        }

        /**
         * Find audio for a download. Searches a few places:
         *
         * 1. `preview_files` FES File Uplaod Field (playlist)
         * 2. `audio` FES URL Field (oEmbed)
         * 3. Attached audio files (playlist)
         *
         * @since 2.0.0
         *
         * @return mixed $audio
         */
        private function _get_audio() {
            $audio = false;

            // check to see if the FES file upload field exists
            $audio = get_post()->preview_files;

            // check to see if the FES URL field exists
            if (!$audio || '' == $audio) {
                $field = apply_filters('marketify_audio_field', 'audio');
                $audio = get_post()->$field;
            }

            // query attached media
            if (!$audio || '' == $audio) {
                $audio = get_attached_media('audio', get_post()->ID);

                if (!empty($audio)) {
                    $audio = wp_list_pluck($audio, 'ID');
                }
            }

            return $audio;
        }

        public function featured_video() {
            $video = $this->_get_video();

            if ('' == $video || empty($video)) {
                return;
            }

            $info = wp_check_filetype($video);
            $atts = apply_filters('marketify_featured_video_embed_atts', '');

            if ('' == $info['ext']) {
                global $wp_embed;

                $output = $wp_embed->run_shortcode(sprintf('[embed %s]%s[/embed]', $atts, $video));
            } else {
                $output = do_shortcode(sprintf('[video %s="%s" %s]', $info['ext'], $video, $atts));
            }

            echo '<div class="download-video">' . $output . '</div>';
        }

        /**
         * Find an associated video for the download.
         *
         * @since 2.9.0
         * @return mixed Video URL or false
         */
        public function _get_video() {
            $video = false;

            $field = apply_filters('marketify_video_field', 'video');
            $video = get_post()->$field;

            // query attached media
            if (!$video || '' == $video) {
                $video = get_attached_media('video', get_post()->ID);

                if (!empty($video)) {
                    $video = current($video);
                    $video = wp_get_attachment_url($video->ID);
                }
            }

            return $video;
        }

    }
    