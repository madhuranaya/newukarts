<?php
class Marketify_Template_Page_Header {

    /**
     * The HTML tag to use for the page title.
     *
     * @since 2.9.0
     * @var $tag string
     */
    protected $tag;

    public function __construct() {
        $this->tag = apply_filters( 'marketify_page_header_tag', 'h2' );

        add_filter( 'marketify_page_header', array( $this, 'tag_atts' ) );
        add_action( 'marketify_entry_before', array( $this, 'close_div' ), 10 ); // close the .page-header div opened in each callback
        add_action( 'marketify_entry_before', array( $this, 'close_div' ), 20 ); // close the .header-outer div opened in header.php

        add_filter( 'get_the_archive_title', array( $this, 'get_the_archive_title' ) );

        add_action( 'marketify_entry_before', array( $this, 'archive_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'page_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'attachment_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'post_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'home_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'blog_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'not_found_title' ), 5 );
    }

    public function close_div() {
        echo '</div>';
    }

    /**
     * Get the tag to wrap page headers with.
     *
     * @since 2.9.0
     *
     * @return string $tag
     */
    public function get_tag() {
        return esc_attr( $this->tag );
    }

    public function blog_title() {
        if ( ! is_home() ) {
            return;
        }
?>
<div class="page-header container">
    <div <?php// echo $this->get_tag(); ?> class="page-title 2"><?php echo get_option( 'page_for_posts' ) ? get_the_title( get_option( 'page_for_posts' ) ) : __( 'Blog', 'marketify' ); ?></div>
<?php
    }

    public function home_title() { 
        if ( ! is_front_page() || is_home() ) {
            return;
        }

        if ( is_page_template( 'page-templates/home-search.php' ) ) {
?>
<div class="page-header__search">
<?php
    add_filter( 'get_search_form', array( marketify()->template->header, 'search_form' ) );
    get_search_form();
    remove_filter( 'get_search_form', array( marketify()->template->header, 'search_form' ) );
?>
</div>
<?php
        }

        the_post();
        the_content();
        rewind_posts();
    }

    public function page_title() {
        if ( ! is_singular( 'page' ) ) {
            return;
        }

        the_post();
?>
<div class="page-header page-header--singular container">
    <div <?php //echo $this->get_tag(); ?> ><h2 class="page-title"><?php the_title(); ?></h2></div>
<?php
        rewind_posts();
    }

    /**
     * Page title for attachments.
     *
     * @since 2.7.0
     *
     * @return void
     */
    public function attachment_title() {
        if ( ! is_attachment() ) {
            return;
        }

        the_post();

        $post = get_post();
?>
<div class="page-header page-header--singular container">
    <div <?php //echo $this->get_tag(); ?> class="page-title 6"><a href="<?php echo esc_url( get_permalink( $post->post_parent ) ); ?>" title="<?php echo esc_attr( sprintf( __( 'Return to %s', 'marketify' ), strip_tags( get_the_title( $post->post_parent ) ) ) ); ?>" rel="gallery"><?php
        /* translators: %s - title of parent post */
        printf( __( '<span class="meta-nav">&larr;</span> %s', 'marketify' ), get_the_title( $post->post_parent ) );
    ?></a></div>
<?php
        rewind_posts();
    }

    public function archive_title() {
        if ( ! is_archive() ) {
            return;
        }
?>
<div class="page-header container">
    <div <?php// echo $this->get_tag(); ?> class="page-title 5"><?php the_archive_title(); ?></div>
<?php
    }

    public function get_the_archive_title( $title ) { 
        if ( is_search() ) {
            $title = get_search_query();
        } else if ( is_post_type_archive( 'download' ) ) {
            $title = edd_get_label_plural();
        } else if ( is_tax() ) {
            $title = single_term_title( '', false );

            if ( did_action( 'marketify_downloads_before' ) ) {
                $title = sprintf( __( 'All %s', 'marketify' ), $title );
            }
        } else if ( function_exists( 'fes_get_vendor' ) && fes_get_vendor() ) {
            $title = sprintf( __( 'Recent %s', 'marketify' ), edd_get_label_plural() );
        }

        return $title;
    }

    public function post_title() {
        if ( ! is_singular( 'post' ) ) {
            return;
        }

        the_post();

        $social = marketify()->template->entry->social_profiles();
?>
<div class="page-header page-header--single container">
    <div <?php// echo $this->get_tag(); ?> class="page-title 4"><?php //the_title(); ?></div>
    
    </div>
    
	<?php $get_the_id= get_the_id(); ?>
	<div class="post_thumbnail_get"><?php echo get_the_post_thumbnail(); ?></div>
    <div class="post-custom-section"> 
	
    <!--h2>Single post section</h2-->
    
          <div class="post-content-section">
          <h3 class="post-title-custom"><?php echo the_title(); ?></div>

        <div class="post-date-custom"><?php echo the_date(); ?></div>
    </div>
    <div class="content-post-custom demo">
	<div class="container demooo">
        <div class="blog-page-title"><h2><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2></div>


        <!--  <div class="blog-page-thumbnail">
            <a href="<?php  the_permalink(); ?>"><?php echo the_post_thumbnail();  ?></a></div> 
 -->
       <div class="section-second-content"> <?php echo the_content(); ?></div>
      <?php //get_template_part( 'content', 'entry-meta' ); ?>
     
      <div class="social-icon-share"><span>Share</span><?php echo do_shortcode('[feather_share]'); ?></div>
</div>
    </div>

    <ul class="tribe-events-sub-nav">
<li class="tribe_left_navigation_classes">
    <span class="line-inner-cls"><?php previous_post_link('%link', 'Back') ?> </span> 
  

</li>   

    <div class="inner-line-io">|</div>
     
    <li class="tribe_right_navigation_classes"> 
<span class="line-inner-cls"> <?php next_post_link('%link', 'More') ?> </span>

 
</li>

</ul> 




    </div>
    </div>
<?php //echo  $the_get_id= get_the_id(); ?>
    
    
    
    <div class="page-header__entry-meta page-header__entry-meta--date entry-meta entry-meta--hentry">
        <?php get_template_part( 'content', 'entry-meta' ); ?>
    </div>
<?php
    rewind_posts();
    }

    public function not_found_title() {
        if ( ! is_404() ) {
            return;
        }
?>
<div class="page-header page-header--singular container">
    <div <?php //echo $this->get_tag(); ?> class="page-title 3"><?php _e( 'Oops! That page can&rsquo;t be found.', 'marketify' ); ?></div>
<?php
    }

    /**
     * Add the HTML attributes to the wrapper div around the header elements
     */
    public function tag_atts( $args = array() ) {
        $defaults = apply_filters( 'marketify_page_header_defaults', array(
            'class' => array( 'header-outer' ),
            'object_ids' => false,
            'size' => 'full',
            'style' => array()
        ) );

        $args = wp_parse_args( $args, $defaults );
        $atts = $this->build_tag_atts( $args );

        $output = '';

        foreach ( $atts as $attribute => $properties ) {
            $output .= sprintf( '%s="%s"', $attribute, implode( ' ', $properties ) );
        }

        return $output;
    }

    private function build_tag_atts( $args ) {
        $args = $this->add_background_image( $args );
        $args = $this->add_background_video( $args );

        $allowed = apply_filters( 'marketify_page_header_allowed_atts', array( 'class', 'style' ) );
        $atts = apply_filters( 'marketify_page_header_atts', $args );

        $atts = array_intersect_key( $atts, array_flip( $allowed ) );

        return $atts;
    }

    private function add_background_image( $args ) {
        $background_image = $this->find_background_image( $args );

        if ( $background_image ) {
            //$args[ 'style' ][] = 'background-image:url(' . $background_image . ');';
           // $args[ 'class' ][] = 'has-image';
        }    else {
            $args[ 'class' ][] = 'no-image';
        }

        return $args;
    }

    private function add_background_video( $args ) {
        $video = true;

        if ( $video ) {
            $args[ 'class' ][] = 'has-video';
        }

        return $args;
    }

    private function find_background_image( $args ) {
        $background_image = false;
        $format_style_is_background = false;

        if ( marketify()->get( 'edd' ) ) {
            $format_style_is_background = marketify()->get( 'edd' )->template->download->is_format_style( 'background' );
        }

        if (
            is_singular( array( 'post', 'page' ) ) ||
            is_home() || 
            ( is_singular( 'download' ) && $format_style_is_background )
        ) {
            $id = get_post_thumbnail_id();

            if ( is_home() ) {
                $id = get_post_thumbnail_id( get_option( 'page_for_posts' ) );
            }

            $background_image = wp_get_attachment_image_src( $id, $args[ 'size' ] );
            $background_image = $background_image[0];
        }

        return apply_filters( 'marketify_page_header_image', $background_image, $args );
    }

}
