<?php
/**
 * Template Name: Test Template
 *
 * @package Marketify
 */
get_header();
?>
<div id="primary" class="content-area col-sm-12">
    <main id="main" class="site-main" role="main">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content"> 
                <section class="regular-pro">
        <?php
        global $wpdb;

        // $findID = $wpdb->get_results("SELECT * FROM ua_fes_vendors  WHERE status = 'approved' ORDER BY id DESC Limit 12",ARRAY_A);
        //echo "<pre>";
        //print_r($findID);
        ?>  
        <div class="row">
            <div class="col-lg-12 producers-main">
                <?php
                $args = array(
                    'role' => 'shop_vendor',
                    'orderby' => 'name',
                    'order' => 'asc',
                    'number' => -1
                );
                $findID = get_users($args);
                //echo get_avatar( get_the_author_meta( 'ID' ), 100 ); 
                echo '<div>';
//echo "<pre>";
//print_r($users);
                if (!empty($findID)) {
                    $firstLetter = 0;
                    $desiredColumns = 6;  //you can change this!
                    $columnCount = (count($findID) + 26) / $desiredColumns + 1;
                    echo "<table><tr><td>";
                    //echo "<pre>";
                    // print_r($findID);       
                    foreach ($findID as $key => $cur) {
                        $flag = 0;
                        global $wpdb;
                        $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $cur->ID . " AND meta_key = 'first_name'");
                        $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $cur->ID . " AND meta_key = 'last_name'");
                        if ($first_letter != substr($findu[0]->meta_value, 0, 1)) {
                            $first_letter = substr($findu[0]->meta_value, 0, 1);
                            $flag = 1;
                        }

                        if ($flag) {
                            echo '<strong style="text-transform: uppercase;">' . $first_letter . '</strong><br>'; // Output the first letter
                        }
                        ?>
                        <a class="user-fullname" href="http://beta.uk-arts.com/artist-main-2?user_id=<?php echo $cur->ID; ?>"><?php //echo $cur->display_name;                                                                                                                        ?>
                            <?php
//echo "<pre>";
//print_r($findu);
                            ?>
                            <?php
                            echo $findu[0]->meta_value;
                            echo " ";
                            echo $findu1[0]->meta_value;
                            ?>


                        </a><br />
                        <?php
                        //echo "<a href=".home_url()."/artist-main/>".$cur['name']."</a><br/>";
                    }
                    echo "</td><tr></table>";
                } else {
                    echo 'No users found.';
                }
                echo '</div>';
                ?>  
            </div>
        </div>
    </section>
                <br /><br /><br /><br />
                
                
                
                <?php
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $args = array(
                    'paged' => $paged,
                    'orderby' => 'DESC',
                    'posts_per_page' => 5,
                    'post_type' => 'competitions'
                );
                $loop = new WP_Query($args);

                if ($loop->have_posts()) :
                    while ($loop->have_posts()) : $loop->the_post();
                        ?><h2><?php the_title(); ?></h2> <?php endwhile;
                    ?>
                    <div class="pagination-compition demo">
                        <?php custamPagination($paged, $loop->max_num_pages); // Pagination Function ?></div> <?php
                endif;
                wp_reset_postdata();
                ?>




















                <?php
                global $wpdb;
                $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

                $limit = 6; // number of rows in page
                $offset = ( $pagenum - 1 ) * $limit;
                $table_name = $wpdb->prefix . "posts";
                $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type='competitions'");

                $num_of_pages = ceil($total / $limit);
                ?>
                <div class="blog-post-outer gtgggggg">
                    <!--h2>May 2018</h2-->  
                    <?php
                    $author_querya = array('post_type' => 'competitions', 'paged' => $pagenum,
                        'posts_per_page' => $limit);
                    $author_postsa = new WP_Query($author_querya);
                    ?>
                </div>

                <div class="blog-post-outer">
                    <div class="container">
                        <div class="row">

                            <?php while ($author_postsa->have_posts()) : $author_postsa->the_post(); ?>

                                <?php
                                $get_the_id = get_the_id();
                                $my_meta = get_post_meta($get_the_id, 'date_compition', TRUE);
                                $date_compition = $my_meta;
                                $date_comp = substr($date_compition, -5, 2);


                                $current_month = get_the_date('F');
                                $author_postsa->current_post;
                                $f = $author_postsa->current_post - 1;
                                $old_date = mysql2date('F', $author_postsa->posts[$f]->post_date);
                                if ($current_month != $old_date) {
                                    ?>
                                    <h2><?php the_date('F Y'); ?> </h2>
                                    <?php
                                }
                                ?>
                                <div class="col-md-4 col-sm-12">
                                    <div class="blog-post-inner">
                                        <div class="blog-page-thumbnail">
                                            <?php if (has_post_thumbnail()) { ?>
                                                <div class="blog-page-thumbnail"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail(); ?></a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="blog-page-thumbnail"><a href="<?php echo the_permalink(); ?>"><img src="http://beta.uk-arts.com/wp-content/uploads/edd/2018/05/Winter-light-Mawgan-Porth.jpg" class="dummy_img" /></a>
                                                </div>

                                            <?php } ?>

                                        </div>
                                        <div class="blog-page-title">
                                            <a href="<?php the_permalink(); ?>">
                                                <h2><?php the_title(); ?>
                                            </a>
                                            </h2>
                                        </div>

                                        <div class="blog-page-date">
                                            <h2><?php
                                                echo date("d M Y", strtotime(get_the_date($date_compition)));
                                                ?></h2><span class="add-new-btnd">  
                                                <h2> <?php
                                                    $post_date1 = get_the_date();
                                                    $date = strtotime(str_replace('/', '-', $post_date1));
                                                    $today = strtotime(date("d-m-Y"));
                                                    if ($date >= $today) {
                                                        
                                                    } else { //echo "Expiredbhhhh";   
                                                    }
                                                    ?></h2>
                                            </span>
                                        </div>
                                        <div class="blog-page-date">
                                            <h3>competition Date:  <?php
                                                echo date("d M Y", strtotime(get_the_date($date_compition)));
                                                ?></h3>
                                        </div>
                                        <div class="blog-page-content"><? echo the_excerpt(); ?></div>  
                                    </div>
                                </div>
                                <?php
//}
                            endwhile;
                            ?>
                            <div class="submit-comption-main">
                                <div class="btn btn-info btn-lg compe-butt" data-toggle="modal" data-target="#myModal">SUBMIT A COMPETITION</div></div>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Contact For Competition</h4>
                                        </div>  
                                        <div class="modal-body">
                                            <?php echo do_shortcode('[contact-form-7 id="3852" title="competion-form"]'); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="pagination-compition demo">

                                <?php
                                $page_links = paginate_links(array(
                                    'base' => add_query_arg('pagenum', '%#%'),
                                    'format' => '',
                                    'prev_text' => __(' <span class="bac">Back</span>', 'text'),
                                    'next_text' => __('<span class="mor">More</span> ', 'text'),
                                    'total' => $num_of_pages,
                                    'current' => $pagenum
                                ));

                                if ($page_links) {
                                    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '<div class="bar_text"></div></div></div>';
                                }
                                ?>    
                            </div>
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div>









                <!--div class="col-sm-12 col-md-12">
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $data = new WP_Query(array(
                    'post_type' => 'post', // your post type name
                    'posts_per_page' => 5, // post per page
                    'paged' => $paged,
                    'orderby' => 'date',
                    'order' => 'DESC',
                ));

                if ($data->have_posts()) :
                    while ($data->have_posts()) : $data->the_post();
                        ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <h1 style="margin-bottom: 20px;"><?php the_title(); ?></h1>
                        <?php
                    endwhile;
                    ?> 
                                                                                                                                                                                                                                                                                                                                        <div class="pagination-compition demo">
                    <?php
                    $total_pages = $data->max_num_pages;

                    if ($total_pages > 1) {

                        $current_page = max(1, get_query_var('paged'));

                        $pagination = paginate_links(
                                array(
                                    'base' => get_pagenum_link(1) . '%_%',
                                    'format' => '/page/%#%',
                                    'current' => $current_page,
                                    'total' => $total_pages,
                                    'prev_text' => __('<span class="bac">Back</span>', 'text'),
                                    'next_text' => __('<span class="mor">More</span> ', 'text'),
                                )
                        );
                        if ($pagination) {
                            echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $pagination . '<div class="bar_text"></div></div></div>';
                        }
                    }
                    ?> 
                                                                                                                                                                                                                                                                                                                                        </div>
                <?php else : ?>
                                                                                                                                                                                                                                                                                                                                        <h3><?php _e('404 Error&#58; Not Found', ''); ?></h3>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                </div--><!-- .col -->

            </div><!-- .entry-content -->
        </article><!-- #post-## -->
    </main>
</div>


<?php get_footer(); ?>