<?php
$user_id = get_current_user_id();
$result = $wpdb->get_results("select * from " . $wpdb->prefix . "commission where  $field_name =" . $user_id . " and status_code=0");
$result1 = $wpdb->get_results("select * from " . $wpdb->prefix . "commission where  $field_name=" . $user_id . " and status_code=1");
$result2 = $wpdb->get_results("select * from " . $wpdb->prefix . "commission where  $field_name=" . $user_id . " and status_code=2");
$result3 = $wpdb->get_results("select * from " . $wpdb->prefix . "commission where  $field_name=" . $user_id . " and status_code=3");
//print_r($result);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="account-users-main-right">
    <ul>
        <li>Filter by : </li>
        <li>
            <label class="checkbox-inline">
                <input type="radio" name="optionsRadiosinline" id="pending" value="pending" checked="">
                Pending
            </label>
        </li>
        <li>
            <label class="checkbox-inline"><input type="radio" name="optionsRadiosinline" id="ongoing" value="ongoing">
                On Going 
            </label>
        </li>

        <li>
            <label class="checkbox-inline">
                <input type="radio" name="optionsRadiosinline" id="completed" value="completed">
                Completed
            </label>
        </li>

        <li>
            <label class="checkbox-inline">
                <input type="radio" name="optionsRadiosinline" id="cancelled" value="cancelled">
                Cancel
            </label>
        </li>
    </ul>
</div>
<div class="pending_pro">
    <table>
        <tr>
            <th>S.No 1</th>
            <th> Ref Id </th>
            <th> Client Name </th>
            <th>Amount</th>
            <th>status</th>
            <th>action</th>
        </tr>


        <?php
        $i = 1;
        if (count($result) > 0) {
            foreach ($result as $results) {
                ?>
                <tr id="<?php echo $results->ref_id; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $results->ref_id; ?></td>
                    <td><?php echo to_get_user_name($results->sender_id); ?></td>
                    <td><?php echo $results->budget; ?></td>
                    <td><?php
                        if ($results->status_code == 0) {
                            echo "Pending";
                        }
                        ?></td>
                    <td>
                        <?php if ($results->status_code == 0) { ?> 
                            <?php if (!empty(to_show_payment_status($results->ref_id, 'total_payment'))) { ?> 
                                <button class="accept" data-id="<?php echo $results->ref_id; ?>">Accept</button> 
                                <button class="cancelled_p" data-id="<?php echo $results->ref_id; ?>" >Cancel</button>
                                <?php
                            }
                        }
                        ?>
                        <a href="<?php echo site_url(); ?>/commmission-chat?ref_id=<?php echo $results->ref_id; ?>&author_id=<?php echo $results->receiver_id; ?>">chat </a>
                    </td>


                </tr>
                <?php
                ++$i;
            }
        } else {

            echo "<tr><td colspan='6'>No record found</td></tr>";
        }
        ?>
    </table>
</div>
<div class="on_going" style="display:none">
    <table>
        <tr><th>S.No</th><th> Ref Id </th><th> Client Name </th><th>Amount</th><th>status</th><th>action</th></tr>


        <?php
        $i = 1;
        if (count($result1) > 0) {
            foreach ($result1 as $results1) {
                ?>
                <tr id="<?php echo $results1->ref_id; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $results1->ref_id; ?></td>
                    <td><?php echo to_get_user_name($results1->sender_id); ?></td>
                    <td><?php echo $results1->budget; ?></td>
                    <td><?php
                        if ($results1->status_val == "accept") {
                            echo "On Going";
                        } else if ($results1->status_val == "client_wait") {
                            echo "wait for client complete";
                        }
                        ?></td>
                    <td><?php if ($results1->status_code == 1) { ?> 
                            <button class="complete_p" data-id="<?php echo $results1->ref_id; ?>" <?php if ($results1->status_val == "client_wait") { ?>disabled<?php } ?> data-status_val="client_wait" data-status_code="1">Mark as Complete</button> <button class="cancelled_p" data-id="<?php echo $results1->ref_id; ?>" >cancel</button>
                        <?php } ?> <a href="<?php echo site_url(); ?>/commmission-chat?ref_id=<?php echo $results1->ref_id; ?>&author_id=<?php echo $results1->receiver_id; ?>">chat </a> </td>


                </tr>
                <?php
                ++$i;
            }
        } else {

            echo "<tr><td colspan='6'>No record found</td></tr>";
        }
        ?>
    </table>

</div>

<div class="complete" style="display:none">
    <table>
        <tr><th>S.No</th><th> Ref Id </th><th> Client Name </th><th>Amount</th><th>Pending Amount</th><th>Earn  Amount</th><th>status</th><th>action</th></tr>


        <?php
        $i = 1;
        if (count($result2) > 0) {
            foreach ($result2 as $results2) {
                ?>
                <tr id="<?php echo $results->ref_id; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $results2->ref_id; ?></td>
                    <td><?php echo to_get_user_name($results2->sender_id); ?></td>
                    <td><?php echo $results2->budget; ?></td>
                    <td><?php echo to_show_payment_status($results2->ref_id, 'pending_money'); ?></td>
                    <td><?php
                        if (empty(to_show_payment_status($results2->ref_id, 'escrow_money'))) {
                            echo "0";
                        } else {
                            echo to_show_payment_status($results2->ref_id, 'escrow_money');
                        }
                        ?></td>
                    <td><?php if ($results2->status_code == 2) { ?> 

                            <?php
                            if (to_show_payment_status($results2->ref_id, 'pending_money') == 0) {
                                echo "complete";
                            } else {
                                echo "wait for payment";
                            }
                            ?>
                        <?php } ?></td>
                    <td><a href="<?php echo site_url(); ?>/commmission-chat?ref_id=<?php echo $results2->ref_id; ?>&author_id=<?php echo $results2->receiver_id; ?>">chat </a></td>


                </tr>
                <?php
                ++$i;
            }
        } else {

            echo "<tr><td colspan='8'>No record found</td></tr>";
        }
        ?>
    </table>

</div>
<div class="cancelled" style="display:none" >
    <table>
        <tr><th>S.No</th><th> Ref Id </th><th> Client Name </th><th>Amount</th><th>status</th><th>action</th></tr>


        <?php
        $i = 1;
        if (count($result3) > 0) {
            foreach ($result3 as $results3) {
                ?>
                <tr id="<?php echo $results3->ref_id; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $results3->ref_id; ?></td>
                    <td><?php echo to_get_user_name($results3->sender_id); ?></td>
                    <td><?php echo $results3->budget; ?></td>
                    <td><?php
                        if ($results3->status_code == 3) {
                            echo "Cancel";
                        }
                        ?></td>
                    <td><?php if ($results3->status_code == 3) { ?> 
                            <button >Done</button> 
                        <?php } ?> <a href="<?php echo site_url(); ?>/commmission-chat?ref_id=<?php echo $results2->ref_id; ?>&author_id=<?php echo $results3->receiver_id; ?>">chat </a></td>


                </tr>
                <?php
                ++$i;
            }
        } else {

            echo "<tr><td colspan='6'>No record found</td></tr>";
        }
        ?>
    </table>
</div>