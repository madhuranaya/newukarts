<?php
   /**
    * Template Name: Register buyer 	
    *
    * @package Marketify
    */
    get_header(); 
	do_action( 'marketify_entry_before' );
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	
<div class="artist-main-img-log">
  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/03/collect-page.jpg">
</div>  
<div class="wpb_wrapper">
<h2 class="vc_custom_heading hed">Add some UK Artists to your collection</h2>
</div>

 <div class="container">
 <div class="apply today">
 
 <div class="artist_platform">
 
  <div class="artist_platform">
  <p style="text-align: center;">
  Rewarding artists for the work they create is at the heart of UK Arts. Our carefully selected artists receive 95% of the sale of their artwork giving them more financial freedom to create.
  </p>
  </div>
  
  <div class="artist_platform">
 <p class="believe_con">
 <strong class="why-buy" style="font-size:18px;">
 Why buy from UK Arts</strong></br>
 <strong>– A broaden selection of original art</strong></br>
 <strong>– Textile art and craft supported</strong></br>
 <strong>– Commission your favourite artists</strong></br>
 <strong>– Regular new and up and coming artist alerts</strong></br>
 <strong>–  Secure payments and returns</strong></br>
 
 </p>
 <div class="artist_platform">
  <p style="text-align: center;">
  <strong class="why-buy">Fill out the simple form below and start collecting today </strong>
  </p>
  </div>
 </div>
   <?php  if(isset($_GET['m']) && $_GET['m']=="successful" ){ ?><div class="center"><center> User registered successfully.</center></div><?php } ?>
 
	<div class="custome-register-form">
	<!--<div class="rmheader">Register with us by filling out the form below.</div> -->
	<?php if ( !is_user_logged_in() ) {
		?>
   
      <form id="register-form" method="post" name="myForm" enctype='multipart/form-data'>
         <input name="vendor-buyer" type="hidden" id="action" value="role_buyer" />
        <div class="row">
        	<div class="col-sm-6">
		 <input id="fname" type="text"  name="uname" placeholder="First name*" required>
		 <span id="fname_errors"></span>
		</div>
		<div class="col-sm-6">
			<input id="surname" type="text"  name="surname" placeholder="Surname*" required>
		 <span id="surname_errors"></span>

	     	</div>
     	</div>
      <div class="row">
      	 <div class="col-sm-6">
      	  <input id="email" type="email" name="uemail" placeholder="Email*" onblur="return validateForm();" required >
		  <span id="email_errors"></span>	
      	  <span id="email_error"></span>	
      	 </div>
		 <!--script>
			$(document).ready(function(){
				$("#fname").change(function(){
					var name = $(this).val();
					$("#pass").val(name+'@123');
				});
			});
		 </script-->
		 <!--div class="col-sm-6">
      	  <input id="pass" type="hidden" name="pass" >	
      	 </div-->
      	 <div class="col-sm-6">
      	  <input type="text" name="contact_no" id="contact_no" placeholder="preferrs contact number *" required minlength="0" maxlength="15"> 
           <span id="contact_no_errors"></span>
		   
      	 </div>
      	</div>  
          <script>
		$(document).ready(function () {
  
			  $("#contact_no").keypress(function (e) {
				 
				 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					
					$("#errmsg").html("Digits Only").show().fadeOut("slow");
						   return false;
				}
			   });
			   
		});
	</script>
		<div class="row">
			<div class="col-sm-6">
		      
		  <textarea  id="description" name="description" placeholder="International addresses *" required maxlength="250"></textarea>  
		    <span id="description_errors"></span>
		</div>
		<div class="col-sm-6"><p> <input type="text" id="datepicker" placeholder="dd/mm/yy"></p></div> 
	</div>   
		<?php //echo do_shortcode('[avatar_upload user="admin"]');?>
		
	<div class="rmrow">
	<input type="checkbox" name="checkbox"  value="1" id="action" required > I agree to UK Arts <a href="<?php echo get_site_url(); ?>/terms-conditions">Terms and Conditions for Artist</a></br>
	
	<input type="checkbox" name="checkbox"  value="1" id="action" required > Tick to receive Art  <a href="<?php echo get_site_url(); ?>">Collection eNewletter, latest artist news and offers</a></br>
	<span id="action_errors"></span></br>
	
        <input id="myModal" type="button" value="Submit"  />
		</div>
		
    </form>
	  
	
	<div id="myModal2" class="modal"> 

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
	<center><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/06/cropped-logo.png"></center>
    <p>Thank you for your application to join as an art buyer. Plese check your email. 

</p>
	<div class="sub_miy"><span class="clsddd btn">Go</span></div>
  </div>

</div>
	<!--onclick="show_alert();"  -->
<script>
$("#myModal").click(function(){
	var ss=$('input[type=checkbox]').prop('checked');
	var  fname=$("#fname").val();
	var email=$("#email").val();
	var surname=$("#surname").val();
	var contact_no =$("#contact_no").val();
	var des=$("#description").val();
   // var inp = document.getElementById('avatar_user');
   // var inp2 = document.getElementById('user_certificate');
	var modal = document.getElementById('myModal2');
	var span = document.getElementsByClassName("close")[0];
    var er='';
     
	if(fname==""){

    er = $("#fname_errors").html("Required field");
	}
	if(surname == ""){
    er = $("#surname_errors").html("Required field");
	}
	if(email==""){
    er = $("#email_errors").html("Required field");
	} 
	if(contact_no == ""){
     er = $("#contact_no_errors").html("Required Field");
	} 
	if(des == ""){
    er = $("#description_errors").html("Required field");
	} 
	if(ss == false){
     er = $("#action_errors").html("Required field");
	}
	 
	if(er === ''){
		
		
		 //$("#myModal2").show();
		var x = $("#email").val();
	//var p = $("#pass").val();
	var f = $("#fname").val();
	var s = $("#surname").val();
	//alert(x);
	$.ajax({
      type: "POST",
       data:"fname="+f+"&sname="+s+"&email="+x+"&action8=email_sub8",
       url:'<?php  echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
      success: function(data) {
                    	alert(data);
						window.location.href = 'http://beta.uk-arts.com/uk-artists-collection/';
					}
      
    });
		
	}
	else {
	return false;
	} 
   

});
$(".clsddd").click(function(){
	$("#register-form").submit();
});

function show_alert() {
   alert("Thank you for your application to join UK Arts. Our team will review your application over the next 1-2 weeks, and if you are successful you will be sent a link to activate your account and complete your profile.");
}
function validateForm() {
    var x = $("#email").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
	 
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        $("#email_error").html("Not a valid e-mail address");
        return false;
    }else{
    	$("#email_error").html("");
    	//alert(email);
    	//alert(x);
	     $.ajax({
                   type:'POST', 
                   data:"email="+x+"&action3=email_sub",
                   url:'<?php  echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                    success: function (data){
                      //$(".chat_message").html(result);
                      //alert(data);
                    if(data=="yes"){
                    	//alert(data);
						$("#myModal2").hide();
						$('#myModal').prop('disabled', true);
                    	//$("#email_error").html(" Email already Exists");
						$("#email_errors").html("<p style='color:red'> Email already Exists</p>");
						
					}else{
						//alert("gcfvfd");
						$('#myModal').prop('disabled', false);
						//$("#email_errors").html("dddddd");
					}
					 

                        //$('#dddd').html(result);
                    },
			error: function(error){
		//alert("dd");
		}  
                }); 

    }
}

</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
		dateFormat: 'dd/mm/yy'
	});
  } );
  </script>

	
<?php } 
?>  
	 
	
	




	
<?php //add_action('init','create_account'); ?>
</div>	

	 </div>
	 </div>
	
	<div class="container">
		<div id="content" class="site-content row">

			<div id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; ?>

					<?php do_action( 'marketify_loop_after' ); ?>

				<?php else : ?>

					<?php get_template_part( 'no-results', 'index' ); ?>

				<?php endif; ?>
	        </main><!-- #main -->
			</div> <!-- #primary -->

		</div>
	</div>
	
	<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>

</div>
<?php get_footer(); ?>