<?php
/**
 * Template Name: Layout: buyer approved 
 *
 * @package Marketify
 */

get_header(); ?>
<?php do_action( 'marketify_entry_before' ); ?>
<?php
   $user_id= $_GET['user_id'];
   get_user_meta( $user_id,'pw_user_status' ,true);
   if(get_user_meta( $user_id,'pw_user_status' ,true)=="pending"){
 $user_data= update_user_meta( $user_id,'pw_user_status' ,'approved');
		
     echo "<p class='app_center'>User Approved Successfully. ";
	    echo '<a href="'.get_site_url().'/login/">Click here to login</a> </p>';
   }else{  
	   echo "<p class='app_center'>User Already Approved. ";
	    echo '<a href="'.get_site_url().'/login/">Click here to login</a> </p>';
   }
   

  ?>
<style>
p.app_center {
    text-align: center;
    margin-top: 140px;
}
</style>		
<?php get_footer(); ?>