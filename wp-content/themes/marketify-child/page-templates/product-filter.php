<?php
/**
 * Template Name: Layout: Product-filter
 *
 * @package Marketify
 */

get_header(); ?>
 
	<?php do_action( 'marketify_entry_before' ); 
	?>
<div class="filter-painting">
<div class="container"><?php //echo do_shortcode('[searchandfilter id="839"]'); ?></div>
</div>
<div class="container src-ak">

<?php //echo do_shortcode('[searchandfilter id="839" show="results"]');


  $current_url = $_SERVER['REQUEST_URI'];
  $current_cat_get= substr($current_url,27,4);    
if($current_cat_get=="pain")
{ echo "painting"; echo do_shortcode('[searchandfilter id="4015"]'); }
elseif($current_cat_get=="draw")  { echo "Drawing"; echo do_shortcode('[searchandfilter id="4018" show="results"]'); } 
elseif($current_cat_get=="scul")  { echo "sculpture"; echo do_shortcode('[searchandfilter id="4020" show="results"]'); } 
elseif($current_cat_get=="phot")  { echo "photographs"; echo do_shortcode('[searchandfilter id="4021" show="results"]'); } 
elseif($current_cat_get=="coll")  { echo "collage"; echo do_shortcode('[searchandfilter id="4023" show="results"]'); } 
elseif($current_cat_get=="prin")  { echo "prints"; echo do_shortcode('[searchandfilter id="4025" show="results"]'); } 
elseif($current_cat_get=="craf")  { echo "craft"; echo do_shortcode('[searchandfilter id="4029" show="results"]'); } 
elseif($current_cat_get=="digi")  { echo "digital-art"; echo do_shortcode('[searchandfilter id="4030" show="results"]'); } 
else { echo "other"; }

 ?>

</div>

			
	<div class="container">
		<div id="content" class="site-content row">

			<div id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<?php //if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php //while ( have_posts() ) : the_post(); ?>

						<?php //get_template_part( 'content', 'page' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							//if ( comments_open() || '0' != get_comments_number() )
							//	comments_template();
						?>

					<?php// endwhile; ?>

					<?php //do_action( 'marketify_loop_after' ); ?>

				<?php //else : ?>

					<?php //get_template_part( 'no-results', 'index' ); ?>

				<?php //endif; ?>
	</main><!-- #main -->
			</div><!-- #primary -->

		</div>
	</div>
<?php get_footer(); ?>
