<?php
/**
 * Template Name: Layout: Dummy event templte
 *
 * @package Marketify
 */

get_header(); ?>

	<?php do_action( 'marketify_entry_before' ); ?>
	</br>
	</br>
	</br>
	</br>
	</br>
<div class="container">
	<?php 
/**
 * Adding via filter or you can directly add in template file
 */
add_action( 'event_manager_event_filters_search_events_end', 'filter_by_country_field' );
function filter_by_country_field() {
	?>
	<div class="search_event_types">
		<label for="search_event_types"><?php _e( 'Country', 'event_manager' ); ?></label>
		<input type="text" >
		<select name="filter_by_country" class="event-manager-filter">
			<option value=""><?php _e( 'Select country', 'event_manager' ); ?></option>
			<option value="de"><?php _e( 'Germany', 'event_manager' ); ?></option>
			<option value="india"><?php _e( 'India', 'event_manager' ); ?></option>
			<option value="us"><?php _e( 'USA', 'event_manager' ); ?></option>
		</select>
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the event search query
 */
add_filter( 'event_manager_get_listings', 'filter_by_country_field_query_args', 10, 2 );
function filter_by_country_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by country
		if ( ! empty( $form_data['filter_by_country'] ) ) {
			$event_country = sanitize_text_field( $form_data['filter_by_country'] );
			$query_args['meta_query'][] = array(
						'key'     => '_event_location',
						'value'   => $event_country,
						);
		}
	}
	return $query_args;
} ?>
	<?php echo do_shortcode(' [events]'); ?>
</div>	
<?php get_footer(); ?>