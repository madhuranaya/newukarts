<?php
/**
 * Template Name: artist Reviews table
 *
 * @package Marketify
 */
require_once($_SERVER['DOCUMENT_ROOT'] . $folder . '/wp-config.php');
get_header(); ?>
	<?php do_action( 'marketify_entry_before' ); ?>
	<div class="container">
		<div id="content" class="site-content row two_table">

			<div id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<form name="artist-inert-table" method="post">
<div class="two-section-table">
<div class="two-section-tableinner">
  <?php
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		 //$parts = parse_url($actual_link);
		// echo "<pre>";
		// print_r($parts);
		parse_str(parse_url($actual_link)['query'], $params);
		
		//echo $params['product_id'];
   $current_user = wp_get_current_user();
   //print_r($current_user);
    $currentid=$current_user->ID;
   //echo $currentid;
  ?>

  <select name="sellercomm">
      <option value=""  selected>Seller Communication</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
</select></div>
  <div class="two-section-tableinne">
    <div class="two-section-tableinner">
  <select name="deliverynpaint">
    <option value="" selected>Delivery and Packing</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
</select></div>
   </div>
  </div>
  
<div class="two-section-table">
<div class="two-section-tableinner">
  <select name="listingacc">    
  <option value="" selected>Listing Accuracy</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
</select>
  </div>
  <div class="two-section-tableinner">
  <input type="hidden" name="username" value="<?php echo $currentid;?>"/>
</div>
<div class="two-section-tableinner">
  <input type="hidden" name="p_id" value="<?php echo $params['product_id']; ?>" />
</div>
<div class="two-section-tableinner">
  <input type="text" name="discription" placeholder="Description" />
  </div>
  <?php  $today = date("d/m/y"); ?>
  <div class="two-section-tableinner">
  <input type="hidden" name="date" value="<?php echo $today; ?>" />
</div>

</div>
    
<div class="one-section-table">
  <?php
  
	// finds the last URL segment  
	  
  
  $user = get_user_by('ID', $_GET['user_id'] );
  $args = array(
    'role'    => 'shop_vendor',
    'orderby' => 'id'
	
);
$users = get_users( $args );
//$wp_user_search = $wpdb->get_results("SELECT ID, display_name FROM $wpdb->users ORDER BY ID");
?>
<select name="artistlist"><?php
       foreach ( $users as $userid ) {
	$user_id       = (int) $userid->ID;
	?>
  <option value="<?php echo  $userid->ID; ?>"><?php echo  $userid->display_name; ?></option><?php
} ?></select>
</div>
<div class="one-section-table">
   <input type="submit" name="submit" value="submit" />
</div>
</form>
				</main><!-- #main -->
			</div><!-- #primary -->

		</div>
	</div>
	
	<?php 
	if (isset($_POST["submit"])) {
    //echo "Yes";
 $sellercomm = $_POST['sellercomm'];
 $deliverynpaint = $_POST['deliverynpaint'];
 $listingacc = $_POST['listingacc'];
 $average_starss= ($sellercomm + $deliverynpaint + $listingacc)/3;
 $average_stars= round($average_starss);
 $username = $_POST['username'];
 $discription = $_POST['discription'];
 $date = $_POST['date'];    
 $artistlist = $_POST['artistlist'];   
 $pro_id = $_POST['p_id'];   


//$dbhandle = mysql_connect('localhost', 'ukarts_beta', 'C$aU6Y!pw@bp');

 //if($dbhandle){
 //echo "Connected to MySQL<br>";
 //}
 //else{
 //	echo "no";
 //}
//$abc=mysql_select_db('ukarts_beta');
// if($abc){
// echo "Select to MySQL<br>"; 
//}
 //else{
 	//echo "no";
 //}

 global $wpdb;
 //$wpdb->query($sql);
$sql = $wpdb->prepare("INSERT INTO `artistreviews`(`sellercomm`,`deliverynpaint`,`listingacc`,`username`,`discription`,`date`,`artistlist`,`artistid`,`product_id`) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)", $sellercomm,$deliverynpaint,$listingacc,$username,$discription,$date,$artistlist,$average_stars,$pro_id);
$wpdb->query($sql);

  
}else{  
   
}
	
	?>
	
<style>
.two-section-table {
    width: 100%;
    float: left;
    margin-right: 2%;
    /* margin-bottom: 2%; */
}
.one-section-table {
    width: 100%;
    float: left;
}
.two-section-tableinner {
    width: 48%;
    float: left;
    margin-right: 2%;
}
select {
    width: 98%;
    padding: 18px 0 0 0;
}
</style>
<?php get_footer(); ?>
