<?php
/**
 * Template Name: Layout: Competitions page
 *
 * @package Marketify
 */
get_header();
?>

<?php do_action('marketify_entry_before'); ?>

<div class="container-main-cons">
    <div class="container">



        <div id="content" class="site-content row secouyy">

            <div id="primary" class="content-area col-sm-12">
                <main id="main" class="site-main" role="main">

                    <?php if (have_posts()) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while (have_posts()) : the_post(); ?>

                            <?php get_template_part('content', 'page'); ?>

                            <?php
                            // If comments are open or we have at least one comment, load up the comment template
                            if (comments_open() || '0' != get_comments_number())
                                comments_template();
                            ?>

                        <?php endwhile; ?>

                        <?php do_action('marketify_loop_after'); ?>

                    <?php else : ?>

                        <?php get_template_part('no-results', 'index'); ?>

                    <?php endif; ?>
                </main><!-- #main -->
            </div><!-- #primary -->

        </div>
    </div>
</div>
<?php get_footer(); ?>
<style type="text/css">.page-numbers {
        //display: none !important;
    }
    .pagination-compition.demo li{
        float:left;
    }
    .next {
        display: block !important;
        width: 10%;
    }
    .prev {
        display: block !important;
        width: 10%;
    }</style> 