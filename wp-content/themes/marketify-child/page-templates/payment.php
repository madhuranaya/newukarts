<?php
   /**
    * Template Name: payment page
    *
    * @package Marketify
    */
    get_header(); 
	do_action( 'marketify_entry_before' ); ?>
<div class="current-username"><h2>Your registration complete pay now</h2></div>
 <div class="container">
 

<form target="" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" style="text-align: center;">
<input type="hidden" name="cmd" value="_xclick-subscriptions"><input type="hidden" name="business" value="munishgup-facilitator@gmail.com"><input type="hidden" name="lc" value="EN_US">
<input type="hidden" name="currency_code" value="USD"><input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="no_note" value="1"><input type="hidden" name="custom" value="2250">
<input type="hidden" name="return" value="http://beta.uk-arts.com/payment-success/">
<input type="hidden" name="bn" value="WPPlugin_SP">
<input type="hidden" name="cancel_return" value="http://beta.uk-arts.com/payment-page/">
<input type="hidden" name="item_name" value="Register">
<input type="hidden" name="item_number" value="<?php echo $_GET['user_name']; ?>">
<input type="hidden" name="notify_url" value="http://beta.uk-arts.com/index.php?wpeppsub-listener=IPN">
<input type="hidden" name="a3" value="5.00">
<input type="hidden" name="p3" value="1">
<input type="hidden" name="t3" value="M">
<input type="hidden" name="src" value="1">
<input class="wpeppsub_paypalbuttonimage" type="image" src="http://beta.uk-arts.com/wp-content/uploads/2018/03/images.jpg" border="0" name="submit" alt="Make your payments with PayPal. It is free, secure, effective." style="border: none;">
<img alt="" border="0" style="border:none;display:none;" src="https://www.paypal.com/EN_US/i/scr/pixel.gif" width="1" height="1"></form>

	 </div>
	
	
<?php get_footer(); ?>