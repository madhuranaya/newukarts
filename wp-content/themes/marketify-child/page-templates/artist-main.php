<?php
/**
 * Template Name: Layout: Artist main
 *
 * @package Marketify
 */
get_header();
?>
<?php do_action('marketify_entry_before');
?>



<style>
    * {
        box-sizing: border-box;
    }

    .heading {
        font-size: 25px;
        margin-right: 25px;
    }

    .fa {
        font-size: 25px;
    }

    .checked {
        color: orange;
    }

    /* Three column layout */
    .side {
        float: left;
        margin-top: 6px;
        width: 6%;
    }

    .middle {
        float: left;
        margin-top: 10px;
        width: 31%;
    }

    /* Place text to the right */
    .right {
        float: left;
        margin-left: 8px;
        text-align: left;
        width: 9%;
    }
    .first_line {
        float: left;
        width: 100%;
    }
    .middle {
        float: left;
        margin-top: 10px;
        width: 28%;
    }
    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .review-inner-part {
        float: left;
        width: 30%;
    }
    .ght {
        width: auto !important;
        float: left;
        margin-right: 10px;
    }
    .review-inner-part > span {
        max-width: 190px;
    }
    .review-overall {
        font-size: 18px;
    }
    /* The bar container */
    .bar-container {
        width: 100%;
        background-color: #f1f1f1;
        text-align: center;
        color: white;
    }
    .review-inner-part.macrto {
        float: right;
        width: 22%;
        text-align: left;
    }
    #review .review-inner-main-right {
        width: 56%;
    }
    #review .review-inner-main-left {
        float: left;
        width: 40%;
    }
    #review .review-inner-part > h2 {

        margin-top: 5px;
    }
    #review .review-inner-part > h2 {
        text-transform: capitalize;
    }
    .review-inner-main-right .review-inner-part > span {
        color: #758286;
    }
    .review-inner-main-right .review-inner-part {
        float: left;
        width: 100%;
    }
    .review-inner-main-left .review-inner-part {
        float: left;
        width: 100%;
    }
    /* Individual bars */
    .bar-5 {
        background-color: #e4e000;
        height: 18px;
        width: 90%;
    }  
    .bar-4 {width: 30%; height: 18px; background-color: #e4e000;}
    .bar-3 {width: 10%; height: 18px; background-color: #e4e000;}
    .bar-2 {width: 4%; height: 18px; background-color: #e4e000;}
    .bar-1 {width: 15%; height: 18px; background-color: #e4e000;}
    .visit-or-website {
        display: none;
    }
    .main-cls-both {
        display: none;
    }
    .manage-product {
        background-color: #dedc00;
        float: right;
        text-align: center !important;
    }

    #blog_social_3 {
        display: none;
    }
</style>
<?php

function to_get_thetitlle($id) {
    global $wpdb;
    $resul = $wpdb->get_row("select * from " . $wpdb->prefix . "commission where ref_id=" . $id);
    return $resul->name;
}

function to_pay_on_paypal($refrence_id, $amount, $budget) {

    $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
    $paypalID = 'munishgup-facilitator@gmail.com'; //Business Email 
    ?>
    <form action="<?php echo $paypalURL; ?>" method="post">

        <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">

        <!-- Specify a Buy Now button. -->
        <input type="hidden" name="cmd" value="_xclick">

        <input type="hidden" name="item_name" value="<?php echo $refrence_id; ?> ">
        <input type="hidden" name="item_number" value="<?php echo $budget . '_' . $amount . '_final'; ?>">
        <input type="hidden" name="amount" value="<?php echo $amount; ?>">
        <input type="hidden" name="currency_code" value="USD">

        <!-- Specify URLs -->
        <input type='hidden' name='cancel_return' value='<?php echo site_url(); ?>/ukarts/'>
        <input type='hidden' name='return' value='<?php echo site_url(); ?>/ukarts/confirm-payment'>        
        <input type="submit" name="hire" value="Complete">
    </form>  
<?php } ?>
<?php
$user_id = "";
$user_name = "";
$current_user = wp_get_current_user();

// echo $current_user ->ID;
if (!empty($_GET['user_id'])) {
    $user_id = $_GET['user_id'];
    $user2 = get_user_by('ID', $_GET['user_id']);

    $user_name = $user2->display_name;
} else {
    $user_id = $current_user->ID;
    $user_name = $current_user->user_firstname;
    $user_last_name = $current_user->user_lastname;
}
//echo $user_id;
//echo $user_name;
if (is_user_logged_in()) {

// $current_user = wp_get_current_user();
    if (($current_user instanceof WP_User)) {
        ?>
        <div class="artist-main-img-log demo">

            <?php
            if ($_GET['user_id']) {
// $jjde= get_current_user_id();
                $bbb = $_GET['user_id'];
                $args = array('post_type' => 'about_us', 'author' => $bbb, 'posts_per_page' => 1);
                query_posts($args);
                if (query_posts($args)) {
                    while (have_posts()) : the_post();
                        if (!has_post_thumbnail()) {
                            ?>
                            <img width="1024" height="768" src="/wp-content/uploads/event-manager-uploads/organizer_logo/2019/01/event.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image">

                            <?php
                        } else {
                            the_post_thumbnail();
                            ?>

                            <?php
                        }
                    endwhile;
                } else {
                    ?>
                    <img width="1024" height="768" src="/wp-content/uploads/event-manager-uploads/organizer_logo/2019/01/event.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image">
                    <?php
                }
                /*
                  else {

                  echo "string";
                  }
                 */
            } else {
                $bbb = $_GET['user_id'];
                $args = array('post_type' => 'about_us', 'author' => $_GET['user_id'], 'posts_per_page' => 1);
                query_posts($args);
                if (query_posts($args)) {
// the Loop
                    while (have_posts()) : the_post();
//Do your stuff  
//You can access your feature image like this:
                        get_current_user_id();
                        $bbb = $_GET['user_id'];
                        if (has_post_thumbnail()) {
                            echo the_post_thumbnail();
                        } else {
                            ?>
                            <img width="1024" height="768" src="/wp-content/uploads/event-manager-uploads/organizer_logo/2019/01/event.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image">
                            <?php
                        }

//echo $id = get_the_ID();
                    endwhile;
                } else {
                    ?>
                    <img width="1024" height="768" src="/wp-content/uploads/event-manager-uploads/organizer_logo/2019/01/event.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image">
                    <?php
                }
            }
            ?>
        </div>
        <div class="current-username demo">

            <h2> 

                <?
//echo   the_title(); 
//$current_user = wp_get_current_user();
                /**
                 * @example Safe usage:
                 * $current_user = wp_get_current_user();
                 * if ( ! $current_user->exists() ) {
                 *     return;
                 * }
                 */
// echo $user_id;
                global $wpdb;
                $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'first_name'");
                $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'last_name'");
                echo $findu[0]->meta_value;
                echo " ";
                echo $findu1[0]->meta_value;
//echo "<pre>";
//print_r($findu);
//echo $user_name .' '. $user_last_name; 
// echo  $current_user->user_nicename;
                ?> 

            </h2>
            <p><?php
// echo get_the_ID();
                $des = get_post_meta(get_the_ID(), 'artist_statement', true);
                echo $shortexcerpt = wp_trim_words($des, $num_words = 250, $more = '');

//   endwhile;    
                ?></p>
        </div>
        <?php
//  echo get_avatar( $current_user->user_email, 32 );
    }
} else {
    ?>
    <div class="artist-main-img-log demo1">
        <?php
        if ($_GET['user_id']) {

            $bbb = $_GET['user_id'];
            $args = array('post_type' => 'about_us', 'author' => $bbb, 'posts_per_page' => 1);
            query_posts($args);

            while (have_posts()) : the_post();
                if (has_post_thumbnail()) {
//echo $id = get_the_ID();
                    the_post_thumbnail();
                }
            endwhile;
            /*
              else {

              echo "string";
              }
             */
        } else {
            $bbb = $_GET['user_id'];
            $args = array('post_type' => 'about_us', 'author' => $bbb, 'posts_per_page' => 1);
            query_posts($args);

// the Loop
            while (have_posts()) : the_post();
//Do your stuff  
//You can access your feature image like this:
                echo the_post_thumbnail();
//echo $id = get_the_ID();
            endwhile;
        }
        ?>
    </div>
    <div class="current-username">
        <h2> 
            <?php
            global $wpdb;
            $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'first_name'");
            $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'last_name'");
            echo $findu[0]->meta_value;
            echo " ";
            echo $findu1[0]->meta_value;

//echo $user_name;  
            ?> <?php //echo $user_last_name;  ?>
            <?php
            $author_queryb = array('post_type' => 'about_us',
                'posts_per_page' => '1', 'author' => $user_id);
            $author_postsb = new WP_Query($author_queryb);
            while ($author_postsb->have_posts()) : $author_postsb->the_post();
//					the_title(); 
// get_post_meta( get_the_ID(), 'surname', true ); 
                ?>

            </h2>
            <p><?php
// echo get_the_ID();
                $des = get_post_meta(get_the_ID(), 'artist_statement', true);
                echo $shortexcerpt = wp_trim_words($des, $num_words = 250, $more = '');

            endwhile;
            ?></p>
    </div>
    <?php
//  echo get_avatar( $current_user->user_email, 32 );
}
?>   
<div class="button-login-user">
    <div class="container">
        <div class="tab">
            <?php
            if (is_user_logged_in()) {
// $current_user = wp_get_current_user();
                if (($current_user instanceof WP_User)) {
                    $current_user_name = $user_name;

                    $user_id_get_id = get_current_user_id();
                    $user_meta = get_userdata($_GET['user_id']);
                    $Current_user_role = $user_meta->roles[0];
//print_r($user_roles);
                    ?>

                    <a class="tablinks  artist67 active" href="<?php echo get_permalink(3344); ?>?user_id=<?php echo $_GET['user_id'] ?>">Artworks</a>
                    <button class="tablinks like-button">
                        <span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/dil.png" /></span>

                        <?php
                        global $wp_query;
                        $curauth = $wp_query->get_queried_object();
                        $post_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = '" . $_GET['user_id'] . "' AND post_type = 'edd_wish_list' AND post_status = 'publish'");
                        ?>
                        (<?php echo $post_count; ?>)

                        <?php
//  echo get_avatar( $current_user->user_email, 32 );
                    }
                } else if (!is_user_logged_in()) {

                    $user = get_user_by('ID', $_GET['user_id']);
                    $user_id_get = $_GET['user_id'];
                    ?>
                    <button class="tablinks" onclick="openCity(event, 'artist')">Artworks</button>

                    <button class="tablinks like-button">
                        <span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/dil.png" /></span> 
                        <?php
                        global $wp_query;
                        $curauth = $wp_query->get_queried_object();
                        $post_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = '" . $user_id_get . "' AND post_type = 'edd_wish_list' AND post_status = 'publish'");
                        ?>
                        (<?php echo $post_count; ?>)


                    <?php } ?>
                    <button class="tablinks about-us-button <?php
                    if (isset($_GET['tab']) && $_GET['tab'] == "about") {
                        echo 'active';
                    }
                    ?>" onclick="openCity(event, 'aboutme')">About Me</button>
                    <button class="tablinks about-us-button aboutmesocial <?php
                    if (isset($_GET['tab']) && $_GET['tab'] == "account") {
                        echo 'active';
                    }
                    ?> " onclick="openCity(event, 'aboutmesocial')">Social icons</button>
                            <?php if (!is_user_logged_in()) { ?>
                        <button class="tablinks myblog <?php
                        if (isset($_GET['tab']) && $_GET['tab'] == "blog") {
                            echo 'active';
                        }
                        ?>" onclick="openCity(event, 'myblog')">My blog</button>
                            <?php } else { ?>
                        <a href="http://beta.uk-arts.com/artist-main-2/?tab=blog" class="tablinks myblog blog_1 ">My blog</a>

                    <?php } ?>
                    <style>
                        .tab a {
                            background: #f2f2f2 none repeat scroll 0 0 !important;
                            color: #000;
                            font-size: 20px !important;
                            font-weight: normal;
                            text-transform: capitalize;
                        }
                        .tab a {
                            padding: 10px 20px !important;
                        }
                        .tab a.active {
                            background-color: #dedc00 !important;
                        }
                    </style>
                    <?php
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//$parts = parse_url($actual_link);
//echo "<pre>";
//print_r($parts);
                    parse_str(parse_url($actual_link)['query'], $params);

                    if ($params['tab'] == 'blog') {
                        ?>
                        <script>
                            $(document).ready(function () {
                                $('.artist67').removeClass('active');
                                $('.blog_1').addClass('active');
                                // $('.blog_1').click(function(){
                                // $('.artist67').removeClass('active');
                                // $('.blog_1').addClass('active');
                                // });
                            });
                        </script>

                        <?php
                    } if (is_user_logged_in()) {
//  $current_user = wp_get_current_user();
                        if (($current_user instanceof WP_User)) {
                            ?>


                            <button class="tablinks Reviews"  onclick="openCity(event, 'review')">Reviews
                                <?php
                                global $wpdb;
//print_r($current_user);
//$current_user = $user_name;
                                $current_user_name = $user_name;
                                $avgsell = $wpdb->get_results("SELECT COUNT(artistid) as artsellered FROM artistreviews where artistlist= '$user_id'");
//print_r($avgsell);  
                                $artsellered = $avgsell[0]->artsellered;
                                if (!$artsellered == "0") {
                                    ?>(<?php echo $artsellered; ?>)<?php
                                } else {
                                    
                                }
                                ?>
                            </button>
                            <?php
                            if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                
                            } else {
                                ?>

                                <button class="tablinks commission" onclick="openCity(event, 'commission')">Commission</button>

                                <button class="tablinks bank" onclick="openCity(event, 'bank')">Bank         </button>
                                <?php
                            }
                        }
                    } else if (!is_user_logged_in()) {
                        $user = get_user_by('ID', $_GET['user_id']);
                        ?>
                        <button class="tablinks review" onclick="openCity(event, 'review')">Reviews
                            <?php
                            global $wpdb;
//print_r($current_user);
//$current_user = $user_name;  
                            $current_user_name = $user_name;
                            $avgsell = $wpdb->get_results("SELECT COUNT(artistid) as artsellered FROM artistreviews where artistlist= '$user_id'");
//print_r($avgsell);  
                            $artsellered = $avgsell[0]->artsellered;
                            if (!$artsellered == "0") {
                                ?>(<?php echo $artsellered; ?>)<?php
                            } else {
                                
                            }
                            ?> </button>
                    <?php } ?>

                    <!--button class="tablinks contact" onclick="openCity(event, 'contact')">Contact</button-->
                    <button type="button" class="btn btn-info btn-lg tablinks contact" data-toggle="modal" data-target="#myModal12">Contact</button>
                    <button class="tablinks events <?php
                    if (isset($_GET['tab']) && $_GET['tab'] == "event") {
                        echo 'active';
                    }
                    ?>" onclick="openCity(event, 'events')">Events         </button>

                    </div>


                    </div>
                    </div>  
                    <div class="button-login-user11"> 
                        <div class="container">
                            <div class="download-current-user">
                                <?php if (is_user_logged_in()) { ?>
                                    <div id="bank" class="tabcontent">
                                        <div class="container">
                                            <?php
                                            $get_curent_user = get_current_user_id();
                                            global $wpdb;
                                            $bamkdetail_sel = $wpdb->get_results("SELECT * FROM bankdetail where user_id=$get_curent_user");
//print_r($bamkdetail_sel);
                                            foreach ($bamkdetail_sel as $bamkdetail_seled) {
                                                $acc_secl = $bamkdetail_seled->accountno;
                                                $acc_sort = $bamkdetail_seled->sortcode;
                                            }
                                            ?> 
                                            <?php
                                            if (is_user_logged_in()) {
                                                if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                    ?>
                                                    <h2>Bank Details</h2>
                                                    <form name="bankdetail" method="post" action="<?php echo site_url(); ?>/bank-detail/">

                                                        <div class="bank-detail-main">
                                                            <div class="accoun-nomr"> Account Number 
                                                                <div class="echo-no-class"><?php echo $acc_secl; ?></div></div>

                                                            <br>
                                                            <div class="sort-code"> Sort Code <div class="echo-no-class"> <?php echo $acc_sort; ?>
                                                                </div> 
                                                            </div>
                                                        </div>


                                                    </form>
                                                <?php } else { ?><h2>Bank Details</h2>
                                                    <form name="bankdetail" method="post" action="<?php echo site_url(); ?>/bank-detail/">
                                                        <input type="hidden" name="accountno" value="<?php echo $acc_secl; ?>" placeholder="Please Enter Account Number" />
                                                        <input type="text" name="accountnoed" value="<?php echo $acc_secl; ?>" maxlength="17" placeholder="Please Enter Account Number" id="phone_num" />
                                                        <input type="text" name="sortcode"  id="sortcode" value="<?php echo $acc_sort; ?>"  maxlength="7" placeholder="Please Enter Sort Code" />  
                                                        <input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>" />  
                                                        <input type="hidden" name="user_name" value="Admin" />  
                                                        <input type="submit" name="submit" value="submit"  />
                                                    </form>
                                                    <script>
                                                        $(document).ready(function () {

                                                            $("#phone_num, #sortcode").keypress(function (e) {

                                                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

                                                                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                                                                    return false;
                                                                }
                                                            });

                                                        });
                                                    </script>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="download-current-user">
                                <?php if (!is_user_logged_in()) { ?>
                                    <div id="bank" class="tabcontent">


                                        <?php echo "Login First"; ?>
                                    </div>
                                <?php } ?> </div>




                            <?php
                            if (is_user_logged_in()) {
//$current_user = wp_get_current_user();
                                if (($current_user instanceof WP_User)) {

//  echo get_avatar( $current_user->user_email, 32 );
                                }
                            } else if (!is_user_logged_in()) {
                                $user = get_user_by('ID', $_GET['user_id']);
                                ?>

                            <?php }
                            ?>
                            <?php if (!isset($_GET['user_id']) || get_current_user_id() == $_GET['user_id']) {
                                ?>
                                <!--commision start-->
                                <div id="commission" class="tabcontent">
                                    <?php //print_r($current_user);   ?>
                                    <div class="third-section-authorname">Commission <?php echo $user_name; ?>
                                        <a href="#">How Does it work? </a>
                                    </div>
                                    <?php

                                    function to_get_user_name($user_id1) {
                                        $user = get_user_by('ID', $user_id1);
                                        return $user->display_name;
                                        ;
                                    }

                                    function to_show_payment_status($ref_id, $stt) {
                                        global $wpdb;
                                        $resul = $wpdb->get_row("select * from " . $wpdb->prefix . "commission_payment where refrence_id=" . $ref_id);
                                        return $resul->$stt;
                                    }

                                    $user = wp_get_current_user();
// print_r($user);
                                    $field_name = "receiver_id";

                                    if (!in_array('shop_vendor', (array) $user->roles)) {
                                        include("contracter_profile.php");
                                    } else {
// echo "sss";
                                        include("shop_vender_profile.php");
                                    }
                                    ?>
                                    <script>
                                        $("input[name='optionsRadiosinline']").click(function () {
                                            var rad_val = $(this).val();
                                            if (rad_val == "pending") {
                                                $(".pending_pro").show();
                                                $(".on_going").hide();
                                                $(".complete").hide();
                                                $(".cancelled").hide();


                                            } else if (rad_val == "ongoing") {
                                                $(".pending_pro").hide();
                                                $(".on_going").show();
                                                $(".complete").hide();
                                                $(".cancelled").hide();

                                            } else if (rad_val == "completed") {
                                                $(".pending_pro").hide();
                                                $(".on_going").hide();
                                                $(".complete").show();
                                                $(".cancelled").hide();

                                            } else if (rad_val == "cancelled") {
                                                $(".pending_pro").hide();
                                                $(".on_going").hide();
                                                $(".complete").hide();
                                                $(".cancelled").show();
                                            }
                                        });

                                        $(".accept").click(function () {
                                            var ref_id = $(this).attr("data-id");

                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo site_url(); ?>/ajax.php",
                                                data: "ref_id=" + ref_id + "&status=accept&status_code=1&action=accept",
                                                cache: false,
                                                beforeSend: function () {
                                                    $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
                                                },
                                                success: function (html) {
                                                    location.reload();
                                                },
                                                error: function (error) {
                                                    // alert(error);
                                                }
                                            });

                                        });

                                        $(".complete_p").click(function () {
                                            var ref_id = $(this).attr("data-id");
                                            var status_val = $(this).attr("data-status_val");
                                            var status_code = $(this).attr("data-status_code");

                                            // alert(ref_id);
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo site_url(); ?>/ajax.php",
                                                data: "ref_id=" + ref_id + "&status=" + status_val + "&status_code=" + status_code + "&action=complete",
                                                cache: false,
                                                beforeSend: function () {
                                                    $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
                                                },
                                                success: function (html) {
                                                    //alert("yes");  
                                                    location.reload();
                                                },
                                                error: function (error) {
                                                    //alert("non");
                                                }
                                            });

                                        })


                                        $(".cancelled_p").click(function () {

                                            var aa = $(this).attr("data-id");

                                            var result = confirm("Want to delete?");
                                            if (result) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                                    data: "ref_id=" + aa + "&status=cancel&status_code=3&action=delete",
                                                    cache: false,
                                                    beforeSend: function () {
                                                        $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
                                                    },
                                                    success: function (html) {
                                                        location.reload();
                                                        //alert(html);
                                                    },
                                                    error: function (error) {
                                                        //alert(error);
                                                    }
                                                });
                                            }
                                        });
                                    </script>

                                </div>
                            </div>	  
                        <?php } ?> 
                        <div class="container"> 			
                            <?php if (is_user_logged_in()) { ?> 

                                <?
                                $author_querya = array('post_type' => 'social_accounts',
                                    'posts_per_page' => '1', 'author' => $user_id);
                                $author_postsa = new WP_Query($author_querya);
                                if ($author_postsa->have_posts()) :
//  print_r($author_postsa);vb
                                    while ($author_postsa->have_posts()) : $author_postsa->the_post();
                                        $get_the_id = get_the_id();
                                        ?>
                                        <?php $web_acc_url = get_post_meta($get_the_id, 'website_url', TRUE); ?>

                                        <?php
                                    endwhile;
                                else :
                                    ?>
                                <?php endif; ?> 


                                <!--social icon end-->

                                <?php
                            }

                            else if (!is_user_logged_in()) {
                                $user = get_user_by('ID', $_GET['user_id']);
                                ?>
                                <!-- <div class="visit-or-website"><?
                                $author_querya = array('post_type' => 'social_accounts',
                                    'posts_per_page' => '1', 'author' => $user_id);
                                $author_postsa = new WP_Query($author_querya);
                                if ($author_postsa->have_posts()) :
//  print_r($author_postsa);
                                    while ($author_postsa->have_posts()) : $author_postsa->the_post();
                                        $get_the_id = get_the_id();
                                        ?>
                                        <?php $web_acc_url = get_post_meta($get_the_id, 'website_url', TRUE); ?>
                                                                                                                                                        <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
                                        <?php
                                    endwhile;
                                else :
                                    ?>
                                                                                                                                            <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>Visit my website</a>
                                <?php endif; ?>  
                                </div> -->


                            <?php } ?>



                            <div id="events" class="tabcontent"  <?php if (isset($_GET['tab']) && $_GET['tab'] == "event") { ?> style="display:block"   <?php } ?>>


                                <?php
                                if (is_user_logged_in()) {
                                    if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                        ?>

                                    <?php } else { ?><a class="add-event-button" href="<?php echo site_url(); ?>/post-an-event/">Add event</a><?php
                                    }
                                }
                                ?>


                                <?php // if ( is_user_logged_in() ){  ?>
                                <div class="third-section">    
                                    <div class="third-section-authorname"><!--Blog--></div>
                                    <?php
//echo $user_id;
                                    $author_queryc = array('posts_per_page' => '-1', 'author' => $user_id, 'post_status' => 'publish', 'post_type' => 'event_listing');
                                    $author_postsc = new WP_Query($author_queryc);
                                    /*       if ( have_posts() )
                                      { */
//    $count_posts = wp_count_posts();
//echo  $published_posts = $count_posts->publish;
                                    while ($author_postsc->have_posts()) : $author_postsc->the_post();
                                        ?>
                                        <div class="section-third-inner 222">
                                            <?php
                                            $get_the_id = get_the_id();
                                            if (has_post_thumbnail()) {
                                                ?>                     
                                                <div class="section-third-thumb 5"><a href="<?php echo the_permalink(); ?>"><?php echo display_event_banner(); ?></a></div><?php } else {
                                                ?>
                                                <div class="section-third-thumb 6"><a href="<?php echo the_permalink(); ?>"><img src="<?php echo site_url(); ?>/wp-content/themes/marketify-child/images/dummy.jpg" ></a></div>
                                            <?php } ?>
                                            <?php
                                            if (is_user_logged_in()) {
                                                if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                    ?>

                                                <?php } else { ?><a href="<?php echo site_url(); ?>/event-dashboard/?action=edit&event_id=<?php echo $get_the_id; ?>">Edit</a><?php
                                                }
                                            }
                                            ?> 


                                            <div class="section-third-date"> <? echo get_the_date(); ?></div>

                                            <div class="section-third-title test123"><? echo the_title(); ?></div>
                                            <div class="section-third-content"><? echo the_excerpt(); ?></div>
                                        </div>
                                        <?php
                                    endwhile;
                                    /* } else {
                                      echo  "No Events";
                                      } */
                                    ?>     
                                </div>
                                <?php // }            ?>
                            </div>
                        </div>
                        <div class="macro">
                            <div id="content" class="site-content row">
                                <div id="primary" class="content-area col-sm-12">
                                    <main id="main" class="site-main" role="main">
                                        <style>
                                            .tab {  overflow: hidden;border: 1px solid #ccc; background-color: #f1f1f1; }
                                            .tab button {   background-color: inherit; float: left; border: none; outline: none; cursor: pointer; padding: 14px 16px; transition: 0.3s;
                                                            font-size: 17px; }
                                            .tab button:hover {    background-color: #ddd; }
                                            .tab button.active {    background-color: #ccc; }
                                            .tabcontent {   display: none; padding: 6px 12px; border: 1px solid #ccc; border-top: none;}
                                        </style>
                                        <script>
                                            function openCity(evt, cityName) {
                                                // alert(cityName);
                                                var i, tabcontent, tablinks;
                                                if (cityName == "aboutmesocial") {
                                                    $("#blog_social_2").show();
                                                    $("#title_order").hide();
                                                } else {
                                                    $("#blog_social_2").hide();
                                                    $("#title_order").show();
                                                    $("#blog_social_8").hide();
                                                    $("#blog_social_9").hide();
                                                }

                                                if (cityName == "aboutmesocial") {
                                                    $("#blog_social_3").show();
                                                    $("#title_order").hide();
                                                } else {
                                                    $("#blog_social_3").hide();
                                                    $("#title_order").show();
                                                    $("#blog_social_8").hide();
                                                    $("#blog_social_9").hide();
                                                }

                                                if (cityName == "aboutmesocial") {
                                                    $("#blog_social_4").show();
                                                    $("#title_order").hide();
                                                } else {
                                                    $("#blog_social_4").hide();
                                                    $("#title_order").show();
                                                    $("#blog_social_8").hide();
                                                    $("#blog_social_9").hide();
                                                }

                                                if (cityName == "aboutmesocial") {
                                                    $("#blog_social_5").show();
                                                    $("#blog_social_8").show();
                                                    $("#blog_social_9").show();

                                                    $("#title_order").hide();
                                                } else {
                                                    $("#blog_social_5").hide();
                                                    $("#title_order").show();
                                                    $("#blog_social_8").hide();
                                                    $("#blog_social_9").hide();
                                                }

                                                tabcontent = document.getElementsByClassName("tabcontent");
                                                for (i = 0; i < tabcontent.length; i++) {
                                                    tabcontent[i].style.display = "none";
                                                }
                                                tablinks = document.getElementsByClassName("tablinks");
                                                for (i = 0; i < tablinks.length; i++) {
                                                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                                                }
                                                document.getElementById(cityName).style.display = "block";
                                                evt.currentTarget.className += " active";
                                            }
                                        </script>


                                        <!--script>
                                        $(document).ready(function(){
                                        var divs = $("#artist > div");
                                        for(var i = 0; i < divs.length; i+=3) {
                                        divs.slice(i, i+3).wrapAll("<div class='new'></div>");
                                        }
                                        $( "#artist .new" ).last().addClass("artist_last");
                                        $('<div id="post-5856" class="empty" style="visibility: hidden;"><div class="download-current-user-inner"></div></div>').insertAfter('.artist_last div:first');
                                        $('.new.artist_last').insertAfter('#artist div:first');
                                        var divs = $("#artist > div").length;
                                        
                                        // if(divs < 3 ) {
                                        // $('#artist .new').removeClass('artist_last');
                                        
                                        // }
                                        });
                                        
                                        </script-->
                                        <div class="download-current-user"><div class="container">
                                                <?php
                                                if (is_user_logged_in()) {
                                                    $user_id = get_current_user_id();
                                                    $user_meta = get_userdata($user_id);
                                                    $user_roles = $user_meta->roles;
                                                    $rol = $user_roles[0];
                                                    ?>  
                                                    <?php if ($rol == 'administrator') { ?>
                                                        <style>
                                                            .comlex-info-product {
                                                                display: none;
                                                            }
                                                        </style>

                                                        <?php
                                                    } if ($rol == 'contributor') {

                                                        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                                                        $parts = parse_url($actual_link);
//echo "<pre>";
//print_r($parts);
                                                        parse_str(parse_url($actual_link)['query'], $params);
//echo "<pre>";
//print_r($params);
//echo $parts['path'];
                                                        if ($parts['path'] == '/manage-commission/') {
                                                            ?>
                                                            <style>
                                                                .manage-product{ display:none;}
                                                                .artist67,.like-button,.myblog,.Reviews,.contact,.events,a.tablinks.myblog.blog_1{display:none;}

                                                            </style>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $('.commission').addClass('active');
                                                                    $('#commission').show();
                                                                    $('#artist').hide();

                                                                });
                                                            </script>
                                                            <?php
                                                        } $user_id = $_GET['user_id'];
                                                    }
                                                    ?>
                                                    <div id="artist" class="tabcontent" style="display:block;" >

                                                        <?php
                                                        if ($rol != contributor) {
                                                            ?>


                                                            <?php
                                                            if (is_user_logged_in()) {
                                                                if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                                    ?> 
                                                                <?php } else { ?>

                                                                    <a class="add-product-button" href="<?php echo site_url(); ?>/add-product/">Add New Artwork</a><?php
                                                                }
                                                            }
                                                            ?>									
                                                        <? } ?>

                                                        <?php
// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                                        $pagenum = isset($_GET['artwork']) ? absint($_GET['artwork']) : 1;

                                                        global $wpdb;


                                                        $limit = 8; // number of rows in page
                                                        $offset = ( $pagenum - 1 ) * $limit;
                                                        $table_name = $wpdb->prefix . "posts";
                                                        $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type='download' and post_status='publish' and post_author=" . $user_id);
//echo $total.'<br>';
//echo $limit.'<br>';

                                                        $num_of_pages = ceil($total / $limit);
                                                        $args = array(
                                                            'posts_per_page' => $limit,
                                                            'post_type' => 'download',
                                                            'author' => $user_id,
                                                            'meta_key' => 'edd_price',
                                                            'orderby' => 'ID',
                                                            'order' => 'DESC',
                                                            'post_status' => 'publish',
                                                            'paged' => $pagenum
                                                        );
                                                        $wp_query = new WP_Query($args);
// echo "<pre>";
// print_r($wp_query);
                                                        if ($wp_query->have_posts()):while ($wp_query->have_posts()):$wp_query->the_post();
                                                                ?>

                                                                <div id="post-<?php the_ID(); ?>" class="post<?php echo $count; ?>" style="display:none;">
                                                                    <?php //echo the_title();  ?>
                                                                    <div class="download-current-user-inner"> 
                                                                        <?
                                                                        $get_the_id = get_the_id();
                                                                        if (!get_post_meta($get_the_id, 'edd_sale_price', true)) {
                                                                            
                                                                        } else {
                                                                            ?> <span  class="onsale-product">Sale</span><?php } ?>
                                                                        <div class="main-tbl-blf-dd">
                                                                            <div class="hover-img"><a href="<?php echo the_permalink(); ?>">VIEW DETAILS</a></div> 

                                                                            <?php
                                                                            if (has_post_thumbnail()) {
                                                                                echo '<p class="thumbnail_custome">';
                                                                                the_post_thumbnail("small");
                                                                                echo '</p>';
                                                                            }
                                                                            ?>
                                                                        </div>  
                                                                        <div class="download-current-user-name">   
                                                                            <a href="<?php the_permalink(); ?>" title="<?php //the_title_attribute();                                                                                                                                                                                                                                                           ?>"><?php //the_title();                                                                                                                                                                                                                                                           ?></a> 
                                                                        </div>

                                                                        <div class="basic-info-product">

                                                                            <div class="main_dd"> 
                                                                                <?php
                                                                                $user_id = get_current_user_id();
                                                                                $user_meta = get_userdata($user_id);
                                                                                $user_roles = $user_meta->roles;
                                                                                $rol = $user_roles[0];
                                                                                if ($rol != 'contributor') {
                                                                                    ?>
                                                                                    <div class="manage-product">
                                                                                        <?php $get_the_id = get_the_id(); ?>
                                                                                        <a href="<?php echo site_url(); ?>/vendor-dashboard/?task=edit-product&post_id=<?php echo $get_the_id; ?>">Edit</a> <a class="post_d" id="<?php echo get_the_id(); ?>">Delete</a> 
                                                                                    </div>
                                                                                <?php } ?>	
                                                                                <div class="left-img">
                                                                                    <?php
                                                                                    $txt = sprintf('<span>%1$s</span>', get_avatar(get_the_author_meta('ID'), 50, apply_filters('marketify_default_avatar', null)));
                                                                                    echo $txt;
                                                                                    ?>

                                                                                </div>


                                                                                <div class="right-cont">	
                                                                                    <h2 class="title-edit-btn"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h2> 				
                                                                                    <p>	<?php
                                                                                        $get_the_id = get_the_author_meta('ID');
                                                                                        $site_url = site_url();
                                                                                        printf(
                                                                                                __('<span class="byline ddd">by %1$s</span>', 'marketify'), sprintf('<span class="author vcard"><a class="url fn n" href="' . $site_url . '/artist-main/?user_id=' . $get_the_id . '" title="%2$s"> %4$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'marketify'), get_the_author())), get_avatar(get_the_author_meta('ID'), 50, apply_filters('marketify_default_avatar', null)), esc_html(get_the_author_meta('display_name'))
                                                                                                )
                                                                                        );
                                                                                        ?></p>

                                                                                </div>
                                                                            </div>

                                                                            <div class="main-price">
                                                                                <p class="price_symbol">
                                                                                    <?php
                                                                                    $get_the_id = get_the_id();
                                                                                    if (!get_post_meta($get_the_id, 'edd_sale_price', true)) {
                                                                                        ?>£ <?
                                                                                        echo $price = get_post_meta($get_the_id, 'edd_price', true);
                                                                                    } else {
                                                                                        ?>  <strong class="item-price"><span><del>£ <? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price = get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php } ?>

                                                                                    <span class="add-new-btnd">
                                                                                        <?php
                                                                                        $current_date = strtotime(date("d-m-Y"));
//$datet = strtotime($datet);
                                                                                        $past_date1 = date("d-m-Y", strtotime('-10 days'));
                                                                                        $past_date = strtotime($past_date1);
                                                                                        $post_date1 = get_the_date();
//echo $post_date=strtotime($post_date1);
                                                                                        $post_date2 = str_replace('/', '-', $post_date1);
                                                                                        $todays_date = date("d-m-Y");
                                                                                        $post_date = strtotime($post_date2); //echo $expiration_date.' | '.$today.'<br>';
                                                                                        if (($post_date > $past_date) && ($post_date <= $current_date)) {
                                                                                            echo '<div class="New-buuton-cls">New</div>';
                                                                                        }
                                                                                        ?>   </span></p>
                                                                            </div>
                                                                        </div>



                                                                        <div class="comlex-info-product">
                                                                            <?php //dynamic_sidebar('reviews');    ?>

                                                                            <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">
                                                                                <a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                                <div class="edd_purchase_submit_wrapper">

                                                                                    <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="£ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>
                                                                                </div>          <!--end .edd_purchase_submit_wrapper-->
                                                                            </form>                   
                                                                            <style>
                                                                                .black_overlay{
                                                                                    display: none;
                                                                                    position: fixed;
                                                                                    top: 0;
                                                                                    left:0;
                                                                                    width: 100%;
                                                                                    /* height: 100%; */
                                                                                    z-index:9998;
                                                                                    /* background: rgba(0,0,0,0.8); */

                                                                                }
                                                                                .white_content<?php echo $get_the_id; ?> {
                                                                                    display: none;
                                                                                    position: fixed;
                                                                                    top: 25%;
                                                                                    left: 50%;
                                                                                    margin-left: -400px;
                                                                                    width: 800px;
                                                                                    /*height: 50%;*/
                                                                                    padding: 20px;
                                                                                    /*border: 16px solid orange;*/
                                                                                    background-color: white;
                                                                                    z-index:9999;

                                                                                    box-shadow: 0 0 10px rgba(0,0,0,0.3);
                                                                                } .white_content<?php echo $get_the_id; ?> > a
                                                                                {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                                                                                 font:700 12px/30px arial; text-align: center; display: block;
                                                                                 background: #dfdf1f;  
                                                                                 color: #fff !important;
                                                                                 border-radius: 100px;}   

                                                                                .white_content<?php echo $get_the_id; ?> input[type="text"]
                                                                                {height:50px; width: 100%; border:1px solid #ccc;}

                                                                                .white_content<?php echo $get_the_id; ?> input[type="submit"]
                                                                                {position: absolute; right:0; top: 0; height: 50px;}

                                                                                .white_content<?php echo $get_the_id; ?> form
                                                                                {position: relative;}
                                                                            </style>
                                                                            <p class="flex-caption"> <a href ="javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'block';
                                                                                    document.getElementById('fade').style.display = 'block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
                                                                            <div id="light<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
                                                                                <a class="close_icon" href ="javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'none';
                                                                                        document.getElementById('fade').style.display = 'none'">X</a>

                                                                                <?php
                                                                                global $wpdb;
                                                                                $get_the_id = get_the_id();
                                                                                $curentuser_contact = $wpdb->get_results("SELECT AVG(artistid) as cvcvcx FROM artistreviews where product_id= '$get_the_id'");

                                                                                $curentuser_contact_count = $wpdb->get_results("SELECT count(artistid) as edstar FROM artistreviews where product_id= '$get_the_id'");
//  print_r($curentuser_contact_count);
                                                                                foreach ($curentuser_contact_count as $curentuser_contact_counted) {
                                                                                    $star_counted = $curentuser_contact_counted->edstar;
                                                                                }

// print_r($curentuser_contact);
                                                                                foreach ($curentuser_contact as $curentuser_contacted) {
                                                                                    $current_first = $curentuser_contacted->cvcvcx;
                                                                                }
                                                                                $current_first;
                                                                                $current_f_user = round($current_first);
                                                                                if ($current_f_user == "1") { //echo "1"; 
                                                                                    ?> <div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" />
                                                                                        ( <?php echo $star_counted; ?> )
                                                                                    </div><?
                                                                                } elseif ($current_f_user == "2") { //echo "2";
                                                                                    ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /> ( <?php echo $star_counted; ?> )</div><?
                                                                                } elseif ($current_f_user == "3") { //echo "3";
                                                                                    ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /> ( <?php echo $star_counted; ?> )</div><?
                                                                                    } elseif ($current_f_user == "4") { // echo "4"; 
                                                                                        ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /> ( <?php echo $star_counted; ?> )</div><?
                                                                                    } elseif ($current_f_user == "5") { //echo "5";
                                                                                        ?><div class="artist-review-custome">Artist Reviews <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /> ( <?php echo $star_counted; ?> )</div> <? } else {
                                                                                        ?>
                                                                                    <div class="artist-review-custome">No Reviews </div>
                                                                                <?php } ?>





                                                                                                                                                                                                                    <!--form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
                                                                                                                                                                                                                    <input type="text" name="review_title" placeholder="Review Title*" required/>
                                                                                                                                                                                                                    <select name="review_starts">
                                                                                                                                                                                                                    <option name="0">Select Review star:</option>
                                                                                                                                                                                                                    <option name="1">1</option>
                                                                                                                                                                                                                    <option name="2">2</option>
                                                                                                                                                                                                                    <option name="3">3</option>
                                                                                                                                                                                                                    <option name="4">4</option>
                                                                                                                                                                                                                    <option name="5">5</option>
                                                                                                                                                                                                                    </select>
                                                                                                                                                                                                                    <input type="text" name="review_description" placeholder="Review Description*" required/>
                                                                                                                                                                                                                    <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
                                                                                                                                                                                                                    <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
                                                                                                                                                                                                                    <input type="submit" name="submit" value="submit" />
                                                                                                                                                                                                                    </form-->
                                                                            </div>  <?php
                                                                            $auth = get_post(get_the_id()); // gets author from post

                                                                            $authid = $auth->post_author . 'dsfsdfd';
                                                                            $user_id14 = get_current_user_id();
                                                                            if ($authid != $user_id14) {
                                                                                ?>
                                                                                <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>
                                                                            <?php } else {
                                                                                ?>
                                                                                <a href="#" class="checkout-custome-button"></a>
                                                                            <?php }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php $count++; ?>
                                                                <?php
                                                            endwhile;
                                                        else:
                                                            ?>
                                                            <h1><? // echo "No Results Found";                                                                                                                                                                                                                                                             ?> </h1> 
                                                        <? endif; ?> 

                                                        <style>
                                                            .masanry-gallery{max-width: 1100px;width: 100%;margin:0 auto;}	

                                                            .row1 {
                                                                width: 32%;
                                                                float: left;
                                                                margin-right: 20px;
                                                            }
                                                            .row1:nth-child(2) .box1 {
                                                                height: 100% !important;
                                                            }
                                                            .box1 {

                                                                width: 100%;
                                                                float: left;
                                                                height: 100%;
                                                                margin: 1px;
                                                                font-size: 30px;
                                                                text-align: center;
                                                                color: #fff;
                                                            }
                                                        </style>
                                                        <div class="masanry-gallery">
                                                            <div class="row1 ">
                                                                <div class="box1" id="box_1">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_1").append($(".post").html());
                                                                        });
                                                                    </script>
                                                                </div>
                                                                <div class="box1" id="box_4">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_4").append($(".post3").html());
                                                                        });
                                                                    </script>
                                                                </div>
                                                                <div class="box1" id="box_7">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_7").append($(".post6").html());
                                                                        });
                                                                    </script>
                                                                </div>	
                                                            </div>
                                                            <div class="row1 row2">
                                                                <div class="box1" id="box_2">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_2").append($(".post1").html());
                                                                        });
                                                                    </script>
                                                                </div>
                                                                <div class="box1" id="box_6">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_6").append($(".post5").html());
                                                                        });
                                                                    </script>
                                                                </div>	
                                                            </div>
                                                            <div class="row1 row3">
                                                                <div class="box1" id="box_3">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_3").append($(".post2").html());
                                                                        });
                                                                    </script>
                                                                </div>
                                                                <div class="box1" id="box_5">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_5").append($(".post4").html());
                                                                        });
                                                                    </script>
                                                                </div>
                                                                <div class="box1" id="box_8">
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            $("#box_8").append($(".post7").html());
                                                                        });
                                                                    </script>
                                                                </div>	
                                                            </div>
                                                        </div>

                                                        <script>
                                                            jQuery(document).ready(function ($) {
                                                                //$('#artist').show();
                                                                $('.post_d').click(function () {
                                                                    //alert(this.id);
                                                                    var id = $(this).attr("id");
                                                                    //alert(id);
                                                                    var result = confirm("Want to delete?");
                                                                    if (result) {
                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                                                            data: "id=" + id + "&action=post_del",
                                                                            cache: false,
                                                                            success: function (html) {
                                                                                location.reload();
                                                                                //alert(html);
                                                                            },
                                                                            error: function (error) {
                                                                                //alert(error);
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        </script>
                                                        <div class="pagination-compition">
                                                            <?php
                                                            $page_links = paginate_links(array(
                                                                'base' => add_query_arg('artwork', '%#%'),
                                                                'format' => '',
                                                                'prev_text' => __('&laquo;', 'text-domain'),
                                                                'next_text' => __('&raquo;', 'text-domain'),
                                                                'total' => $num_of_pages,
                                                                'current' => $pagenum
                                                            ));

                                                            if ($page_links) {
                                                                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
                                                            }
                                                            ?>
                                                        </div>  
                                                        <?php ?>
                                                        <?php // next_posts_link( __('Newer Posts &larr; '));   ?>
                                                        </div-->
                                                        <?php wp_reset_query(); ?>
                                                    </div>
                                                </div>

                                                <div class="container">
                                                    <!--about pop up start-->
                                                    <div id="aboutme" class="tabcontent" <?php if (isset($_GET['tab']) && $_GET['tab'] == "about") { ?> style="display:block"   <?php } ?>>

                                                        <div class="section-second">
                                                            <?php
                                                            if (is_user_logged_in()) {
                                                                if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                                    ?>

                                                                <?php } else { ?>
                                                                    <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'block';
                                                                            document.getElementById('fade').style.display = 'block'">Edit</a></p><?php
                                                                    }
                                                                }
                                                                ?>

                                                                                                                                                                                                            <!--div class="second-section-authorname"><h2>About  <?php // echo $user_name;                                                                                                                                                                                                                                                             ?></h2>
                                                            <?php $current_user->ID; ?></div-->
                                                            <?
                                                            $author_queryb = array('post_type' => 'about_us',
                                                                'posts_per_page' => '1', 'author' => $user_id);
                                                            $author_postsb = new WP_Query($author_queryb);
                                                            while ($author_postsb->have_posts()) : $author_postsb->the_post();
                                                                ?>
                                                                <div class="second-section-authorname"><h2>About  <?php echo $user_name; ?> <?php //echo $user_last_name;                                                                                                                                                                                                                                                             ?></div>
                                                                <strong>
                                                                    <div class="section-second-title">  <?php
                                                                        $des = get_post_meta(get_the_ID(), 'artist_statement', true);
                                                                        echo $shortexcerpt = wp_trim_words($des, $num_words = 250, $more = '');
                                                                        ?></div>
                                                                </strong>
                                                                <strong></strong>
                                                                <div class="section-second-content"><? echo the_content(); ?></div>
                                                            <? endwhile; ?>
                                                        </div>

                                                        <div id="light" class="white_content" style="display:none;">
                                                            <div id="postbox">
                                                                <form id="new_post" name="new_post" method="post" action="<?php echo site_url(); ?>/thanks-about/"  enctype="multipart/form-data">

                                                                    <!--Name
                                                                    <input type="text" id="title" value="<? echo the_title(); ?>" tabindex="1" size="20" name="title" /> -->
                                                                    <input type="hidden" name="post_idd" value="<?php echo get_the_ID(); ?>"> 
                                                                    About Artistf
                                                                    <textarea id="description" tabindex="3" name="description44" cols="50" rows="6"><?php echo get_post_meta(get_the_ID(), 'about_artist', true); ?></textarea>

                                                                    Artist Statement
                                                                    <textarea id="description" tabindex="3" name="description" cols="50" rows="6" maxlength="250" placeholder="Write only 250 words."><? echo get_post_meta(get_the_ID(), 'artist_statement', true); ?></textarea>
                                                                    <input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />
                                                                    <input type="hidden" name="post_id" id="post_id" value="55" />
                                                                    <?php wp_nonce_field('my_image_upload', 'my_image_upload_nonce'); ?>
                                                                    <p align="right"><input type="submit" value="submit" tabindex="6" id="submit" name="submited" /></p></form>
                                                            </div>
                                                            <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'none';
                                                                    document.getElementById('fade').style.display = 'none'">X</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $ggd = $_GET['user_id']; ?>
                                                <div id="fade" class="black_overlay"></div>
                                                <!--- social link start -->
                                                <?php
                                                if (is_user_logged_in()) {
                                                    if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                        ?><div class="container">
                                                            <div class="follow-us-social" id="blog_social_4" style="display:none;">
                                                                <div class="footer-social">
                                                                    Follow me on   
                                                                    <?php
                                                                    $author_querya = array('post_type' => 'social_accounts',
                                                                        'posts_per_page' => '1', 'author' => $user_id);

                                                                    $author_postsa = new WP_Query($author_querya);
//print_r($author_postsa);
                                                                    if ($author_postsa->have_posts()) :
//echo count($author_postsa->have_posts());
                                                                        while ($author_postsa->have_posts()) :
                                                                            $author_postsa->the_post();
                                                                            $get_the_id = get_the_id();
                                                                            $twiter_acccv = get_user_meta($ggd, 'twiter_accc', true);
                                                                            if ($twiter_acccv) {
                                                                                ?> 
                                                                                <a href="<?php echo $twiter_acccv; ?>" target="_blank"> <span class="screen-reader-text">Twitter</span></a> 
                                                                            <? } else { ?> <?
                                                                            }

                                                                            $instagram_accc = get_user_meta($ggd, 'instagram_acc', true);
                                                                            if ($instagram_accc) {
                                                                                ?>
                                                                                <a href="<?php echo $instagram_accc; ?>" target="_blank"><span class="screen-reader-text">Instagram</span></a>
                                                                            <? } else { ?>
                                                                                <?
                                                                            }

                                                                            $facebook_accc = get_user_meta($ggd, 'facebook_acc', true);
                                                                            if ($facebook_accc) {
                                                                                ?>

                                                                            <? } else { ?><a href="<?php echo $facebook_accc; ?>" target="_blank"><span class="screen-reader-text">Facebook</span></a><?
                                                                            }

                                                                            $Pintrest_accc = get_user_meta($ggd, 'Pintrest_acc', true);
                                                                            if ($Pintrest_accc) {
                                                                                ?>
                                                                                <a href="<?php echo $Pintrest_accc; ?>" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
                                                                            <? } else { ?> 
                                                                                <?
                                                                            }

                                                                        endwhile;
                                                                    else :


                                                                        $twiter_acccv = get_user_meta($ggd, 'twiter_accc', true);
                                                                        if ($twiter_acccv) {
                                                                            ?> 
                                                                            <a href="<?php echo $twiter_acccv; ?>" target="_blank"> <span class="screen-reader-text">Twitter</span></a> 
                                                                        <? } else { ?> <?
                                                                        }

                                                                        $instagram_accc = get_user_meta($ggd, 'instagram_acc', true);
                                                                        if ($instagram_accc) {
                                                                            ?>
                                                                            <a href="<?php echo $instagram_accc; ?>" target="_blank"><span class="screen-reader-text">Instagram</span></a>
                                                                        <? } else { ?>
                                                                            <?
                                                                        }

                                                                        $facebook_accc = get_user_meta($ggd, 'facebook_acc', true);
                                                                        if ($facebook_accc) {
                                                                            ?>

                                                                            <a href="<?php echo $facebook_accc; ?>" target="_blank"><span class="screen-reader-text">Facebook</span></a>
                                                                        <? } else { ?> <?php
                                                                        }

                                                                        $Pintrest_accc = get_user_meta($ggd, 'Pintrest_acc', true);
                                                                        if ($Pintrest_accc) {
                                                                            ?>
                                                                            <a href="<?php echo $Pintrest_accc; ?>" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
                                                                        <? } else { ?> 
                                                                        <? } ?><?php
                                                                    endif;
                                                                    ?>
                                                                </div> 



                                                            </div> 
                                                            <div class="follow-us-social" id="blog_social_3" >
                                                                <?php
                                                                $Website_accc = get_user_meta($ggd, 'Website_acc', true);
                                                                if ($Website_accc) {
                                                                    ?>
                                                                    <div class="visit-or-website">  

                                                                        <a href="<?php echo $Website_accc; ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
                                                                    </div>
                                                                <?php } ?> 
                                                            </div>  



                                                        <? } else { ?>	  
                                                            <div id="aboutmesocial" class="tabcontent" <?php if (isset($_GET['tab']) && $_GET['tab'] == "account") { ?> style="display:block"   <?php } ?>>
                                                                <?php include('social_page.php'); ?>
                                                            </div>	  
                                                        </div> 
                                                        <?php
                                                    }
                                                }
                                                ?>




                                                <div id="fade" class="black_overlay"></div>
                                                <!---social link end-->
                                                <?php
                                                if (is_user_logged_in()) {

                                                    global $wpdb;
//print_r($author_postsa);
                                                    $pagenum = isset($_GET['blog']) ? absint($_GET['blog']) : 1;

                                                    $limit = 4; // number of rows in page
                                                    $offset = ( $pagenum - 1 ) * $limit;
                                                    $table_name = $wpdb->prefix . "posts";

                                                    $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type = 'post' AND post_status = 'publish' AND post_author=" . $user_id);
                                                    $num_of_pages = ceil($total / $limit);

//echo "<pre>";
// print_r($total);
                                                    $user = get_user_by('ID', $_GET['user_id']);


                                                    if ($current_user instanceof WP_User) {
                                                        ?>
                                                        <div class="container">
                                                            <div id="myblog" class="tabcontent" <?php if (isset($_GET['tab']) && $_GET['tab'] == "blog") { ?> style="display:block"   <?php } ?>>
                                                                <?php
                                                                $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                                                                $parts = parse_url($actual_link);
//echo "<pre>";
// print_r($parts);
                                                                parse_str(parse_url($actual_link)['query'], $params);

//echo $params['tab'];

                                                                if ($params['tab'] == 'blog') {
//echo "hello";
                                                                    ?>
                                                                    <style>
                                                                        #artist{
                                                                            display:none !important;
                                                                        }
                                                                    </style>
                                                                    <?php
                                                                } if (is_user_logged_in()) {
                                                                    if (isset($_GET['user_id']) && ($_GET['user_id'] != get_current_user_id())) {
                                                                        ?>

                                                                    <?php } else { ?>
                                                                        <a class="add-blog-button" href="<?php echo site_url(); ?>/blog-add/">Add blog</a><?php
                                                                    }
                                                                }
                                                                ?>

                                                                <div class="third-section">    
                                                                    <div class="third-section-authorname"><!--Blog--></div>
                                                                    <?php
                                                                    $author_queryc = array('posts_per_page' => $limit, 'author' => $user_id, 'paged' => $pagenum);



                                                                    $author_postsc = new WP_Query($author_queryc);
                                                                    if (have_posts()) {
                                                                        while ($author_postsc->have_posts()) : $author_postsc->the_post();
                                                                            ?>
                                                                            <div class="section-third-inner 222">
                                                                                <?php $get_the_id = get_the_id(); ?>
                                                                                <?php
                                                                                if (has_post_thumbnail()) {
                                                                                    ?>                     
                                                                                    <div class="section-third-thumb 1"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail(); ?></a></div><?php
                                                                                } else {
                                                                                    ?>
                                                                                    <div class="section-third-thumb 2"><a href="<?php echo the_permalink(); ?>"><img src="<?php echo site_url(); ?>/wp-content/themes/marketify-child/images/dummy.jpg" ></a></div>
                                                                                    <?php
                                                                                }
                                                                                ?>


                                                                                <div class="section-third-date"> <? echo get_the_date(); ?></div>

                                                                                <style>
                                                                                    .black_overlay{
                                                                                        display: none;
                                                                                        position: fixed;
                                                                                        top: 0;
                                                                                        left:0;
                                                                                        width: 100%;
                                                                                        /*  height: 100%; */
                                                                                        z-index:9998;
                                                                                        /*  background: rgba(0,0,0,0.8); */

                                                                                    }
                                                                                    .white_content {
                                                                                        display: none;
                                                                                        position: fixed;
                                                                                        top: 25%;
                                                                                        left: 50%;
                                                                                        margin-left: -400px;
                                                                                        width: 800px;
                                                                                        height: 560px;
                                                                                        padding: 20px;
                                                                                        /*border: 16px solid orange;*/
                                                                                        background-color: white;
                                                                                        z-index:9999;

                                                                                        box-shadow: 0 0 10px rgba(0,0,0,0.3);
                                                                                    } .white_content > a
                                                                                    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                                                                                     font:700 12px/30px arial; text-align: center; display: block;background: #dfdf1f;
                                                                                     color: #fff !important;
                                                                                     border-radius: 100px;}   

                                                                                    .white_content input[type="text"]
                                                                                    {height:50px; width: 100%; border:1px solid #ccc;}

                                                                                    .white_content input[type="submit"]
                                                                                    {position: absolute; right:0; top: 0; height: 50px;}

                                                                                    .white_content form
                                                                                    {position: relative;}

                                                                                    .black_overlay{
                                                                                        display: none;
                                                                                        position: fixed;
                                                                                        top: 0;
                                                                                        left:0;
                                                                                        width: 100%;
                                                                                        /*   height: 100%; */z-index:9998;
                                                                                        /*  background: rgba(0,0,0,0.8); */

                                                                                    }
                                                                                    .white_content<?php echo $get_the_id; ?> {
                                                                                        display: none;
                                                                                        position: fixed;
                                                                                        top: 10%;
                                                                                        left: 50%;
                                                                                        margin-left: -400px;
                                                                                        width: 800px;
                                                                                        height: 560px;
                                                                                        padding: 20px;
                                                                                        /*border: 16px solid orange;*/
                                                                                        background-color: #fff;
                                                                                        z-index:9999;

                                                                                        box-shadow: 0 0 10px rgba(0,0,0,0.3);
                                                                                    } .white_content<?php echo $get_the_id; ?> > a
                                                                                    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                                                                                     font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
                                                                                     background: #70C0EF;
                                                                                     border-radius: 100px;}   

                                                                                    .white_content<?php echo $get_the_id; ?> input[type="text"]
                                                                                    {height:50px; width: 100%; border:1px solid #ccc;}

                                                                                    .white_content<?php echo $get_the_id; ?> input[type="submit"]
                                                                                    {position: absolute; right:0; top: 0; height: 50px;}

                                                                                    .white_content<?php echo $get_the_id; ?> form   
                                                                                    {position: relative;}
                                                                                </style>
                                                                                <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'block';
                                                                                        document.getElementById('fade<?php echo $get_the_id; ?>').style.display = 'block'">Edit</a>
                                                                                    <a class="post_de" id="<?php echo get_the_id(); ?>">Delete</a> 

                                                                                </p>

                                                                                <div id="light<?php echo $get_the_id; ?>" class="white_content hyu<?php echo $get_the_id; ?>"><?php $get_the_id; ?>
                                                                                    <?php
                                                                                    $post_to_edit = get_post($get_the_id);
//print_r($post_to_edit); 
                                                                                    ?>
                                                                                    <!-- edit Post Form -->
                                                                                    <div id="postbox">
                                                                                        <form id="new_post" name="new_post" method="post" action="<?php echo site_url(); ?>/thanks/"  enctype="multipart/form-data">
                                                                                            <p><label for="title">Title</label><br />
                                                                                                <input type="text" id="title" value="<?php echo $post_to_edit->post_title; ?>" tabindex="1" size="20" name="title" />
                                                                                            </p>
                                                                                            <p><label for="description">Description</label><br/>
                                                                                                <textarea id="description" tabindex="3" name="description" cols="50" rows="6"><?php echo $post_to_edit->post_content; ?></textarea>
                                                                                            </p> 
                                                                                            <p><input type="file" name="image"></p>
                                                                                            <p align="right"><input type="submit" value="submit" tabindex="6" id="submit" name="submited" /></p>
                                                                                            <input type="hidden" name="action" value="f_edit_post" />
                                                                                            <input type="hidden" name="pid" value="<?php echo $post_to_edit->ID; ?>" />
                                                                                            <?php wp_nonce_field('new-post'); ?>
                                                                                        </form>
                                                                                    </div>   
                                                                                    <!--// edit Post Form -->
                                                                                    <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'none';
                                                                                            document.getElementById('fade<?php echo $get_the_id; ?>').style.display = 'none'" >X</a></div>  
                                                                                <div id="fade<?php echo $get_the_id; ?>" class="black_overlay"></div>  <div class="section-third-title 123"><? echo the_title(); ?></div>

                                                                                <div class="section-third-content"><span><? echo the_excerpt(); ?></span></div>
                                                                            </div> <?php
                                                                        endwhile;
                                                                    } else {
                                                                        echo "No Post found";
                                                                    }
                                                                    ?> </div>

                                                                <script>
                                                                    jQuery(document).ready(function ($) {
                                                                        //$('#artist').show();
                                                                        $('.post_de').click(function () {
                                                                            //alert(this.id);
                                                                            var id = $(this).attr("id");
                                                                            //alert(id);
                                                                            var result = confirm("Want to delete?");
                                                                            if (result) {
                                                                                $.ajax({
                                                                                    type: "POST",
                                                                                    url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                                                                    data: "id=" + id + "&action=post_dele",
                                                                                    cache: false,
                                                                                    success: function (html) {
                                                                                        location.reload();
                                                                                        //alert(html);
                                                                                    },
                                                                                    error: function (error) {
                                                                                        //alert(error);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    });
                                                                </script>
                                                                <div class="pagination-compition">
                                                                    <?php
                                                                    $page_links = paginate_links(array(
                                                                        'base' => add_query_arg('blog', '%#%'),
                                                                        'format' => '',
                                                                        'prev_text' => __('&laquo;', 'text-domain'),
                                                                        'next_text' => __('&raquo;', 'text-domain'),
                                                                        'total' => $num_of_pages,
                                                                        'current' => $pagenum
                                                                    ));

                                                                    if ($page_links) {
                                                                        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>  <div id="contact" class="tabcontent">
                                                    <div class="third-section">
                                                        <div class="third-section-authorname">Contact <?php $user_name; ?></div>
                                                        <form name="contactusartist" method="post">
                                                            <input  class="input_uk" type="text" name="ftext" placeholder="First name*" required />
                                                            <input class="input_us" type="text" name="ltext" placeholder="Surname*" required />
                                                            <textarea rows="4" cols="50" name="ymessage" placeholder="Your message*" required></textarea>
                                                            <input type="hidden" name="currentuser" value=" <?php echo $current_user->display_name; ?>" />
                                                            <input class="input_submit" type="submit" name="submit" value="Send" />
                                                            <p>You can only contact the artist, if you have a buyer account. If you don't please <a href="<?php echo site_url(); ?>/ukarts/uk-artists-collection/">signup</a> for free.</p>
                                                        </form>
                                                        <?php
                                                        if (isset($_POST["submit"])) {
//echo "Yes";
                                                            $firstname = $_POST['ftext'];
                                                            $surname = $_POST['ltext'];
                                                            $message = $_POST['ymessage'];
                                                            $currentuser = $_POST['currentuser'];
                                                            global $wpdb;
//$wpdb->query($sql);
                                                            $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname, $surname, $message, $currentuser);
                                                            $wpdb->query($sql);
                                                        } else {
//  echo "N0";
                                                        }
                                                        global $wpdb;
//print_r($current_user);
                                                        $current_user_name = $current_user->user_login;
                                                        $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
//print_r($curentuser_contact);
                                                        foreach ($curentuser_contact as $curentuser_contacted) {
                                                            $current_first = $curentuser_contacted->firstname;
                                                            $current_surename = $curentuser_contacted->surname;
                                                            $current_message = $curentuser_contacted->message;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="container">
                                                    <div id="review" class="tabcontent">
                                                        <div class="review-main-section">
                                                            <h2>Artist Reviews</h2>
                                                            <div class="review-overall">
                                                                <?php
//echo $user_id;
                                                                $row = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 5");
                                                                $max_rating = $row[0]->max_rating;
                                                                $aggregate_rating = $row[0]->aggregate_rating;
                                                                $err = round($aggregate_rating);
                                                                $total_reviews = $row[0]->total;
                                                                $totl = $aggregate_rating * 20;
                                                                $wpdb->flush();

                                                                $y5 = 100;
                                                                $percent_5 = $total_reviews / $y5;
                                                                $percent_width_5 = $percent_5 * 100;
                                                                ?>
                                                                <div class="row first_reviews"> 
                                                                    <div class="first_line">  
                                                                        <div class="side">
                                                                            <div>5 star</div>
                                                                        </div>
                                                                        <div class="middle">
                                                                            <div class="bar-container">
                                                                                <div class="bar-5" style="width:<?php echo $percent_width_5; ?>% !important;" ></div>
                                                                            </div>
                                                                        </div> 
                                                                        <div class="side right">
                                                                            <div><?php echo $total_reviews; ?></div>
                                                                        </div> 
                                                                        <?php ?>
                                                                        <div class="review-inner-part"><span>Seller Communication: </span>  <?php
                                                                            global $wpdb;
//print_r($current_user);
                                                                            $current_user_name = $user_name;
                                                                            $avgsell = $wpdb->get_results("SELECT AVG(sellercomm) as seller FROM artistreviews where artistlist= '$user_id'");
//print_r($avgsell); 
                                                                            $average_sell = $avgsell[0]->seller;
                                                                            $average_sell = round($average_sell);
                                                                            if ($average_sell == "1") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($average_sell == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($average_sell == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($average_sell == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($average_sell == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?> </div> 

                                                                        <div class="review-inner-part macrto"><span class="ght">Overall</span>  
                                                                            <?php
                                                                            global $wpdb;
//print_r($current_user);
                                                                            $current_user_name = $user_name;
                                                                            $avgsell = $wpdb->get_results("SELECT AVG(artistid) as artseller FROM artistreviews where artistlist= '$user_id'");
                                                                            $avg_artseller = $avgsell[0]->artseller;
                                                                            $avg_artseller = round($avg_artseller);
                                                                            if ($avg_artseller == "1") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($avg_artseller == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($avg_artseller == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($avg_artseller == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($avg_artseller == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?> </div>    
                                                                    </div> 
                                                                    <?php
                                                                    $row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 4");
                                                                    $max_rating = $row[0]->max_rating;
                                                                    $aggregate_rating = $row1[0]->aggregate_rating;
                                                                    $erro = round($aggregate_rating);
                                                                    $total_reviews = $row1[0]->total;
                                                                    $totl = $aggregate_rating * 20;
                                                                    $wpdb->flush();

                                                                    $y4 = 100;
                                                                    $percent_5 = $total_reviews / $y4;
                                                                    $percent_width_4 = $percent_4 * 100;
                                                                    ?>
                                                                    <div class="first_line">
                                                                        <div class="side">
                                                                            <div>4 star</div>
                                                                        </div>
                                                                        <div class="middle">
                                                                            <div class="bar-container">
                                                                                <div class="bar-4" style="width:<?php echo $percent_width_4; ?>% !important;" ></div>
                                                                            </div></div>
                                                                        <div class="side right">
                                                                            <div><?php echo $total_reviews; ?></div>
                                                                        </div>

                                                                        <div class="review-inner-part"><span>Delivery and Packing: </span>  <?php
                                                                            global $wpdb;
//print_r($current_user);
                                                                            $current_user_name = $user_name;
                                                                            $avgsell = $wpdb->get_results("SELECT AVG(deliverynpaint) as deliseller FROM artistreviews where artistlist= '$user_id'");
                                                                            $average_delisell = $avgsell[0]->deliseller;
                                                                            $average_delisell = round($average_delisell);
                                                                            if ($average_delisell == "1") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($average_delisell == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($average_delisell == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($average_delisell == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($average_delisell == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?> </div> 

                                                                    </div>
                                                                    <?php
                                                                    $row2 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 3");
                                                                    $max_rating = $row[0]->max_rating;
                                                                    $aggregate_rating = $row2[0]->aggregate_rating;
                                                                    $err3 = round($aggregate_rating);
                                                                    $total_reviews = $row2[0]->total;
                                                                    $totl = $aggregate_rating * 20;
                                                                    $wpdb->flush();
                                                                    $y3 = 100;
                                                                    $percent_3 = $total_reviews / $y3;
                                                                    $percent_width_3 = $percent_3 * 100;
                                                                    ?> 
                                                                    <div class="first_line">
                                                                        <div class="side">
                                                                            <div>3 star</div>
                                                                        </div>
                                                                        <div class="middle">
                                                                            <div class="bar-container">
                                                                                <div class="bar-3"style="width:<?php echo $percent_width_3; ?>% !important;"></div>
                                                                            </div></div>
                                                                        <div class="side right">
                                                                            <div><?php echo $total_reviews; ?></div>
                                                                        </div>
                                                                        <?php ?>
                                                                        <div class="review-inner-part"><span>Listing Accuracy: </span>  <?php
                                                                            global $wpdb;
//print_r($current_user);
                                                                            $current_user_name = $user_name;
                                                                            $avgsell = $wpdb->get_results("SELECT AVG(listingacc) as listseller FROM artistreviews where artistlist= '$user_id'");
                                                                            $average_listsell = $avgsell[0]->listseller;
                                                                            $average_listsell = round($average_listsell);
                                                                            if ($average_listsell == "1") {
                                                                                ?>
                                                                                <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($average_listsell == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($average_listsell == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($average_listsell == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($average_listsell == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?> </div>  
                                                                    </div> 
                                                                    <?php
                                                                    $row3 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 2");
                                                                    $max_rating = $row[0]->max_rating;
                                                                    $aggregate_rating = $row3[0]->aggregate_rating;
                                                                    $err4 = round($aggregate_rating);
                                                                    $total_reviews = $row3[0]->total;
                                                                    $totl = $aggregate_rating * 20;
                                                                    $wpdb->flush();
                                                                    $y2 = 100;
                                                                    $percent_2 = $total_reviews / $y2;
                                                                    $percent_width_2 = $percent_2 * 100;
                                                                    ?> 
                                                                    <div class="first_line"> 
                                                                        <div class="side">
                                                                            <div>2 star</div>
                                                                        </div>
                                                                        <div class="middle">
                                                                            <div class="bar-container">
                                                                                <div class="bar-2" style="width:<?php echo $percent_width_2; ?>% !important;"></div>
                                                                            </div></div>
                                                                        <div class="side right">
                                                                            <div><?php echo $total_reviews; ?></div>
                                                                        </div></div>
                                                                    <?php
                                                                    $row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 1");
                                                                    $max_rating = $row[0]->max_rating;
                                                                    $aggregate_rating = $row1[0]->aggregate_rating;
                                                                    $err5 = round($aggregate_rating);
                                                                    $total_reviews = $row1[0]->total;
                                                                    $totl = $aggregate_rating * 20;
                                                                    $wpdb->flush();
                                                                    $y1 = 100;
                                                                    $percent_1 = $total_reviews / $y1;
                                                                    $percent_width_1 = $percent_1 * 100;
                                                                    ?>
                                                                    <div class="first_line">     
                                                                        <div class="side">
                                                                            <div>1 star</div>
                                                                        </div>
                                                                        <div class="middle">
                                                                            <div class="bar-container">
                                                                                <div class="bar-1" style="width:<?php echo $percent_width_1; ?>% !important;"></div>
                                                                            </div></div>
                                                                        <div class="side right">
                                                                            <div><?php echo $total_reviews; ?></div>
                                                                        </div></div>

                                                                    <?php
                                                                    ?>                             
                                                                </div><div class="review-inner-left">
                                                                    <?php
                                                                    global $wpdb;
//print_r($current_user);
// echo $user_id;
                                                                    $current_user_name = $user_name;
                                                                    $findID = $wpdb->get_results("SELECT * FROM artistreviews where artistlist= '$user_id'  ORDER BY id DESC");
//print_r($findID); 
                                                                    foreach ($findID as $fivesdraft) {
//print_r($fivesdraft);
                                                                        ?> 
                                                                        <div class="outer-review">
                                                                            <div class="review-inner-main-left">

                                                                                <div class="review-inner-part"><span>Delivery and Packing </span><?php
                                                                                    $deliverynpaint_pic = $fivesdraft->deliverynpaint;
                                                                                    if ($deliverynpaint_pic == "1") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                                    } elseif ($deliverynpaint_pic == "2") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                                    } elseif ($deliverynpaint_pic == "3") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                                    } elseif ($deliverynpaint_pic == "4") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                                    } elseif ($deliverynpaint_pic == "5") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                                    } else {
                                                                                        
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <div class="review-inner-part">
                                                                                    <span>Listing Accuracy </span><?php
                                                                                    $listingacc_pic = $fivesdraft->listingacc;
                                                                                    if ($listingacc_pic == "1") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                                    } elseif ($listingacc_pic == "2") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                                    } elseif ($listingacc_pic == "3") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                                    } elseif ($listingacc_pic == "4") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                                    } elseif ($listingacc_pic == "5") {
                                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                                    } else {
                                                                                        
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <div class="review-inner-part"><span><b>Overall Rating</b> </span><?php
                                                                                    $overall_rating_con = $fivesdraft->artistid;
                                                                                    $overall_rating_con = round($overall_rating_con);

                                                                                    if ($overall_rating_con == "1") {
                                                                                        ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /></span><?
                                                                                            } elseif ($overall_rating_con == "2") {
                                                                                                ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /></span><?
                                                                                        } elseif ($overall_rating_con == "3") {
                                                                                            ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /></span><?
                                                                                        } elseif ($overall_rating_con == "4") {
                                                                                            ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /></span><?
                                                                                        } elseif ($overall_rating_con == "5") {
                                                                                            ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /></span><?
                                                                                        } else {
                                                                                            
                                                                                        }
                                                                                        ?>
                                                                                </div>
                                                                            </div>

                                                                            <div class="review-inner-main-right">
                                                                                <div class="review-inner-part">
                                                                                    <?php
                                                                                    $wp_user_search = $wpdb->get_results("SELECT ID, user_nicename FROM $wpdb->users WHERE ID='$fivesdraft->username'");
//print_r($wp_user_search);
                                                                                    foreach ($wp_user_search as $page) {
                                                                                        $page->user_nicename;
//echo $page['user_nicename']; 
                                                                                        ?><h2><?php echo $page->user_nicename; ?></h2>
                                                                                    <?php } ?><span>
                                                                                        <?php $date_publish = $fivesdraft->date; ?>
                                                                                        <?php echo $newDate = date("d/m/Y", strtotime($date_publish)); ?>
                                                                                        <?php //echo $fivesdraft->date;   ?></span>
                                                                                    <p><?php echo $fivesdraft->discription; ?></p>
                                                                                    <?php
                                                                                    $reviewid = $fivesdraft->id;
//echo $user_id;
                                                                                    ?>    
                                                                                    <?php
                                                                                    if ($rol != contributor) {
                                                                                        if (is_user_logged_in() && $fivesdraft->artistlist == $user_id) {
                                                                                            ?>
                                                                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">Reply</button>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <div class="container">
                                                                                        <div class="modal fade" id="myModal<?php echo $i; ?>" role="dialog">
                                                                                            <div class="modal-dialog">

                                                                                                <!-- Modal content-->
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                        <h4 class="modal-title"><?php echo $user_name; ?> reply</h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">

                                                                                                        <form name="artist-inert-table" method="post">
                                                                                                            <input type="hidden" class="reviewid<?php echo $i; ?>" value="<?php echo $reviewid; ?>" name="reviewid">
                                                                                                            <div class="two-section-table">
                                                                                                                <div class="two-section-tableinner">
                                                                                                                    <input type="text" class="reply<?php echo $i; ?>" name="reply" placeholder="Reply"/>
                                                                                                                </div>
                                                                                                            </div>      
                                                                                                        </form>
                                                                                                        <style>
                                                                                                            .modal-dialog {
                                                                                                                padding-top: 201px;
                                                                                                            }
                                                                                                            .modal-backdrop {

                                                                                                                z-index: 0 !important;

                                                                                                            }
                                                                                                            .modal-backdrop.in {
                                                                                                                opacity: 0 !important;

                                                                                                            }
                                                                                                        </style>
                                                                                                    </div>
                                                                                                    <div class="modal-footer">

                                                                                                        <?php //echo $fivesdraft->reply;  ?>
                                                                                                        <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm<?php echo $i; ?>()">SUBMIT</button>
                                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                    <script>
                                                                                        function submitContactForm<?php echo $i; ?>() {
                                                                                            var artist_id = $('.reviewid<?php echo $i; ?>').val();
                                                                                            //alert(artist_id);
                                                                                            var reply = $('.reply<?php echo $i; ?>').val();
                                                                                            /// alert(reply);
                                                                                            $.ajax({
                                                                                                type: "POST",
                                                                                                url: "<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php",
                                                                                                data: "artist_id=" + artist_id + "&reply=" + reply,
                                                                                                cache: false,
                                                                                                success: function (html) {
                                                                                                    ///alert("sdjkffhdkj"); 
                                                                                                    location.reload();
                                                                                                },
                                                                                                error: function (error) {
                                                                                                    //alert(error);
                                                                                                }
                                                                                            });
                                                                                        }

                                                                                    </script>
                                                                                    <?php ++$i; ?> 

                                                                                    <p class="reply-message"><strong><?php echo $user_name; ?> Reply:</strong></p>
                                                                                    <?php echo $fivesdraft->reply; ?>  </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>           
                                                                </div>  
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 


                                                <?php
                                            } else {
                                                global $current_user;
                                                wp_get_current_user();
                                                ?>
                                                <div id="artist" class="tabcontent" <?php if (isset($_GET['tab'])) { ?> style="display:none"<?php } ?>>

                                                    <?php
                                                    $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

                                                    global $wpdb;


                                                    $limit = 8; // number of rows in page
                                                    $offset = ( $pagenum - 1 ) * $limit;
                                                    $table_name = $wpdb->prefix . "posts";
                                                    $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type='download' and post_status='publish' and post_author=" . $user_id);
//echo $total.'<br>';
//echo $limit.'<br>';

                                                    $num_of_pages = ceil($total / $limit);
                                                    $args = array(
                                                        'posts_per_page' => $limit,
                                                        'post_type' => 'download',
                                                        'author' => $user_id,
                                                        'meta_key' => 'edd_price',
                                                        'orderby' => 'ID',
                                                        'order' => 'DESC',
                                                        'post_status' => 'publish',
                                                        'paged' => $pagenum
                                                    );
                                                    $wp_query = new WP_Query($args);
                                                    if ($wp_query->have_posts()):while ($wp_query->have_posts()):$wp_query->the_post();
                                                            ?>
                                                            <div id="post-<?php the_ID(); ?>" class="art<?php echo $count; ?>" style="display:none;">
                                                                <?php //echo the_title();   ?>
                                                                <div class="download-current-user-inner wth_log">
                                                                    <?
                                                                    $get_the_id = get_the_id();
                                                                    if (!get_post_meta($get_the_id, 'edd_sale_price', true)) {
                                                                        
                                                                    } else {
                                                                        ?><span  class="onsale-product">Sale</span><?php } ?> 
                                                                    <div class="main-tbl-blf-dd">
                                                                        <div class="hover-img"><a href="<?php echo the_permalink(); ?>">VIEW DETAILS</a></div> 

                                                                        <?php
                                                                        if (has_post_thumbnail()) {
                                                                            echo '<p class="thumbnail_custome">';
                                                                            the_post_thumbnail("small");
                                                                            echo '</p>';
                                                                        }
                                                                        ?>
                                                                    </div> 

                                                                    <div class="download-current-user-name">    <a href="<?php the_permalink(); ?>" title="<?php //the_title_attribute();                                                                                                                                                                                                                                                            ?>"><?php //the_title();                                                                                                                                                                                                                                                            ?></a> </div>



                                                                    <div class="basic-info-product ">



                                                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>  </h2>
                                                                        <p>By <?php the_author(); ?></p>
                                                                        <p class="price_symbol">
                                                                            <?php
                                                                            $get_the_id = get_the_id();

                                                                            if (!get_post_meta($get_the_id, 'edd_sale_price', true)) {
                                                                                ?>£<?
                                                                                echo $price = get_post_meta($get_the_id, 'edd_price', true);
                                                                            } else {
                                                                                ?>  <strong class="item-price"><span><del>£<? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price = get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php } ?>


                                                                            <span class="add-new-btnd">
                                                                                <?php
                                                                                $current_date = strtotime(date("d-m-Y"));
//$datet = strtotime($datet);

                                                                                $past_date1 = date("d-m-Y", strtotime('-10 days'));

                                                                                $past_date = strtotime($past_date1);
                                                                                $post_date1 = get_the_date();

//echo $post_date=strtotime($post_date1);


                                                                                $post_date2 = str_replace('/', '-', $post_date1);
                                                                                $todays_date = date("d-m-Y");
                                                                                $post_date = strtotime($post_date2);

//echo $expiration_date.' | '.$today.'<br>';

                                                                                if (($post_date > $past_date) && ($post_date <= $current_date)) {
                                                                                    echo "New";
                                                                                }
                                                                                ?>      </span> 
                                                                        </p>
                                                                    </div>




                                                                    <div class="comlex-info-product">
                                                                        <?php //dynamic_sidebar('reviews');   ?>


                                                                        <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">
                                                                            <a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single">
                                                                                <i class="glyphicon glyphicon-heart"></i></i>
                                                                                <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>       
                                                                            <div class="edd_purchase_submit_wrapper">
                                                                                <?php
                                                                                /* if($authid != $user_id14 ){ */
                                                                                ?>
                                                                                <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="£ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>
                                                                                <?php /* }else{  ?>
                                                                                  <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="£ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" disabled ><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>

                                                                                  <?php  } */
                                                                                ?>
                                                                            </div>
                                                                            <!--end .edd_purchase_submit_wrapper-->
                                                                        </form>

                                                                        <style>

                                                                            .black_overlay{
                                                                                display: none;
                                                                                position: fixed;
                                                                                top: 0;
                                                                                left:0;
                                                                                width: 100%;
                                                                                /*   height: 100%;z-index:9998;
                                                                                background: rgba(0,0,0,0.8); */

                                                                            }
                                                                            .white_content<?php echo $get_the_id; ?> {
                                                                                display: none;
                                                                                position: fixed;
                                                                                top: 25%;
                                                                                left: 50%;
                                                                                margin-left: -400px;
                                                                                width: 800px;
                                                                                /*height: 50%;*/
                                                                                padding: 20px;
                                                                                /*border: 16px solid orange;*/
                                                                                background-color: white;
                                                                                z-index:9999;

                                                                                box-shadow: 0 0 10px rgba(0,0,0,0.3);
                                                                            } .white_content<?php echo $get_the_id; ?> > a
                                                                            {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                                                                             font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
                                                                             background: #70C0EF;
                                                                             border-radius: 100px;}   

                                                                            .white_content<?php echo $get_the_id; ?> input[type="text"]
                                                                            {height:50px; width: 100%; border:1px solid #ccc;}

                                                                            .white_content<?php echo $get_the_id; ?> input[type="submit"]
                                                                            {position: absolute; right:0; top: 0; height: 50px;}

                                                                            .white_content<?php echo $get_the_id; ?> form
                                                                            {position: relative;}
                                                                        </style>
                                                                        <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'block';
                                                                                document.getElementById('fade').style.display = 'block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
                                                                        <div id=".black_overlay<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
                                                                            <a class="close_icon" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display = 'none';
                                                                                    document.getElementById('fade').style.display = 'none'">X</a>

                                                                            <?php
                                                                            $current_user = wp_get_current_user();
                                                                            $current_user_id = $current_user->ID;
                                                                            ?>

                                                                            <form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
                                                                                <input type="text" name="review_title" placeholder="Review Title*" required/>
                                                                                <select name="review_starts">
                                                                                    <option name="0">Select Review star:</option>
                                                                                    <option name="1">1</option>
                                                                                    <option name="2">2</option>
                                                                                    <option name="3">3</option>
                                                                                    <option name="4">4</option>
                                                                                    <option name="5">5</option>
                                                                                </select>
                                                                                <input type="text" name="review_description" placeholder="Review Description*" required/>
                                                                                <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
                                                                                <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
                                                                                <input type="submit" name="submit" value="submit" />
                                                                            </form>
                                                                        </div>





                                                                        <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php $count++; ?>
                                                            <?php
                                                        endwhile;
                                                    endif;
                                                    ?>     
                                                    <style>
                                                        .masanry-gallery{max-width: 1100px;width: 100%;margin:0 auto;}	

                                                        .row1 {
                                                            width: 32%;
                                                            float: left;
                                                            margin-right: 20px;
                                                        }
                                                        .row1:nth-child(2) .box1 {
                                                            height: 100% !important;
                                                        }
                                                        .box1 {

                                                            width: 100%;
                                                            float: left;
                                                            height: 100%;
                                                            margin: 1px;
                                                            font-size: 30px;
                                                            text-align: center;
                                                            color: #fff;
                                                        }
                                                    </style>
                                                    <div class="masanry-gallery">
                                                        <div class="row1 ">
                                                            <div class="box1" id="box_1">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_1").append($(".art").html());
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="box1" id="box_4">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_4").append($(".art3").html());
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="box1" id="box_7">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_7").append($(".art6").html());
                                                                    });
                                                                </script>
                                                            </div>	
                                                        </div>
                                                        <div class="row1 row2">
                                                            <div class="box1" id="box_2">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_2").append($(".art1").html());
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="box1" id="box_6">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_6").append($(".art5").html());
                                                                    });
                                                                </script>
                                                            </div>	
                                                        </div>
                                                        <div class="row1 row3">
                                                            <div class="box1" id="box_3">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_3").append($(".art2").html());
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="box1" id="box_5">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_5").append($(".art4").html());
                                                                    });
                                                                </script>
                                                            </div>
                                                            <div class="box1" id="box_8">
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $("#box_8").append($(".art7").html());
                                                                    });
                                                                </script>
                                                            </div>	
                                                        </div>
                                                    </div>
                                                    <div class="pagination-compition">
                                                        <?php
                                                        $page_links = paginate_links(array(
                                                            'base' => add_query_arg('pagenum', '%#%'),
                                                            'format' => '',
                                                            'prev_text' => __('&laquo;', 'text-domain'),
                                                            'next_text' => __('&raquo;', 'text-domain'),
                                                            'total' => $num_of_pages,
                                                            'current' => $pagenum
                                                        ));

                                                        if ($page_links) {
                                                            echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
                                                        }
                                                        ?>
                                                    </div>  </div>

                                                <?php //get_footer();   ?>

                                                <?php wp_reset_query(); ?>
                                            </div>
                                            <?php
                                        }
                                        if (!is_user_logged_in()) {
                                            $user = get_user_by('ID', $_GET['user_id']);
                                            ?>
                                            <div id="aboutme" class="tabcontent">
                                                <div class="section-second">

                                                    <?
                                                    $author_queryb = array('post_type' => 'about_us',
                                                        'posts_per_page' => '1', 'author' => $user_name);
                                                    $author_postsb = new WP_Query($author_queryb);
                                                    while ($author_postsb->have_posts()) : $author_postsb->the_post();
                                                        ?>

                                                        <div class="second-section-authorname"><h2>About  <?php echo the_title(); ?></div>


                                                        <strong>  <div class="section-second-title">  <?php
                                                                $des = get_post_meta(get_the_ID(), 'artist_statement', true);
                                                                echo $shortexcerpt = wp_trim_words($des, $num_words = 250, $more = '');
                                                                ?></div></strong>
                                                        <div class="section-second-content"><? echo the_content(); ?></div>
                                                    <? endwhile; ?>
                                                </div>
                                            </div>
                                        <?php } ?>         
                                        <?php
                                        if (!is_user_logged_in()) {

                                            global $wpdb;
//print_r($author_postsa);
                                            $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

                                            $limit = 4; // number of rows in page
                                            $offset = ( $pagenum - 1 ) * $limit;
                                            $table_name = $wpdb->prefix . "posts";
                                            $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type = 'post' AND post_author=" . $user_id);
                                            $num_of_pages = ceil($total / $limit);

//echo "<pre>";
// print_r($total);
                                            $user = get_user_by('ID', $_GET['user_id']);
                                            ?>
                                            <div id="myblog" class="tabcontent">
                                                <div class="third-section">
                                                    <div class="third-section-authorname">Blog <?php $user_name; ?></div>
                                                    <?php
                                                    $author_queryc = array('posts_per_page' => $limit, 'author' => $user_id, 'paged' => $pagenum);


                                                    $author_postsc = new WP_Query($author_queryc);
// echo "<pre>";
// print_r($author_postsc);

                                                    while ($author_postsc->have_posts()) : $author_postsc->the_post();
                                                        ?>
                                                        <div class="section-third-inner aaa">
                                                            <?php if (has_post_thumbnail()) {
                                                                ?>
                                                                <div class="section-third-thumb 3"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail(); ?></a></div>
                                                            <?php } else {
                                                                ?>
                                                                <div class="section-third-thumb 4"><a href="<?php echo the_permalink(); ?>"><img src="/wp-content/themes/marketify-child/images/dummy.jpg"></a></div>
                                                            <?php } ?>

                                                            <div class="section-third-date"> <? echo get_the_date(); ?></div>
                                                            <div class="section-third-title"><? echo the_title(); ?></div>


                                                            <div class="section-third-content"><? echo the_excerpt(); ?></div>
                                                        </div>
                                                    <?php endwhile; ?>		






                                                </div>
                                                <div class="pagination-compition">
                                                    <?php
                                                    $page_links = paginate_links(array(
                                                        'base' => add_query_arg('pagenum', '%#%'),
                                                        'format' => '',
                                                        'prev_text' => __('&laquo;', 'text-domain'),
                                                        'next_text' => __('&raquo;', 'text-domain'),
                                                        'total' => $num_of_pages,
                                                        'current' => $pagenum
                                                    ));

                                                    if ($page_links) {
                                                        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>


                                        <?php } ?> 






                                        <?php
                                        if (!is_user_logged_in()) {
                                            $user = get_user_by('ID', $_GET['user_id']);
                                            ?>
                                            <div id="contact" class="tabcontent">
                                                <div class="third-section">
                                                    <div class="third-section-authorname">Contact  <?php // echo  $user_name;                                                                                                                                                                                                                                                            ?></div>
                                                    <form name="contactusartist" method="post">
                                                        <input  class="input_uk" type="text" name="ftext" placeholder="First name*" required />
                                                        <input class="input_us" type="text" name="ltext" placeholder="Surname*" required />
                                                        <textarea rows="4" cols="50" name="ymessage" placeholder="Your message*" required>
                                                        </textarea>
                                                        <input type="hidden" name="currentuser" value=" <?php echo $user->display_name; ?>" />
                                                        <input class="input_submit" type="submit" name="submit" value="Send" />
                                                        <p>You can only contact the artist, if you have a buyer account. If you don't please <a href="<?php echo site_url(); ?>/ukarts/uk-artists-collection/">signup</a> for free.</p>
                                                    </form>
                                                    <?php
                                                    if (isset($_POST["submit"])) {
//echo "Yes";
                                                        $firstname = $_POST['ftext'];
                                                        $surname = $_POST['ltext'];
                                                        $message = $_POST['ymessage'];
                                                        $currentuser = $_POST['currentuser'];

                                                        global $wpdb;
//$wpdb->query($sql);
                                                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname, $surname, $message, $currentuser);
                                                        $wpdb->query($sql);
                                                    } else {
//  echo "N0";
                                                    }


                                                    global $wpdb;
//print_r($current_user);
                                                    $current_user_name = $user->user_login;
                                                    $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
//print_r($curentuser_contact);
                                                    foreach ($curentuser_contact as $curentuser_contacted) {
                                                        $current_first = $curentuser_contacted->firstname;
                                                        $current_surename = $curentuser_contacted->surname;
                                                        $current_message = $curentuser_contacted->message;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <style>
                                            #myModal12 {
                                                z-index: 99999 !important;
                                            }
                                            #myModal12 button.close {
                                                background: #dedc00;
                                                opacity: 1;
                                                color: #fff;
                                                border-radius: 50%;
                                                width: 30px;
                                                padding: 1px 0px;
                                                position: absolute;
                                                right: -10px;
                                                top: -10px;
                                            }

                                            #myModal12 input.input_uk {
                                                border: 1px solid #ccc;
                                                width: 48%;
                                                float: left;
                                                margin-right: 0px !important;
                                                background-color: #f2f2f2;
                                                font-size: 15px;
                                                color: #000;
                                            }
                                            #myModal12 input.input_submit {
                                                border: none;
                                                background: #dedc00;
                                                padding: 10px 60px;
                                                margin-right: 40px;
                                            }
                                            a.input_submit {
                                                border: none;
                                                background: #8BAEBC;
                                                padding: 10px 40px;
                                                margin-right: 50px;
                                                color: #000 !important;
                                            }

                                            #myModal12 input.input_us {
                                                border: 1px solid #ccc;
                                                width: 48%;
                                                float: right;
                                                background-color: #f2f2f2;
                                                font-size: 15px;
                                                color: #000;
                                            }
                                            #myModal12 input.email-cc {
                                                width: 100%;
                                                border: 1px solid #ccc;
                                                background-color: #f2f2f2;
                                                font-size: 15px;
                                                color: #000;
                                            }
                                            #myModal12 .modal-content {
                                                width: 100%;
                                                padding: 30px;
                                            }
                                            #myModal12 .third-section-authorname {
                                                text-align: center;
                                                font-size: 24px;
                                                font-weight: 600;
                                                margin: 15px 0;
                                            }
                                            #myModal12 a {
                                                color: #dedc00;
                                            }
                                            #myModal12 .modal-dialog {
                                                width: 800px;
                                            }
                                        </style>
                                        <div id="myModal12" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="gfg">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                    </div>
                                                    <div class="welcome-popup" style="display:none;">
                                                        <h1 class="welcome-title"><img src="/wp-content/uploads/2018/03/smily-Welcome.png">Welcom to UK Arts</h1>
                                                        <p class="welcome-des">Please check your for our verification email and the link to complete your profile. We look forward to </br>sharing the world of UK Art with you</p>

                                                        <div class="wel-close-button" data-dismiss="modal">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>


                                                    </div>
                                                    <div id="lower_div" class="">
                                                        <div class="third-section">
                                                            <?php if (!is_user_logged_in()) { ?>
                                                                <div class="third-section-authorname">Join Us Today</div>

                                                                <?php
                                                            }
                                                            if (is_user_logged_in()) {
                                                                ?>
                                                                <div class="third-section-authorname" style="text-transform: capitalize;">Contact 
                                                                    <?php
                                                                    global $wpdb;
                                                                    $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'first_name'");
                                                                    $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user_id . " AND meta_key = 'last_name'");
                                                                    echo $findu[0]->meta_value;
                                                                    echo " ";
                                                                    echo $findu1[0]->meta_value;
//echo $user_name;  
                                                                    ?></div>
                                                            <?php } ?>
                                                            <form name="contactusartist" method="post">
                                                                <input  class="input_uk" type="text" id="f_n" name="ftext" placeholder="First name*" required />
                                                                <input class="input_us" type="text" id="l_n" name="ltext" placeholder="Surname*" required />
                                                                <?php
                                                                if (is_user_logged_in()) {
                                                                    $us = $_GET['user_id'];
                                                                    $user_info = get_userdata($us);
//echo "<pre>";
//print_r($user_info);
// echo $user_info->user_email;
                                                                    ?>
                                                                    <textarea class="email-cc" id="t_mes" name="ymessage" placeholder="Message*"></textarea>
                                                                <?php } if (!is_user_logged_in()) { ?>
                                                                    <input class="email-cc" type="email" id="u_e" name="ymessage" placeholder="Email*" onblur="return validateForm();" required>
                                                                    <span id="email_error"></span>
                                                                    <span id="email_errors"></span>
                                                                    <script>
                                                                        function validateForm() {
                                                                            var x = $("#u_e").val();
                                                                            var atpos = x.indexOf("@");
                                                                            var dotpos = x.lastIndexOf(".");

                                                                            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                                                                                $("#email_error").html("<p style='color:red'>Not a valid e-mail address</p>");
                                                                                $("#email_errors").html("");
                                                                                return false;
                                                                            } else {
                                                                                $("#email_error").html("");
                                                                                //alert(email);
                                                                                //alert(x);
                                                                                $.ajax({
                                                                                    type: 'POST',
                                                                                    data: "email=" + x + "&action3=email_sub",
                                                                                    url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                                                                    success: function (data) {
                                                                                        //$(".chat_message").html(result);
                                                                                        //alert(data);
                                                                                        if (data == "yes") {
                                                                                            $("#email_errors").html("<p style='color:red'> Email already Exists</p>");

                                                                                        } else {
                                                                                            //alert("gcfvfd");
                                                                                            $('#j_sub').prop('disabled', false);
                                                                                            //$("#email_errors").html("dddddd");
                                                                                        }


                                                                                        //$('#dddd').html(result);
                                                                                    },
                                                                                    error: function (error) {
                                                                                        //alert("dd");
                                                                                    }
                                                                                });

                                                                            }
                                                                        }
                                                                    </script>
                                                                <?php } ?>
                                                                <input type="hidden" name="currentuser" value="<?php echo $current_user->display_name; ?>"> 
                                                                <input type="hidden" name="cu_email" id="c_id" value="<?php echo $current_user->user_email; ?>">
                                                                <input type="hidden" name="u_email" id="u_id" value="<?php echo $user_info->user_email; ?>">
                                                                <p>By Clicking 'Join' you confirm that you have read and accepted our <a href="http://beta.uk-arts.com/terms-conditions/">T&C.</a>  </p>
                                                                <?php if (is_user_logged_in()) { ?>
                                                                    <input class="input_submit" type="submit" name="submit" id="e_sub" value="Contact" />
                                                                <?php } if (!is_user_logged_in()) { ?>
                                                                    <input class="input_submit" type="submit" name="submit" id="j_sub" value="Join" />

                                                                <?php } if (!is_user_logged_in()) { ?>
                                                                    <a href="http://beta.uk-arts.com/login/" class="input_submit">Already Join?Login here</a>
                                                                <?php } ?>
                                                            </form>

                                                            <?php
                                                            if (isset($_POST["submit"])) {
//echo "Yes";
                                                                $firstname = $_POST['ftext'];
                                                                $surname = $_POST['ltext'];
                                                                $message = $_POST['ymessage'];
                                                                $currentuser = $_POST['currentuser'];

                                                                global $wpdb;
//$wpdb->query($sql);
                                                                $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname, $surname, $message, $currentuser);
                                                                $wpdb->query($sql);
                                                            } else {
//  echo "N0";
                                                            }


                                                            global $wpdb;
//print_r($current_user);
                                                            $current_user_name = $current_user->user_login;
                                                            $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
//print_r($curentuser_contact);
                                                            foreach ($curentuser_contact as $curentuser_contacted) {
                                                                $current_first = $curentuser_contacted->firstname;
                                                                $current_surename = $curentuser_contacted->surname;
                                                                $current_message = $curentuser_contacted->message;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.css">
                                        <style>.welcome-popup {
                                                background: #fff;
                                                border-radius: 20px;
                                                -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
                                                -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
                                                box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);
                                                padding: 30px 75px;
                                                width: 100%;
                                                max-width: 1000px;
                                                margin: 0 auto;
                                            }
                                            .welcome-title {
                                                text-align: center;
                                                margin: 25px 0 40px 0;
                                                font-size: 30px;
                                                min-height: 50px;
                                                display: flex;
                                                justify-content: center;
                                            }
                                            .welcome-title img {
                                                height: 38px;
                                                margin-right: 10px;
                                            }

                                            .welcome-des {
                                                font-size: 20px;
                                                text-align: left;
                                                letter-spacing: 0px;
                                                line-height: 20px;
                                            }
                                            .wel-close-button .fa.fa-times {
                                                border-radius: 50%;
                                                width: 36px;
                                                border: 1px solid #ccc;
                                                padding: 10px 0;
                                                color: #ccc;
                                            }
                                            .wel-close-button {
                                                text-align: center;
                                            }

                                        </style>



                                        <?php
                                        if (!is_user_logged_in()) {
                                            $user = get_user_by('ID', $_GET['user_id']);
                                            ?>
                                            <div id="review" class="tabcontent">
                                                <div class="review-main-section">
                                                    <h2>Artist Reviews</h2>
                                                    <div class="review-overall">
                                                        <?php
                                                        $row = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 5");
                                                        $max_rating = $row[0]->max_rating;

                                                        if (!empty($row[0]->aggregate_rating)) {
                                                            $aggregate_rating = $row[0]->aggregate_rating;
                                                        } else {
                                                            $aggregate_rating = 0;
                                                        }
                                                        $err = round($aggregate_rating);
                                                        $total_reviews = $row[0]->total;
                                                        $totl = $aggregate_rating * 20;
                                                        $wpdb->flush();
                                                        $y5 = 100;
                                                        $percent_5 = $total_reviews / $y5;
                                                        $percent_width_5 = $percent_5 * 100;
                                                        ?>
                                                        <div class="row first_reviews">
                                                            <div class="first_line">     
                                                                <div class="side">
                                                                    <div>5 star</div>
                                                                </div>
                                                                <div class="middle">
                                                                    <div class="bar-container">
                                                                        <div class="bar-5" style="width:<?php echo $percent_width_5; ?>% !important;"></div>
                                                                    </div>
                                                                </div> 
                                                                <div class="side right">
                                                                    <div><?php echo $total_reviews; ?></div>
                                                                </div>

                                                                <div class="review-inner-part"><span>Seller Communication </span>  <?php
                                                                    global $wpdb;
//print_r($current_user);
                                                                    $current_user_name = $user_name;
                                                                    $avgsell = $wpdb->get_results("SELECT AVG(sellercomm) as seller FROM artistreviews where artistlist= '$user_id'");
                                                                    $average_sell = $avgsell[0]->seller;
                                                                    $average_sell = round($average_sell);
                                                                    if ($average_sell == "1") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                    } elseif ($average_sell == "2") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                    } elseif ($average_sell == "3") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                    } elseif ($average_sell == "4") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                    } elseif ($average_sell == "5") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                    } else {
//echo "fff";
                                                                    }
                                                                    ?> </div>




                                                                <div class="review-inner-part macrto"><span class="ght">Overall</span> <?php
                                                                    global $wpdb;
//print_r($current_user);
                                                                    $current_user_name = $user_name;
                                                                    $avgsell = $wpdb->get_results("SELECT AVG(artistid) as artseller FROM artistreviews where artistlist= '$user_id'");
                                                                    $avg_artseller = $avgsell[0]->artseller;
                                                                    $avg_artseller = round($avg_artseller);
                                                                    if ($avg_artseller == "1") {
                                                                        ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                        } elseif ($avg_artseller == "2") {
                                                                            ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                        } elseif ($avg_artseller == "3") {
                                                                            ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                        } elseif ($avg_artseller == "4") {
                                                                            ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                        } elseif ($avg_artseller == "5") {
                                                                            ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                        } else {
                                                                            
                                                                        }
                                                                        ?> </div>



                                                            </div>
                                                            <?php
                                                            $row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 4");
                                                            $max_rating = $row[0]->max_rating;
                                                            if (!empty($row[0]->aggregate_rating)) {
                                                                $aggregate_rating = $row1[0]->aggregate_rating;
                                                            } else {
                                                                $aggregate_rating = 0;
                                                            }
                                                            $erro = round($aggregate_rating);
                                                            $total_reviews = $row1[0]->total;
                                                            $totl = $aggregate_rating * 20;
                                                            $wpdb->flush();
                                                            $y4 = 100;
                                                            $percent_4 = $total_reviews / $y4;
                                                            $percent_width_4 = $percent_4 * 100;
                                                            ?>
                                                            <div class="first_line">  
                                                                <div class="side">
                                                                    <div>4 star</div>
                                                                </div>
                                                                <div class="middle">
                                                                    <div class="bar-container">
                                                                        <div class="bar-4" style="width:<?php echo $percent_width_4; ?>% !important;">></div>
                                                                    </div></div>
                                                                <div class="side right">
                                                                    <div><?php echo $total_reviews; ?></div>
                                                                </div>
                                                                <div class="review-inner-part"><span>Delivery and Packing </span>  <?php
                                                                    global $wpdb;
//print_r($current_user);
                                                                    $current_user_name = $user_name;
                                                                    $avgsell = $wpdb->get_results("SELECT AVG(deliverynpaint) as deliseller FROM artistreviews where artistlist= '$user_id'");
                                                                    $average_delisell = $avgsell[0]->deliseller;
                                                                    $average_delisell = round($average_delisell);
                                                                    if ($average_delisell == "1") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                    } elseif ($average_delisell == "2") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                    } elseif ($average_delisell == "3") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                    } elseif ($average_delisell == "4") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                    } elseif ($average_delisell == "5") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                    } else {
                                                                        
                                                                    }
                                                                    ?> </div>




                                                            </div> 
                                                            <?php
                                                            $row2 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 3");
                                                            $max_rating = $row[0]->max_rating;
                                                            if (!empty($row[0]->aggregate_rating)) {
                                                                $aggregate_rating = $row2[0]->aggregate_rating;
                                                            } else {
                                                                $aggregate_rating = 0;
                                                            }
                                                            $err3 = round($aggregate_rating);
                                                            $total_reviews = $row2[0]->total;
                                                            $totl = $aggregate_rating * 20;
                                                            $wpdb->flush();
                                                            $y3 = 100;
                                                            $percent_3 = $total_reviews / $y3;
                                                            $percent_width_3 = $percent_3 * 100;
                                                            ?> 
                                                            <div class="first_line">   
                                                                <div class="side">
                                                                    <div>3 star</div>
                                                                </div>
                                                                <div class="middle">
                                                                    <div class="bar-container">
                                                                        <div class="bar-3" style="width:<?php echo $percent_width_3; ?>% !important;"></div>
                                                                    </div></div>
                                                                <div class="side right">
                                                                    <div><?php echo $total_reviews; ?></div>
                                                                </div>

                                                                <div class="review-inner-part"><span>Listing Accuracy </span>  <?php
                                                                    global $wpdb;
//print_r($current_user);
                                                                    $current_user_name = $user_name;
                                                                    $avgsell = $wpdb->get_results("SELECT AVG(listingacc) as listseller FROM artistreviews where artistlist= '$user_id'");
                                                                    $average_listsell = $avgsell[0]->listseller;
                                                                    $average_listsell = round($average_listsell);

                                                                    if ($average_listsell == "1") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                    } elseif ($average_listsell == "2") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                    } elseif ($average_listsell == "3") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                    } elseif ($average_listsell == "4") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                    } elseif ($average_listsell == "5") {
                                                                        ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                    } else {
                                                                        
                                                                    }
                                                                    ?> </div>



                                                            </div>
                                                            <?php
                                                            $row3 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 2");
                                                            $max_rating = $row[0]->max_rating;
                                                            if (!empty($row3[0]->aggregate_rating)) {
                                                                $aggregate_rating = $row[0]->aggregate_rating;
                                                            } else {
                                                                $aggregate_rating = 0;
                                                            }
                                                            $err4 = round($aggregate_rating);
                                                            $total_reviews = $row3[0]->total;
                                                            $totl = $aggregate_rating * 20;
                                                            $wpdb->flush();
                                                            $y2 = 100;
                                                            $percent_2 = $total_reviews / $y2;
                                                            $percent_width_2 = $percent_2 * 100;
                                                            ?> 
                                                            <div class="first_line">    
                                                                <div class="side">
                                                                    <div>2 star</div>
                                                                </div>
                                                                <div class="middle">
                                                                    <div class="bar-container">
                                                                        <div class="bar-2"style="width:<?php echo $percent_width_2; ?>% !important;"></div>
                                                                    </div></div>
                                                                <div class="side right">
                                                                    <div><?php echo $total_reviews; ?></div>
                                                                </div>
                                                            </div>

                                                            <?php
                                                            $row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 1");
                                                            $max_rating = $row[0]->max_rating;
                                                            if (!empty($row[0]->aggregate_rating)) {
                                                                $aggregate_rating = $row1[0]->aggregate_rating;
                                                            } else {
                                                                $aggregate_rating = 0;
                                                            }
                                                            $err5 = round($aggregate_rating);
                                                            $total_reviews = $row1[0]->total;
                                                            $totl = $aggregate_rating * 20;
                                                            $wpdb->flush();
                                                            $y1 = 100;
                                                            $percent_1 = $total_reviews / $y1;
                                                            $percent_width_1 = $percent_1 * 100;
                                                            ?>
                                                            <div class="first_line">        
                                                                <div class="side">
                                                                    <div>1 star</div>
                                                                </div>
                                                                <div class="middle">
                                                                    <div class="bar-container">
                                                                        <div class="bar-1"style="width:<?php echo $percent_width_1; ?>% !important;"></div>
                                                                    </div></div>
                                                                <div class="side right">
                                                                    <div><?php echo $total_reviews; ?></div>
                                                                </div>
                                                            </div>

                                                            <?php $err; ?>                      
                                                            <?php $erro; ?>                      
                                                            <?php $err3; ?>                       
                                                            <?php $err4; ?>                       
                                                            <?php $err5; ?>    
                                                            <?php if ($err == "0" && $erro == "" && $err3 == "" && $err4 == "" && $err5 == "") {
                                                                ?>
                                                                <div class="no-record-found">
                                                                    <?php echo "No Record Found"; ?>
                                                                </div><?
                                                            } else { //echo "no";
                                                            }
                                                            ?>





                                                        </div>
                                                        <div class="review-inner-left">
                                                            <?php $i = 1; ?>
                                                            <?php
                                                            global $wpdb;
//print_r($current_user);
//echo $current_user_name= $current_user->display_name;
                                                            $findID = $wpdb->get_results("SELECT * FROM artistreviews where artistlist= '$user_id' ORDER BY id DESC");
//print_r($findID); 
                                                            foreach ($findID as $fivesdraft) {
                                                                ?> 
                                                                <div class="outer-review 1111">
                                                                    <div class="review-inner-main-left">

                                                                        <div class="review-inner-part"><span>Delivery and Packing </span><?php
                                                                            $deliverynpaint_pic = $fivesdraft->deliverynpaint;
                                                                            if ($deliverynpaint_pic == "1") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($deliverynpaint_pic == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($deliverynpaint_pic == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($deliverynpaint_pic == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($deliverynpaint_pic == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div class="review-inner-part"><span>Listing Accuracy </span><?php
                                                                            $listingacc_pic = $fivesdraft->listingacc;
                                                                            if ($listingacc_pic == "1") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><?
                                                                            } elseif ($listingacc_pic == "2") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><?
                                                                            } elseif ($listingacc_pic == "3") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><?
                                                                            } elseif ($listingacc_pic == "4") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><?
                                                                            } elseif ($listingacc_pic == "5") {
                                                                                ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><?
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div class="review-inner-part"><span><b>Overall Rating</b> </span><?php
                                                                            $overall_rating_con = $fivesdraft->artistid;
                                                                            $overall_rating_con = round($overall_rating_con);

                                                                            if ($overall_rating_con == "1") {
                                                                                ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /></span><?
                                                                                    } elseif ($overall_rating_con == "2") {
                                                                                        ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /></span><?
                                                                                } elseif ($overall_rating_con == "3") {
                                                                                    ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /></span><?
                                                                                } elseif ($overall_rating_con == "4") {
                                                                                    ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /></span><?
                                                                                } elseif ($overall_rating_con == "5") {
                                                                                    ?><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /></span><?
                                                                                } else {
                                                                                    
                                                                                }
                                                                                ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="review-inner-main-right">
                                                                        <div class="review-inner-part">
                                                                            <?php
                                                                            $wp_user_search = $wpdb->get_results("SELECT ID, user_nicename FROM $wpdb->users WHERE ID='$fivesdraft->username'");
//print_r($wp_user_search);
                                                                            foreach ($wp_user_search as $page) {
                                                                                $page->user_nicename;
//echo $page['user_nicename']; 
                                                                                ?>
                                                                                <h2><?php echo $page->user_nicename; ?></h2>
                                                                            <?php } ?>

                                                                            <span>
                                                                                <?php $date_publish = $fivesdraft->date; ?>
                                                                                <?php echo $newDate = date("d/m/Y", strtotime($date_publish)); ?>
                                                                                <?php //echo $fivesdraft->date;    ?></span>
                                                                            <p><?php echo $fivesdraft->discription; ?></p>
                                                                            <?php
                                                                            $reviewid = $fivesdraft->ID;
//  $userss = wp_get_current_user();
//echo $userss->ID.'jjjjj';
//echo $userss->ID;  
                                                                            ?>    
                                                                            <?php if (is_user_logged_in() && $userss->ID == '$user_id') { ?>
                                                                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">Reply</button>
                                                                            <?php } ?>

                                                                            <div class="container">
                                                                                <!--h2>Modal Example</h2-->
                                                                                <!-- Trigger the modal with a button -->

                                                                                <!-- Modal -->
                                                                                <div class="modal fade" id="myModal<?php echo $i; ?>" role="dialog">
                                                                                    <div class="modal-dialog">

                                                                                        <!-- Modal content-->
                                                                                        <div class="modal-content">
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                <h4 class="modal-title"><?php echo $user_name; ?> reply</h4>
                                                                                            </div>


                                                                                            <div class="modal-body">

                                                                                                <form name="artist-inert-table" method="post">
                                                                                                    <input type="hidden" class="reviewid<?php echo $i; ?>" value="<?php echo $reviewid; ?>" name="reviewid">
                                                                                                    <div class="two-section-table">
                                                                                                        <div class="two-section-tableinner">
                                                                                                            <input type="text" class="reply<?php echo $i; ?>" name="reply" placeholder="Reply"/>
                                                                                                        </div>
                                                                                                    </div>      

                                                                                                </form>
                                                                                                <style>
                                                                                                    .modal-dialog {
                                                                                                        padding-top: 201px;
                                                                                                    }
                                                                                                </style>

                                                                                            </div>
                                                                                            <div class="modal-footer">

                                                                                                <?php echo $fivesdraft->reply; ?>
                                                                                                <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm<?php echo $i; ?>()">SUBMIT</button>
                                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <script>

                                                                                function submitContactForm<?php echo $i; ?>() {
                                                                                    var artist_id = $('.reviewid<?php echo $i; ?>').val();
                                                                                    //alert(artist_id);
                                                                                    var reply = $('.reply<?php echo $i; ?>').val();
                                                                                    /// alert(reply);
                                                                                    $.ajax({
                                                                                        type: "POST",
                                                                                        url: "<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php",
                                                                                        data: "artist_id=" + artist_id + "&reply=" + reply,
                                                                                        cache: false,
                                                                                        success: function (html) {
                                                                                            ///alert("sdjkffhdkj"); 
                                                                                            location.reload();
                                                                                        },
                                                                                        error: function (error) {
                                                                                            //alert(error);
                                                                                        }
                                                                                    });
                                                                                }

                                                                            </script>
                                                                            <?php ++$i; ?> 

                                                                            <p class="reply-message"><strong><?php echo $user_name; ?> Reply:</strong></p>
                                                                            <?php echo $fivesdraft->reply; ?>  </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                </hr>

                                                            <?php } ?>
                                                        </div>
                                                        <div class="review-inner-right">
                                                            <div class="review-inner-part">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                        ?>






                                        <style>
                                            .download-current-user-inner {
                                                width: 30%;
                                                float: left;
                                                margin-right: 2%;
                                                /* height: 300px; */
                                                overflow: hidden;
                                                margin-bottom: 3%;
                                            }
                                            .download-current-thumbnail { height: 300px; }
                                            .artist-work-button {
                                                background-color: #dedc00;
                                                width: 550px;
                                                padding: 8px 19px 1px 12px;
                                                margin-right: 43px;
                                            }
                                            .section-third-inner {
                                                width: 50%;
                                                float: left;
                                            } 
                                            div#artist {
                                                display: block;  
                                            }

                                            .tab button.active {
                                                background-color: #dedc00;
                                            }
                                            #aboutme a.linkw {
                                                color: #dedc00;
                                                width: 100%;
                                            }
                                            }
                                        </style>

                                        <div id="myModal" class="modal myModal container">
                                            <div class="modal-content">
                                                <span class="close">&times;</span>
                                                <div class="col-sm-12" ><?php
                                                    $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
                                                    $paypalID = 'munishgup-facilitator@gmail.com'; //Business Email
                                                    ?>
                                                    <form action="<?php echo $paypalURL; ?>" method="post">
                                                        <label>Amount Agreed</label>
                                                        <input type="text" name="agree_budgt" id="agree_budgt" value="">
                                                        <label>Amount Pending</label>
                                                        <input type="text" name="first_milestone" id="first_budget" value="" readonly>
                                                        <label>Amount Deposite</label>
                                                        <input type="text" name="second_milestone" id="second_budget" value="" readonly>
                                                        <!-- Specify details about the item that buyers will purchase. -->
                                                        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">

                                                        <!-- Specify a Buy Now button. -->
                                                        <input type="hidden" name="cmd" value="_xclick">

                                                        <input type="hidden" name="item_name" id="refrence_id" value="">
                                                        <input type="hidden" name="item_number" id="item_number" value="">
                                                        <input type="hidden" name="amount" id="amount_main" value="">
                                                        <input type="hidden" name="currency_code" value="USD">

                                                        <!-- Specify URLs -->
                                                        <input type='hidden' name='cancel_return' value='<?php echo site_url(); ?>/ukarts/'>
                                                        <input type='hidden' name='return' value='<?php echo site_url(); ?>/ukarts/confirm-payment'>
                                                        <!--<input type="image" name="submit" border="0"
                                                        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
                                                        <!-- Display the payment button. -->

                                                        <input type="submit" name="hire" value="confirm Hire">
                                                    </form>

                                                </div>




                                            </div>
                                        </div> 

                                        <?php $ggd = $_GET['user_id']; ?>
                                        <div id="fade" class="black_overlay"></div>
                                        <!--- social link startGGG -->
                                        <?php
                                        if (!is_user_logged_in()) {
                                            if (isset($_GET['user_id'])) {
                                                ?>
                                                <div class="follow-us-social" id="blog_social_8">
                                                    <div class="footer-social">
                                                        Follow me on   
                                                        <?php
                                                        $author_querya = array('post_type' => 'social_accounts',
                                                            'posts_per_page' => '1', 'author' => $user_id);

                                                        $author_postsa = new WP_Query($author_querya);
//print_r($author_postsa);
                                                        if ($author_postsa->have_posts()) :
//echo count($author_postsa->have_posts());
                                                            while ($author_postsa->have_posts()) :
                                                                $author_postsa->the_post();
                                                                $get_the_id = get_the_id();
                                                                $twiter_acccv = get_user_meta($ggd, 'twiter_accc', true);
                                                                if ($twiter_acccv) {
                                                                    ?> 
                                                                    <a href="<?php echo $twiter_acccv; ?>" target="_blank"> <span class="screen-reader-text">Twitter</span></a> 
                                                                <? } else { ?> <?
                                                                }

                                                                $instagram_accc = get_user_meta($ggd, 'instagram_acc', true);
                                                                if ($instagram_accc) {
                                                                    ?>
                                                                    <a href="<?php echo $instagram_accc; ?>" target="_blank"><span class="screen-reader-text">Instagram</span></a>
                                                                <? } else { ?>
                                                                    <?
                                                                }

                                                                $facebook_accc = get_user_meta($ggd, 'facebook_acc', true);
                                                                if ($facebook_accc) {
                                                                    ?>
                                                                    <a href="<?php echo $facebook_accc; ?>" target="_blank"><span class="screen-reader-text">Facebook</span></a>
                                                                <? } else { ?><?php
                                                                }

                                                                $Pintrest_accc = get_user_meta($ggd, 'Pintrest_acc', true);
                                                                if ($Pintrest_accc) {
                                                                    ?>
                                                                    <a href="<?php echo $Pintrest_accc; ?>" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
                                                                <? } else { ?> 
                                                                    <?
                                                                }

                                                            endwhile;
                                                        else :


                                                            $twiter_acccv = get_user_meta($ggd, 'twiter_accc', true);
                                                            if ($twiter_acccv) {
                                                                ?> 
                                                                <a href="<?php echo $twiter_acccv; ?>" target="_blank"> <span class="screen-reader-text">Twitter</span></a> 
                                                            <? } else { ?> <?
                                                            }

                                                            $instagram_accc = get_user_meta($ggd, 'instagram_acc', true);
                                                            if ($instagram_accc) {
                                                                ?>
                                                                <a href="<?php echo $instagram_accc; ?>" target="_blank"><span class="screen-reader-text">Instagram</span></a>
                                                            <? } else { ?>
                                                                <?
                                                            }

                                                            $facebook_accc = get_user_meta($ggd, 'facebook_acc', true);
                                                            if ($facebook_accc) {
                                                                ?>

                                                                <a href="<?php echo $facebook_accc; ?>" target="_blank"><span class="screen-reader-text">Facebook</span></a>
                                                            <? } else { ?> <?php
                                                            }

                                                            $Pintrest_accc = get_user_meta($ggd, 'Pintrest_acc', true);
                                                            if ($Pintrest_accc) {
                                                                ?>
                                                                <a href="<?php echo $Pintrest_accc; ?>" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
                                                            <? } else { ?> 
                                                            <? } ?><?php
                                                        endif;
                                                        ?>
                                                    </div> 



                                                </div> 
                                                <div class="follow-us-social" id="blog_social_9" >
                                                    <?php
                                                    $Website_accc = get_user_meta($ggd, 'Website_acc', true);
                                                    if ($Website_accc) {
                                                        ?>
                                                        <div class="visit-or-website">  

                                                            <a href="<?php echo $Website_accc; ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
                                                        </div>
                                                    </div>  
                                                <?php } ?> 


                                            <? } else { ?>	    

                                                <?php
                                            }
                                        }
                                        ?>






                                        <script>
                                            $(document).ready(function () {
                                                $("#blog_social_8").hide();
                                                $("#blog_social_9").hide();

                                            });
                                            $(".myBtn").click(function () {

                                                $(".myModal").css("display", "block");
                                                var amount = $(this).attr('data-amount');
                                                var half = amount / 2;

                                                var ref_id = $(this).attr("data-refid");
                                                $("#agree_budgt").val(amount);
                                                $("#first_budget").val(half);
                                                $("#second_budget").val(half);
                                                $("#refrence_id").val(ref_id);
                                                $("#amount_main").val(half);
                                                var author = $(this).attr('data-author');
                                                var item_name = amount + '_' + half + '_<?php echo get_current_user_id(); ?>_' + author;
                                                $("#item_number").val(item_name);
                                            });

                                            $("#agree_budgt").blur(function () {
                                                var aaaaa = $(this).val();
                                                var bbb = aaaaa / 2;
                                                $("#first_budget").val(bbb);
                                                $("#second_budget").val(bbb);
                                                $("#amount_main").val(bbb);

                                            });

                                            // Get the modal

                                            var modal = document.getElementsByClassName('myModal')[0];

                                            // Get the button that opens the modal
                                            var btn = document.getElementsByClassName("myBtn")[0];

                                            // Get the <span> element that closes the modal
                                            var span = document.getElementsByClassName("close")[0];

                                            // When the user clicks the button, open the modal 
                                            btn.onclick = function () {
                                                modal.style.display = "block";
                                                //  alert

                                            }

                                            // When the user clicks on <span> (x), close the modal
                                            span.onclick = function () {
                                                modal.style.display = "none";
                                            }
                                            // When the user clicks anywhere outside of the modal, close it
                                            window.onclick = function (event) {
                                                if (event.target == modal) {
                                                    modal.style.display = "none";
                                                }
                                            }
                                        </script>  </main>
                                    <!-- #main -->
                                </div>
                                <!-- #primary -->
                            </div>

                        </div>
                    </div>
                    </div>
                    </div>

                    </div> 

                    <style>
                        .new-msg{
                            display:none !important;
                        }

                        body {font-family: Arial, Helvetica, sans-serif;}

                        /* The Modal (background) */
                        .modal {
                            display: none; /* Hidden by default */
                            position: fixed; /* Stay in place */
                            z-index: 1; /* Sit on top */
                            padding-top: 100px; /* Location of the box */
                            left: 0;
                            top: 0;
                            width: 100%; /* Full width */
                            height: 100%; /* Full height */
                            overflow: auto; /* Enable scroll if needed */
                            background-color: rgb(0,0,0); /* Fallback color */
                            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                        }

                        /* Modal Content */
                        .modal-content {
                            background-color: #fefefe;
                            margin: auto;
                            padding: 20px;
                            border: 1px solid #888;
                            width: 80%;
                        }

                        /* The Close Button */
                        .close {
                            color: #aaaaaa;
                            float: right;
                            font-size: 28px;
                            font-weight: bold;
                        }

                        .close:hover,
                        .close:focus {
                            color: #000;
                            text-decoration: none;
                            cursor: pointer;
                        }
                        .container2 {
                            border: 2px solid #dedede;
                            background-color: #f1f1f1;
                            border-radius: 5px;
                            padding: 10px;
                            margin: 10px 0;
                        }

                        .darker {
                            border-color: #ccc;
                            background-color: #ddd;
                        }

                        .container2::after {
                            content: "";
                            clear: both;
                            display: table;
                        }

                        .container2 img {
                            float: left;
                            max-width: 60px;
                            width: 100%;
                            margin-right: 20px;
                            border-radius: 50%;
                        }

                        .container2 img.right {
                            float: right;
                            margin-left: 20px;
                            margin-right:0;
                        }

                        .time-right {
                            float: right;
                            color: #aaa;
                        }

                        .time-left {
                            float: left;
                            color: #999;
                        }
                        .page-id-645 p #submit {
                            position: absolute;
                            left: 20px;
                            bottom: 30px;
                            margin-top: 0;
                            float: left;
                        }
                        .page-id-645 .white_content input[type="submit"] {
                            top: auto;
                        }

                    </style>  
                    <script>
                        $(document).ready(function () {

                            $('#j_sub').prop('disabled', true);
                            $("#e_sub").click(function () {

                                var x = $("#c_id").val();
                                var f = $("#u_id").val();
                                var s = $("#t_mes").val();
                                var fn = $("#f_n").val();
                                var ln = $("#l_n").val();
                                //alert(s);
                                $.ajax({
                                    type: "POST",
                                    data: "from=" + f + "&mess=" + s + "&to=" + x + "&fname=" + fn + "&lname=" + ln + "&action9=email_sub9",
                                    url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                    success: function (data) {
                                        alert(data);

                                    }

                                });

                            });
                            $("#j_sub").click(function () {

                                var f = $("#f_n").val();
                                var l = $("#l_n").val();
                                var e = $("#u_e").val();
                                //alert(s);
                                $.ajax({
                                    type: "POST",
                                    data: "fname=" + f + "&sname=" + l + "&email=" + e + "&action8=email_sub8",
                                    url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                    success: function (data) {
                                        //alert(data);
                                        $("#lower_div").hide();
                                        $(".gfg").hide();
                                        $(".welcome-popup").show();

                                    }

                                });

                            });

                        });

                    </script>
                    <?php get_footer(); ?>  