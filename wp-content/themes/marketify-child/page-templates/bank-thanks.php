<?php
/**
 * Template Name: Layout: bank thanks 
 *
 * @package Marketify
 */
get_header();
?>

<?php do_action('marketify_entry_before'); ?>

<?php
if (isset($_POST["submit"])) {
    $accountno = $_POST['accountnoed'];
    $sortcode = $_POST['sortcode'];
    $user_id = $_POST['user_id'];
    $user_name = $_POST['user_name'];

    global $wpdb;
    $findID = $wpdb->get_results("SELECT COUNT(user_id) as coutersel FROM bankdetail where user_id=$user_id");
    $couterselrow = $findID[0]->coutersel;
    if ($couterselrow > 0) {

        global $wpdb;
        $wpdb->update('bankdetail', array('accountno' => $accountno, 'sortcode' => $sortcode, 'user_name' => $user_name
                ), array('user_id' => $user_id), array(
            '%d',
            '%d', '%s'
                ), array('%d')
        );
    } else {
        global $wpdb;
        $wpdb->insert('bankdetail', array(
            'accountno' => $accountno,
            'sortcode' => $sortcode,
            'user_id' => $user_id,
            'user_name' => $user_name,
        ));

    }
}
?> 
<style>
    .thaks-outer-msg {
        text-align: center;
        font-size: 20px;
    }
    .go-back-thanks-button {
        background: #dedc00 none repeat scroll 0 0;
        padding: 1% 5% 1% 7%;
        color: white;
    }

    .go-back-thanks-button:hover {	color: white !important; }
</style>

<div class="container">
    <div class="thaks-outer-msg">
        <div class="thanks-message">Bank Detail Updated</div>
        <div class="thanks-message"> Please click on Below link to go your Profile</div></br>
        <a class="go-back-thanks-button" href="<?php echo site_url(); ?>/artist-main/">Go Back >></a></div>

</div>

<div class="container">
    <div id="content" class="site-content row">

        <div id="primary" class="content-area col-sm-12">
            <main id="main" class="site-main" role="main">

                <?php if (have_posts()) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('content', 'page'); ?>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || '0' != get_comments_number())
                            comments_template();
                        ?>

                    <?php endwhile; ?>

                    <?php do_action('marketify_loop_after'); ?>

                <?php else : ?>

                    <?php get_template_part('no-results', 'index'); ?>

<?php endif; ?>
            </main><!-- #main -->
        </div><!-- #primary -->

    </div>
</div>
<?php get_footer(); ?>