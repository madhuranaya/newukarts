<?php

/**
 * Template Name: Layout: about thanks
 *
 * @package Marketify
 */
?>
<?php

if (isset($_POST["submited"])) {
    $userId = $_POST['userid'];
    $artistfname = $_POST['artistfname'];
    $artistlname = $_POST['artistlname'];
    $artistStmt = $_POST['artistStmt'];
    $artistdescription = $_POST['artistdescription'];
    wp_update_user(array('ID' => $userId, 'first_name' => $artistfname, 'last_name' => $artistlname));
    update_user_meta($userId, 'artistStmt', $artistStmt);
    update_user_meta($userId, 'description', $artistdescription);
    if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'update-user') {

        // These files need to be included as dependencies when on the front end.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        // Let WordPress handle the upload.
        $img_id = media_handle_upload('avatar_user', 0);

        if (is_wp_error($img_id)) {
            echo "Error";
        } else {
            $attachment_ID = update_user_meta($current_user->ID, 'avatar_user', $img_id);
            $attachmentURL = wp_get_attachment_url($img_id);
            $result = update_user_meta($current_user->ID, 'bannerImage', $attachmentURL);
        }
    }


    header("location:" . site_url() . "/artist-main/");
} else {
    echo "N0";
}
get_header();
do_action('marketify_entry_before');
?>	
<style>
    .thaks-outer-msg {
        text-align: center;
        font-size: 20px;
    }
    .go-back-thanks-button {
        background: #dedc00 none repeat scroll 0 0;
        padding: 1% 5% 1% 7%;
        color: white;
    }
    .go-back-thanks-button:hover {	color: white !important; }
</style>	
<?php get_footer(); ?>