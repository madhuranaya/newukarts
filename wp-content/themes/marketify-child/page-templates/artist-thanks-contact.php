<?php
/**
 * Template Name: Layout: artist thanks contact
 *
 * @package Marketify
 */

get_header(); ?>

	<?php do_action( 'marketify_entry_before' ); ?>

		<?php  
	if (isset($_POST["submit"])) {
    //echo "Yes";
 echo $firstname = $_POST['ftext'];
 echo $surname = $_POST['ltext'];
 echo $message = $_POST['ymessage'];
 echo $currentuser = $_POST['currentuser'];
 
 global $wpdb;
 //$wpdb->query($sql);
$sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser);
 $wpdb->query($sql);   

 
 
}else{  
  //  echo "N0";
}


	?>	
	<style>
	.thaks-outer-msg {
    text-align: center;
    font-size: 20px;
}
.go-back-thanks-button {
    background: #dedc00 none repeat scroll 0 0;
    padding: 1% 5% 1% 7%;
	color: white;
}
	
	.go-back-thanks-button:hover {	color: white !important; }
	</style>
	
	
	<?php 
if (isset($_POST["submited"])) {
    //echo "Yes";    
	  $title = $_POST['title'];
	  $description = $_POST['description'];
	  $get_the_id = $_POST['pid'];
	 /*      $my_post = array(
      'ID'           =>  $get_the_id,
      'post_title'   =>  $title,
      'post_content' => $description
  ); */
  
  $upload = wp_upload_bits($_FILES["image"]["name"], null, @file_get_contents($_FILES["image"]["tmp_name"]));
	
	$post_id = $get_the_id; //set post id to which you need to set post thumbnail
	$filename = $upload['file'];
	$wp_filetype = wp_check_filetype($filename, null );
	$attachment = array(
		'post_mime_type' => $wp_filetype['type'],
		'post_title' => $title,
		'post_content' => $description
		//'post_status' => 'inherit'
	);
	$attach_id = wp_insert_attachment( $attachment, $filename, $get_the_id );
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	set_post_thumbnail( $get_the_id, $attach_id );
   //wp_update_post($my_post);
   
       $my_post = array(
      'ID'           =>  $get_the_id,
      'post_title'   =>  $title,
      'post_content' => $description
  );  wp_update_post($my_post);
}
?>
    <div class="container">
	<div class="thaks-outer-msg">
	 <div class="thanks-message">Blog Updated</div>
	 <div class="thanks-message"> Please click on Below link to go your Profile</div></br>
	 <a class="go-back-thanks-button" href="<?php echo site_url(); ?>/artist-main/">Go Back >></a></div>

	 </div>
	
	<div class="container">
		<div id="content" class="site-content row">

			<div id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; ?>

					<?php do_action( 'marketify_loop_after' ); ?>

				<?php else : ?>

					<?php get_template_part( 'no-results', 'index' ); ?>

				<?php endif; ?>
	</main><!-- #main -->
			</div><!-- #primary -->

		</div>
	</div>
<?php get_footer(); ?>