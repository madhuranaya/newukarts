<?php
/**
 * Template Name: Register form
 *
 * @package Marketify
 */
get_header();
do_action('marketify_entry_before');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<div class="artist-main-img-log">
    <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/imgpsh_fullsizebanner.jpeg">
</div>
<div class="wpb_wrapper">
    <h2 class="vc_custom_heading hed">Apply Today</h2>
</div>

<div class="container">
    <div class="apply today">

        <p class="believe_con1">UK Arts believes Artists should be rewarded for the work they create. Many online galleries charge high commission rates per sale which leave artists out of pocket for their creativity.</p>

        <div class="believe_con2"><strong class="bolddddd">Our Commission</strong><br>
            <p>We charge just 5% commission on each sale. This is simply to cover any credit or debit card transaction fees.</p></div>

        <div class="believe_con2"><strong class="bolddddd">Monthly subscription</strong><br>
            <p>We believe a small monthly subcription fee is a fairer way to generate the revenue needed to promote the website, your work and exhibitions.</p></div>

        <div class="believe_con2">
            <P>Here’s an example how artists are more profitable on UK Arts over a 12 month period:</P></div>

        <div class="artist_platform">
            <div class="col-sm-6 uhbgjht">
                <p class="believe_con"><strong class="bolddddd">Other online artist platform</strong></br>
                    Artist annual sales: £500.00</br>
                    Commission fee 30% (average): -£150.00</br>
                    Total recieved in sales: £350.00
                </p></div>

            <div class="col-sm-6 juhgtugh">
                <strong class="bolddddd">UK Arts online artist platform</strong><p class="believe_con">
                    Artist annual sales: £500.00</br>
                    Commission fee 5%: -£25.00</br>
                    Monthly subscription (12 payments): -£30.00</br>
                    Total received in sales:<span style="font-size: 18px; font-weight: bold; margin-left: 5px;">£445.00</span>
                </p>
            </div>
        </div>

        <div class="artist_platform">
            <p class="believe_con">
                <strong class="bolddddd">Our UK Arts Monthly subscription includes:</strong>
            <ul>
                <li>Manage your own profile with header image, artists statement and biography</li>
                <li>Unlimited number of artworks for sale </li>
                <li>Control your own pricing and offers</li>
                <li>Your own blog</li>
                <li>Take commissions</li>
                <li>Receive fast and secure payments</li>
                <li>Promote your exhibition and events on our calendar</li>
                <li>Ongoing promotion from the UK Arts team across social media</li>
                <li>Receive exclusive discounts from our Directory partners
                    </strong>
                    <ul>
                        </br>
                        </div>
                        <div class="premim">
                            <strong>Our  Premium UK Arts Monthly D’imensions’ subscription includes:</strong></br>
                            <p><strong class="lim">• All of the above PLUS Video. Upload your artwork as videos, perfectfor sculpture, crafts and  3D artwork.</strong></p>
                            <a class="join-uk-a" href="#">Limited Availability</a>
                        </div>
                        <div class="artist_platform">
                            <p style="text-align: center;">
                                <button>DOWNLOAD OUR FREE ARTIST PROFILE GUIDE BOOK</button>
                            </p>
                        </div>

                        <div class="artist_platform2">
                            <p class="believe_con">
                                <strong style="font-size:18px; font-weight:700;">
                                    Application</strong>
                            <ul>
                                <li>– You must be a UK Citizen and have proof of ID</li>
                                <li>– You must be 18 years or over</li>
                                <li>– We only accept original art. No reproductions and no copies of other artist’s original work</li>
                                <li>– All Limited Editions must be limited to a maximum of 150, signed and numbered by the artist and with a
                                    certificate of authenticity</li>
                            </ul>

                            </p>
                            <?php if (!is_user_logged_in()) {
                                ?>
                                <p><!-- Register with us by filling out the form below -->
                                <?php } ?>
                            </p>
                        </div>
                        <?php if (isset($_GET['m']) && $_GET['m'] == "successful") { ?><div class="center"><center> User registered successfully.</center></div>

                        <?php } ?>

                        <div class="custome-register-form">
                            <!--<div class="rmheader">Register with us by filling out the form below.</div> -->
                            <?php if (!is_user_logged_in()) {
                                ?>

                                <form id="register-form" method="post" name="myForm" enctype='multipart/form-data'>
                                    <input name="vendor-buyer" type="hidden" id="action" value="role_vendor" /> 
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input id="fname" type="text"  name="first_name" placeholder="First name*" required>
                                            <span id="fname_errors"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <input id="surname" type="text"  name="last_name" placeholder="Surname*" required>
                                            <span id="surname_errors"></span>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input id="email" type="email" name="uemail" placeholder="Email*" onblur="return validateForm();" required >
                                            <span id="email_errors"></span>	
                                            <span id="email_error"></span>	
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $("#fname").change(function () {
                                                    var name = $(this).val();
                                                    $("#pass").val(name + '@123');
                                                });
                                            });
                                        </script>

                                        <script>
                                            $(document).ready(function () {
                                                $('#txtPhone').blur(function (e) {
                                                    if (validatePhone('txtPhone')) {
                                                        $('#spnPhoneStatus').html('');
                                                        $('#spnPhoneStatus').css('color', 'green');
                                                    }
                                                    else {
                                                        $('#spnPhoneStatus').html('Please fill valid number');
                                                        $('#spnPhoneStatus').css('color', 'red');
                                                    }
                                                });
                                            });

                                            function validatePhone(txtPhone) {
                                                var a = document.getElementById(txtPhone).value;
                                                var filter = /^[0-9-+s()]*$/;
                                                if (filter.test(a)) {

                                                    return true;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                        </script>
                                        <style>body
                                            {
                                                padding: 10px;
                                                font-family: Arial;
                                                Font-size: 10pt;
                                            }
                                            span
                                            {
                                                font-weight:bold;
                                            }
                                        </style>
                                        <div class="col-sm-6">
                                            <input id="pass" type="hidden" name="pass" >	
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  name="contact_no" id='txtPhone' placeholder="Preferred contact number *" minlength="0" maxlength="15" required  />
                                            <span id="spnPhoneStatus"></span>

                                        </div>
                                    </div>  
                                    <script>
                                        $(document).ready(function () {

                                            $("#txtPhone").keypress(function (e) {

                                                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

                                                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                                                    return false;
                                                }
                                            });

                                        });
                                    </script>

                                    <div class="row">
                                        <div class="col-sm-12">

                                            <textarea  id="description" name="description" placeholder="Tell us about yourself*" required maxlength="250"></textarea>  
                                            <span id="description_errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Subscription Level?</label>
                                            <input type="checkbox" name="Canvas" value="Canvas">Canvas
                                            <input type="checkbox" name="Canvas" value="Dimensions">Dimensions<br>
                                            <span id="description_errors"></span>
                                        </div>
                                    </div>
                                    <?php //echo do_shortcode('[avatar_upload user="admin"]');?>
                                    <script>$("#avatar_user").change(function () {
                                            filename = this.avatar_user[0].name
                                            console.log(filename);
                                        });</script>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p class="choose"><span> Example of artwork*</span><label for="avatar_user" class="btn">Choose files</label><input class="text-input" name="wp-user-avatar" style="visibility:hidden;" type="file" id="avatar_user" multiple="false"/></p>
                                            <span  id="avatar_user_errors"></span>
                                            <p>Please upload JPG PNG or PDF file formats only.  </p>

                                        </div>
                                        <!--input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('GRAVAR', 'profile'); ?>" /--> 
                                        <script>$("#user_certificate").change(function () {
                                                filename = this.user_certificate[0].name
                                                console.log(filename);
                                            });</script>
                                        <div class="col-sm-6">
                                            <input name="action" type="hidden" id="action" value="update-user" />

                                            <p class="choose"><span>Proof of Citizenship* </span><label for="user_certificate" class="btn">Choose files </label><input class="text-input" required type="file" style="visibility:hidden;" name="wp-user_certificate" id="user_certificate"/></p>
                                            <span id="user_certificate_errors"></span>
                                            <p>Please send us a JPG  PNG or PDF of your UK Driving License or UK Passport clearly displaying your address.</p>
                                        </div>
                                    </div>
                                    <div class="rmrow">

                                        <input type="checkbox" name="checkbox"  value="1" id="action" required > I agree to UK Arts <a href="<?php echo get_site_url(); ?>/terms-conditions"><u>Terms and Conditions for Artist</u></a></br>
                                        <span id="action_errors"></span>
                                        <input id="myModal" class="demo" type="button" value="Submit"  />
                                    </div>
                                </form>
                                <div class="rmrow">	<strong>Please Note:</strong> Due to our launch we are currently experiencing a very  high volume of application. Please allow 1-2 weeks for us to process your application.</div>
                                <div id="myModal2" class="modal"> 

                                    <!-- Modal content -->
                                    <div class="modal-content">
                                        <span class="close">&times;</span>
                                        <center><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/06/cropped-logo.png"></center>
                                        <p>Thank you for your application to join UK Arts. Our team will review your application over the next 1-2 weeks, and if you are successful you will be sent a link to activate your account and complete your profile.</p>
                                        <div class="sub_miy"><span class="clsddd btn">Go</span></div>
                                    </div>

                                </div>
                                <!--onclick="show_alert();"  -->
                                <script>
                                    $("#myModal").click(function () {
                                        var ss = $('input[type=checkbox]').prop('checked');
                                        var fname = $("#fname").val();
                                        var email = $("#email").val();
                                        var surname = $("#surname").val();
                                        var contact_no = $("#contact_no").val();
                                        var des = $("#description").val();
                                        var inp = document.getElementById('avatar_user');
                                        var inp2 = document.getElementById('user_certificate');
                                        var modal = document.getElementById('myModal2');
                                        var span = document.getElementsByClassName("close")[0];
                                        var er = '';

                                        if (fname == "") {

                                            er = $("#fname_errors").html("Required field");
                                        }
                                        if (surname == "") {
                                            er = $("#surname_errors").html("Required field");
                                        }
                                        if (email == "") {
                                            er = $("#email_errors").html("Required field");
                                        }
                                        if (contact_no == "") {
                                            er = $("#contact_no_errors").html("Required Field");
                                        }
                                        if (des == "") {
                                            er = $("#description_errors").html("Required field");
                                        }
                                        if (ss == false) {
                                            er = $("#action_errors").html("Required field");
                                        }
                                        if (inp.files.length === 0) {

                                            er = $("#avatar_user_errors").html("Attachment Required");
                                            inp.focus();

                                        }
                                        if (inp2.files.length === 0) {

                                            er = $("#user_certificate_errors").html("Attachment Required");
                                            inp2.focus();


                                        }
                                        if (er === '') {


                                            $("#myModal2").show();


                                        }
                                        else {
                                            return false;
                                        }


                                    });
                                    $(".clsddd").click(function () {
                                        $("#register-form").submit();
                                        var x = $("#email").val();
                                        var p = $("#pass").val();
                                        var f = $("#fname").val();
                                        var s = $("#surname").val();
                                        //alert(x);
                                        $.ajax({
                                            type: "POST",
                                            data: "fname=" + f + "&sname=" + s + "&email=" + x + "&pass=" + p + "&action6=email_sub6",
                                            url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                            success: function (data) {
                                                console.log(data);
                                            }

                                        });
                                    });



                                    function show_alert() {
                                        alert("Thank you for your application to join UK Arts. Our team will review your application over the next 1-2 weeks, and if you are successful you will be sent a link to activate your account and complete your profile.");
                                    }
                                    function validateForm() {
                                        var x = $("#email").val();
                                        var atpos = x.indexOf("@");
                                        var dotpos = x.lastIndexOf(".");

                                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                                            $("#email_error").html("Not a valid e-mail address");
                                            return false;
                                        } else {
                                            $("#email_error").html("");
                                            //alert(email);
                                            //alert(x);
                                            $.ajax({
                                                type: 'POST',
                                                data: "email=" + x + "&action3=email_sub",
                                                url: '<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                                success: function (data) {
                                                    //$(".chat_message").html(result);
                                                    //alert(data);
                                                    if (data == "yes") {
                                                        //alert(data);
                                                        $("#myModal2").hide();
                                                        $('#myModal').prop('disabled', true);
                                                        $("#email_error").html(" Email already Exists");
                                                        //$("#email_errors").html("<p style='color:red'> Email already Exists</p>");

                                                    } else {
                                                        //alert(data);
                                                        $('#myModal').prop('disabled', false);
                                                        //$("#email_errors").html("dddddd");
                                                    }


                                                    //$('#dddd').html(result);
                                                },
                                                error: function (error) {
                                                    //alert("dd");
                                                }
                                            });

                                        }
                                    }

                                </script>


                            <?php }
                            ?>  



                            <script>
                                /* $("#email").blur(function(){
                                 var email =$(this).val();
                                 //alert(email);
                                 $.ajax({
                                 type:'POST',
                                 data:"email="+email+"&action3=email_sub",
                                 url:'<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                 success: function (data){
                                 //$(".chat_message").html(result);
                                 //alert(data);
                                 if(data=="yes"){
                                 $("#email_errors").html("<p style='color:red'> Email already Exists</p>");
                                 
                                 }else{
                                 $("#email_errors").html("dddddd");
                                 }
                                 
                                 
                                 //$('#dddd').html(result);
                                 },
                                 error: function(error){
                                 alert("dd");
                                 }
                                 }); 
                                 
                                 });
                                 */
                            </script>

                            <script>
                                /*$("#fname").blur(function(){
                                 var emailsd =$(this).val();
                                 // alert("hkjhkhkjh");
                                 $.ajax({
                                 type:'POST',
                                 data:"emailsd="+emailsd+"&actionuu=email_subs",
                                 url:'<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php',
                                 success: function (data){
                                 //$(".chat_message").html(result);
                                 if(data=="yes"){
                                 
                                 $("#fname_errors").html("<p style='color:red'> Name already Exists</p>");
                                 
                                 }else{
                                 $("#fname_errors").html("");
                                 }
                                 
                                 
                                 //$('#dddd').html(result);
                                 },
                                 error: function(error){
                                 alert("dd");
                                 }
                                 }); 
                                 
                                 });
                                 */
                            </script>


                            <?php //add_action('init','create_account'); ?>
                        </div>	

                        </div>
                        </div>

                        <div class="container">
                            <div id="content" class="site-content row">

                                <div id="primary" class="content-area col-sm-12">
                                    <main id="main" class="site-main" role="main">

                                        <?php if (have_posts()) : ?>

                                            <?php /* Start the Loop */ ?>
                                            <?php while (have_posts()) : the_post(); ?>

                                                <?php get_template_part('content', 'page'); ?>

                                                <?php
                                                // If comments are open or we have at least one comment, load up the comment template
                                                if (comments_open() || '0' != get_comments_number())
                                                    comments_template();
                                                ?>

                                            <?php endwhile; ?>

                                            <?php do_action('marketify_loop_after'); ?>

                                        <?php else : ?>

                                            <?php get_template_part('no-results', 'index'); ?>

                                        <?php endif; ?>
                                    </main><!-- #main -->
                                </div> <!-- #primary -->

                            </div>
                        </div>

                        <style>
                            body {font-family: Arial, Helvetica, sans-serif;}

                            /* The Modal (background) */
                            .modal {
                                display: none; /* Hidden by default */
                                position: fixed; /* Stay in place */
                                z-index: 1; /* Sit on top */
                                padding-top: 100px; /* Location of the box */
                                left: 0;
                                top: 0;
                                width: 100%; /* Full width */
                                height: 100%; /* Full height */
                                overflow: auto; /* Enable scroll if needed */
                                background-color: rgb(0,0,0); /* Fallback color */
                                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                            }

                            /* Modal Content */
                            .modal-content {
                                background-color: #fefefe;
                                margin: auto;
                                padding: 20px;
                                border: 1px solid #888;
                                width: 80%;
                            }

                            /* The Close Button */
                            .close {
                                color: #aaaaaa;
                                float: right;
                                font-size: 28px;
                                font-weight: bold;
                            }

                            .close:hover,
                            .close:focus {
                                color: #000;
                                text-decoration: none;
                                cursor: pointer;
                            }
                        </style>
                        <script>


                            // Get the modal
                            var modal = document.getElementById('myModal2');

                            // Get the button that opens the modal
                            var btn = document.getElementById("myBtn2");

                            // Get the <span> element that closes the modal
                            var span = document.getElementsByClassName("close")[0];

                            // When the user clicks the button, open the modal/*  
                            /* btn.onclick = function() {
                             modal.style.display = "block";
                             } */

                            // When the user clicks on <span> (x), close the modal
                            span.onclick = function () {
                                modal.style.display = "none";
                            }

                            // When the user clicks anywhere outside of the modal, close it
                            window.onclick = function (event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                        </script>


                        <?php get_footer(); ?>