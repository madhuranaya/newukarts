<?php
/**
 * Marketify child theme.
 */
function marketify_child_styles() {
    wp_enqueue_style( 'marketify-child', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'marketify_child_styles', 999 );

/** Place any new code below this line */


register_sidebar( array(
      'name' => __( 'headerer'),
      'id' => 'headerer', 
      'description' => __( 'An optional widget area for your site Footer' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );
register_sidebar( array(
      'name' => __( 'featured'),
      'id' => 'featured', 
      'description' => __( 'An optional widget area for your site Footer' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );

register_sidebar( array(
      'name' => __( 'recents'),
      'id' => 'recents', 
      'description' => __( 'An optional widget area for your site Footer' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );

register_sidebar( array(
      'name' => __( 'reviews'),
      'id' => 'reviews', 
      'description' => __( 'An optional widget area for your site Footer' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );
    
    register_sidebar( array(
      'name' => __( 'Social icon'),
      'id' => 'socialicon', 
      'description' => __( 'An optional widget area for your site Footer' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );
// Shortcode to output custom PHP in Visual Composer
function featured_products() {
   dynamic_sidebar( 'featured' ); 
}
add_shortcode( 'featured_produ', 'featured_products');


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}



// Shortcode to output custom PHP in Visual Composer
function side_events() {
   dynamic_sidebar( 'recents' ); 
}
add_shortcode( 'sidee_bar', 'side_events');



add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}



/*************** add diffrent navigation for diffrent user ************************/
/* Functioin to change header */
function add_login_logout_register_menu( $items, $args ) {
if ( $args->theme_location != 'primary' ) {
return $items;
}
$user = wp_get_current_user();
//print_r($user);
//echo $user->roles; 
if ( in_array( 'subscriber', (array) $user->roles ) ) {
$items .= '<li><a href="' . wp_logout_url() . '">' . __( 'Log Out' ) . '</a></li>';
//$items .= '<li><a href="' . get_site_url() .'/my-account/">' . __( 'My Profile' ) . '</a></li>';
} 
else if ( in_array( 'shop_vendor', (array) $user->roles ) ) {
   $items .= '<li class="mydashboard_li"><a href="#">' . __( 'My Dashboard' ) . '</a> 
  
  <ul class="profile-custome">
<li><a href="' . get_site_url() .'/add-product/">Manage your artwork</a></li>
<li><a href="' . get_site_url() .'/vendor-blog-add/">Add Your Blog</a></li>
<li><a href="' . get_site_url() .'/arts-about-us/">Add About your-self</a></li>
<li><a href="' . get_site_url() .'/social-account/">Add Social Account</a></li>
<li><a href="' . get_site_url() .'/artist-review-table/">Add Review</a></li>
<li><a href="' . get_site_url() .'/artist-main/">Your Profile</a></li>
<li><a href="' . get_site_url() .'/add-directory-listing/">Create your Listing</a></li>
</ul>
  </li>';
$items .= '<li><a href="' . wp_logout_url() . '">' . __( 'Log Out' ) . '</a></li>'; }
else if ( in_array( 'administrator', (array) $user->roles ) ) {
  $items .= '<li class="mydashboard_li"><a href="#">' . __( 'My Dashboard' ) . '</a> 
  
  <ul class="profile-custome">
<li><a href="' . get_site_url() .'/add-product/">Manage your artwork</a></li>
<li><a href="' . get_site_url() .'/vendor-blog-add/">Add Your Blog</a></li>
<li><a href="' . get_site_url() .'/arts-about-us/">Add About your-self</a></li>
<li><a href="' . get_site_url() .'/social-account/">Add Social Account</a></li>
<li><a href="' . get_site_url() .'/ artist-review-table/">Add Review</a></li>
<li><a href="' . get_site_url() .'/artist-main/">Your Profile</a></li>
<li><a href="' . get_site_url() .'/add-directory-listing/">Create your Listing</a></li>
</ul>
  </li>';
  http://anayaclients.com/ukarts/artist-review-table/
$items .= '<li><a href="' . wp_logout_url() . '">' . __( 'Log Out' ) . '</a></li>';

}


else if ( in_array( 'contributor', (array) $user->roles ) ) {
  $items .= '<li class="mydashboard_li"><a href="#">' . __( 'My Dashboard' ) . '</a> 
  
  <ul class="profile-custome">

<li><a href="' . get_site_url() .'/vendor-blog-add/">Add Your Blog</a></li>
<li><a href="' . get_site_url() .'/arts-about-us/">Add About your-self</a></li>
<li><a href="' . get_site_url() .'/artist-main/">Your Profile</a></li>
</ul>
  </li>';    
 $items .= '<li><a href="' . wp_logout_url() . '">' . __( 'Log Out' ) . '</a></li>';

}



 else  {
$items .= '<li class="sign-cls"><a href="'.  get_site_url().'/login/">' . __('Sign in') . '</a></li>';
}
return $items;  
}
add_filter( 'wp_nav_menu_items', 'add_login_logout_register_menu', 199, 2 );
add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
wp_redirect( home_url() );
exit();
}

register_sidebar( array(
      'name' => __( 'filter'),
      'id' => 'filter', 
      'description' => __( 'An optional widget area for your site filter' ),
      'before_widget' => '<aside id="%1$s" class="footer-section-first">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="footer-secton-title">',
      'after_title' => '</h3>',
      ) );
    
    
    //hook into the init action and call create_style_taxonomies when it fires
add_action( 'init', 'createstyle_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function createstyle_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'style', 'taxonomy general name' ),
'singular_name' => _x( 'style', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by style' ),
'all_items' => __( 'All style' ),
'parent_item' => __( 'Parent style' ),
'parent_item_colon' => __( 'Parent style:' ),
'edit_item' => __( 'Edit style' ),
'update_item' => __( 'Update style' ),
'add_new_item' => __( 'Add New style' ),
'new_item_name' => __( 'New style Name' ),
'menu_name' => __( 'style' ),
);   
// Now register the taxonomy
register_taxonomy('style',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'style' ),
));
}




//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function create_topics_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'Color', 'taxonomy general name' ),
'singular_name' => _x( 'Color', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by colors' ),
'all_items' => __( 'All Colors' ),
'parent_item' => __( 'Parent Color' ),
'parent_item_colon' => __( 'Parent Color:' ),
'edit_item' => __( 'Edit Color' ),
'update_item' => __( 'Update Color' ),
'add_new_item' => __( 'Add New Color' ),
'new_item_name' => __( 'New Color Name' ),
'menu_name' => __( 'Color' ),
);   
// Now register the taxonomy
register_taxonomy('Color',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'color' ),
));
}


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function create_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'subject', 'taxonomy general name' ),
'singular_name' => _x( 'subject', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by subject' ),
'all_items' => __( 'All subjects' ),
'parent_item' => __( 'Parent subject' ),
'parent_item_colon' => __( 'Parent subject:' ),
'edit_item' => __( 'Edit subject' ),
'update_item' => __( 'Update subject' ),
'add_new_item' => __( 'Add New subject' ),
'new_item_name' => __( 'New subject Name' ),
'menu_name' => __( 'subject' ),
);   
// Now register the taxonomy
register_taxonomy('subject',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'subject' ),
));
}


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'createnew_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function createnew_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'medium', 'taxonomy general name' ),
'singular_name' => _x( 'medium', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by medium' ),
'all_items' => __( 'All medium' ),
'parent_item' => __( 'Parent medium' ),
'parent_item_colon' => __( 'Parent medium:' ),
'edit_item' => __( 'Edit medium' ),
'update_item' => __( 'Update medium' ),
'add_new_item' => __( 'Add New medium' ),
'new_item_name' => __( 'New medium Name' ),
'menu_name' => __( 'medium' ),
);   
// Now register the taxonomy
register_taxonomy('medium',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'medium' ),
));
}


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'addnew_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function addnew_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'orientation', 'taxonomy general name' ),
'singular_name' => _x( 'orientation', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by orientation' ),
'all_items' => __( 'All orientation' ),
'parent_item' => __( 'Parent orientation' ),
'parent_item_colon' => __( 'Parent orientation:' ),
'edit_item' => __( 'Edit orientation' ),
'update_item' => __( 'Update orientation' ),
'add_new_item' => __( 'Add New orientation' ),
'new_item_name' => __( 'New orientation Name' ),
'menu_name' => __( 'orientation' ),
);   
// Now register the taxonomy
register_taxonomy('orientation',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'orientation' ),
));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'addnewsize_hierarchical_taxonomy', 0 );
//create a custom taxonomy name it topics for your posts
function addnewsize_hierarchical_taxonomy() {
$labels = array(
'name' => _x( 'size', 'taxonomy general name' ),
'singular_name' => _x( 'size', 'taxonomy singular name' ),
'search_items' =>  __( 'Search by size' ),
'all_items' => __( 'All size' ),
'parent_item' => __( 'Parent size' ),
'parent_item_colon' => __( 'Parent size:' ),
'edit_item' => __( 'Edit size' ),
'update_item' => __( 'Update size' ),
'add_new_item' => __( 'Add New size' ),
'new_item_name' => __( 'New size Name' ),
'menu_name' => __( 'size' ),
);   
// Now register the taxonomy
register_taxonomy('size',array('download'), array(
'hierarchical' => true,
'labels' => $labels,
'show_ui' => true,
'show_admin_column' => true,
'query_var' => true,
'rewrite' => array( 'slug' => 'size' ),
));
}

if (class_exists('MultiPostThumbnails')) {
 
new MultiPostThumbnails(array(
'label' => 'Second Featured Image',
'id' => 'second-featured-image',
'post_type' => 'download' 
 ) );
 
 new MultiPostThumbnails(array(
'label' => 'Third Featured Image',
'id' => 'third-featured-image',
'post_type' => 'download'
 ) );
 
  new MultiPostThumbnails(array(
'label' => 'Fourth Featured Image',
'id' => 'fourth-featured-image',
'post_type' => 'download'
 ) );
  new MultiPostThumbnails(array(
'label' => 'Fifth Featured Image',
'id' => 'fifth-featured-image',
'post_type' => 'download'
 ) );
  new MultiPostThumbnails(array(
'label' => 'Sixth Featured Image',
'id' => 'sixth-featured-image',
'post_type' => 'download'
 ) );
   
 }
 
 
// Register Custom Post Type
function about_us_custome() {

  $labels = array(
    'name'                  => _x( 'About us', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'About us', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'About us', 'text_domain' ),
    'name_admin_bar'        => __( 'About us', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add About us', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'view_items'            => __( 'View Items', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'About us', 'text_domain' ),
    'description'           => __( 'About us Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'about_us', $args );

}
add_action( 'init', 'about_us_custome', 0 );


define('MY_WORDPRESS_FOLDER',$_SERVER['DOCUMENT_ROOT']);
define('MY_THEME_FOLDER',str_replace("\\",'/',dirname(__FILE__)));
define('MY_THEME_PATH','/' . substr(MY_THEME_FOLDER,stripos(MY_THEME_FOLDER,'wp-content')));
 
add_action('admin_init','my_meta_init');
 
function my_meta_init()
{
     wp_enqueue_style('my_meta_css', MY_THEME_PATH . '/custom/meta.css');
 
     foreach (array('download','fep_message','competitions') as $type) 
    {
        add_meta_box('my_all_meta', 'custome box', 'my_meta_setup', $type, 'normal', 'high');
    }     
    add_action('save_post','my_meta_save');
}
 
function my_meta_setup()
{
    global $post;  
     $meta = get_post_meta($post->ID,'_my_meta',TRUE);
   include(MY_THEME_FOLDER . '/custom/meta.php');
   echo '<input type="hidden" name="my_meta_noncename" value="' . wp_create_nonce(__FILE__) . '" />';
}
  
function my_meta_save($post_id) 
{
     if (!wp_verify_nonce($_POST['my_meta_noncename'],__FILE__)) return $post_id;
 
    if ($_POST['post_type'] == 'page') 
    {
        if (!current_user_can('edit_page', $post_id)) return $post_id;
    }
    else
    {
        if (!current_user_can('edit_post', $post_id)) return $post_id;
    }
 
   $current_data = get_post_meta($post_id, '_my_meta', TRUE);  
  
    $new_data = $_POST['_my_meta'];
 
    my_meta_clean($new_data);
     
    if ($current_data) 
    {
        if (is_null($new_data)) delete_post_meta($post_id,'_my_meta');
        else update_post_meta($post_id,'_my_meta',$new_data);
    }
    elseif (!is_null($new_data))
    {
        add_post_meta($post_id,'_my_meta',$new_data,TRUE);
    }
 
    return $post_id;
}
 
function my_meta_clean(&$arr)
{
    if (is_array($arr))
    {
        foreach ($arr as $i => $v)
        {
            if (is_array($arr[$i])) 
            {
                my_meta_clean($arr[$i]);
 
                if (!count($arr[$i])) 
                {
                    unset($arr[$i]);
                }
            }
            else
            {
                if (trim($arr[$i]) == '') 
                {
                    unset($arr[$i]);
                }
            }
        }
 
        if (!count($arr)) 
        {
            $arr = NULL;
        }
    }
} 





// Register Custom Post Type
function reviews_custome() {

  $labels = array(
    'name'                  => _x( 'Reviews', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Reviews', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Reviews', 'text_domain' ),
    'name_admin_bar'        => __( 'Reviews', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add Reviews', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'view_items'            => __( 'View Items', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Reviews', 'text_domain' ),
    'description'           => __( 'Reviews Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor'),
 //   'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'reviews_customes', $args );

}
add_action( 'init', 'reviews_custome', 0 );

  

  // Register Custom Post Type
function social_custome() {

  $labels = array(
    'name'                  => _x( 'Social account', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Social account', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Social account', 'text_domain' ),
    'name_admin_bar'        => __( 'Social account', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add Social account', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'view_items'            => __( 'View Items', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Social account', 'text_domain' ),
    'description'           => __( 'Social account Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'social_accounts', $args );

}
add_action( 'init', 'social_custome', 0 );





  

  // Register Custom Post Type
function social_customea() {

  $labels = array(
    'name'                  => _x( 'Competitions', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'competitions', 'Post Type Singular Name', 'text_domain' ), 
    'menu_name'             => __( 'Competitions', 'text_domain' ),
    'name_admin_bar'        => __( 'Competitions', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add competitions account', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'view_items'            => __( 'View Items', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Competitions', 'text_domain' ),
    'description'           => __( 'Competitions Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'taxonomies'            => array( 'post_category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'competitions', $args );

}
add_action( 'init', 'social_customea', 0 );



function to_send_commission_mail($to,$reciver_email,$message){
  $subject="commission email";
  $headers = array('Content-Type: text/html; charset=UTF-8');
  wp_mail( $send_email, $subject, $message, $headers );
}


/*************** Show all Vendors  ************************/
function vendor_show_all(){
?>
<section class="regular-pro">  
  <?php
global $wpdb;
$findID = $wpdb->get_results("SELECT * FROM wp_fes_vendors  WHERE status = 'approved' ORDER BY username ASC ",ARRAY_A); ?>  
  <div class="row"> 
    <div class="col-lg-12 producers-main">
    <?php
if (!empty($findID)) {   
$firstLetter = 0;  
$desiredColumns = 6;  //you can change this!
$columnCount = (count($findID)+26)/$desiredColumns+1;
echo "<table><tr><td>"; 
//print_r($findID);       
foreach($findID as $key => $cur) {
  if ($key != 0 && $key % $desiredColumns == 0) echo "</td><td>";
    if ($cur['name'][0] !== $firstLetter)
    {   
          $firstLetter = $cur['name'][0];
          echo "<strong>$firstLetter</strong> <br />";
    }
     echo "<a href=".home_url()."/artist-main"."?user_id=".$cur['user_id'].">".$cur['name']."</a><br/>";
    //echo "<a href=".home_url()."/artist-main/>".$cur['name']."</a><br/>";
     } 
echo "</td><tr></table>";
}
    
else {  
echo 'No users found.';
}
?>  
    </div>  
  </div>
</section>
<?
}
add_shortcode('all_vendors', 'vendor_show_all');



/*************** end Show all Vendors  ************************/



/*************** Show all Vendors  ************************/
function post_blog_page(){

                  $author_querya = array('post_type' => 'post',
                  'posts_per_page' => '-1');
                    $author_postsa = new WP_Query($author_querya);
                  //print_r($author_postsa);
				  ?><div class="blog-post-outer"><?
                  while($author_postsa->have_posts()) : $author_postsa->the_post();
                      $get_the_id=get_the_id();  
					 ?>
					 <div class="blog-post-inner">
					 <div class="blog-page-thumbnail"><? echo the_post_thumbnail(); ?></div>
					 <div class="blog-page-title"><? echo the_title(); ?></div>
					 <div class="blog-page-date"><? echo get_the_date(); ?></div>
					 <div class="blog-page-content"><? echo the_excerpt(); ?></div>
					 <div class="blog-page-readmore"><a href="<?php echo the_permalink(); ?>">Read More</a></div> 
					 </div>
					 <?		endwhile;  ?>
					 </div>	  <?
} 
add_shortcode('blog_shortcode', 'post_blog_page');



function post_competitions_page(){

                  $author_querya = array('post_type' => 'competitions',
                  'posts_per_page' => '-1');
                    $author_postsa = new WP_Query($author_querya);
                  //print_r($author_postsa);
				  ?>
          <div class="blog-post-outer"><h2>May 2018</h2>
            <?
                  while($author_postsa->have_posts()) : $author_postsa->the_post();
                      $get_the_id=get_the_id(); 
 $my_meta = get_post_meta($get_the_id,'_my_meta',TRUE);
                            $date_compition= $my_meta['date_compition']; 
						    $date_comp= substr($date_compition,-5,2);
                     		if($date_comp=="05") 	{   ?>
					 <div class="blog-post-inner">
					 <div class="blog-page-thumbnail"><? echo the_post_thumbnail(); ?></div>
					 <div class="blog-page-title"><h2><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2></div>
					 <div class="blog-page-date"><h2><? echo get_the_date(); ?></h2></div>
					 <div class="blog-page-date"><h3>Submission Date: <? echo $date_compition; 		?></h3></div>
					 <div class="blog-page-content"><? echo the_excerpt(); ?></div> 
					
					 </div> 
							<?	}	endwhile;  ?>
					 
					 
					 </div>	  
					 <?php 
					 
					    $author_querya = array('post_type' => 'competitions',
                  'posts_per_page' => '-1');
                    $author_postsa = new WP_Query($author_querya);
                  //print_r($author_postsa);
				  ?><div class="blog-post-outer"><h2>June 2018</h2><?
                  while($author_postsa->have_posts()) : $author_postsa->the_post();
                      $get_the_id=get_the_id(); 
 $my_meta = get_post_meta($get_the_id,'_my_meta',TRUE);
                             $date_compition= $my_meta['date_compition']; 
						     $date_comp= substr($date_compition,-5,2);
                     		if($date_comp=="06") {
                     		  ?>
					 <div class="blog-post-inner">
					 <div class="blog-page-thumbnail"><? echo the_post_thumbnail(); ?></div>
					 <div class="blog-page-title"><a href="<?php echo the_permalink(); ?>"><h2><?php echo the_title(); ?></a></h2></div>
					 <div class="blog-page-date"><h2><? echo get_the_date(); ?></h2></div>
					 <div class="blog-page-date"><h2>Submission Date: <? echo $date_compition; ?><h2></div>
					 <div class="blog-page-content"><? echo the_excerpt(); ?></div>
					
					 </div> 
<?	}	endwhile;  ?>
					 <div class="submit-compition"><a class="submit-a-competion" href="#">SUBMIT A COMPETITION</a></div>
					 <div class="pagination-compition">

 


					 <a href="">< Back</a>
					 <a href="">Next ></a>
					 </div>
					 </div>	  
					 
					 <?
} 
add_shortcode('post_competitions', 'post_competitions_page');
