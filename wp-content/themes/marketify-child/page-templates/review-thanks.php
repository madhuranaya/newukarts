<?php
/**
 * Template Name: Layout: review thanks page
 *
 * @package Marketify
 */
 
get_header(); ?>

	<?php do_action( 'marketify_entry_before' ); ?>

		<?php  
                        if (isset($_POST["submit"])) {
                           //echo "Yes";
                   $review_title = $_POST['review_title'];
                   $review_starts = $_POST['review_starts'];
                   $review_discription = $_POST['review_description'];
                   $product_id = $_POST['product_id'];
                   $user_id = $_POST['user_id'];
                       
                        global $wpdb;
                        //$wpdb->query($sql);
                        $sql = $wpdb->prepare("INSERT INTO product_review(product_id,user_id,review_title,review_discription,stars) values (%s,%s,%s,%s,%s)", $product_id,$user_id,$review_title,$review_discription,$review_starts);
                        $wpdb->query($sql);   
                        }else{  
                         //  echo "N0";
                        }
						?>
                        
	  

	<div class="container">
	<h2 class="thanks-submit-review-text">Thanks, For Submit Review</h2>
		<div id="content" class="site-content row">

			<div id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; ?>

					<?php do_action( 'marketify_loop_after' ); ?>

				<?php else : ?>

					<?php get_template_part( 'no-results', 'index' ); ?>

				<?php endif; ?>
	</main><!-- #main -->
			</div><!-- #primary -->

		</div>
	</div>
<?php get_footer(); ?>