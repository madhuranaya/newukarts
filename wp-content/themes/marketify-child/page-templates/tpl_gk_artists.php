<?php

/**
 * Template Name: GK Artists
 *
 * @package Marketify
 */
get_header();
?>
<?php do_action('marketify_entry_before');
?>



<style>
    * {
        box-sizing: border-box;
    }

    .heading {
        font-size: 25px;
        margin-right: 25px;
    }


    .checked {
        color: orange;
    }

    /* Three column layout */
    .side {
        float: left;
        margin-top: 6px;
        width: 6%;
    }

    .middle {
        float: left;
        margin-top: 10px;
        width: 31%;
    }

    /* Place text to the right */
    .right {
        float: left;
        margin-left: 8px;
        text-align: left;
        width: 9%;
    }
    .first_line {
        float: left;
        width: 100%;
    }
    .middle {
        float: left;
        margin-top: 10px;
        width: 28%;
    }
    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .review-inner-part {
        float: left;
        width: 30%;
    }
    .ght {
        width: auto !important;
        float: left;
        margin-right: 10px;
    }
    .review-inner-part > span {
        max-width: 190px;
    }
    .review-overall {
        font-size: 18px;
    }
    /* The bar container */
    .bar-container {
        width: 100%;
        background-color: #f1f1f1;
        text-align: center;
        color: white;
    }
    .review-inner-part.macrto {
        float: right;
        width: 22%;
        text-align: left;
    }
    #review .review-inner-main-right {
        width: 56%;
    }
    #review .review-inner-main-left {
        float: left;
        width: 40%;
    }
    #review .review-inner-part > h2 {

        margin-top: 5px;
    }
    #review .review-inner-part > h2 {
        text-transform: capitalize;
    }
    .review-inner-main-right .review-inner-part > span {
        color: #758286;
    }
    .review-inner-main-right .review-inner-part {
        float: left;
        width: 100%;
    }
    .review-inner-main-left .review-inner-part {
        float: left;
        width: 100%;
    }
    /* Individual bars */
    .bar-5 {
        background-color: #e4e000;
        height: 18px;
        width: 90%;
    }  
    .bar-4 {width: 30%; height: 18px; background-color: #e4e000;}
    .bar-3 {width: 10%; height: 18px; background-color: #e4e000;}
    .bar-2 {width: 4%; height: 18px; background-color: #e4e000;}
    .bar-1 {width: 15%; height: 18px; background-color: #e4e000;}
    .visit-or-website {
        display: none;
    }
    .main-cls-both {
        display: none;
    }
    .manage-product {
        background-color: #dedc00;
        float: right;
        text-align: center !important;
    }

    #blog_social_3 {
        display: none;
    }
    .tab a {
        background: #f2f2f2 none repeat scroll 0 0 !important;
        color: #000;
        font-size: 20px !important;
        font-weight: normal;
        text-transform: capitalize;
    }
    .tab a {
        padding: 10px 20px !important;
    }
    .tab a.active {
        background-color: #dedc00 !important;
    }
</style>
<!--GET THE ROLE-->
<?php

if (is_user_logged_in()) {
    $currentUserId = get_current_user_id();
    $currentUser = get_userdata($currentUserId);
    $currUserRole = $currentUser->roles[0];
    $currUserFirstName = $currentUser->first_name;
    $currUserLastName = $currentUser->last_name;
    $currUserTagLine = get_user_meta($currentUserId, 'artistStmt', true);
    $currUserDesac = get_user_meta($currentUserId, 'description', true);
    //print_r($currentUser); 

    if ($currUserRole == 'shop_vendor') {
        
        get_template_part('content', 'artist');
    } /*elseif ($currUserRole == 'administrator') {
        get_template_part('content', 'administrator');
    }*/  elseif ($currUserRole == 'contributor') {
        
        get_template_part('content', 'buyer');
    }
} else {
    get_template_part('content', 'withoutLogin');
}
?>

<?php get_footer(); ?>