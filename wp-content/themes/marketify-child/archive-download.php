<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Marketify
 */

get_header(); ?>

    <?php do_action( 'marketify_entry_before' ); ?>

<?php // dynamic_sidebar( 'filter' );  ?>
<div class="category-page-title">
 <div class="single_page_title_cat container">
    <h2 class="page-title"><?php single_cat_title(); ?></h2>
</div>
<style>
.single_page_title_cat h2 { margin-top: 2%; }
.category-page-title {
    background-color: #dfdc01 !important;
    text-align: center;
    width: 100%;
    height: 100px;
}
</style>
</div>

<div class="filter-painting main-filter-custome">

<?php
   $current_url = $_SERVER['REQUEST_URI'];
  
  $current_cat_get= substr($current_url,20,4);    
if($current_cat_get=="pain")
{ //echo "painting";
?><div class="filter-box-form"><div class="container"><?php echo do_shortcode('[searchandfilter id="4015"]');  
?></div></div><div class="filter-box-result"><div class="container"><?php echo do_shortcode('[searchandfilter id="4015" show="results"]');        ?></div></div><?php   }
elseif($current_cat_get=="draw")  
{     //echo "Drawing"; 
  ?><div class="filter-box-form"><div class="container"><?php    echo do_shortcode('[searchandfilter id="4018"]'); ?></div></div><div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4018" show="results"]');  ?></div></div><?php     } 
elseif($current_cat_get=="scul")
 {    //echo "sculpture"; 
  ?><div class="filter-box-form"><div class="container"><?php    echo do_shortcode('[searchandfilter id="4020"]');  ?></div></div><div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4020" show="results"]'); ?></div></div><?php     } 
elseif($current_cat_get=="phot")  
 {    //echo "photographs"; 
  ?><div class="filter-box-form"><div class="container"><?php    echo do_shortcode('[searchandfilter id="4021"]'); ?></div></div><div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4021" show="results"]');   ?></div></div><?php    } 
elseif($current_cat_get=="coll")
 { //echo "collage"; 
  ?><div class="filter-box-form"><div class="container"><?php    echo do_shortcode('[searchandfilter id="4023"]'); ?></div></div><div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4023" show="results"]');  ?></div></div><?php 
  } 
elseif($current_cat_get=="prin")
 { ?><div class="filter-box-form"><div class="container"><?php   echo "prints"; echo do_shortcode('[searchandfilter id="4025"]'); ?></div></div><div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4025" show="results"]');   ?></div></div><?php  
	  } 
elseif($current_cat_get=="craf") 
 { //echo "craft";
?><div class="filter-box-form"><div class="container"><?php    echo do_shortcode('[searchandfilter id="4029"]'); ?></div></div><div class="filter-box-result"><div class="container"><?php 
    echo do_shortcode('[searchandfilter id="4029" show="results"]');   ?></div></div><?php      
	} 
elseif($current_cat_get=="digi")
  { //echo "digital-art";
?>

<div class="filter-box-form"><div class="container">
   <?php echo do_shortcode('[searchandfilter id="4030"]');?></div></div>
   <div class="filter-box-result"><div class="container"><?php 
      echo do_shortcode('[searchandfilter id="4030" show="results"]');    ?>
</div></div><?php   
	} 
else { //echo "other"; 
}
?>

<?php //echo do_shortcode('[searchandfilter id="839"]'); ?>


</div>
<div class="container src-ak"><?php //echo do_shortcode('[searchandfilter id="839" show="results"]'); ?></div>

    <div id="content" class="site-content container">
        <?php do_action( 'marketify_shop_before' ); ?>
        <?php get_template_part( 'content-grid-download', 'popular' ); ?>
       
        <div class="marketify-archive-download row">
            <div role="main" class="content-area 33 col-xs-12 <?php echo is_active_sidebar( 'sidebar-download' ) ? 'col-md-12 single_page' : ''; ?>">

                <?php do_action( 'marketify_downloads_before' ); ?>

				<?php do_action( 'marketify_downloads' ); ?>

                <?php do_action( 'marketify_downloads_after' ); ?>

            </div><!-- #primary -->

            <?php get_sidebar( 'archive-download' ); ?>
        </div>

        <?php do_action( 'marketify_shop_after' ); ?>
</div>

<?php get_footer(); ?>
