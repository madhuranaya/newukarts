<?php
 
/**
 * Marketify child theme.
 */
function marketify_child_styles() {
    wp_enqueue_style('marketify-child', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'marketify_child_styles', 999);

/** Place any new code below this line */
register_sidebar(array(
    'name' => __('headerer'),
    'id' => 'headerer',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => __('featured'),
    'id' => 'featured',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));


register_sidebar(array(
    'name' => __('recents'),
    'id' => 'recents',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));


register_sidebar(array(
    'name' => __('reviews'),
    'id' => 'reviews',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => __('register-vendor'),
    'id' => 'register',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => __('Social icon'),
    'id' => 'socialicon',
    'description' => __('An optional widget area for your site Footer'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));
/*  add_filter('wp_authenticate_user', function($user) {
  if ($user->monthly_subscription == 'no') {
  return $user;
  }
  return wp_redirect( 'http://www.example.dev/page/' );

  }, 10, 2); */

// Shortcode to output custom PHP in Visual Composer
function featured_products() {
    dynamic_sidebar('featured');
}

add_shortcode('featured_produ', 'featured_products');
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

// Shortcode to output custom PHP in Visual Composer
function side_events() {
    dynamic_sidebar('recents');
}

add_shortcode('sidee_bar', 'side_events');
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator')) {
        show_admin_bar(false);
    }
}

/* * ************* add diffrent navigation for diffrent user *********************** */
/* Functioin to change header */

function add_login_logout_register_menu($items, $args) {
    if ($args->theme_location != 'primary') {
        return $items;
    }
    $user = wp_get_current_user();
    //print_r($user);
    //echo $user->roles; 
    if (in_array('subscriber', (array) $user->roles)) {
        //$items .= '<li><a href="' . wp_logout_url() . '">' . __( 'Log Out' ) . '</a></li>';
        //$items .= '<li><a href="' . get_site_url() .'/my-account/">' . __( 'My Profile' ) . '</a></li>';
    } else if (in_array('shop_vendor', (array) $user->roles)) {

        if (is_page(array(645, 629, 661, 1124, 3040, 3344))) {

            $items .= '<li  id="menu-item-1786" class="mydashboard_li current-menu-ancestor current-menu-parent dfdfdf"><a href="' . get_site_url() . '/profile/">' . __('My Dashboard') . '</a> 
     ';
        } else {
            $items .= '<li  id="menu-item-1786" class="mydashboard_li"><a href="' . get_site_url() . '/profile/">' . __('My Dashboard') . '</a> 
     ';
        }

        $items .= '<ul class="profile-custome vendor">';

        if (is_page(645)) {
            $items .= '<li class="active"><a href="' . get_site_url() . '/artist-main/?tab=mya" >Manage your artwork</a></li>';
        } else {
            $items .= '<li ><a href="' . get_site_url() . '/artist-main/">Manage your artwork</a></li>';
        }

        if (is_page(661)) {
            $items .= '<li class="active"><a href="' . get_site_url() . '/arts-about-us/">Add your details</a></li>';
        } else {
            $items .= '<li><a href="' . get_site_url() . '/arts-about-us/">Add your details</a></li>';
        }
        if (is_page(3344)) {
            $items .= '<li class="active"><a href="' . get_site_url() . '/profile/">View Your Profile</a></li>';
        } else {
            $items .= '<li><a href="' . get_site_url() . '/profile/">View Your Profile</a></li>';
        }


        /*
          if(is_page(629)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=blog">Add Your Blog  </a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=blog">Add Your Blog  </a></li>';
          }
          if(is_page(1124)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=account">Add Social Account</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=account">Add Social Account</a></li>';
          }



          if(is_page(3040)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/post-an-event/">Add an Exhibition/Event</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/post-an-event/">Add an Exhibition/Event</a></li>';
          } */
    } else if (in_array('administrator', (array) $user->roles)) {

        if (is_page(array(645, 629, 661, 1124, 3040, 3344))) {

          /*   $items .= '<li  id="menu-item-1786" class="mydashboard_li current-menu-ancestor current-menu-parent dfdfdf"><a href="' . get_site_url() . '/artist-main/?tab=about">' . __('My Dashboard') . '</a>   
     '; */
        } else {
       /*     $items .= '<li  id="menu-item-1786" class="mydashboard_li"><a href="' . get_site_url() . '/artist-main/?tab=about">' . __('My Dashboard') . '</a> 
     '; */
        }

        $items .= '<ul class="profile-custome administ">';

        if (is_page(645)) {
            $items .= '<li class="active"><a href="' . get_site_url() . '/artist-main-2/" >Manage your artwork</a></li>';
        } else {
            $items .= '<li ><a href="' . get_site_url() . '/artist-main-2/">Manage your artwork</a></li>';
        }


        if (is_page(661)) {
            $items .= '<li class="active"><a href="' . get_site_url() . '/arts-about-us">Add your details</a></li>';
        } else {
            $items .= '<li><a href="' . get_site_url() . '/arts-about-us">Add your details</a></li>';
        }



        /*
          if(is_page(629)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=blog">Add Your Blog</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=blog">Add Your Blog</a></li>';
          }

          if(is_page(1124)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=account">Add Social Account</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=account">Add Social Account</a></li>';
          }

          if(is_page(3344)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=about">View Your Profile</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=about">View Your Profile</a></li>';
          }

          if(is_page(3040)){
          $items .= '<li class="active"><a href="' . get_site_url() .'/artist-main/?tab=event">Add an Exhibition/Event</a></li>';
          }else{
          $items .= '<li><a href="' . get_site_url() .'/artist-main/?tab=event">Add an Exhibition/Event</a></li>';
          }
         */
    } else if (in_array('contributor', (array) $user->roles)) {
        $items .= '<li id="menu-item-1786" class="mydashboard_buyer"><a href="' . get_site_url() . '/profile/">' . __('My Dashboard') . '</a>     
     <ul class="profile-custome buyer">   
  
   <li><a href="' . get_site_url() . '/artist-main-2/">Manage Commission </a></li>
   <li><a href="' . get_site_url() . '/arts-about-us/">Add your details</a></li>
   <li><a href="' . get_site_url() . '/profile/">View Your Profile</a></li>
   
    
 
   
   </ul>
     </li>';
    } else {
        
    }
    return $items;
}

add_filter('wp_nav_menu_items', 'add_login_logout_register_menu', 199, 2);
add_action('wp_logout', 'auto_redirect_after_logout');

function auto_redirect_after_logout() {
    wp_redirect(home_url());
    exit();
}

register_sidebar(array(
    'name' => __('filter'),
    'id' => 'filter',
    'description' => __('An optional widget area for your site filter'),
    'before_widget' => '<aside id="%1$s" class="footer-section-first">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="footer-secton-title">',
    'after_title' => '</h3>',
));

//hook into the init action and call create_style_taxonomies when it fires
add_action('init', 'createstyle_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
//add_action('wp_logout', 'your_function');
function createstyle_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('style', 'taxonomy general name'),
        'singular_name' => _x('style', 'taxonomy singular name'),
        'search_items' => __('Search by style'),
        'all_items' => __('All style'),
        'parent_item' => __('Parent style'),
        'parent_item_colon' => __('Parent style:'),
        'edit_item' => __('Edit style'),
        'update_item' => __('Update style'),
        'add_new_item' => __('Add New style'),
        'new_item_name' => __('New style Name'),
        'menu_name' => __('style'),
    );
    // Now register the taxonomy
    register_taxonomy('style', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'style'),
    ));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'create_topics_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
function create_topics_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('Color', 'taxonomy general name'),
        'singular_name' => _x('Color', 'taxonomy singular name'),
        'search_items' => __('Search by colors'),
        'all_items' => __('All Colors'),
        'parent_item' => __('Parent Color'),
        'parent_item_colon' => __('Parent Color:'),
        'edit_item' => __('Edit Color'),
        'update_item' => __('Update Color'),
        'add_new_item' => __('Add New Color'),
        'new_item_name' => __('New Color Name'),
        'menu_name' => __('Color'),
    );
    // Now register the taxonomy
    register_taxonomy('Color', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'color'),
    ));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'create_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
function create_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('subject', 'taxonomy general name'),
        'singular_name' => _x('subject', 'taxonomy singular name'),
        'search_items' => __('Search by subject'),
        'all_items' => __('All subjects'),
        'parent_item' => __('Parent subject'),
        'parent_item_colon' => __('Parent subject:'),
        'edit_item' => __('Edit subject'),
        'update_item' => __('Update subject'),
        'add_new_item' => __('Add New subject'),
        'new_item_name' => __('New subject Name'),
        'menu_name' => __('subject'),
    );
    // Now register the taxonomy
    register_taxonomy('subject', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'subject'),
    ));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'createnew_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
function createnew_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('medium', 'taxonomy general name'),
        'singular_name' => _x('medium', 'taxonomy singular name'),
        'search_items' => __('Search by medium'),
        'all_items' => __('All medium'),
        'parent_item' => __('Parent medium'),
        'parent_item_colon' => __('Parent medium:'),
        'edit_item' => __('Edit medium'),
        'update_item' => __('Update medium'),
        'add_new_item' => __('Add New medium'),
        'new_item_name' => __('New medium Name'),
        'menu_name' => __('medium'),
    );
    // Now register the taxonomy
    register_taxonomy('medium', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'medium'),
    ));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'addnew_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
function addnew_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('orientation', 'taxonomy general name'),
        'singular_name' => _x('orientation', 'taxonomy singular name'),
        'search_items' => __('Search by orientation'),
        'all_items' => __('All orientation'),
        'parent_item' => __('Parent orientation'),
        'parent_item_colon' => __('Parent orientation:'),
        'edit_item' => __('Edit orientation'),
        'update_item' => __('Update orientation'),
        'add_new_item' => __('Add New orientation'),
        'new_item_name' => __('New orientation Name'),
        'menu_name' => __('orientation'),
    );
    // Now register the taxonomy
    register_taxonomy('orientation', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'orientation'),
    ));
}

//hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'addnewsize_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts
function addnewsize_hierarchical_taxonomy() {
    $labels = array(
        'name' => _x('size', 'taxonomy general name'),
        'singular_name' => _x('size', 'taxonomy singular name'),
        'search_items' => __('Search by size'),
        'all_items' => __('All size'),
        'parent_item' => __('Parent size'),
        'parent_item_colon' => __('Parent size:'),
        'edit_item' => __('Edit size'),
        'update_item' => __('Update size'),
        'add_new_item' => __('Add New size'),
        'new_item_name' => __('New size Name'),
        'menu_name' => __('size'),
    );
    // Now register the taxonomy
    register_taxonomy('size', array('download'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'size'),
    ));
}

if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(array(
        'label' => 'Second Featured Image',
        'id' => 'second-featured-image',
        'post_type' => 'download'
    ));

    new MultiPostThumbnails(array(
        'label' => 'Third Featured Image',
        'id' => 'third-featured-image',
        'post_type' => 'download'
    ));
    new MultiPostThumbnails(array(
        'label' => 'Fourth Featured Image',
        'id' => 'fourth-featured-image',
        'post_type' => 'download'
    ));
    new MultiPostThumbnails(array(
        'label' => 'Fifth Featured Image',
        'id' => 'fifth-featured-image',
        'post_type' => 'download'
    ));
    new MultiPostThumbnails(array(
        'label' => 'Sixth Featured Image',
        'id' => 'sixth-featured-image',
        'post_type' => 'download'
    ));
}

// Register Custom Post Type
function about_us_custome() {
    $labels = array(
        'name' => _x('About us', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('About us', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('About us', 'text_domain'),
        'name_admin_bar' => __('About us', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'attributes' => __('Item Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'add_new' => __('Add About us', 'text_domain'),
        'new_item' => __('New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'view_items' => __('View Items', 'text_domain'),
        'search_items' => __('Search Item', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('About us', 'text_domain'),
        'description' => __('About us Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('about_us', $args);
}

add_action('init', 'about_us_custome', 0);
define('MY_WORDPRESS_FOLDER', $_SERVER['DOCUMENT_ROOT']);
define('MY_THEME_FOLDER', str_replace("\\", '/', dirname(__FILE__)));
define('MY_THEME_PATH', '/' . substr(MY_THEME_FOLDER, stripos(MY_THEME_FOLDER, 'wp-content')));
add_action('admin_init', 'my_meta_init');

function my_meta_init() {
    wp_enqueue_style('my_meta_css', MY_THEME_PATH . '/custom/meta.css');
    foreach (array('download', 'fep_message', 'competitions') as $type) {
        add_meta_box('my_all_meta', 'custome box', 'my_meta_setup', $type, 'normal', 'high');
    }
    add_action('save_post', 'my_meta_save');
}

function my_meta_setup() {
    global $post;
    $meta = get_post_meta($post->ID, '_my_meta', TRUE);
    $date_compition = get_post_meta($post->ID, 'date_compition', TRUE);
    include(MY_THEME_FOLDER . '/custom/meta.php');
    echo '<input type="hidden" name="my_meta_noncename" value="' . wp_create_nonce(__FILE__) . '" />';
}

function my_meta_save($post_id) {
    global $wpdb;
    if (!wp_verify_nonce($_POST['my_meta_noncename'], __FILE__))
        return $post_id;
    if ($_POST['post_type'] == 'page') {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    }
    else {
        if (!current_user_can('edit_post', $post_id))
            return $post_id;
    }
    $my_post = array(
        'ID' => $post_id,
        'post_date' => $_POST['date_compition'] . ' ' . date('H:i:s'),
    );

    $wpdb->update(
            'ua_posts', array(
        'post_date' => $_POST['date_compition']  // string
            ), array('ID' => $post_id)
    );

    $date_compition = get_post_meta($post_id, 'date_compition', TRUE);
    if ($date_compition) {
        update_post_meta($post_id, 'date_compition', $_POST['date_compition']);
    } else {
        add_post_meta($post_id, 'date_compition', $_POST['date_compition'], TRUE);
    }


    // wp_update_post( $my_post,true );
    if (is_wp_error($post_id)) {
        $errors = $post_id->get_error_messages();
        foreach ($errors as $error) {
            echo $error;
        }
        die();
    }
    $current_data = get_post_meta($post_id, '_my_meta', TRUE);


    $current_data = get_post_meta($post_id, '_my_meta', TRUE);
    $new_data = $_POST['_my_meta'];
    my_meta_clean($new_data);
    if ($current_data) {
        if (is_null($new_data))
            delete_post_meta($post_id, '_my_meta');
        else
            update_post_meta($post_id, '_my_meta', $new_data);
    }
    elseif (!is_null($new_data)) {
        add_post_meta($post_id, '_my_meta', $new_data, TRUE);
    }

    return $post_id;
}

function my_meta_clean(&$arr) {
    if (is_array($arr)) {
        foreach ($arr as $i => $v) {
            if (is_array($arr[$i])) {
                my_meta_clean($arr[$i]);

                if (!count($arr[$i])) {
                    unset($arr[$i]);
                }
            } else {
                if (trim($arr[$i]) == '') {
                    unset($arr[$i]);
                }
            }
        }
        if (!count($arr)) {
            $arr = NULL;
        }
    }
}

// Register Custom Post Type
function reviews_custome() {
    $labels = array(
        'name' => _x('Reviews', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Reviews', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Reviews', 'text_domain'),
        'name_admin_bar' => __('Reviews', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'attributes' => __('Item Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'add_new' => __('Add Reviews', 'text_domain'),
        'new_item' => __('New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'view_items' => __('View Items', 'text_domain'),
        'search_items' => __('Search Item', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Reviews', 'text_domain'),
        'description' => __('Reviews Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor'),
        //   'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('reviews_customes', $args);
}

add_action('init', 'reviews_custome', 0);

// Register Custom Post Type
function social_custome() {
    $labels = array(
        'name' => _x('Social account', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Social account', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Social account', 'text_domain'),
        'name_admin_bar' => __('Social account', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'attributes' => __('Item Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'add_new' => __('Add Social account', 'text_domain'),
        'new_item' => __('New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'view_items' => __('View Items', 'text_domain'),
        'search_items' => __('Search Item', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Social account', 'text_domain'),
        'description' => __('Social account Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('social_accounts', $args);
}

add_action('init', 'social_custome', 0);

// Register Custom Post Type
function social_customea() {
    $labels = array(
        'name' => _x('Competitions', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('competitions', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Competitions', 'text_domain'),
        'name_admin_bar' => __('Competitions', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'attributes' => __('Item Attributes', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Items', 'text_domain'),
        'add_new_item' => __('Add New Item', 'text_domain'),
        'add_new' => __('Add competitions account', 'text_domain'),
        'new_item' => __('New Item', 'text_domain'),
        'edit_item' => __('Edit Item', 'text_domain'),
        'update_item' => __('Update Item', 'text_domain'),
        'view_item' => __('View Item', 'text_domain'),
        'view_items' => __('View Items', 'text_domain'),
        'search_items' => __('Search Item', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image' => __('Use as featured image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Competitions', 'text_domain'),
        'description' => __('Competitions Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'taxonomies' => array('post_category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('competitions', $args);
}

add_action('init', 'social_customea', 0);

function to_send_commission_mail($to, $reciver_email, $message) {
    $subject = "commission email";
    $headers = array('Content-Type: text/html; charset=UTF-8');
    wp_mail($send_email, $subject, $message, $headers);
}

/* * ************* Show all Vendors  *********************** */

function vendor_show_all() {
    ?>
    <section class="regular-pro">
        <?php
        global $wpdb;

        // $findID = $wpdb->get_results("SELECT * FROM ua_fes_vendors  WHERE status = 'approved' ORDER BY id DESC Limit 12",ARRAY_A);
        //echo "<pre>";
        //print_r($findID);
        ?>  
        <div class="row">
            <div class="col-lg-12 producers-main">
                <?php
                $args = array(
                    'role' => 'shop_vendor',
                    'orderby' => 'name',
                    'order' => 'asc',
                    'number' => -1
                );
                $findID = get_users($args);
                //echo get_avatar( get_the_author_meta( 'ID' ), 100 ); 
                echo '<div>';
//echo "<pre>"; 
//print_r($users);
                if (!empty($findID)) {
                    $firstLetter = 0;
                    $desiredColumns = 6;  //you can change this!
                    $columnCount = (count($findID) + 26) / $desiredColumns + 1;
                    echo "<table><tr><td>";
                    //echo "<pre>";
                    // print_r($findID);       
                    foreach ($findID as $key => $cur) {
                        $flag = 0;
                        global $wpdb;
                        $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $cur->ID . " AND meta_key = 'first_name'");
                        $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $cur->ID . " AND meta_key = 'last_name'");
                        if ($first_letter != substr($findu[0]->meta_value, 0, 1)) {
                            $first_letter = substr($findu[0]->meta_value, 0, 1);
                            $flag = 1;
                        }

                        if ($flag) {
                            echo '<strong style="text-transform: uppercase;">' . $first_letter . '</strong><br>'; // Output the first letter
                        }
                        ?>
                        <a class="user-fullname" href="<?php echo get_permalink(6633); ?>?artist=<?php echo $cur->ID; ?>"><?php //echo $cur->display_name;                                                                                                                           ?>
                            <?php
//echo "<pre>";
//print_r($findu);
                            ?>
                            <?php
                            echo $findu[0]->meta_value;
                            echo " ";
                            echo $findu1[0]->meta_value;
                            ?>


                        </a><br />
                        <?php
                        //echo "<a href=".home_url()."/artist-main/>".$cur['name']."</a><br/>";
                    }
                    echo "</td><tr></table>";
                } else {
                    echo 'No users found.';
                }
                echo '</div>';
                ?>  
            </div>
        </div>
    </section>
    <?
}

add_shortcode('all_vendors', 'vendor_show_all');

/* * ************* end Show all Vendors  *********************** */

/* * ************* Show all grid Vendors  *********************** */

function vendor_show_all_list() {
    ?>
    <section class="regular-pro">
        <?php global $wpdb; ?>  
        <div class="row">
            <div class="col-lg-12 producers-main">
                <?php
                $args = array(
                    'role' => 'shop_vendor',
                    'orderby' => 'date',
                    'order' => 'Desc',
                    'number' => ''
                );
                $users = get_users($args);

                foreach ($users as $user) {

                    $use = $user->ID;
                    ?>
                    <div class="col-sm-3">
                        <div class="main_clas">
                            <a class="" href="<?php echo get_permalink(6633); ?>?artist=<?php echo $user->ID; ?>" > 
                                <div class="post_img">
                                    <?php
                                    global $wpdb;
                                    $findID = $wpdb->get_results("SELECT * from ua_usermeta as da INNER JOIN ua_postmeta as d ON da.meta_value = d.post_id GROUP BY da.user_id =" . $user->ID . " AND d.meta_key = '_wp_attached_file'
");
//echo "<pre>";
//print_r($findID);
                                    if ($findID[1]->meta_value == '') {
                                        ?>
                                        <div class="dummy_img">
                                            <?php
                                            echo get_avatar(get_the_author_meta($user->ID), 200);
                                            ?></div>
                                        <?php
                                    } else {
                                        ?>
                                        <img class="img_200" src="http://beta.uk-arts.com/wp-content/uploads/<?php echo $findID[1]->meta_value; ?>">
                                    <?php } ?>
                                </div>

                                <div class="post_link">
                                    <?php
                                    global $wpdb;
                                    $findu = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user->ID . " AND meta_key = 'first_name'");
                                    $findu1 = $wpdb->get_results("SELECT * from ua_usermeta where user_id =" . $user->ID . " AND meta_key = 'last_name'");
//echo "<pre>";
//print_r($findu);
                                    ?>
                                    <?php
                                    echo $findu[0]->meta_value;
                                    echo " ";
                                    echo $findu1[0]->meta_value;
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                echo '</div>';
                ?>  

            </div>
    </section>
    <?
}

add_shortcode('all_vendors_list', 'vendor_show_all_list');

/* * ************* end Show grid all Vendors  *********************** */


/* * ************* Show all Vendors  *********************** */

function post_blog_page() {
    global $wpdb;
    //print_r($author_postsa);
    $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

    $limit = 4; // number of rows in page
    $offset = ( $pagenum - 1 ) * $limit;
    $table_name = $wpdb->prefix . "posts";
    $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type='post' and post_status='publish'");
    //echo $total.'<br>';
    //echo $limit.'<br>';

    $num_of_pages = ceil($total / $limit);

    $author_querya = array('post_type' => 'post', 'paged' => $pagenum,
        'posts_per_page' => $limit);
    $author_postsa = new WP_Query($author_querya);
    //print_r($author_postsa);
    ?>
    <div class="blog-post-outer">
        <?php
        while ($author_postsa->have_posts()) : $author_postsa->the_post();
            $get_the_id = get_the_id();
            ?>
            <div class="blog-post-inner">
                <?php if (has_post_thumbnail()) { ?>
                    <div class="blog-page-thumbnail"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail(); ?></a>
                    </div>
                <?php } else { ?>
                    <div class="blog-page-thumbnail"><a href="<?php echo the_permalink(); ?>"><img src="/wp-content/uploads/edd/2018/05/Winter-light-Mawgan-Porth.jpg" class="dummy_img" /></a>
                    </div>

                <?php } ?>

                <div class="blog-page-date"><? echo get_the_date(); ?></div>
                <div class="blog-page-title">
                    <h2><a href="<?php echo the_permalink(); ?>"><? echo the_title(); ?></a></h2>
                </div>
                <div class="blog-page-content"><? echo the_excerpt(); ?></div>
                <div class="blog-page-readmore"><a href="<?php echo the_permalink(); ?>">Read More</a></div>
            </div>
        <?php endwhile; ?>
        <div class="pagination-compition">
            <?php
            $page_links = paginate_links(array(
                'base' => add_query_arg('pagenum', '%#%'),
                'format' => '',
                'prev_text' => __('&laquo;', 'text-domain'),
                'next_text' => __('&raquo;', 'text-domain'),
                'total' => $num_of_pages,
                'current' => $pagenum
            ));

            if ($page_links) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
            }
            ?>
        </div>
    </div>
    <?php
}

add_shortcode('blog_shortcode', 'post_blog_page');

function post_competitions_page() {
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $author_querya = array('paged' => $paged,
        'orderby' => 'DESC',
        'posts_per_page' => 6,
        'post_type' => 'competitions');
    $author_postsa = new WP_Query($author_querya);
    ?>

    <div class="blog-post-outer">

        <?php if ($author_postsa->have_posts()) : while ($author_postsa->have_posts()) : $author_postsa->the_post(); ?>

                <?php
                $get_the_id = get_the_id();
                $my_meta = get_post_meta($get_the_id, 'date_compition', TRUE);
                $date_compition = $my_meta;
                $date_comp = substr($date_compition, -5, 2);
                $current_month = get_the_date('F');
                $author_postsa->current_post;
                $f = $author_postsa->current_post - 1;
                $old_date = mysql2date('F', $author_postsa->posts[$f]->post_date);
                if ($current_month != $old_date) {
                    ?>
                    <h2><?php the_date('F Y'); ?> </h2>
                <?php } ?>
                <div class="blog-post-inner">
                    <div class="blog-page-thumbnail">
                        <?php if (has_post_thumbnail()) { ?>
                            <div class="blog-page-thumbnail">
                                <a href="<?php echo the_permalink(); ?>">
                                    <? echo the_post_thumbnail(); ?>
                                </a>
                            </div>
                        <?php } else { ?>
                            <div class="blog-page-thumbnail">
                                <a href="<?php echo the_permalink(); ?>">
                                    <img src="/wp-content/uploads/edd/2018/05/Winter-light-Mawgan-Porth.jpg" class="dummy_img" />
                                </a>
                            </div>

                        <?php } ?>

                    </div>
                    <div class="blog-page-title">
                        <a href="<?php the_permalink(); ?>">
                            <h2><?php the_title(); ?>
                        </a>
                        </h2>
                    </div>

                    <div class="blog-page-date">
                        <h2><?php
                            echo date("d M Y", strtotime(get_the_date($date_compition)));
                            ?></h2><!--span class="add-new-btnd">  
                            <h2> <?php
                        $post_date1 = get_the_date();
                        $date = strtotime(str_replace('/', '-', $post_date1));
                        $today = strtotime(date("d-m-Y"));
                        if ($date >= $today) {
                            
                        } else { //echo "Expiredbhhhh";   
                        }
                        ?></h2>
                        </span-->
                    </div>
                    <div class="blog-page-date">
                        <h3>competition Date:  <?php
                            echo date("d M Y", strtotime(get_the_date($date_compition)));
                            ?></h3>
                    </div>
                    <div class="blog-page-content"><? echo the_excerpt(); ?></div>  
                </div>

                <?php
//}
            endwhile;
            ?>

            <div class="submit-comption-main">
                <div class="btn btn-info btn-lg compe-butt" data-toggle="modal" data-target="#myModal">
                    SUBMIT A COMPETITION
                </div>                                   
            </div>
            <div class="pagination-compition demo">
                <?php echo custamPagination($paged, $author_postsa->max_num_pages); ?>    
            </div>
            <?php
        endif;
        wp_reset_postdata();
        ?>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Contact For Competition</h4>
                    </div>  
                    <div class="modal-body">
                        <?php echo do_shortcode('[contact-form-7 id="3852" title="competion-form"]'); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}

add_shortcode('post_competitions', 'post_competitions_page');
add_action('load-upload.php', 'custom_load_edit');

function custom_load_edit() {

    add_action('pre_get_posts', 'custom_pre_get_posts');
    add_filter('disable_months_dropdown', 'custom_disable_months_dropdown', 10, 2);
    add_action('admin_print_styles', 'custom_admin_print_styles');
}

function custom_pre_get_posts($query) {

    $query->set('author', get_current_user_id());
}

function custom_disable_months_dropdown($false, $post_type) {

    $disable_months_dropdown = $false;

    $disable_post_types = array('attachment');

    if (in_array($post_type, $disable_post_types)) {

        $disable_months_dropdown = true;
    }

    return $disable_months_dropdown;
}

function custom_admin_print_styles() {

    echo '<style>';
    echo '#posts-filter #attachment-filter, #posts-filter #post-query-submit, #posts-filter .search-form { display: none; }';
    echo ' .media-toolbar .media-toolbar-secondary #media-attachment-date-filters, .media-toolbar .media-toolbar-secondary #media-attachment-filters, .media-toolbar .media-toolbar-secondary .media-button, .media-toolbar .search-form { display: none; } { display: none; }';
    echo '</style>';
}

add_action('ajax_query_attachments_args', 'custom_ajax_query_attachments_args');

function custom_ajax_query_attachments_args($query) {

    if ($query['post_type'] != 'attachment') {

        return $query;
    }

    $query['author'] = get_current_user_id();

    return $query;
}

function add_width_height() {

    echo '<div class="row bjhgjg">
<div class="col-md-2">
<div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Width"></div>
</div>
<div class="col-md-2">
<div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Height"></div>
</div>
<div class="col-md-2">
<div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Depth"></div>
</div>
<div class="col-md-2">
<div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Price"></div>
</div>
<div class="col-md-3">
<input type="button" id="somebutton" value="Add Size">
</div>
</div>';
}

add_action('add_width_height_depth', 'add_width_height', 10, 2);

function create_account() {
    //You may need some data validation here
    if (isset($_POST['vendor-buyer'])) {
        if ($_POST['vendor-buyer'] == "role_buyer") {

            //  die("buyer form");
            $user_id = get_the_id('user_id');
            $user = ( isset($_POST['uname']) ? $_POST['uname'] : '' );
            $pass = ( isset($_POST['upass']) ? $_POST['upass'] : '' );
            $email = ( isset($_POST['uemail']) ? $_POST['uemail'] : '' );
            $pass = ( isset($_POST['pass']) ? $_POST['pass'] : '' );
            $parts = explode("@", $email);
            $username4 = $parts[0];

                        $last = ( isset($_POST['surname']) ? $_POST['surname'] : '' );
            
            




            if (!email_exists($email)) {
                //$user_id = wp_create_user( $user, $pass, $email );
                $userdata = array(
                    'user_login' => $username4,
                    'user_email' => $email,
                    'user_pass' => $pass,
                    'first_name' => $user,
                    'last_name' => $last,
                    'user_nicename' => $user . ' ' . $_POST['surname'], // When creating an user, `user_pass` is expected.
                );

                $user_id = wp_insert_user($userdata);
                if (!is_wp_error($user_id)) {
                    //user has been created
                    $user = new WP_User($user_id);
                    $user->set_role('contributor');

                    $met_pass = get_user_meta($user_id, 'ps', true);
                    if (!empty($met_pass)) {
                        update_user_meta($user_id, 'ps', $pass);
                    } else {
                        add_user_meta($user_id, 'ps', $pass);
                    }




                    $useremail = get_user_by('email', $email);



                    $url_status = get_site_url() . "/buyer-approved?user_id=" . $user_id;

                    $to = $email;
                    $subject = 'Subject: Welcome to the UK Arts Marketplace';
                    $body = "Hi " . $useremail->user_nicename . ", 
Thanks for joining the UK-Arts Marketplace. We will love to serve you here.
Your login details to the marketplace are:
Username:  " . $username4 . " 
For security purpose you need to click on activation link below to get your account activated: " . $url_status . " 
 Password:  " . $pass . "
 Regards
 UK-Arts Family";

                    mail($to, $subject, $body);
                    $user_data = update_user_meta($user_id, 'pw_user_status', 'pending');

                    /* If profile was saved, update profile. */
                    if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'update-user') {
                        require_once( ABSPATH . 'wp-admin/includes/image.php' );
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                        require_once( ABSPATH . 'wp-admin/includes/media.php' );
                        $img_id = media_handle_upload('wp-user-avatar', $user_id);
                        // $img_id = media_handle_upload( 'ua_user_avatar', 0 );   
                        update_user_meta($user_id, 'ua_user_avatar', $img_id);
                        add_user_meta($user_id, 'paymet_fess', 0);
                        $wpusercert = media_handle_upload('wp-user_certificate', $user_id);
                        $met_uu = get_user_meta($user_id, 'rf', true);
                        if (!empty($met_uu)) {
                            update_user_meta($user_id, 'ua_user_certificate', $wpusercert);
                        } else {
                            add_user_meta($user_id, 'ua_user_certificate', $wpusercert);
                        }

                        $first = ( isset($_POST['first_name']) ? $_POST['first_name'] : '' );
                        update_user_meta($user_id, 'first_name', $first);

                        $last = ( isset($_POST['last_name']) ? $_POST['last_name'] : '' );
                        update_user_meta($user_id, 'last_name', $last);

                        $description = ( isset($_POST['description']) ? $_POST['description'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'description', $description);

                        $contact_no = ( isset($_POST['contact_no']) ? $_POST['contact_no'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'contact_no', $contact_no);

                        $checkbox = ( isset($_POST['checkbox']) ? $_POST['checkbox'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'checkbox', $checkbox);
                    }
                    wp_redirect(get_site_url() . '/uk-artists-collection/?m=successful');
                    exit;
                } else {
                    //$user_id is a WP_Error object. Manage the error
                }
            } else {
                wp_redirect(get_site_url() . '/uk-artists-collection/?m=failed' . email_exists($email));
            }
        } else if ($_POST['vendor-buyer'] == "role_vendor") {
            //  die("vendor form ");
            echo $user_id = get_the_id('user_id');
            $user = ( isset($_POST['uname']) ? $_POST['uname'] : '' );
            //$pass = ( isset($_POST['upass']) ? $_POST['upass'] : '' );
            $email = ( isset($_POST['uemail']) ? $_POST['uemail'] : '' );
            $pass = ( isset($_POST['pass']) ? $_POST['pass'] : '' );
            $parts = explode("@", $email);
            $username4 = $parts[0];



            if (!email_exists($email)) {
                //$user_id = wp_create_user( $user, $pass, $email );
                $userdata = array(
                    'user_login' => $username4,
                    'user_email' => $email,
                    'user_pass' => $pass,
                    'user_nicename' => $user . ' ' . $_POST['surname'], // When creating an user, `user_pass` is expected.
                );
                $user_id = wp_insert_user($userdata);

                if (!is_wp_error($user_id)) {
                    //user has been created
                    $user = new WP_User($user_id);
                    $user->set_role('shop_vendor');

                    $met_pass = get_user_meta($user_id, 'ps', true);
                    if (!empty($met_pass)) {
                        update_user_meta($user_id, 'ps', $pass);
                    } else {
                        add_user_meta($user_id, 'ps', $pass);
                    }

                    //Redirect
                    //  wp_redirect( 'http://gloanaya.com/ukarts/demo-register/' );


                    /* If profile was saved, update profile. */
                    if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'update-user') {
                        require_once( ABSPATH . 'wp-admin/includes/image.php' );
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                        require_once( ABSPATH . 'wp-admin/includes/media.php' );
                        $img_id = media_handle_upload('wp-user-avatar', $user_id);
                        // $img_id = media_handle_upload( 'ua_user_avatar', 0 );   
                        update_user_meta($user_id, 'ua_user_avatar', $img_id);


                        add_user_meta($user_id, 'paymet_fess', 0);



                        $wpusercert = media_handle_upload('wp-user_certificate', $user_id);
                        $met_uu = get_user_meta($user_id, 'rf', true);
                        if (!empty($met_uu)) {
                            update_user_meta($user_id, 'ua_user_certificate', $wpusercert);
                        } else {
                            add_user_meta($user_id, 'ua_user_certificate', $wpusercert);
                        }

                        $first = ( isset($_POST['first_name']) ? $_POST['first_name'] : '' );
                        update_user_meta($user_id, 'first_name', $first);

                        $last = ( isset($_POST['last_name']) ? $_POST['last_name'] : '' );
                        update_user_meta($user_id, 'last_name', $last);

                        $description = ( isset($_POST['description']) ? $_POST['description'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'description', $description);



                        $contact_no = ( isset($_POST['contact_no']) ? $_POST['contact_no'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'contact_no', $contact_no);

                        $checkbox = ( isset($_POST['checkbox']) ? $_POST['checkbox'] : '' );
                        // $description = $_POST['description'];
                        update_user_meta($user_id, 'checkbox', $checkbox);
                    }

                    wp_redirect(get_site_url() . '/join-uk-arts-family/?m=successful');
                    exit;
                } else {
                    //$user_id is a WP_Error object. Manage the error
                }
            } else {
                wp_redirect(get_site_url() . '/join-uk-arts-family/?m=failed' . email_exists($email));
            }
        }
    }
}

add_action('init', 'create_account');

function sumobi_edd_display_checkout_fields() {
    // get user's phone number if they already have one stored
    if (is_user_logged_in()) {
        $user_id = get_current_user_id();
        $phone = get_the_author_meta('_edd_user_phone', $user_id);
        $phonea = get_the_author_meta('_edd_user_phonea', $user_id);
    }
    $phone = isset($phone) ? esc_attr($phone) : '';
    $phonea = isset($phonea) ? esc_attr($phonea) : '';
    ?>
    <p id="edd-phone-wrap">
        <label class="edd-label" for="edd-phone">
            <?php echo 'Contact Number'; ?>
        </label>
        <span class="edd-description">
            <?php echo 'Enter your phone number so we can get in touch with you.'; ?>
        </span>
        <input class="edd-input" type="text" name="edd_phone" id="edd-phone" placeholder="<?php echo 'Contact Number'; ?>" value="<?php echo $phone; ?>" />

        <input class="edd-input" type="text" name="edd_phonea" id="edd-phonea" value="<?php echo $phonea; ?>" />
    </p>


    <?php
}

add_action('edd_purchase_form_user_info', 'sumobi_edd_display_checkout_fields');

/**
 * Make phone number required
 * Add more required fields here if you need to
 */
function sumobi_edd_required_checkout_fields($required_fields) {
    $required_fields = array(
        'edd_phone' => array(
            'error_id' => 'invalid_phone',
            'error_message' => 'Please enter a valid Phone number'
        ),
    );

    return $required_fields;
}

add_filter('edd_purchase_form_required_fields', 'sumobi_edd_required_checkout_fields');

/**
 * Set error if phone number field is empty
 * You can do additional error checking here if required
 */
function sumobi_edd_validate_checkout_fields($valid_data, $data) {
    if (empty($data['edd_phone'])) {
        edd_set_error('invalid_phone', 'Please enter your phone number.');
    }
}

add_action('edd_checkout_error_checks', 'sumobi_edd_validate_checkout_fields', 10, 2);

/**
 * Store the custom field data into EDD's payment meta
 */
function sumobi_edd_store_custom_fields($payment_meta) {
    $payment_meta['phone'] = isset($_POST['edd_phone']) ? sanitize_text_field($_POST['edd_phone']) : '';
    $payment_meta['phonea'] = isset($_POST['edd_phonea']) ? sanitize_text_field($_POST['edd_phonea']) : '';

    return $payment_meta;
}

add_filter('edd_payment_meta', 'sumobi_edd_store_custom_fields');

/**
 * Add the phone number to the "View Order Details" page
 */
function sumobi_edd_view_order_details($payment_meta, $user_info) {
    $phone = isset($payment_meta['phone']) ? $payment_meta['phone'] : 'none';
    $phonea = isset($payment_meta['phonea']) ? $payment_meta['phonea'] : 'none';
    ?>
    <div class="column-container">
        <div class="column">
            <strong><?php echo 'Phone: '; ?></strong>
            <input type="text" name="edd_phone" value="<?php esc_attr_e($phone); ?>" class="medium-text" />
            <input type="text" name="edd_phonea" value="<?php esc_attr_e($phonea); ?>" class="medium-text" />
            <p class="description"><?php _e('Customer phone number', 'edd'); ?></p>
        </div>
    </div>
    <?php
}

add_action('edd_payment_personal_details_list', 'sumobi_edd_view_order_details', 10, 2);

/**
 * Save the phone field when it's modified via view order details
 */
function sumobi_edd_updated_edited_purchase($payment_id) {

    // get the payment meta
    $payment_meta = edd_get_payment_meta($payment_id);

    // update our phone number
    $payment_meta['phone'] = isset($_POST['edd_phone']) ? $_POST['edd_phone'] : false;
    $payment_meta['phonea'] = isset($_POST['edd_phonea']) ? $_POST['edd_phonea'] : false;

    // update the payment meta with the new array 
    update_post_meta($payment_id, '_edd_payment_meta', $payment_meta);
}

add_action('edd_updated_edited_purchase', 'sumobi_edd_updated_edited_purchase');

/**
 * Add a {phone} tag for use in either the purchase receipt email or admin notification emails
 */
if (function_exists('edd_add_email_tag')) {
    edd_add_email_tag('phone', 'Customer\'s phone number', 'sumobi_edd_email_tag_phone');
}

/**
 * The {phone} email tag
 */
function sumobi_edd_email_tag_phone($payment_id) {
    $payment_data = edd_get_payment_meta($payment_id);
    return $payment_data['phone'];
}

/**
 * Update user's phone number in the wp_usermeta table
 * This phone number will be shown on the user's edit profile screen in the admin
 */
function sumobi_edd_store_usermeta($payment_id) {
    // return if user is not logged in
    if (!is_user_logged_in())
        return;
    // get the user's ID
    $user_id = get_current_user_id();
    // update phone number
    update_user_meta($user_id, '_edd_user_phone', $_POST['edd_phone']);
    update_user_meta($user_id, '_edd_user_phonea', $_POST['edd_phonea']);
}

add_action('edd_complete_purchase', 'sumobi_edd_store_usermeta');

/**
 * Save the field when the values are changed on the user's WP profile page
 */
function sumobi_edd_save_extra_profile_fields($user_id) {
    if (!current_user_can('edit_user', $user_id))
        return false;
    update_user_meta($user_id, '_edd_user_phone', $_POST['phone']);
    update_user_meta($user_id, '_edd_user_phonea', $_POST['phonea']);
}

add_action('personal_options_update', 'sumobi_edd_save_extra_profile_fields');
add_action('edit_user_profile_update', 'sumobi_edd_save_extra_profile_fields');

/**
 * Save the field when the value is changed on the EDD profile editor
 */
function sumobi_edd_pre_update_user_profile($user_id, $userdata) {

    $phone = isset($_POST['edd_phone']) ? $_POST['edd_phone'] : '';
    $phonea = isset($_POST['edd_phonea']) ? $_POST['edd_phonea'] : '';
    // Make sure user enters a phone number
    if (!$phone) {
        edd_set_error('phone_required', __('Please enter a phone number', 'edd'));
    }
    if (!$phonea) {
        edd_set_error('phone_requireda', __('Please enter a phone number', 'edd'));
    }
    // update phone number
    update_user_meta($user_id, '_edd_user_phone', $phone);
    update_user_meta($user_id, '_edd_user_phonea', $phonea);
}

add_action('edd_pre_update_user_profile', 'sumobi_edd_pre_update_user_profile', 10, 2);

/**
 * Add the Phone to the "Contact Info" section on the user's WP profile page
 */
function sumobi_user_contactmethods($methods, $user) {
    $methods['_edd_user_phone'] = 'Phone';
    $methods['_edd_user_phonea'] = 'Phonea';
    return $methods;
}

add_filter('user_contactmethods', 'sumobi_user_contactmethods', 10, 2);

/* function to_update_event_mail(){
  if($_GET['post_type']=="event_listing" &&  isset($_GET['approve_event'])  ){

  $author_id = get_post_field ('post_author', $_GET['approve_event']);
  $user_email = get_the_author_meta( 'user_email' , $author_id );
  $post=get_post( $_GET['approve_event'] );
  //echo $user_email;
  $to = $user_email;
  //$from = 'UK-Arts';
  $subject = 'Event Approve '.$post->post_title;
  $body = 'Your Event '.$post->post_title.' has been approved. Thanks for work with UK-Arts  ';
  $headers = array('Content-Type: text/html; charset=UTF-8');
  //$headerss .= 'From: uuu <silktbr@gmail.com>' . "\r\n";
  wp_mail( $to, $subject, $body, $headers);

  }

  }
  add_action( 'init', 'to_update_event_mail' ); */

function remove_core_updates() {
    global $wp_version;
    return(object) array('last_checked' => time(), 'version_checked' => $wp_version,);
}

add_filter('pre_site_transient_update_core', 'remove_core_updates');
add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
add_filter('pre_site_transient_update_themes', 'remove_core_updates');

function my_login_logo_one() {
    ?> 
    <style type="text/css">  
        body.login div#login h1 a {
            background-image: url(http://gloanaya.com/ukarts/wp-content/uploads/2018/08/C_Users_dell_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_b5c72c75-3820-4500-b16e-08ba0c961061.png);  //Add your own logo image in this url 
            padding-bottom: 30px; 
        } 
        .page-id-106 .pagination-compition .next.page-numbers::after {
            content: "\f105" !important;
            font-family: FontAwesome !important;
            font-style: normal;
            font-weight: normal;
            font-size: 36px;
        }
        .page-id-106 .pagination-compition .prev.page-numbers::after {
            content: "\f104" !important;
            font-family: FontAwesome !important;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;
            font-size: 36px;
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo_one');

function custom_excerpt_length($length) {
    return 13;
}

add_filter('excerpt_length', 'custom_excerpt_length', 30);


/* 	function new_excerpt_more($more) {
  global $post;
  return '<span class="post_an"><a class="moretag" href="'. get_permalink($post->ID) . '"> Read More...</a></span>';
  }
  add_filter('excerpt_more', 'new_excerpt_more'); */

function pw_load_scripts() {
    wp_enqueue_script('custom-js', '/wp-content/themes/marketify-child/js/custom.js');
}

add_action('admin_enqueue_scripts', 'pw_load_scripts');

function send_email_notification_to_organizer($post_id) {
    $admin_email = get_option('admin_email');

    $post = get_post($post_id);
    $author = get_userdata($post->post_author);
    //$author_mail = $author->user_email;
    $author_mail = 'aish.anayacommunication@gmail.com';
    $to = $admin_email;
    $subject = 'Ukarts';
    $message = "
	  Hi " . $author->display_name . ",
	  Your listing, " . $post->post_title . " has just been approved at " . get_permalink($post_id) . ". 
   ";
    $headers = array('Content-Type: text/html; charset=UTF-8', 'Cc: ' . $author_mail . '');
    wp_mail($to, $subject, $message, $headers);
}

add_action('publish_event_listing', 'send_email_notification_to_organizer');



/* CREATE SHORTCODE FOR EVENT LISTING */

function post_event_listing() {
    global $wpdb;
    //print_r($author_postsa);
    $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

    $limit = 1; // number of rows in page
    $offset = ( $pagenum - 1 ) * $limit;
    $table_name = $wpdb->prefix . "posts";
    $total = $wpdb->get_var("SELECT COUNT(`id`) FROM " . $table_name . " where post_type='event_listing' and 'post_status' => 'publish'");

    $num_of_pages = ceil($total / $limit);
    ?>

    <div class="blog-post-outer">

        <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $author_querya = array('paged' => $paged,
            'orderby' => 'DESC',
            'posts_per_page' => 1,
            'post_type' => 'event_listing',
            'order' => 'DESC',
            'post_status' => 'publish');
        $author_postsa = new WP_Query($author_querya);

        while ($author_postsa->have_posts()) : $author_postsa->the_post();

            $get_the_id = get_the_id();
            $my_meta = get_post_meta($get_the_id, '_event_start_date', TRUE);
            $date_compition = $my_meta;
            $date_comp = substr($date_compition, -5, 2);
            $current_month = get_the_date('F');
            $author_postsa->current_post;
            $f = $author_postsa->current_post - 1;
            $old_date = mysql2date('F', $author_postsa->posts[$f]->post_date);
            if ($current_month != $old_date) {
                ?>
                <h2><?php the_date('F Y'); ?> </h2><?php
            }
            ?>
            <div class="blog-post-inner">
                <div class="blog-page-thumbnail">
                    <?php if (has_post_thumbnail()) { ?>
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    <?php } else { ?>

                        <a href="<?php echo the_permalink(); ?>">
                            <img src="/wp-content/uploads/edd/2018/05/Winter-light-Mawgan-Porth.jpg" class="dummy_img" />
                        </a>
                    <?php } ?>

                </div>
                <div class="blog-page-title">
                    <a href="<?php the_permalink(); ?>">
                        <h2><?php the_title(); ?>
                    </a>
                    </h2>
                </div>

                <div class="blog-page-date">
                    <h2><?php
                        echo date("d M Y", strtotime(get_the_date($date_compition)));
                        ?></h2><span class="add-new-btnd">  
                        <h2> <?php
                            $post_date1 = get_the_date();
                            $date = strtotime(str_replace('/', '-', $post_date1));
                            $today = strtotime(date("d-m-Y"));
                            if ($date >= $today) {
                                
                            } else { //echo "Expiredbhhhh";   
                            }
                            ?></h2>
                    </span>
                </div>
                <div class="blog-page-date">
                    <h3>Date:  <?php
                        echo date("d M Y", strtotime(get_the_date($date_compition)));
                        ?></h3>
                </div>
                <div class="blog-page-content"><?php echo the_excerpt(); ?></div>  
            </div>
            <?php
        //}
        endwhile;
        ?>
    </div>
    <div class="pagination-compition demo">
        <?php
        echo custamPagination($paged, $author_postsa->max_num_pages);
        ?>   
    </div>
    <?php
}

add_shortcode('post_custom_events', 'post_event_listing');

/* FUNCTION FOR CUSTOM PAGINATION */

if (!function_exists('custamPagination')) {

    function custamPagination($paged = '', $max_page = '') {
        $big = 999999999; // need an unlikely integer
        if (!$paged)
            $paged = get_query_var('paged');
        if (!$max_page)
            $max_page = $wp_query->max_num_pages;

        echo paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => max(1, $paged),
            'total' => $max_page,
            'mid_size' => 1,
             'prev_text' => __('<img src="/wp-content/uploads/2018/03/left-angle.png" alt="">', 'text'), 
            'next_text' => __('<img src="/wp-content/uploads/2018/03/left-angle.png" alt="">', 'text'),
            'type' => 'list'
        ));
    }

}

/* CUSTOM FIELD IN USERS */

function custom_user_profile_fields($user) {
    ?>
    <h3>Extra profile information</h3>
    <table class="form-table">
        <tr>
            <th><label for="artistStmt">Artist Statement</label></th>
            <td>
                <input type="text" class="regular-text" name="artistStmt" value="<?php echo esc_attr(get_the_author_meta('artistStmt', $user->ID)); ?>" id="artistStmt" /><br />
            </td>
        </tr>
        <tr>
            <th><label for="bannerImage">Banner Image URL</label></th>
            <td>
                <input type="text" class="regular-text" name="bannerImage" value="<?php echo esc_attr(get_the_author_meta('bannerImage', $user->ID)); ?>" id="bannerImage" /><br />
            </td>

        </tr>
    </table>
    <?php
}

add_action('show_user_profile', 'custom_user_profile_fields');
add_action('edit_user_profile', 'custom_user_profile_fields');
add_action("user_new_form", "custom_user_profile_fields");

function save_custom_user_profile_fields($user_id) {
    # again do this only if you can
    if (!current_user_can('manage_options'))
        return false;

    # save my custom field
    update_usermeta($user_id, 'artistStmt', $_POST['artistStmt']);
    update_usermeta($user_id, 'bannerImage', $_POST['bannerImage']);
}

add_action('user_register', 'save_custom_user_profile_fields');
add_action('profile_update', 'save_custom_user_profile_fields');
