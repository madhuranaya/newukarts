<?php
/**
 * @package Marketify
 */
?>


 <div class="container">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
       <span class="thumbnail-img"><?php the_post_thumbnail(); ?></span> <span class="cont-desc-mn"> <?php the_content(); ?></span>
       <br>

        <span class="mand-saab"> <?php echo do_shortcode('[feather_share]'); ?>
        <?php
            wp_link_pages( array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'marketify' ),
                'after'  => '</div>',
            ) );
        ?>
</span>
        <div class="entry-meta entry-meta--hentry entry-meta--footer">
            <?php the_tags( '<span class="entry-tags">', ', ', '</span>' ); ?>
            <span class="entry-categories"><?php the_category( ', ' ); ?></span>
            <?php edit_post_link( __( 'Edit', 'marketify' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-meta -->
    </div><!-- .entry-content -->
</article><!-- #post-## -->
</div>  
