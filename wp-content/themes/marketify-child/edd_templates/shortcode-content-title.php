<?php
/**
 *
 */
global $post;
?>

<header class="content-grid-download__entry-header">
    <?php do_action('marketify_download_entry_title_before_' . get_post_format()); ?>


    <?php do_action('marketify_download_entry_title_after_' . get_post_format()); ?>

    <?php if (get_theme_mod('downloads-archives-excerpt', '') && $post->post_excerpt) : ?>
        <div class="entry-excerpt"><?php echo esc_attr(wp_trim_words($post->post_excerpt)); ?></div>
    <?php endif; ?>

    <div class="entry-meta">
        <div class="main_dd">
            <div class="left-img">
                <?php
                $txt = sprintf('<span>%1$s</span>', get_avatar(get_the_author_meta('ID'), 50, apply_filters('marketify_default_avatar', null)));
                echo $txt;
                ?>

            </div>
            <div class="right-cont">
                <h3 class="entry-title <?php if ('on' == esc_attr(get_theme_mod('downloads-archives-truncate-title', 'on'))) : ?> entry-title--truncated<?php endif; ?>">
                    <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                </h3>
                <?php
//echo get_the_ID();
// echo get_post_meta(get_the_ID(),'product_status',true);
                ?>
                <?php do_action('marketify_download_entry_meta_before_' . get_post_format()); ?><?php //do_action( 'marketify_download_entry_meta' ); ?><?php do_action('marketify_download_entry_meta_after_' . get_post_format()); ?>

                <?php
                $get_the_id = get_the_author_meta('ID');
                $site_url = site_url();
                printf(
                        __('<span class="byline ddd">by%1$s</span>', 'marketify'), sprintf('<span class="author vcard"><a class="url fn n" href="' . $site_url . '/artist-main/?user_id=' . $get_the_id . '" title="%2$s">%4$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'marketify'), get_the_author())), get_avatar(get_the_author_meta('ID'), 50, apply_filters('marketify_default_avatar', null)), esc_html(get_the_author_meta('display_name'))
                        )
                );
                if (!empty(get_post_meta(get_the_ID(), 'product_status', true)) && get_post_meta(get_the_ID(), 'product_status', true) == "Active") {
                    ?>

                <?php }
                ?> 
            </div>	  
        </div>
        <div class="main-price">
            <p class="price-cls">
                <?php /* $values = get_post_meta( get_the_id(), 'edd_price' ); 
                  echo edd_format_amount($values[0]);
                  echo $values[0]; */ ?>
                <?php $get_the_ID = get_the_ID(); ?>
                <strong class="item-price"><span><?php printf(__('%s', 'marketify'), edd_price(get_the_ID(), false)); ?></span></strong>
                <?php
                $button_text = apply_filters('marketify_purchase_button_text', __('<i class="fa fa-shopping-cart" aria-hidden="true"></i>', 'marketify'), $download_id);

                $button = !empty($edd_options['add_to_cart_text']) ? $edd_options['add_to_cart_text'] : $button_text;
                $class = 'button buy-now popup-trigger';

// printf( '<a href="#buy-now-%s" class="%s">%s</a>', $post->ID, $class, $button );
// echo $form;
                ?>     </p>
        </div>



        <div class="comlex-info-product" id="complex-information">
            <?php // dynamic_sidebar('reviews');
            ?>



            <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">

                <a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>



                <div class="edd_purchase_submit_wrapper">
                    <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="€ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>

                </div><!--end .edd_purchase_submit_wrapper-->


            </form>


            <div class="star-shape-custome">
                <style>
                    .black_overlay{
                        display: none;
                        position: fixed;
                        top: 0;
                        left:0;
                        width: 100%;
                        height: 100%;z-index:9998;
                        background: rgba(0,0,0,0.8);

                    }
                    .white_content<?php echo $get_the_id; ?> {
                        display: none;
                        position: fixed;
                        top: 25%;
                        left: 50%;
                        margin-left: -400px;
                        width: 800px;
                        /*height: 50%;*/
                        padding: 20px;
                        /*border: 16px solid orange;*/
                        background-color: white;
                        z-index:9999;

                        box-shadow: 0 0 10px rgba(0,0,0,0.3);
                    } .white_content<?php echo $get_the_id; ?> > a
                    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
                     font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
                     background: #70C0EF;
                     border-radius: 100px;}   

                    .white_content<?php echo $get_the_id; ?> input[type="text"]
                    {height:50px; width: 100%; border:1px solid #ccc;}

                    .white_content<?php echo $get_the_id; ?> input[type="submit"]
                    {position: absolute; right:0; top: 0; height: 50px;}

                    .white_content<?php echo $get_the_id; ?> form
                    {position: relative;}
                </style>
                <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'block';
                        document.getElementById('fade<?php echo $get_the_id; ?>').style.display = 'block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
                <div id="light<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
                    <?php
                    $current_user = wp_get_current_user();
                    $current_user_id = $current_user->ID;
                    ?>

                    <form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
                        <input type="text" name="review_title" placeholder="Review Title" />
                        <select name="review_starts">
                            <option name="0">Select Review star:</option>
                            <option name="1">1</option>
                            <option name="2">2</option>
                            <option name="3">3</option>
                            <option name="4">4</option>
                            <option name="5">5</option>
                        </select>
                        <input type="text" name="review_description" placeholder="Review Description" />
                        <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
                        <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
                        <input type="submit" name="submit" value="submit" />

                    </form>
                    <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display = 'none';
                            document.getElementById('fade<?php echo $get_the_id; ?>').style.display = 'none'">X</a>



                </div>
            </div>




            <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>



        </div> 

    </div>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 25px;
        }

        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 15px;
            width: 15px;
            left: 3px;
            bottom: 5px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #dedc00;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
</header><!-- .entry-header -->
