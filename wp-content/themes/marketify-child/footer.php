<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Marketify
 */
?>

<?php
//$current_user_id = get_current_user_id();
//echo $current_user_id= $current_user_id; 
?>


<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
    $(document).ready(function () {

        $("#phone").keypress(function (e) {

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

    });
</script>
<script>
    webshims.setOptions('forms-ext', {types: 'date'});
    webshims.polyfill('forms forms-ext');
</script>
<script>
    $(document).ready(function () {
        $("#somebutton").click(function () {
            $(".bjhgjg:last").after('<div class="row bjhgjg"><div class="col-md-2"><div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Width"></div></div><div class="col-md-2"><div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Height"></div></div><div class="col-md-2"><div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Depth"></div></div><div class="col-md-2"><div class="inputtt"><input type="text" name="add_width_height_depth[]" placeholder="Price"></div></div></div>');

        });
        $('.add_width_height_depth').on('click', '#some_close', function () {
            $(this).parent().remove();

        });
        $('#Color').click(function () {
            /*var id = $('.fes-price-value').val();
             var RP = id.substring(0, id.length);
             alert(RP);
             //console.log("id iss"+id); 
             var no = $('#edd_sale_price').val(); 
             var SP = no.substring(0, no.length);
             alert(SP);
             //console.log("val iss"+no);
             //alert(no);
             if(SP > RP){
             alert("Sale Price Invalid");
             $( "#edd_sale_price" ).focus();
             }	*/

            var firstInput = document.getElementById("options[0][price]").value;
            var rp = parseInt(firstInput);
            //alert(rp);
            var secondInput = document.getElementById("edd_sale_price").value;
            var sp = parseInt(secondInput);
            //alert(sp);

            if (sp > rp) {
                alert("Value must be equal to or lower than price");
                $("#edd_sale_price").focus();
            }


        });

        /* Pagination Centeral Line */
        $("<div class='pagiCenter'></div>").insertAfter(".prev.page-numbers");
    });
</script>
<?php
if (is_user_logged_in()) {
    ?><style>  
        li.current-cart.menu-item.menu-item-has-children.xzvcxvcx {
            margin-right: 10%;
            margin-top: -5.1%;
        }
    </style><?
} else {
    
}
?>
<style>
    .archive.tax-download_category .site-content, .widget-area { display: none !important; }
    .archive.tax-download_category .page-header.container { display: none; }

    option[value="200+200"]   {   display: none;  } 
    .rmrow.rm_pricefield_row  {    display: none; }
    fieldset.rmfieldset label {   display: none;  }
    div#rm_terms_textarea     {    display: none; }


    .search-results span.comments-link {   display: none; }
    .search-results span.edit-link {     display: none; }
    .page-id-890 .fes-feat-image-upload img{     height: 300px !important; }
    div#tribe-bar-collapse-toggle1 {
        display: none;
    }
    .sabai-googlemaps-map {
        display: none;
    }
    a.sabai-btn.sabai-btn-info.sabai-btn-xs.sabai-googlemaps-find-on-map{
        display: none;
    }
    a.sabai-btn.sabai-btn-info.sabai-btn-xs.sabai-googlemaps-get-from-map{
        display: none;
    }
    .sabai-form-field.sabai-form-field-add.sabai-form-nolabel.sabai-form-type-item {
        display: none;
    }
    .tribe-events-day-time-slot h5 {
        display: none;
    }
    p.tribe-events-promo {
        display: none;
    }
    .fes-submission-form-div .fes-el {
        clear: both;
    }
</style>

<?php if (is_page(890)) {
    ?>    

    <script>
        jQuery(document).ready(function () {
            jQuery(".explain_others").hide();
            jQuery(".prints_available").hide();
            jQuery(".are_the_prints_numbered_and_signed").hide();
            jQuery(".what_media_are_the_prints_printed_on").hide();
            jQuery(".how_prints_are").hide();
            jQuery(".how_many_prints_are_available").hide();
            jQuery('#how_many_prints_are_available').on('change', function () {
                //alert("hello");
                var FieldVal = $('#how_many_prints_are_available').val();
                //alert(FieldVal);
                if (FieldVal < 150) {
                    //alert("Valid!");         
                } else {
                    alert("Value must be equal to or lower than 150");
                    $("#how_many_prints_are_available").focus();
                }
            });
            jQuery("#download_category").change(function () {
                var cat = jQuery(this).val();
                if ((cat == 64) || (cat == 100) || (cat == 104) || (cat == 102) || (cat == 101) || (cat == 98) || (cat == 105) || (cat == 103) || (cat == 106) || (cat == 99) || (cat == 73) || (cat == 127) || (cat == 133) || (cat == 128) || (cat == 129) || (cat == 132) || (cat == 131) || (cat == 126) || (cat == 130)) {
                    jQuery(".prints_available").hide();

                } else {
                    jQuery(".prints_available").show();
                }
            });

            jQuery("input[name='prints_available']").click(function () {
                var cat = jQuery(this).val();
                if (cat === 'Yes') {
                    jQuery(".are_the_prints_numbered_and_signed").show();
                    jQuery(".what_media_are_the_prints_printed_on").show();
                    jQuery(".how_prints_are").show();
                    jQuery(".how_many_prints_are_available").show();


                } else {
                    jQuery(".are_the_prints_numbered_and_signed").hide();
                    jQuery(".what_media_are_the_prints_printed_on").hide();
                    jQuery(".how_prints_are").hide();
                    jQuery(".how_many_prints_are_available").hide();
                }
            });

            jQuery("input[name='what_media_are_the_prints_printed_on']").click(function () {
                if (jQuery(this).val() === 'Other') {
                    jQuery(".explain_others").show();
                } else {
                    jQuery(".explain_others").hide();
                }
            });

        });


        // jStepper 1.5.4
        !function (e) {
            e.fn.jStepper = function (t, n, a) {
                if (this.length > 1)
                    return this.each(function () {
                        $(this).jStepper(t)
                    }), this;
                if ("string" == typeof t)
                    return"option" === t && (null === a && (a = e.fn.jStepper.defaults[n]), this.data("jstepper.o")[n] = a), this;
                var l = e.extend({}, e.fn.jStepper.defaults, t);
                e.metadata && (l = e.extend({}, l, this.metadata())), this.data("jstepper.o", l), l.disableAutocomplete && this.attr("autocomplete", "off"), e.isFunction(this.mousewheel) && this.mousewheel(function (t, n) {
                    if (n > 0) {
                        var a = e.Event("keydown");
                        return a.keyCode = 38, i(1, a, this), !1
                    }
                    if (0 > n) {
                        var a = e.Event("keydown");
                        return a.keyCode = 40, i(0, a, this), !1
                    }
                }), this.blur(function () {
                    r(this, null)
                }), this.keydown(function (e) {
                    var t = e.keyCode;
                    if (38 === t)
                        i(1, e, this);
                    else if (40 === t)
                        i(0, e, this);
                    else if ("ignore" === l.overflowMode) {
                        var n = 0 === $(this).val().indexOf("-") ? l.minValue : l.maxValue;
                        if (n && $(this).val().length >= n.toString().length && (t >= 48 && 57 >= t || t >= 96 && 105 >= t) && this.selectionStart === this.selectionEnd)
                            return!1
                    }
                }), this.keyup(function (e) {
                    r(this, e)
                });
                var r = function (t, n) {
                    var a = e(t), r = a.val(), i = r;
                    l.disableNonNumeric && (r = r.replace(/[^\d\.,\-]/gi, ""), r = r.replace(/-{2,}/g, "-"), r = r.replace(/(.+)\-+/g, "$1"));
                    var s = !1;
                    null !== l.maxValue && parseFloat(r) > l.maxValue && (r = l.maxValue, s = !0), null !== l.minValue && "" != r && parseFloat(r) < parseFloat(l.minValue) && (r = l.minValue, s = !0), (c(n) === !0 || null === n || s === !0) && (r = u(r)), i != r && a.val(r)
                }, i = function (t, n, a) {
                    var i, u = e(a);
                    i = n ? n.ctrlKey ? l.ctrlStep : n.shiftKey ? l.shiftStep : l.normalStep : l.normalStep;
                    var s = u.val(), o = s.length - a.selectionStart, f = s.length - a.selectionEnd;
                    s = s.replace(/,/g, "."), s = s.replace(l.decimalSeparator, "."), s += "", -1 != s.indexOf(".") && (s = s.match(new RegExp("-{0,1}[0-9]+[\\.][0-9]*"))), s += "", -1 != s.indexOf("-") && (s = s.match(new RegExp("-{0,1}[0-9]+[\\.]*[0-9]*"))), s += "", s = s.match(new RegExp("-{0,1}[0-9]+[\\.]*[0-9]*")), ("" === s || "-" == s || null === s) && (s = l.defaultValue), s = 1 === t ? e.fn.jStepper.AddOrSubtractTwoFloats(s, i, !0) : e.fn.jStepper.AddOrSubtractTwoFloats(s, i, !1);
                    var m = !1;
                    return null !== l.maxValue && s >= l.maxValue && (s = l.maxValue, m = !0), null !== l.minValue && s <= l.minValue && (s = l.minValue, m = !0), s = s.toString().replace(/\./, l.decimalSeparator), u.val(s), a.selectionStart = s.length - o, a.selectionEnd = s.length - f, r(a, n), l.onStep && l.onStep(u, t, m), !1
                }, u = function (e) {
                    var t = e.toString();
                    return t = s(t), t = o(t), t = f(t), t = m(t)
                }, s = function (e) {
                    var t = e;
                    if (l.minDecimals > 0) {
                        var n;
                        if (-1 != t.indexOf(".")) {
                            var a = t.length - (t.indexOf(".") + 1);
                            a < l.minDecimals && (n = l.minDecimals - a)
                        } else
                            n = l.minDecimals, t += ".";
                        for (var r = 1; n >= r; r++)
                            t += "0"
                    }
                    return t
                }, o = function (e) {
                    var t = e;
                    if (l.maxDecimals > 0) {
                        var n = 0;
                        -1 != t.indexOf(".") && (n = t.length - (t.indexOf(".") + 1), l.maxDecimals < n && (t = t.substring(0, t.indexOf(".")) + "." + t.substring(t.indexOf(".") + 1, t.indexOf(".") + 1 + l.maxDecimals)))
                    }
                    return t
                }, f = function (e) {
                    var t = e;
                    return l.allowDecimals || (t = t.toString().replace(l.decimalSeparator, "."), t = t.replace(new RegExp("[\\.].+"), "")), t
                }, m = function (e) {
                    var t = e;
                    if (null !== l.minLength) {
                        var n = t.length;
                        -1 != t.indexOf(".") && (n = t.indexOf("."));
                        var a = !1;
                        if (-1 != t.indexOf("-") && (a = !0, t = t.replace(/-/, "")), n < l.minLength)
                            for (var r = 1; r <= l.minLength - n; r++)
                                t = "0" + t;
                        a && (t = "-" + t)
                    }
                    return t
                }, c = function (e) {
                    var t = !1;
                    return null !== e && (38 === e.keyCode || 40 === e.keyCode) && (t = !0), t
                };
                return this
            }, e.fn.jStepper.AddOrSubtractTwoFloats = function (e, t, n) {
                var a = e.toString(), l = t.toString(), r = "";
                if (a.indexOf(".") > -1 || l.indexOf(".") > -1) {
                    -1 === a.indexOf(".") && (a += ".0"), -1 === l.indexOf(".") && (l += ".0");
                    for (var i = a.substr(a.indexOf(".") + 1), u = l.substr(l.indexOf(".") + 1), s = a.substr(0, a.indexOf(".")), o = l.substr(0, l.indexOf(".")), f = !0; f; )
                        i.length !== u.length ? i.length < u.length ? i += "0" : u += "0" : f = !1;
                    for (var m = i.length, c = 0; c <= i.length - 1; c++)
                        s += i.substr(c, 1), o += u.substr(c, 1);
                    var d, h = Number(s), p = Number(o);
                    d = n ? h + p : h - p;
                    var g = !1;
                    0 > d && (g = !0, d = Math.abs(d)), r = d.toString();
                    for (var v = 0; v < m - r.length + 1; v++)
                        r = "0" + r;
                    r.length >= m && (r = r.substring(0, r.length - m) + "." + r.substring(r.length - m)), g === !0 && (r = "-" + r)
                } else
                    r = n ? Number(e) + Number(t) : Number(e) - Number(t);
                return Number(r)
            }, e.fn.jStepper.defaults = {maxValue: null, minValue: null, normalStep: 1, shiftStep: 5, ctrlStep: 10, minLength: null, disableAutocomplete: !0, defaultValue: 1, decimalSeparator: ",", allowDecimals: !0, minDecimals: 0, maxDecimals: null, disableNonNumeric: !0, onStep: null, overflowMode: "default"}
        }(jQuery);
        $(document).ready(function () {
            $('#how_many_prints').jStepper({minValue: 1, maxValue: 150});

        });

    </script> <?php
}
?>
<?php if (is_page(444)) {
    ?>  

    <script>
        jQuery(document).ready(function () {

            //jQuery(".explain_others").hide();
            //jQuery(".are_the_prints_numbered_and_signed").hide();
            //jQuery(".what_media_are_the_prints_printed_on").hide();
            jQuery('#how_many_prints_are_available').on('change', function () {
                //alert("hello");
                var FieldVal = $('#how_many_prints_are_available').val();
                //alert(FieldVal);
                if (FieldVal < 150) {
                    //alert("Valid!");         
                } else {
                    alert("Value must be equal to or lower than 150");
                    $("#how_many_prints_are_available").focus();
                }
            });
            var ptt = jQuery('input[name=prints_available]:checked').val();
            var ott = jQuery('input[name=what_media_are_the_prints_printed_on]:checked').val();
            if (ott == 'Other') {

                jQuery(".explain_others").show();

            } else {
                jQuery(".explain_others").hide();
            }
            //alert(ptt);

            var catt = jQuery('#download_category').find(":selected").val();

            if ((catt == 64) || (catt == 100) || (catt == 104) || (catt == 102) || (catt == 101) || (catt == 98) || (catt == 105) || (catt == 103) || (catt == 106) || (catt == 99) || (catt == 73) || (catt == 127) || (catt == 133) || (catt == 128) || (catt == 129) || (catt == 132) || (catt == 131) || (catt == 126) || (catt == 130)) {
                jQuery(".prints_available").hide();
                jQuery(".are_the_prints_numbered_and_signed").hide();
                jQuery(".what_media_are_the_prints_printed_on").hide();
                jQuery(".explain_others").hide();
                jQuery(".how_prints_are").hide();
                //jQuery("input[name='prints_available'][value='No']").attr('checked', 'checked'); 
                if (ott == 'Other') {

                    jQuery(".explain_others").show();

                }
            } else if (ptt == 'No') {
                jQuery(".are_the_prints_numbered_and_signed").hide();
                jQuery(".what_media_are_the_prints_printed_on").hide();
                jQuery(".explain_others").hide();
                jQuery(".how_prints_are").hide();
                jQuery(".how_many_prints_are_available").hide();

            } else {
                jQuery(".prints_available").show();
                jQuery(".are_the_prints_numbered_and_signed").show();
                jQuery(".what_media_are_the_prints_printed_on").show();
                jQuery(".how_prints_are").show();
                jQuery(".how_many_prints_are_available").show();
                // jQuery(".explain_others").hide();
                //jQuery("input[name='prints_available'][value='No']").attr('checked', 'checked');

            }


            jQuery("#download_category").change(function () {
                var cat = jQuery(this).val();
                if ((cat == 64) || (cat == 100) || (cat == 104) || (cat == 102) || (cat == 101) || (cat == 98) || (cat == 105) || (cat == 103) || (cat == 106) || (cat == 99) || (cat == 73) || (cat == 127) || (cat == 133) || (cat == 128) || (cat == 129) || (cat == 132) || (cat == 131) || (cat == 126) || (cat == 130)) {
                    jQuery(".prints_available").hide();
                    jQuery(".explain_others").hide();
                    jQuery(".are_the_prints_numbered_and_signed").hide();
                    jQuery(".what_media_are_the_prints_printed_on").hide();
                    jQuery("input[name='prints_available'][value='No']").attr('checked', 'checked');
                    jQuery(".how_prints_are").hide();
                    jQuery(".how_many_prints_are_available").hide();


                    jQuery('#how_many_prints_are_available').on('change', function () {
                        //alert("hello");
                        var FieldVal = $('#how_many_prints_are_available').val();
                        //alert(FieldVal);
                        if (FieldVal < 150) {
                            //alert("Valid!");         
                        } else {
                            alert("Value must be equal to or lower than 150");
                            $("#how_many_prints_are_available").focus();
                        }
                    });
                } else {
                    jQuery(".prints_available").show();
                    //jQuery("input[name='prints_available'][value='No']").attr('checked', 'checked');
                }
            });

            jQuery("input[name='prints_available']").click(function () {
                var caat = jQuery(this).val();
                if (caat === 'Yes') {
                    jQuery(".are_the_prints_numbered_and_signed").show();
                    jQuery(".what_media_are_the_prints_printed_on").show();
                    jQuery(".how_prints_are").show();
                    jQuery(".how_many_prints_are_available").show();
                } else {
                    jQuery(".are_the_prints_numbered_and_signed").hide();
                    jQuery(".what_media_are_the_prints_printed_on").hide();
                    jQuery(".how_prints_are").hide();
                    jQuery(".how_many_prints_are_available").hide();
                }
            });

            jQuery("input[name='what_media_are_the_prints_printed_on']").click(function () {
                if (jQuery(this).val() === 'Other') {
                    jQuery(".explain_others").show();

                } else {

                    jQuery(".explain_others").hide();
                }
            });

        });
        // jStepper 1.5.4
        !function (e) {
            e.fn.jStepper = function (t, n, a) {
                if (this.length > 1)
                    return this.each(function () {
                        $(this).jStepper(t)
                    }), this;
                if ("string" == typeof t)
                    return"option" === t && (null === a && (a = e.fn.jStepper.defaults[n]), this.data("jstepper.o")[n] = a), this;
                var l = e.extend({}, e.fn.jStepper.defaults, t);
                e.metadata && (l = e.extend({}, l, this.metadata())), this.data("jstepper.o", l), l.disableAutocomplete && this.attr("autocomplete", "off"), e.isFunction(this.mousewheel) && this.mousewheel(function (t, n) {
                    if (n > 0) {
                        var a = e.Event("keydown");
                        return a.keyCode = 38, i(1, a, this), !1
                    }
                    if (0 > n) {
                        var a = e.Event("keydown");
                        return a.keyCode = 40, i(0, a, this), !1
                    }
                }), this.blur(function () {
                    r(this, null)
                }), this.keydown(function (e) {
                    var t = e.keyCode;
                    if (38 === t)
                        i(1, e, this);
                    else if (40 === t)
                        i(0, e, this);
                    else if ("ignore" === l.overflowMode) {
                        var n = 0 === $(this).val().indexOf("-") ? l.minValue : l.maxValue;
                        if (n && $(this).val().length >= n.toString().length && (t >= 48 && 57 >= t || t >= 96 && 105 >= t) && this.selectionStart === this.selectionEnd)
                            return!1
                    }
                }), this.keyup(function (e) {
                    r(this, e)
                });
                var r = function (t, n) {
                    var a = e(t), r = a.val(), i = r;
                    l.disableNonNumeric && (r = r.replace(/[^\d\.,\-]/gi, ""), r = r.replace(/-{2,}/g, "-"), r = r.replace(/(.+)\-+/g, "$1"));
                    var s = !1;
                    null !== l.maxValue && parseFloat(r) > l.maxValue && (r = l.maxValue, s = !0), null !== l.minValue && "" != r && parseFloat(r) < parseFloat(l.minValue) && (r = l.minValue, s = !0), (c(n) === !0 || null === n || s === !0) && (r = u(r)), i != r && a.val(r)
                }, i = function (t, n, a) {
                    var i, u = e(a);
                    i = n ? n.ctrlKey ? l.ctrlStep : n.shiftKey ? l.shiftStep : l.normalStep : l.normalStep;
                    var s = u.val(), o = s.length - a.selectionStart, f = s.length - a.selectionEnd;
                    s = s.replace(/,/g, "."), s = s.replace(l.decimalSeparator, "."), s += "", -1 != s.indexOf(".") && (s = s.match(new RegExp("-{0,1}[0-9]+[\\.][0-9]*"))), s += "", -1 != s.indexOf("-") && (s = s.match(new RegExp("-{0,1}[0-9]+[\\.]*[0-9]*"))), s += "", s = s.match(new RegExp("-{0,1}[0-9]+[\\.]*[0-9]*")), ("" === s || "-" == s || null === s) && (s = l.defaultValue), s = 1 === t ? e.fn.jStepper.AddOrSubtractTwoFloats(s, i, !0) : e.fn.jStepper.AddOrSubtractTwoFloats(s, i, !1);
                    var m = !1;
                    return null !== l.maxValue && s >= l.maxValue && (s = l.maxValue, m = !0), null !== l.minValue && s <= l.minValue && (s = l.minValue, m = !0), s = s.toString().replace(/\./, l.decimalSeparator), u.val(s), a.selectionStart = s.length - o, a.selectionEnd = s.length - f, r(a, n), l.onStep && l.onStep(u, t, m), !1
                }, u = function (e) {
                    var t = e.toString();
                    return t = s(t), t = o(t), t = f(t), t = m(t)
                }, s = function (e) {
                    var t = e;
                    if (l.minDecimals > 0) {
                        var n;
                        if (-1 != t.indexOf(".")) {
                            var a = t.length - (t.indexOf(".") + 1);
                            a < l.minDecimals && (n = l.minDecimals - a)
                        } else
                            n = l.minDecimals, t += ".";
                        for (var r = 1; n >= r; r++)
                            t += "0"
                    }
                    return t
                }, o = function (e) {
                    var t = e;
                    if (l.maxDecimals > 0) {
                        var n = 0;
                        -1 != t.indexOf(".") && (n = t.length - (t.indexOf(".") + 1), l.maxDecimals < n && (t = t.substring(0, t.indexOf(".")) + "." + t.substring(t.indexOf(".") + 1, t.indexOf(".") + 1 + l.maxDecimals)))
                    }
                    return t
                }, f = function (e) {
                    var t = e;
                    return l.allowDecimals || (t = t.toString().replace(l.decimalSeparator, "."), t = t.replace(new RegExp("[\\.].+"), "")), t
                }, m = function (e) {
                    var t = e;
                    if (null !== l.minLength) {
                        var n = t.length;
                        -1 != t.indexOf(".") && (n = t.indexOf("."));
                        var a = !1;
                        if (-1 != t.indexOf("-") && (a = !0, t = t.replace(/-/, "")), n < l.minLength)
                            for (var r = 1; r <= l.minLength - n; r++)
                                t = "0" + t;
                        a && (t = "-" + t)
                    }
                    return t
                }, c = function (e) {
                    var t = !1;
                    return null !== e && (38 === e.keyCode || 40 === e.keyCode) && (t = !0), t
                };
                return this
            }, e.fn.jStepper.AddOrSubtractTwoFloats = function (e, t, n) {
                var a = e.toString(), l = t.toString(), r = "";
                if (a.indexOf(".") > -1 || l.indexOf(".") > -1) {
                    -1 === a.indexOf(".") && (a += ".0"), -1 === l.indexOf(".") && (l += ".0");
                    for (var i = a.substr(a.indexOf(".") + 1), u = l.substr(l.indexOf(".") + 1), s = a.substr(0, a.indexOf(".")), o = l.substr(0, l.indexOf(".")), f = !0; f; )
                        i.length !== u.length ? i.length < u.length ? i += "0" : u += "0" : f = !1;
                    for (var m = i.length, c = 0; c <= i.length - 1; c++)
                        s += i.substr(c, 1), o += u.substr(c, 1);
                    var d, h = Number(s), p = Number(o);
                    d = n ? h + p : h - p;
                    var g = !1;
                    0 > d && (g = !0, d = Math.abs(d)), r = d.toString();
                    for (var v = 0; v < m - r.length + 1; v++)
                        r = "0" + r;
                    r.length >= m && (r = r.substring(0, r.length - m) + "." + r.substring(r.length - m)), g === !0 && (r = "-" + r)
                } else
                    r = n ? Number(e) + Number(t) : Number(e) - Number(t);
                return Number(r)
            }, e.fn.jStepper.defaults = {maxValue: null, minValue: null, normalStep: 1, shiftStep: 5, ctrlStep: 10, minLength: null, disableAutocomplete: !0, defaultValue: 1, decimalSeparator: ",", allowDecimals: !0, minDecimals: 0, maxDecimals: null, disableNonNumeric: !0, onStep: null, overflowMode: "default"}
        }(jQuery);
        $(document).ready(function () {
            $('#how_many_prints').jStepper({minValue: 1, maxValue: 150});

        });


    </script> <?php
}
?>  

<?php
$get_the_id = get_the_id();
if ($get_the_id == "780") {
    ?><style>#menu-item-1262.current-menu-ancestor > a{   color: #dedc00 !important; }

    </style><?
} else {
    
}
?>

<?php if ($get_the_id == "117") {
    ?><style>#main {

            min-height: 1200px;

        }
    </style><? }
?>
<footer id="colophon" class="site-footer site-footer--<?php echo esc_attr(get_theme_mod('footer-style', 'light')); ?>" role="contentinfo">
    <div class="container">
        <?php do_action('marketify_footer_above'); ?>

        <div class="site-info row<?php echo is_active_sidebar('footer-1') ? ' has-widgets' : ''; ?>">
            <?php do_action('marketify_footer_site_info'); ?>
        </div><!-- .site-info -->

    </div>
</footer><!-- #colophon -->
</div><!-- #page --> 


<script>
    jQuery('select option:contains("All Download Categories")').text('All Categories');
//jQuery('select option:contains("All subjects")').text('Subject');
    jQuery('select option:contains("All medium")').text('Medium');
    jQuery('select option:contains("All orientation")').text('Orientation');
    jQuery('select option:contains("All size")').text('Size');
    jQuery('select option:contains("Color")').text('Overall tonal colour');
//jQuery('select option:contains("All style")').text('Style');
    jQuery('.sf-field-post-meta-edd_price select option:contains("All Items")').text('Price');
    jQuery('.sf-field-post-meta-Height select option:contains("All Items")').text('Size');
    jQuery('.sf-field-post-meta-Height select option:contains("0 - 50")').text('50 X 50');
    jQuery('.sf-field-post-meta-Height select option:contains("50 - 100")').text('50 X 100');
    jQuery('.sf-field-post-meta-Height select option:contains("100 - 150")').text('100 X 150');
    jQuery('.sf-field-post-meta-Height select option:contains("150 - 200")').text('150 +');
    document.getElementsByName('_sf_search[]')[0].placeholder = 'Keyword Search';
</script>  
<style>
    .sf-level-0.sf-item-0.sf-option-active{ display: none; }
    .sf-field-taxonomy-Color option {
        border-radius: 50% !important;
        color: transparent !important;
        font-size: 0 !important;
        height: 30px !important;
        width: 30px !important;
    }


    #preferred_contact_number {
        text-transform: capitalize;
    }
    .page-template-artist-main #artist {
        min-height: 800px;
    }
</style> 
<style>
    /*munish*/  
    @media only screen and (min-width:320px) and (max-width:375px){

    }
    @media only screen and (min-width:320px) and (max-width:499px){
        .page-id-2174 a.arts-button-directory-inner {
            margin-top: 3%;
        } 
        .mob-menu-header-holder.mobmenu .mob-icon-menu-1.mob-menu-icon {
            position: fixed;
            right: 20px;
        }.wpb_wrapper .main-title-directory .contact-title {
            line-height: 30px;
            padding-top: 5%;
        }

        .top-header form.search-form {
            float: left;
            width: 100%;

        }

    }
    @media only screen and (min-width:500px) and (max-width:615px){
        .mob-menu-header-holder.mobmenu .mob-icon-menu-1.mob-menu-icon {
            position: fixed;
            right: 20px;
        }
        .wpb_wrapper .discover {  
            padding: 60px 10px 10px 10px;

        }
        .top-header form.search-form {
            float: left;
            width: 100%;

        }
        .wpb_wrapper .container.banner h1 {
            padding-top: 40px;
        }

    }
    @media only screen and (min-width:616px) and (max-width:767px){
        .mob-menu-header-holder.mobmenu .mob-icon-menu-1.mob-menu-icon {
            position: fixed;
            right: 20px;
        }	
        .top-header form.search-form {
            float: left;
            width: 100%;

        }
        .wpb_wrapper .discover {
            padding: 60px 10px 10px 10px;

        }

        .page-id-117 .edd_downloads_list div[class^="col-"] .content-grid-download {
            max-width: 48% !important;
            float: left;
            margin-left: 10px !important;
            min-height: 315px;

        }
        .wpb_wrapper .container.banner h1 {
            padding-top: 40px;
        }

    }
    @media only screen and (min-width:768px) and (max-width:991px){
        .wpb_wrapper .discover {
            padding: 60px 10px 10px 10px;

        }

        .page-id-117 .edd_downloads_list div[class^="col-"] .content-grid-download {
            max-width: 100% !important;
            float: left;
            margin-left: 10px !important;
            min-height: 374px;
        } 
        .wpb_wrapper .container.banner h1 {
            padding-top: 40px;
        }
        .top-header form.search-form {
            float: left;
            width: 100%;

        }
    }

</style>

<?php
/* $args = array('post_type' => 'about_us',  meta_key => 'image_upload_2',       
  'posts_per_page' => 1);
  query_posts( $args );

  // the Loop
  while (have_posts()) : the_post();
  //Do your stuff

  //You can access your feature image like this:
  echo the_post_thumbnail();
  endwhile; */
?> 
<?php //the_gallery( 973 )    ?>

<?php wp_footer(); ?>