<?php

class Marketify_EDD_Template_Download {

    public function __construct() {
        add_action( 'wp_head', array( $this, 'featured_area' ) );
        add_action( 'wp_	enqueue_scripts', array( $this, 'enqueue_scripts' ) );

        add_action( 'marketify_entry_before', array( $this, 'download_title' ), 5 );
        add_action( 'marketify_entry_before', array( $this, 'featured_area_header_actions' ), 5 );

        add_action( 'marketify_download_info', array( $this, 'download_price' ), 5 );
        add_action( 'marketify_download_actions', array( $this, 'demo_link' ) );

        add_action( 'marketify_download_entry_title_before_audio', array( $this, 'featured_audio' ) );

        add_filter( 'post_class', array( $this, 'post_class' ), 10, 3 );
        add_filter( 'body_class', array( $this, 'body_class' ) );
    }

    public function post_class( $classes, $class, $post_id ) {
        if( ! $post_id || get_post_type( $post_id ) !== 'download' || is_admin() ) {
            return $classes;
        }

        if ( 'on' == esc_attr( get_theme_mod( 'downloads-archives-truncate-title', 'on' ) ) ) {
            $classes[] = 'edd-download--truncated-title';
        }

        return $classes;
    }

    public function body_class( $classes ) {
        $format = $this->get_post_format();
        $setting = esc_attr( get_theme_mod( "download-{$format}-feature-area" ), 'top' );

        $classes[] = 'feature-location-' . $setting;

        return $classes;
    }

    public function enqueue_scripts() {
        wp_enqueue_script( 'marketify-download', get_template_directory_uri() . '/js/download/download.js', array( 'marketify' ) );
    }

    public function download_price() {
        global $post;
?>
<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <span itemprop="price" class="edd_price">
        <?php edd_price( $post->ID ); ?>
    </span>
</span>
<?php
    }

    function demo_link( $download_id = null ) {
        global $post, $edd_options;

        if ( 'download' != get_post_type() ) {
            return;
        }

        if ( ! $download_id ) {
            $download_id = $post->ID;
        }

        $field = apply_filters( 'marketify_demo_field', 'demo' );
        $demo  = get_post_meta( $download_id, $field, true );

        if ( ! $demo ) {
            return;
        }

        $label = apply_filters( 'marketify_demo_button_label', __( 'Demo', 'marketify' ) );

        if ( $post->_edd_cp_custom_pricing ) {
            echo '<br /><br />';
        }

        $class = 'button';

        if ( ! did_action( 'marketify_single_download_content_before' ) ) {
            $class .= ' button--color-white';
        }

        echo apply_filters( 'marketify_demo_link', sprintf( '<a href="%s" class="%s" target="_blank">%s</a>', esc_url( $demo ), $class, $label ) );
    }

    public function get_featured_images() {
        global $post;

        $images  = array();
        $_images = get_post_meta( $post->ID, 'preview_images', true );

        if ( is_array( $_images ) && ! empty( $_images ) ) {
            foreach ( $_images as $image ) {
                $images[] = get_post( $image );
            }
        } else {
            $images = get_attached_media( 'image', $post->ID );
        }

        return apply_filters( 'marketify_download_get_featured_images', $images, $post );
    }

    public function featured_area() {
        global $post;

        if ( ! $post || ! is_singular( 'download' ) ) {
            return;
        }

        $format = get_post_format();

        if ( '' == $format ) {
            $format = 'standard';
        }

        if ( $this->is_format_location( 'top' ) ) {
            add_action( 'marketify_entry_before', array( $this, "featured_{$format}" ), 5 );

            if ( 'standard' != $format && $this->is_format_style( 'inline' ) ) {
                add_action( 'marketify_entry_before', array( $this, 'featured_standard' ), 6 );
            }
        } else {
            add_action( 'marketify_single_download_content_before_content', array( $this, 'featured_' . $format ), 5 );

            if ( method_exists( $this, 'featured_' . $format . '_navigation' ) ) {
                add_action( 'marketify_single_download_content_before_content', array( $this, 'featured_'. $format . '_navigation' ), 7 );
            }

            if ( 'standard' != $format && $this->is_format_style( 'inline' ) ) {
                add_action( 'marketify_single_download_content_before_content', array( $this, 'featured_standard' ), 6 );
                add_action( 'marketify_single_download_content_before_content', array( $this, 'featured_standard_navigation' ), 7 );
            }
        }
    }

    private function get_post_format() {
        global $post;

        if ( ! $post ) {
            return false;
        }

        $format = get_post_format();

        if ( '' == $format ) {
            $format = 'standard';
        }

        return $format;
    }

    public function is_format_location( $location ) {
        if ( ! is_array( $location ) ) {
            $location = array( $location );
        }

        $format = $this->get_post_format();
        $setting = esc_attr( get_theme_mod( "download-{$format}-feature-area", 'top' ) );

        if ( in_array( $setting, $location ) ) {
            return true;
        }

        return false;
    }

    public function is_format_style( $style ) {
        if ( ! is_array( $style ) ) {
            $style = array( $style );
        }

        $format = $this->get_post_format();
        $setting = esc_attr( get_theme_mod( "downloads-{$format}-feature-image", 'background' ) );

        if ( in_array( $setting, $style ) ) {
            return true;
        }

        return false;
    }

    public function download_title() {
        if ( ! is_singular( 'download' ) ) {
            return;
        }

        the_post();
    ?>
	<div class="custome-title-single-page container">
	    <div class="download-title"><? echo the_title(); ?></div>
	    <div class="the_author">by <span><?php the_author(); ?><span>
		</div>
	</div>
	 
    <div class="newdiv container">
        <div class="page-header page-header--download download-header container">
            <h1 class="page-titlxcdvce  dsfds"><?php// the_title(); ?></h1>
			<?php  $get_the_id= get_the_id();
			$categories = get_the_terms( $get_the_id, 'download_category','download' );
//print_r($categories);
//if(count($categories)>0){
foreach( $categories as $category ) {
       $term_id_select= $category->term_id;
 
} 
//}
       $term_id_select= $category->term_id;
	 if($term_id_select=="71" || $term_id_select=="72" || $term_id_select=="70" || $term_id_select=="74" || $term_id_select=="59"  || $term_id_select=="118" || $term_id_select=="119" || $term_id_select=="120" || $term_id_select=="121" || $term_id_select=="122" || $term_id_select=="123" || $term_id_select=="124" || $term_id_select=="125" || $term_id_select=="142" || $term_id_select=="137" || $term_id_select=="144" || $term_id_select=="138" || $term_id_select=="136" || $term_id_select=="141" || $term_id_select=="139" || $term_id_select=="143" || $term_id_select=="134" || $term_id_select=="135" || $term_id_select=="145" ||  $term_id_select=="95" || $term_id_select=="91" || $term_id_select=="86" || $term_id_select=="96" || $term_id_select=="88" || $term_id_select=="93" || $term_id_select=="92" || $term_id_select=="89" || $term_id_select=="94" || $term_id_select=="93" || $term_id_select=="97" ||	  $term_id_select=="80" || $term_id_select=="75" || $term_id_select=="87" || $term_id_select=="85" || $term_id_select=="79" || $term_id_select=="78" || $term_id_select=="76" || $term_id_select=="84" || $term_id_select=="81" || $term_id_select=="83" || $term_id_select=="82" || $term_id_select=="116" ||	  $term_id_select=="110" || $term_id_select=="114" || $term_id_select=="108" || $term_id_select=="112" || $term_id_select=="109" || $term_id_select=="107" || $term_id_select=="111" || $term_id_select=="115" || $term_id_select=="113") 
	 {
?><a href="#">Original</a> <a href="#">Print</a> <?

 //echo "print"; 
 } else{ } 
	 ?> 
			
                            <div class="product-information"><!--/product-information-->
                                <img alt="" class="newarrival" src="<?php echo site_url(); ?>/images/product-details/new.jpg">
                            <?php //  the_excerpt(); ?>
							
				<?php  $get_the_id= get_the_id(); echo "</br>";
							// display download categories
		echo the_terms( $get_the_id, 'medium', 'Media: ', ', ', '' );    echo "</br>";
		echo the_terms( $get_the_id, 'size', 'Size: ', ', ', '' );    echo "</br>";
        echo the_terms( $get_the_id, 'download_category', 'Style: ', ', ', '' );   echo "</br>";
        echo the_terms( $get_the_id, 'subject', 'Subject: ', ', ', '' );    echo "</br>";



?>
                               
                                <a href=""><img alt="" class="share img-responsive" src="<?php echo site_url(); ?>/images/product-details/share.png"></a>
                            </div><!--/product-information-->

<div class="price_addtocart_button">
<p class="price_symbol">£ <?php  $get_the_id= get_the_id(); 
      echo $price = get_post_meta($get_the_id, 'edd_price', true); ?>
	  </br><p class="make-an-offer"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/offer-icon.png" /></span> Make an Offer</p>
	  </p>

	  
	  
	  
        <div class="download-header__info download-header__info--actions">
            <?php do_action( 'marketify_download_actions' ); ?>
        </div>
</div>		
                          
 

    <?php
        rewind_posts();
    }

    public function featured_area_header_actions() {
        if ( ! is_singular( 'download' ) ) {
            return;
        }
    ?>
	
       
<?php// dynamic_sidebar('reviews'); ?>

<?php 
 $curentuser_contact = $wpdb->get_results("SELECT * author FROM artistcontact where currentlogin= '$current_user_name'");
                        //print_r($curentuser_contact);
                        foreach ( $curentuser_contact as $curentuser_contacted )
                        {
                         $current_first= $curentuser_contacted->firstname; 
                         $current_surename= $curentuser_contacted->surname;
                         $current_message= $curentuser_contacted->message; 
                        
                        }
						?>
	<form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">

	<a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart" style="margin: 2%;"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span>Follow this Artist</a>
	
        <div class="download-header__info">
            <?php //do_action( 'marketify_download_info' ); ?>
        </div>

		
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        Shipping
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      Anim pariatur
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <!--div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        Returns & Refunds
                    </a>
                </h4>
            </div-->
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
					
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        Pay by instalments
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    Anim pariatur cliche reprehenderit, 
            </div>
        </div>

	
		
		
    </div><!-- panel-group -->
	
	
	<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                    
					
					<?php  $get_the_id= get_the_id(); ?>
					<?php $get_permalink= get_permalink();	?>
				
                        <!--i class="more-less glyphicon glyphicon-plus"></i-->
                       <div class="share-icon"><div class="s-h">Share</div> <?php //dynamic_sidebar('socialicon'); ?>

						 
						 <aside id="text-4" class="footer-section-first">			<div class="textwidget"><div class="footer-social">

						  
						 
						 
<a href="http://twitter.com/home?status=<?php echo $get_permalink; ?>" target="_blank" title="Click to share this post on Twitter">
<span class="screen-reader-text">Twitter</span></a>
						
<a href="#" title="Click to share this post on instagram"><span class="screen-reader-text">Instagram</span></a>
						
<a href="http://www.facebook.com/share.php?u=<?php echo $get_permalink; ?>" onclick="return fbs_click()"  target="_blank"> <span class="screen-reader-text">Facebook</span>	</a>
							
<a href="http://pinterest.com/?url=<?php echo $get_permalink; ?>" target="_blank" title="Click to share this post on Pintrest"> <span class="screen-reader-text">Pinterest</span></a>


	</div>
</div>
		</aside>					
					  
                 
            </div>
            </div>
            <div id="headingFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    
            </div>
        </div>

    </div><!-- panel-group -->

		<div class="artist-contact-button">
		 <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content {
        display: none;
        position: fixed;
        top: 25%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        /*height: 50%;*/
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: white;
        z-index:99999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
background: #70C0EF;
border-radius: 100px;}   

      .white_content input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

     .white_content form
      {position: relative;}
	  
	  .outer-div {
    width: 48%;
    float: left;
    margin-left: 2%;
	  margin-bottom: 2%; }
input {

    width: 100%;

}
a.signup_button {
    position: relative;
    right: auto;
    display: inline;
    top: auto;
    background: transparent;
    color: #DEDC00;
}
</style>


<div id="vbvbvb">
<!-- Popup Div Starts Here -->
<div id="popupContact">
<!-- Contact Us Form -->
<form action="#" id="form" method="post" name="form">
<img id="close" src="http://anayaclients.com/ukarts/wp-content/uploads/2018/03/Cross.png" onclick ="div_hide()">
<h2>Contact Artist Name</h2>

<input id="name" name="ftext" placeholder="Name" value="<?php //echo the_author(); ?>" type="text" required />
<input id="email" name="ltext" placeholder="Email" type="text" required />
<textarea id="msg" name="ymessage" placeholder="Message"></textarea>
<!--a href="javascript:%20check_empty()" id="submit">Send</a-->
<input class="sub-btn" type="submit" name="submit" value="Send" />
<p class="buyer">You can only contact the artist if you have a buyer account. If you dont, please <span class="signup"><a href="<?php echo site_url(); ?>/terms-conditions/">signup for free</a></span> here.</p>
</form>

      	<?php  
	if (isset($_POST["submit"])) {
    //echo "Yes";
 echo $firstname = $_POST['ftext'];
 echo $surname = $_POST['ltext'];
 echo $message = $_POST['ymessage'];
 echo $currentuser = $_POST['currentuser'];
 echo $authoruser = $_POST['authoruser'];
 
 global $wpdb;
 //$wpdb->query($sql);
$sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin,author) values (%s,%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser,$authoruser);
 $wpdb->query($sql);   

 $to = "money.shrma@gmail.com";

$txt = "Hello world!";
$headers = "From: shanmand01@gmail.com";

mail($to,$message,$firstname,$headers);
 ?>
 <?php echo the_author(); ?>
 <?php echo get_the_author_id(); ?>
 <meta http-equiv="refresh" content="1;url=<?php echo site_url(); ?>/chat/" />
<?php

$cookie_name = "author";
$cookie_value = get_the_author_id();
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

/* if(!isset($_COOKIE[$cookie_name])) {
    echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
    echo "Cookie '" . $cookie_name . "' is set!<br>";
    echo "Value is: " . $_COOKIE[$cookie_name];
} */
  
}else{  
  //  echo "N0";
}   
?>






</div>
</div>

<?php 
if ( is_user_logged_in() ) {
	  // $author_id= get_the_author_id(); ?>

<button id="popup" onclick="div_show()">Contact Artist Name</button>
<?php } else { ?>

 <p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p>
	<div id="light" class="white_content"> You can only contact the artist if you have a buyer account. If you don’t, please <a class="signup_button" href="<?php echo site_url(); ?>/art-lover-register/">signup</a> for free here. <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X</a></div>
    <div id="fade" class="black_overlay"></div>

	 


<? }
?>

<script>
function div_show() {
document.getElementById('vbvbvb').style.display = "block";
}
function div_hide(){
document.getElementById('vbvbvb').style.display = "none";
}
</script>




<style>
form#popupContact {    background-color: white; }
#vbvbvb { width:100%; height:100%; top:0;left:0; display:none;position:fixed; background-color:#313131;
overflow:auto; z-index: 99999;}
img#close {
    position:absolute;right:-14px;
top:-14px;cursor:pointer
}
div#popupContact {
  background: #fff none repeat scroll 0 0;
  font-family: "Raleway",sans-serif;
  left: 20%;
  margin-left: 0;
  padding-bottom: 30px;
  padding-top: 40px;
  position: absolute;
  top: 17%;
  width: 60%;
}
#popupContact {
  min-height: 410px;
  padding: 20px;
}
#popupContact h2 {
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 20px;
  text-align: center;
}
#popupContact input {
  background: #f2f2f2 none repeat scroll 0 0;
  border: medium none;
  display: inline-block;
  float: left;
  height: 40px;
  margin-left: 10px;
  margin-right: 10px;
  padding-left: 10px;
  width: 46%;
}
#popupContact > img {
  background: #fff none repeat scroll 0 0;
  border-radius: 50%;
  margin-right: 0;
  margin-top: 6px;
  padding: 5px;
  width: 40px;
}
.buyer {
  float: left;
  margin-left: 20px;
  margin-top: 30px;
  text-align: left;
  width: 60%;
}
#popupContact textarea {
  background: #eee none repeat scroll 0 0;
  border: medium none;
  float: left;
  height: 140px;
  margin-left: 10px;
  margin-top: 40px;
  padding: 10px;
  width: 95%;
}
.signup {
  color: #e0de0d;
}
.sub-btn {
  background: #dedc00 none repeat scroll 0 0 !important;
  height: 40px !important;
  margin-top: 30px;
  width: 200px !important;
  float: left;
}
</style>



 <!--p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Contact the Artist</a></p-->
	<div id="light" class="white_content">
<?php
if ( is_user_logged_in() ) {
	   $author_id= get_the_author_id(); ?></p>
	
<? // echo do_shortcode('[front-end-pm]');
 //echo do_shortcode('[yobro_chatbox]');  ?>

 
 		  <form name="contactusartist" method="post">
	     <input type="text" name="ftext" placeholder="First name" required />
	     <input type="text" name="ltext" placeholder="Sure" required />
		  <textarea rows="4" cols="50" name="ymessage" placeholder="Your message" required>
</textarea> 
<?php $current_user = wp_get_current_user(); ?>
<?php  echo  $current_user->user_login;  ?>
   
  
 <input type="hidden" name="currentuser" value="<?php  echo  $current_user->user_login;  ?>" />
  <input type="hidden" name="authoruser" value="<?php echo $author_id= get_the_author(); ?>" />
<input type="submit" name="submit" value="Send" />
<p>You can only contact the artist,If you have a buyer account,If you don't,please <a href="#">signup</a> here </p>
	  </form>
	<?php  
	if (isset($_POST["submit"])) {
    //echo "Yes";
 $firstname = $_POST['ftext'];
 $surname = $_POST['ltext'];
 $message = $_POST['ymessage'];
 $currentuser = $_POST['currentuser'];
 $authoruser = $_POST['authoruser'];
 
 global $wpdb;
 //$wpdb->query($sql);
$sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin,author) values (%s,%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser,$authoruser);
 $wpdb->query($sql);   

 
 
}else{  
  //  echo "N0";
}

 
 
} else {
   ?>You can only contact the artist if you have a buyer account. If you don’t, please <a href="<?php echo site_url(); ?>/art-lover-register/">signup</a> for free here.<?php
}
?>
	
 <?php
 if (isset($_POST["submit"])) {    
 $fname= $_POST['fname'];   
 $lname= $_POST['lname'];   
 $email= $_POST['email'];   
 $subject= "Contact of Artist";   
 $message= $_POST['message'];   
 $to = "sliktvbr@gmail.com";
// mail($to,$subject,$email,$message);
 } else{  
    //echo "N0"; 
}

?>
	<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X</a></div>
    <div id="fade" class="black_overlay"></div>
		
		
		
		<!---a href="#">Commission the Artist</a-->
		 <p class="flex-caption"><a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display='block';document.getElementById('fade').style.display='block'">Commission the Artist</a></p>
	<div id="lighted" class="white_content pop_comission_main">

      <div class="popup-comission">


        <?php// echo do_shortcode('[contact-form-7 id="1103" title="commision-pop-up"]'); ?>
	  
	  <form action="/ukarts/downloads/pulse/#wpcf7-f1103-p1079-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
        cdfgdfgdsfdfgdffgdfdf
<div style="display: none;">
<input type="hidden" name="post_id" value="<?php echo get_the_id(); ?>">
<input type="hidden" name="_wpcf7_version" value="5.0">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1103-p1079-o1">
<input type="hidden" name="_wpcf7_container_post" value="1079">
</div>
<p><span class="wpcf7-form-control-wrap firstname"><input type="text" name="firstname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="First name*"></span></p>
<p><span class="wpcf7-form-control-wrap Surname"><input type="text" name="Surname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Surname*"></span></p>
<p><span class="wpcf7-form-control-wrap Budget"><input type="number" name="Budget" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" aria-invalid="false" placeholder="£ Budget*"></span></p>
<p><span class="wpcf7-form-control-wrap PreferredCompletionDate"><input type="date" name="PreferredCompletionDate" value="" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-required wpcf7-validates-as-date" aria-required="true" aria-invalid="false" placeholder="Preferred Completion Date*"></span></p>
<p><span class="wpcf7-form-control-wrap Email"><input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span></p>
<p><span class="wpcf7-form-control-wrap description"><textarea name="description" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Description*"></textarea></span></p>
<div class="left-s">
<p class="prefersize"><span class="wpcf7-form-control-wrap preferredsize"><input type="text" name="preferredsize" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Preferred size*"></span></p>
<p class="terms"><span class="wpcf7-form-control-wrap checkbox-156"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="checkbox-156[]" value="commission agree"><span class="wpcf7-list-item-label">commission agree</span></span></span></span> I agree to the UK Arts <a class="terms" href="#"> Terms &amp; Conditions for Commissions</a></p>
<p><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span>
</p></div>
<div class="right-s">
<span>Upload Inspiration</span> <span class="wpcf7-form-control-wrap UploadInspiration"><input type="file" name="UploadInspiration" size="40" class="wpcf7-form-control wpcf7-file" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv" aria-invalid="false"></span><p></p>
<p class="comting">You are not committing to progress with the commission by submitting this form.<br>
You can only contact the artist if you have a buyer account.if you don't have please <a href="<?php echo site_url(); ?>/uk-artists-collection/">signup</a> here.</p>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form>
	  
	  
	  
	  
	  
	  
	  
	  </div>
 
 
	<a href = "javascript:void(0)" onclick = "document.getElementById('lighted').style.display='none';document.getElementById('fade').style.display='none'">X</a></div>
    <div id="fade" class="black_overlay"></div>

    </div>
	</div>

    </div>
	
    <?php

	}
	
	
 
    public function featured_standard() {
	
		//echo get_the_ID();
		// echo get_the_post_thumbnail( get_the_ID(), $size );
        $images = $this->get_featured_images();
        $before = '<div class="download-gallery">';
      
		$after  = '</div>';

        $size = apply_filters( 'marketify_featured_standard_image_size', 'large' );

       echo $before;

        if ( empty( $images ) && has_post_thumbnail( get_the_ID() ) ) {
          

		  ?><div class="featured-thumbnail-section">
		  <div class="six-featured-section" style= "width: 10%;float: left;" >
		  <?php 
		   if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'second-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'second-featured-image', NULL , 'aaaa' ); endif;
	
	 if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'third-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'third-featured-image', NULL , 'aaaa' ); endif;
	
	 if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'fourth-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'fourth-featured-image', NULL , 'aaaa' ); endif;
	
	 if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'fifth-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'fifth-featured-image', NULL , 'aaaa' ); endif;
	
	 if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'sixth-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'sixth-featured-image', NULL , 'aaaa' ); endif;
	
	 if (class_exists('MultiPostThumbnails')
    && MultiPostThumbnails::has_post_thumbnail('download', 'second-featured-image')) :
    MultiPostThumbnails::the_post_thumbnail('download', 'second-featured-image', NULL , 'aaaa' ); endif;
	
		  ?> 
	</div>
	 <div class="six-featured-section"  style= "width: 80%;float: left;">
	<? echo get_the_post_thumbnail( get_the_ID(), $size );  ?>
	</div>

<div class="single-content-section"><h2>Description by the artist</h2><?php the_content(); ?></div>
    </div><?
            echo $after;
            return;
        } else {
   foreach ( $images as $image ) : ?>
            <div class="download-gallery__image"><a href="<?php echo esc_url( wp_get_attachment_url( $image->ID ) ); ?>"><?php echo wp_get_attachment_image( $image->ID, $size ); ?></a></div>
        <?php endforeach; ?>
    <?php
        }

        echo $after;
		
		
    }

	
	
    public function featured_standard_navigation() {
        $images = $this->get_featured_images();

        if ( empty( $images ) ) {
            return;
        }

        $before = '<div class="download-gallery-navigation ' . ( count ( $images ) > 6 ? 'has-dots' : '' ) . '">';
        $after  = '</div>';

        $size = apply_filters( 'marketify_featured_standard_image_size_navigation', 'thumbnail' );

        if ( count( $images ) == 1 || ( empty( $images ) && has_post_thumbnail( get_the_ID() ) ) ) {
            return;
        } 

        echo $before;

        foreach ( $images as $image ) {
    ?>
        <div class="download-gallery-navigation__image"><?php echo wp_get_attachment_image( $image->ID, $size ); ?></div>
    <?php
        }

        echo $after;
    }

	/**
	 * Output featured audio.
	 *
	 * Depending on the found audio it will be oEmbedded or create a
	 * WordPress playlist from the found tracks.
	 *
	 * @since 2.0.0
	 *
	 * @return mixed
	 */
    public function featured_audio() {
        $audio = $this->_get_audio();

		// if we are using a URL try to embed it (only on single download)
		if ( ! is_array( $audio ) && is_singular( 'download' ) && ! did_action( 'marketify_single_download_content_after' ) ) {
			$audio = wp_oembed_get( $audio );
		} elseif ( $audio ) {
			// grid preview only needs one
			if ( ! is_singular( 'download' ) ) {
				$audio = array_splice( $audio, 0, 1 );
			}

			if ( ! empty( $audio ) ) {
				$audio = wp_playlist_shortcode( array(
					'id' => get_post()->ID,
					'ids' => $audio,
					'images' => false,
					'tracklist' => is_singular( 'download' )
				) );
			}
		}

		$audio = apply_filters( 'marketify_get_featured_audio', $audio );

		if ( $audio ) {
			echo '<div class="download-audio">' . $audio . '</div>';
		}
    }

	/**
	 * Find audio for a download. Searches a few places:
	 *
	 * 1. `preview_files` FES File Uplaod Field (playlist)
	 * 2. `audio` FES URL Field (oEmbed)
	 * 3. Attached audio files (playlist)
	 *
	 * @since 2.0.0
	 *
	 * @return mixed $audio
	 */
    private function _get_audio() {
		$audio = false;

		// check to see if the FES file upload field exists
        $audio = get_post()->preview_files;

		// check to see if the FES URL field exists
		if ( ! $audio || '' == $audio ) {
			$field = apply_filters( 'marketify_audio_field', 'audio' );
			$audio = get_post()->$field;
		}

		// query attached media
        if ( ! $audio || '' == $audio ) {
            $audio = get_attached_media( 'audio', get_post()->ID );

            if ( ! empty( $audio ) ) {
                $audio = wp_list_pluck( $audio, 'ID' );
            }
        }

        return $audio;
    }

    public function featured_video() {
		$video = $this->_get_video();

		if ( '' == $video || empty( $video ) ) {
			return;
		}

        $info = wp_check_filetype( $video );
		$atts = apply_filters( 'marketify_featured_video_embed_atts', '' );

        if ( '' == $info[ 'ext' ] ) {
            global $wp_embed;

            $output = $wp_embed->run_shortcode( sprintf( '[embed %s]%s[/embed]', $atts, $video ) );
        } else {
            $output = do_shortcode( sprintf( '[video %s="%s" %s]', $info[ 'ext' ], $video, $atts ) );
        }

        echo '<div class="download-video">' . $output . '</div>';
    }

	/**
	 * Find an associated video for the download.
	 *
	 * @since 2.9.0
	 * @return mixed Video URL or false
	 */
	public function _get_video() {
		$video = false;

		$field = apply_filters( 'marketify_video_field', 'video' );
		$video = get_post()->$field;

		// query attached media
        if ( ! $video || '' == $video ) {
            $video = get_attached_media( 'video', get_post()->ID );

            if ( ! empty( $video ) ) {
				$video = current( $video );
				$video = wp_get_attachment_url( $video->ID );
            }
        }

        return $video;
	}

}
