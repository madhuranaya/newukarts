<?php
global $wp_post_types;

switch ( $event->post_status ) :

	case 'publish' :	
		printf('<p class="post-submitted-success-green-message">'.__( '%s listed successfully. To view your listing <a href="%s">click here</a>.', 'wp-event-manager' ).'</p>', $wp_post_types['event_listing']->labels->singular_name, get_permalink( $event->ID ) );
	break;
	
	case 'pending' :
		$admin_email = get_option('admin_email');
		$post = get_post($event->ID);
		$author = get_userdata($post->post_author);
		//$author_mail = $author->user_email;
		$author_mail = 'aish.anayacommunication@gmail.com';
		$to = $admin_email;
		$subject = 'Ukarts';
		 $message = "
	  Hi ".$author->display_name.",
	  Your listing, ".$post->post_title." has just been publish. Now admin will review your event and after that it approved your event. 
   ";
		$headers = array('Content-Type: text/html; charset=UTF-8','Cc: '.$author_mail.'');
		wp_mail( $to, $subject, $message, $headers );
		
		printf( '<p class="post-submitted-success-green-message">'.__( '%s submitted successfully. Your listing will be visible once approved.', 'wp-event-manager' ).'</p>', $wp_post_types['event_listing']->labels->singular_name, get_permalink( $event->ID ) );
	break;

	default :
		do_action( 'event_manager_event_submitted_content_' . str_replace( '-', '_', sanitize_title( $event->post_status ) ), $event );
	break;

endswitch;

do_action( 'event_manager_event_submitted_content_after', sanitize_title( $event->post_status ), $event );