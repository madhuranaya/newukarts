<?php
   /**
    * Template Name: Layout: Artist main
    *
    * @package Marketify
    */
    get_header();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php do_action( 'marketify_entry_before' ); ?>

<div class="container">
   <div id="content" class="site-content row">
      <div id="primary" class="content-area col-sm-12">
         <main id="main" class="site-main" role="main">
            <?php if ( have_posts() ) : 
          /* Start the Loop */   
        while ( have_posts() ) : the_post(); 
            get_template_part( 'content', 'page' );
          
               // If comments are open or we have at least one comment, load up the comment template
               if ( comments_open() || '0' != get_comments_number() )
                comments_template();
              
            endwhile; 
            do_action( 'marketify_loop_after' );
           else : 
             get_template_part( 'no-results', 'index' );
          endif; ?>
         </main>
         <!-- #main -->
      </div>



      <!-- #primary -->
   </div>
</div>


<style>
* {
    box-sizing: border-box;
}

.heading {
    font-size: 25px;
    margin-right: 25px;
}

.fa {
    font-size: 25px;
}

.checked {
    color: orange;
}

/* Three column layout */
.side {
  float: left;
  margin-top: 6px;
  width: 6%;
}

.middle {
  float: left;
  margin-top: 10px;
  width: 31%;
}

/* Place text to the right */
.right {
  float: left;
  margin-left: 8px;
  text-align: left;
  width: 15%;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* The bar container */
.bar-container {
    width: 100%;
    background-color: #f1f1f1;
    text-align: center;
    color: white;
}

/* Individual bars */
.bar-5 {
  background-color: #e4e000;
  height: 18px;
  width: 90%;
}  
.bar-4 {width: 30%; height: 18px; background-color: #e4e000;}
.bar-3 {width: 10%; height: 18px; background-color: #e4e000;}
.bar-2 {width: 4%; height: 18px; background-color: #e4e000;}
.bar-1 {width: 15%; height: 18px; background-color: #e4e000;}
.visit-or-website {
    display: none;
}
.main-cls-both {
    display: none;
}
.manage-product {
    background-color: #dedc00;
    float: right;
    text-align: center !important;
    }
</style>
<?php 

function to_get_thetitlle($id){
     global $wpdb;
   $resul=$wpdb->get_row("select * from ".$wpdb->prefix."commission where ref_id=".$id);
          return $resul->name;

}


 function to_pay_on_paypal($refrence_id,$amount,$budget){

  $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
  $paypalID = 'munishgup-facilitator@gmail.com'; //Business Email 
  ?>
<form action="<?php echo $paypalURL; ?>" method="post">
  
      <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
        
        <!-- Specify a Buy Now button. -->
        <input type="hidden" name="cmd" value="_xclick">
        
        <input type="hidden" name="item_name" value="<?php echo $refrence_id; ?> ">
        <input type="hidden" name="item_number" value="<?php echo $budget.'_'.$amount.'_final'; ?>">
        <input type="hidden" name="amount" value="<?php echo $amount; ?>">
        <input type="hidden" name="currency_code" value="USD">
        
        <!-- Specify URLs -->
        <input type='hidden' name='cancel_return' value='<?php  echo site_url(); ?>/ukarts/'>
        <input type='hidden' name='return' value='<?php  echo site_url(); ?>/ukarts/confirm-payment'>        
        <!-- Display the payment button. -->
      <!--  <input type="image" name="submit" border="0"
        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">-->
        <input type="submit" name="hire" value="Complete">
      </form>  
<?php  } ?>
  <?php
   $user_id=""; 
   $user_name="";
   $current_user = wp_get_current_user();
  // echo $current_user ->ID;
  if (!empty($_GET['user_id'])){
  $user_id=$_GET['user_id'];
  $user2 = get_user_by('ID', $_GET['user_id'] );
  $user_name=$user2->display_name;
}else{  
$user_id=$current_user->ID;
$user_name=$current_user->user_firstname;
$user_last_name=$current_user->user_lastname;
}
//echo $user_id;
//echo $user_name;
   if ( is_user_logged_in() ){

      // $current_user = wp_get_current_user();
   if ( ($current_user instanceof WP_User) ) {
     ?>
     <div class="artist-main-img-log">
  <?php
if ($_GET['user_id']) {
 // $jjde= get_current_user_id();
  $bbb=$_GET['user_id']; 
$args = array('post_type' => 'about_us','author' =>  $bbb,'posts_per_page' => 1);
query_posts( $args );

while (have_posts()) : the_post();
if ( has_post_thumbnail() ) {
 //echo $id = get_the_ID();

    the_post_thumbnail();
}
 endwhile;
/*
else {

    echo "string";
} 
*/
  } else {

 $bbb=$_GET['user_id']; 
$args = array('post_type' => 'about_us','author' =>  get_current_user_id(),'posts_per_page' => 1);
query_posts( $args );

// the Loop
while (have_posts()) : the_post();
  //Do your stuff  

  //You can access your feature image like this:
  get_current_user_id();
  $bbb=$_GET['user_id']; 
 
 echo the_post_thumbnail();
  //echo $id = get_the_ID();
endwhile;
}

?>

</div>
<div class="current-username">
   <h2> <?php// echo $user_name;  ?> <?php //echo $user_last_name; ?>
    <?php
 $author_queryb = array('post_type' => 'about_us',
                        'posts_per_page' => '1','author' => $user_id);
                           $author_postsb = new WP_Query($author_queryb);
                    while($author_postsb->have_posts()) : $author_postsb->the_post(); ?>

     <? echo the_title(); ?>
  
     <?php echo  get_post_meta( get_the_ID(), 'surname', true ); ?>

   </h2>
   <p><?php
 
       // echo get_the_ID();
          echo  get_post_meta( get_the_ID(), 'artist_statement', true );

                   endwhile;

     ?></p>
</div>



<?php
   //  echo get_avatar( $current_user->user_email, 32 );
   }
 } else {
  ?>
     <div class="artist-main-img-log">
  <?php
if ($_GET['user_id']) {

  $bbb=$_GET['user_id']; 
$args = array('post_type' => 'about_us','author' =>  $bbb,'posts_per_page' => 1);
query_posts( $args );

  



while (have_posts()) : the_post();
if ( has_post_thumbnail() ) {
 //echo $id = get_the_ID();
    the_post_thumbnail();
}
 endwhile;
/*
else {

    echo "string";
} 
*/
  } else {

$bbb=$_GET['user_id']; 
$args = array('post_type' => 'about_us','author' =>  $bbb,'posts_per_page' => 1);
query_posts( $args );

// the Loop
while (have_posts()) : the_post();
  //Do your stuff  

  //You can access your feature image like this:
 echo the_post_thumbnail();
  //echo $id = get_the_ID();
endwhile; 
}

?>

</div>
<div class="current-username">
   <h2> 
    <?php
 $author_queryb = array('post_type' => 'about_us',
                        'posts_per_page' => '1','author' => $user_id);
                           $author_postsb = new WP_Query($author_queryb);
                    while($author_postsb->have_posts()) : $author_postsb->the_post();  echo the_title(); 
   echo  get_post_meta( get_the_ID(), 'surname', true ); ?>

   </h2>
   <p><?php
 
       // echo get_the_ID();
          echo  get_post_meta( get_the_ID(), 'artist_statement', true );

                   endwhile;

     ?></p>
</div>



<?php

   //  echo get_avatar( $current_user->user_email, 32 );
   }
 
 
 ?>     
<div class="button-login-user">
   <div class="container">
      <div class="tab">
       <?php   if ( is_user_logged_in() ){
      // $current_user = wp_get_current_user();
   if ( ($current_user instanceof WP_User) ) {
        $current_user_name= $user_name;
      $user_id_get_id = get_current_user_id();
     ?>
 <button class="tablinks" onclick="openCity(event, 'artist')">Artworks</button>
         <button class="tablinks like-button"><span><img src="<?php  echo site_url(); ?>/wp-content/uploads/2018/03/dil.png" /></span>
    
   <?php
global $wp_query;
$curauth = $wp_query->get_queried_object();
$post_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = '" . $user_id_get_id . "' AND post_type = 'edd_wish_list' AND post_status = 'publish'");
?>
(<?php echo $post_count; ?>)
       

<?php
   //  echo get_avatar( $current_user->user_email, 32 );
   }
 }else if ( !is_user_logged_in() ){

  $user = get_user_by('ID', $_GET['user_id'] ); 
  $user_id_get = $_GET['user_id']; ?>
 <button class="tablinks" onclick="openCity(event, 'artist')">Artworks</button>
     
         <button class="tablinks like-button">
          <span><img src="<?php  echo site_url(); ?>/wp-content/uploads/2018/03/dil.png" /></span> 
   <?php
global $wp_query;
$curauth = $wp_query->get_queried_object();
$post_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = '" . $user_id_get . "' AND post_type = 'edd_wish_list' AND post_status = 'publish'");
?>
(<?php echo $post_count; ?>)
       

 <?php }?>
 
         <button class="tablinks about-us-button" onclick="openCity(event, 'aboutme')">About Me</button>
         <button class="tablinks" onclick="openCity(event, 'myblog')">My blog</button>

        
        <?php if ( is_user_logged_in() ){
     //  $current_user = wp_get_current_user();
   if ( ($current_user instanceof WP_User) ) { ?>
     <button class="tablinks" onclick="openCity(event, 'review')">Reviews
       <?php  global $wpdb;
            //print_r($current_user);
             //$current_user = $user_name;
              $current_user_name= $user_name;
            $avgsell = $wpdb->get_results("SELECT COUNT(artistid) as artsellered FROM artistreviews where artistlist= '$user_id'");
            //print_r($avgsell);  
            $artsellered= $avgsell[0]->artsellered; 
            if(!$artsellered=="0")
            { ?>(<?php  echo $artsellered;  ?>)<?php } else { }
            ?>
             </button>
<?php
   
   }
 }else if ( !is_user_logged_in() ){
  $user = get_user_by('ID', $_GET['user_id'] ); ?>
<button class="tablinks" onclick="openCity(event, 'review')">Reviews
       <?php  global $wpdb;
            //print_r($current_user);
             //$current_user = $user_name;  
              $current_user_name= $user_name;
            $avgsell = $wpdb->get_results("SELECT COUNT(artistid) as artsellered FROM artistreviews where artistlist= '$user_id'");
            //print_r($avgsell);  
            $artsellered= $avgsell[0]->artsellered; 
            if(!$artsellered=="0")
            { ?>(<?php  echo $artsellered;  ?>)<?php } else { }
            ?> </button>
<?php  }  ?>
   
         <button class="tablinks" onclick="openCity(event, 'contact')">Contact</button>
         <button class="tablinks" onclick="openCity(event, 'commission')">Commission</button>

         <button class="tablinks" onclick="openCity(event, 'bank')">Bank         </button>
         <button class="tablinks" onclick="openCity(event, 'events')">Events         </button>

 <div class="download-current-user">
  <?php if ( is_user_logged_in() ) { ?>
         <div id="bank" class="tabcontent">
  <div class="container">
<?php 
$get_curent_user= get_current_user_id();
 global $wpdb;
$bamkdetail_sel = $wpdb->get_results("SELECT * FROM bankdetail where user_id=$get_curent_user"); 
//print_r($bamkdetail_sel);
  foreach ( $bamkdetail_sel as $bamkdetail_seled ) 
{ 
      $acc_secl= $bamkdetail_seled->accountno;
      $acc_sort= $bamkdetail_seled->sortcode;
}
?> 
<h2>Bank Details</h2>
  <form name="bankdetail" method="post" action="<?php  echo site_url(); ?>/bank-detail/">
        <input type="hidden" name="accountno" value="<?php echo $acc_secl; ?>" placeholder="Please Enter Account Number" />
    <input type="number" name="accountnoed" value="<?php echo $acc_secl; ?>" min="1000000000000000" max="9999999999999999" placeholder="Please Enter Account Number" />
        <input type="number" name="sortcode"  value="<?php echo $acc_sort; ?>"  min="100" max="999" placeholder="Please Enter Sort Code" />  
        <input type="hidden" name="user_id" value="<?php echo $user_id = get_current_user_id(); ?>" />  
        <input type="hidden" name="user_name" value="Admin" />  
        <input type="submit" name="submit" value="submit"  />
   </form>
 </div>
       </div>
 <?php  } ?>
</div>

<div class="download-current-user">
  <?php if ( !is_user_logged_in() ) { ?>
         <div id="bank" class="tabcontent">
  <div class="container">

   <?php    echo "Login First"; ?>
</div></div>
  <?php  } ?> </div>




         <?php  if ( is_user_logged_in() ){
       //$current_user = wp_get_current_user();
   if ( ($current_user instanceof WP_User) ) {
     
   //  echo get_avatar( $current_user->user_email, 32 );
   }  
  } else if ( !is_user_logged_in() ) 
 {
  $user = get_user_by('ID', $_GET['user_id'] ); ?>

 <?php /* <div class="follow-us-social" id="blog_social_2" style="display:none;">
            <div class="footer-social">
               Follow me on   
               <?php 
                  $author_querya = array('post_type' => 'social_accounts',
                  'posts_per_page' => '1','author' => $user_id);
                  $author_postsa = new WP_Query($author_querya);
                  if ( $author_postsa->have_posts()) :
                 //echo count($author_postsa->have_posts());
                  while($author_postsa->have_posts()) :
                   $author_postsa->the_post();
                      $get_the_id=get_the_id();  

        $twitter_acc_url = get_post_meta($get_the_id,'twitter_acc_url',TRUE); 
 
                 if ( $twitter_acc_url ) { ?>
               <a href="https://twitter.com/" target="_blank"><span class="screen-reader-text">Twitter</span></a>
               <?  } else { ?><a href="https://twitter.com/" target="_blank"><span class="screen-reader-text">Twitter</span></a><?  }   
                  $instagram_acc_url = get_post_meta($get_the_id,'instagram_url',TRUE); 
                  if ( $instagram_acc_url ) { ?>
               <a href="https://www.instagram.com/" target="_blank"><span class="screen-reader-text">Instagram</span></a>
               <?  } else { ?><a href="https://www.instagram.com/" target="_blank"><span class="screen-reader-text">Instagram</span></a><?  }  
                  $fb_acc_url = get_post_meta($get_the_id,'fb_acc_url',TRUE); 
                  if ( $fb_acc_url ) { ?>
               <a href="https://www.facebook.com/" target="_blank"><span class="screen-reader-text">Facebook</span></a>
               <?  } else { ?><a href="https://www.facebook.com/" target="_blank"><span class="screen-reader-text">Facebook</span></a><?  }  
                  $pint_acc_url = get_post_meta($get_the_id,'pintrest_url',TRUE); 
                  if ( $pint_acc_url ) { ?>
               <a href="https://in.pinterest.com/" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
               <?  } else { ?><a href="https://in.pinterest.com/" target="_blank"><span class="screen-reader-text">Pinterest</span></a><?  } 
                  endwhile;
                else :
                  ?>
                   <a href="#" ><span class="screen-reader-text">Twitter</span></a>

                    <a href="#" ><span class="screen-reader-text">Instagram</span></a>
 
                    <a href="#" ><span class="screen-reader-text">Facebook</span></a>

                     <a href="#" ><span class="screen-reader-text">Pinterest</span></a><?php 
                   
                 endif;

                   ?>
            </div>
            </div> 
 <div class="follow-us-social" id="blog_social_3" style="display:none;">
               <div class="visit-or-website">
         <?  $author_querya = array('post_type' => 'social_accounts',
            'posts_per_page' => '1','author' => $user_id);
              $author_postsa = new WP_Query($author_querya);
               if ( $author_postsa->have_posts()) :
            //  print_r($author_postsa);vb
            while($author_postsa->have_posts()) : $author_postsa->the_post();
                $get_the_id=get_the_id();  ?>
            <?php  $web_acc_url = get_post_meta($get_the_id,'website_url',TRUE); ?>
            <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
            <?php endwhile; 
             else :
                  ?>
                   <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>Visit my website</a>
                   <?php   endif;  ?> 
         </div>
       </div> */ ?>

<?php  }
?>
  <?php        // if ( is_user_logged_in() ) {
            ?>
            <!--commision start-->
<div id="commission" class="tabcontent">
                <?php  //print_r($current_user); ?>
          <div class="third-section-authorname">Commission <?php  echo  $user_name;  ?>
        <a href="#">How Does it work? </a>
        </div>
        <?php 
        function to_get_user_name($user_id1){
          $user=get_user_by( 'ID', $user_id1 );
          return $user->display_name;;
        }
        function to_show_payment_status($ref_id,$stt){
        global $wpdb;
   $resul=$wpdb->get_row("select * from ".$wpdb->prefix."commission_payment where refrence_id=".$ref_id);
          return $resul->$stt;
        } 
        $user = wp_get_current_user();
       // print_r($user);
         $field_name="receiver_id";
     
        if(!in_array( 'shop_vendor', (array) $user->roles )){
           include("contracter_profile.php");
        }else{
         // echo "sss";
            include("shop_vender_profile.php");
        }
        ?>
      <script>
   $("input[name='optionsRadiosinline']").click(function() {
        var rad_val=$(this).val();
        if(rad_val=="pending"){
           $(".pending_pro").show();
           $(".on_going").hide();
           $(".complete").hide();
           $(".cancelled").hide();


        }else if(rad_val=="ongoing"){
           $(".pending_pro").hide();
           $(".on_going").show();
           $(".complete").hide();
           $(".cancelled").hide();

        }else if(rad_val=="completed"){
              $(".pending_pro").hide();
           $(".on_going").hide();
           $(".complete").show();
           $(".cancelled").hide();

        }else if(rad_val=="cancelled"){
               $(".pending_pro").hide();
           $(".on_going").hide();
           $(".complete").hide();
           $(".cancelled").show();
        }
 });

   $(".accept").click(function(){
     var ref_id=$(this).attr("data-id");
      
      $.ajax({
        type: "POST",
        url: "<?php  echo site_url(); ?>/ajax.php",
        data: "ref_id="+ref_id+"&status=accept&status_code=1&action=accept" , 
        cache: false,
        beforeSend: function () { 
            $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
        },
        success: function(html) {    
        location.reload();
        },
    error: function(error){
   // alert(error);
    }
    });

   });

$(".complete_p").click(function(){
     var ref_id=$(this).attr("data-id");
     var status_val=$(this).attr("data-status_val");
     var status_code=$(this).attr("data-status_code");

     // alert(ref_id);
     $.ajax({
        type: "POST",
        url: "<?php  echo site_url(); ?>/ajax.php",
        data: "ref_id="+ref_id+"&status="+status_val+"&status_code="+status_code+"&action=complete", 
        cache: false,
        beforeSend: function () { 
            $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
        },
        success: function(html) {  
        //alert("yes");  
        location.reload();
        },
    error: function(error){
    //alert("non");
    }
    });

})


  $(".cancelled_p").click(function(){
var aa=$(this).attr("data-id");
var result = confirm("Want to delete?");
if(result){
$.ajax({
        type: "POST",
        url: "<?php  echo site_url(); ?>/ajax.php",
        data: "ref_id="+aa+"&status=cancel&status_code=3&action=delete" , 
        cache: false,
        beforeSend: function () { 
            $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
        },
        success: function(html) {
        location.reload();    
         //alert(html);
        },
    error: function(error){
    //alert(error);
    }
    });
}
  });
      </script>
      </div>  
            <?php //} ?>


            
<?php if ( is_user_logged_in() ) { ?> 
  
<!--social icon start-->
 <div class="follow-us-social" id="blog_social_4" style="display:none;">
            <div class="footer-social">
               Follow me on   
               <?php 
                  $author_querya = array('post_type' => 'social_accounts',
                  'posts_per_page' => '1','author' => $user_id);
                  $author_postsa = new WP_Query($author_querya);
                  if ( $author_postsa->have_posts()) :
                 //echo count($author_postsa->have_posts());
                  while($author_postsa->have_posts()) :
                   $author_postsa->the_post();
                      $get_the_id=get_the_id();  

        $twitter_acc_url = get_post_meta($get_the_id,'twitter_acc_url',TRUE); 
 
                 if ( $twitter_acc_url ) { ?>
               <a href="https://twitter.com/" target="_blank"><span class="screen-reader-text">Twitter</span></a>
               <?  } else { ?><a href="https://twitter.com/" target="_blank"><span class="screen-reader-text">Twitter</span></a><?  }   
                  $instagram_acc_url = get_post_meta($get_the_id,'instagram_url',TRUE); 
                  if ( $instagram_acc_url ) { ?>
               <a href="https://www.instagram.com/" target="_blank"><span class="screen-reader-text">Instagram</span></a>
               <?  } else { ?><a href="https://www.instagram.com/" target="_blank"><span class="screen-reader-text">Instagram</span></a><?  }  
                  $fb_acc_url = get_post_meta($get_the_id,'fb_acc_url',TRUE); 
                  if ( $fb_acc_url ) { ?>
               <a href="https://www.facebook.com/" target="_blank"><span class="screen-reader-text">Facebook</span></a>
               <?  } else { ?><a href="https://www.facebook.com/" target="_blank"><span class="screen-reader-text">Facebook</span></a><?  }  
                  $pint_acc_url = get_post_meta($get_the_id,'pintrest_url',TRUE); 
                  if ( $pint_acc_url ) { ?>
               <a href="https://in.pinterest.com/" target="_blank"><span class="screen-reader-text">Pinterest</span></a>
               <?  } else { ?><a href="https://in.pinterest.com/" target="_blank"><span class="screen-reader-text">Pinterest</span></a><?  } 
                  endwhile;
                else :
                  ?>
                   <a href="#" ><span class="screen-reader-text">Twitter</span></a>

                    <a href="#" ><span class="screen-reader-text">Instagram</span></a>

                    <a href="#" ><span class="screen-reader-text">Facebook</span></a>

                     <a href="#" ><span class="screen-reader-text">Pinterest</span></a><?php 
                   
                 endif;

                   ?>
            </div>
            </div> 

            <!--div class="follow-us-social" id="blog_social_5" style="display:none; #blog_social_3 {
  margin-left: 10px;
}">
            

               <div class="visit-or-website">
         <?  $author_querya = array('post_type' => 'social_accounts',
            'posts_per_page' => '1','author' => $user_id);
              $author_postsa = new WP_Query($author_querya);
               if ( $author_postsa->have_posts()) :
            //  print_r($author_postsa);vb
            while($author_postsa->have_posts()) : $author_postsa->the_post();
                $get_the_id=get_the_id();  ?>
            <?php  $web_acc_url = get_post_meta($get_the_id,'website_url',TRUE); ?>
            <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
            <?php endwhile; 
             else :
                  ?>
                   <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>Visit my website</a>
                   <?php   endif;  ?> 
         </div>
       </div-->
<!--social icon end-->


         <?php }

         else if ( !is_user_logged_in() ){
  $user = get_user_by('ID', $_GET['user_id'] ); ?>
 <!-- <div class="visit-or-website"><?
            $author_querya = array('post_type' => 'social_accounts',
            'posts_per_page' => '1','author' => $user_id);
              $author_postsa = new WP_Query($author_querya);
               if ( $author_postsa->have_posts()) :
            //  print_r($author_postsa);
            while($author_postsa->have_posts()) : $author_postsa->the_post();
                $get_the_id=get_the_id();     ?>
            <?php  $web_acc_url = get_post_meta($get_the_id,'website_url',TRUE); ?>
            <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>visit my website</a>
            <?php endwhile; 
          else :
                  ?>
                   <a href="<?php echo site_url(); ?>"><span><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/attach.png" /></span>Visit my website</a>
                   <?php   endif;  ?>  
         </div> -->


<?php  }  ?>
      </div>

      <div id="events" class="tabcontent">
     <div class="container">
          <?php  // if ( is_user_logged_in() ){ ?>
                  <div class="third-section">    
      <div class="third-section-authorname"><!--Blog--></div>
       <?php $author_queryc = array('posts_per_page' => '-1','author' => $user_id,'post_status' => 'publish','post_type' => 'event_listing');
                        $author_postsc = new WP_Query($author_queryc);
                if ( have_posts() )
        {   
                      while($author_postsc->have_posts()) : $author_postsc->the_post();
                     ?>
                     <div class="section-third-inner 222">
           <?php echo $get_the_id= get_the_id(); ?>
           
                        <div class="section-third-thumb"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail();  ?></a></div>
                       <a href="<?php echo site_url(); ?>/event-dashboard/?action=edit&event_id=<?php echo $get_the_id; ?>">Edit</a>
                        <div class="section-third-date"> <?  echo get_the_date(); ?></div>
            
            
          
                        <div class="section-third-title 123"><? echo the_title(); ?></div>
                        <div class="section-third-content"><? echo the_content(); ?></div>
                     </div>
           
                     <?php    endwhile;      
    } else {
                echo  "No Post found";
              } ?>     
               </div>
          <?php// }         ?>
    </div>
</div>
  

<div class="macro">

   <div id="content" class="site-content row">
      <div id="primary" class="content-area col-sm-12">
         <main id="main" class="site-main" role="main">
            <style>
               .tab {  overflow: hidden;border: 1px solid #ccc; background-color: #f1f1f1; }
               .tab button {   background-color: inherit; float: left; border: none; outline: none; cursor: pointer; padding: 14px 16px; transition: 0.3s;
               font-size: 17px; }
               .tab button:hover {    background-color: #ddd; }
               .tab button.active {    background-color: #ccc; }
               .tabcontent {   display: none; padding: 6px 12px; border: 1px solid #ccc; border-top: none;}
            </style>
            <script>
               function openCity(evt, cityName) {
                   var i, tabcontent, tablinks;
                   if(cityName=="aboutme"){
                    $("#blog_social_2").show();
                    $("#title_order").hide();
                   }else{
                    $("#blog_social_2").hide();
                    $("#title_order").show();
                   }

                   if(cityName=="aboutme"){
                    $("#blog_social_3").show();
                    $("#title_order").hide();
                   }else{
                    $("#blog_social_3").hide();
                    $("#title_order").show();
                   }

                   if(cityName=="aboutme"){
                    $("#blog_social_4").show();
                    $("#title_order").hide();
                   }else{
                    $("#blog_social_4").hide();
                    $("#title_order").show();
                   }

                   if(cityName=="aboutme"){
                    $("#blog_social_5").show();
                    $("#title_order").hide();
                   }else{
                    $("#blog_social_5").hide();
                    $("#title_order").show();
                   }

                   tabcontent = document.getElementsByClassName("tabcontent");
                   for (i = 0; i < tabcontent.length; i++) {
                       tabcontent[i].style.display = "none";
                   }
                   tablinks = document.getElementsByClassName("tablinks");
                   for (i = 0; i < tablinks.length; i++) {
                       tablinks[i].className = tablinks[i].className.replace(" active", "");
                   }
                   document.getElementById(cityName).style.display = "block";
                   evt.currentTarget.className += " active";
               }
            </script>
             
            <div class="download-current-user">
               <?php
                  if ( is_user_logged_in() ){
                     //global $current_user;
                     //wp_get_current_user();
                  ?>  
            <div id="artist" class="tabcontent">
			<a class="add-product-button" href="<?php echo site_url(); ?>/add-product/">Add New Artwork</a>
        <?    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
     $args = array(     
      'posts_per_page'   =>8,
      'post_type'        => 'download',
      'author' => $user_id, 
    'meta_key' => 'edd_price',
      'orderby'   => 'ID',
      'order' => 'DESC',
      'post_status'      => 'publish',   
      'paged'            => $paged
    );
    $wp_query = new WP_Query( $args );
    if( $wp_query->have_posts()):while($wp_query->have_posts()):$wp_query->the_post();
     ?>
      <?php //echo the_title(); ?>
        <div class="download-current-user-inner"> 

<? $get_the_id= get_the_id(); 
			 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) {   }  else {  ?> <span  class="onsale-product">Sale</span><?php  }  ?>
           <div class="main-tbl-blf-dd">
    <div class="hover-img"><a href="<?php echo the_permalink(); ?>">VIEW DETAILS</a></div> 
      
      <?php 
        if ( has_post_thumbnail() ) { 
          echo '<p class="thumbnail_custome">';
          the_post_thumbnail("small");
          echo '</p>';
        } 
      ?>
    </div>  
                     <div class="download-current-user-name">   
                      <a href="<?php the_permalink(); ?>" title="<?php //the_title_attribute(); ?>"><?php //the_title(); ?></a> 
					  </div>
       <div class="basic-info-product">
	  
            <h2 class="title-edit-btn"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

   <div class="manage-product">
             <?php  $get_the_id= get_the_id();  ?>
               <a href="http://gloanaya.com/ukarts/vendor-dashboard/?task=edit-product&post_id=<?php  echo $get_the_id; ?>">Edit</a>

          </div>
            </h2> 
					
                        <p>By <?php  the_author(); ?></p>
                        <p class="price_symbol">
						   <?php  $get_the_id= get_the_id(); 
 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) {   echo $price = get_post_meta($get_the_id, 'edd_price', true); }  else {  ?>  <strong class="item-price"><span><del>£ <? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price= get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php  }  ?>
						                     
               <span class="add-new-btnd">
    <?php  

   
$current_date = strtotime(date("d-m-Y"));
//$datet = strtotime($datet);

  $past_date1= date("d-m-Y", strtotime('-10 days'));
 
  $past_date=  strtotime($past_date1);
    $post_date1= get_the_date();
 
//echo $post_date=strtotime($post_date1);

 
  $post_date2 = str_replace('/', '-', $post_date1);
  $todays_date = date("d-m-Y");
  $post_date = strtotime($post_date2);
  
  //echo $expiration_date.' | '.$today.'<br>';

if (($post_date > $past_date) && ($post_date <= $current_date))
    {
      echo "New";
    }
    

           ?>      </span> 
                        </p>
                     </div> 

                     <div class="comlex-info-product">
                        <?php //dynamic_sidebar('reviews'); ?>
            
                   <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">
                           <a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                           <div class="edd_purchase_submit_wrapper">
                              <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="£ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>
                           </div>
                           <!--end .edd_purchase_submit_wrapper-->
                        </form>
            
            
            
                    
             <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content<?php echo $get_the_id; ?> {
        display: none;
        position: fixed;
        top: 25%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        /*height: 50%;*/
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: white;
        z-index:9999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content<?php echo $get_the_id; ?> > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;
background: #dfdf1f;  
color: #fff !important;
border-radius: 100px;}   

      .white_content<?php echo $get_the_id; ?> input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

      .white_content<?php echo $get_the_id; ?> input[type="submit"]
      {position: absolute; right:0; top: 0; height: 50px;}

      .white_content<?php echo $get_the_id; ?> form
      {position: relative;}
</style>
 <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='block';document.getElementById('fade').style.display='block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
  <div id="light<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
  <a class="close_icon" href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='none';document.getElementById('fade').style.display='none'">X</a>
  <?php
    $current_user = wp_get_current_user();
       $current_user_id= $current_user->ID;
?>

    <form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
            <input type="text" name="review_title" placeholder="Review Title*" required/>
            <select name="review_starts">
                <option name="0">Select Review star:</option>
                <option name="1">1</option>
                <option name="2">2</option>
                <option name="3">3</option>
                <option name="4">4</option>
                <option name="5">5</option>
              </select>
              <input type="text" name="review_description" placeholder="Review Description*" required/>
         <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
         <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
             <input type="submit" name="submit" value="submit" />
       </form>
</div>  
                        <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>
                     </div>
                  </div>
                  <?php 
    endwhile;

    
    else:?>

     <h1><? echo "No Results Found";?> </h1>
    
    <? endif;
  ?>   
  <div class="post-nav-container">
    <?php 
 
 //previous_posts_link( __('&rarr; Older Posts',$wp_query->max_num_pages));
 $big = 999999999; //
echo paginate_links( array(
  'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
  'format' => '?paged=%#%',
  'current' => max( 1, get_query_var('paged') ),
  'total' => $wp_query->max_num_pages
) );

   ///  previous_posts_link( __('&rarr; Older Posts')); ?>
    <?php// next_posts_link( __('Newer Posts &larr; ')); ?>
  </div>
  <?php wp_reset_query(); ?>
               </div> </div>
               <!--about pop up start-->
      <div id="aboutme" class="tabcontent">
                  <div class="section-second">

<p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Edit</a></p>

                     <!--div class="second-section-authorname"><h2>About  <?php// echo $user_name;  ?></h2>
                      <?php  $current_user->ID;  ?></div-->
                     <? $author_queryb = array('post_type' => 'about_us',
                        'posts_per_page' => '1','author' => $user_id);
                           $author_postsb = new WP_Query($author_queryb);
                    while($author_postsb->have_posts()) : $author_postsb->the_post();
                        ?>
                        <div class="second-section-authorname"><h2>About  <?php echo the_title();  ?></div>
                       
  
 <strong>
                <div class="section-second-title">  <?php echo  get_post_meta( get_the_ID(), 'about_artist', true ); ?></div></strong>
                
 
                     <strong></strong>
                     <div class="section-second-content"><? echo the_content(); ?></div>
                     <?         endwhile; ?>
          
            
           
                  </div>
				  <div id="light" class="white_content">
<?php //echo  get_the_ID(); ?>
   <div id="postbox">
    <form id="new_post" name="new_post" method="post" action="<?php echo site_url(); ?>/thanks-about/"  enctype="multipart/form-data">
Name
  
  <input type="text" id="title" value="<? echo the_title(); ?>" tabindex="1" size="20" name="title" />
  <input type="hidden" name="post_idd" value="<?php  echo get_the_ID();?>">

About Artist
<textarea id="description" tabindex="3" name="description44" cols="50" rows="6"><?php echo  get_post_meta( get_the_ID(), 'about_artist', true ); ?></textarea>
 
  
Artist Statement
<textarea id="description" tabindex="3" name="description" cols="50" rows="6"><? echo  get_post_meta( get_the_ID(), 'artist_statement', true ); ?></textarea>




<input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />
  <input type="hidden" name="post_id" id="post_id" value="55" />
  <?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>


<p align="right"><input type="submit" value="submit" tabindex="6" id="submit" name="submited" /></p>


</form>
</div>
    <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X</a>
  </div>

               </div>


<!--about form start here-->

    <div id="fade" class="black_overlay"></div>

    <!--about form start end-->
<?php 
   if ( is_user_logged_in() ){
     //  $current_user = wp_get_current_user();
   if ( ($current_user instanceof WP_User) ) {
     ?>
 <div id="myblog" class="tabcontent">
                  <div class="third-section">    
      <div class="third-section-authorname"><!--Blog--></div>
       <?php $author_queryc = array('posts_per_page' => '-1','author' => $user_id,'post_status' => 'publish');
                        $author_postsc = new WP_Query($author_queryc);
					      if ( have_posts() )
        {   
                      while($author_postsc->have_posts()) : $author_postsc->the_post();
                     ?>
                     <div class="section-third-inner 222">
					 <?php $get_the_id= get_the_id(); ?>
					 
                        <div class="section-third-thumb"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail();  ?></a></div>
                       
                        <div class="section-third-date"> <? echo get_the_date(); ?></div>
						
							 <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content {
        display: none;
        position: fixed;
        top: 25%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        height: 560px;
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: white;
        z-index:9999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;background: #dfdf1f;
color: #fff !important;
border-radius: 100px;}   

      .white_content input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

      .white_content input[type="submit"]
      {position: absolute; right:0; top: 0; height: 50px;}

      .white_content form
      {position: relative;}
</style>	 
					  <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content<?php echo $get_the_id; ?> {
        display: none;
        position: fixed;
        top: 10%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        height: 560px;
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: #fff;
        z-index:9999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content<?php echo $get_the_id; ?> > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
background: #70C0EF;
border-radius: 100px;}   

      .white_content<?php echo $get_the_id; ?> input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

      .white_content<?php echo $get_the_id; ?> input[type="submit"]
      {position: absolute; right:0; top: 0; height: 50px;}

      .white_content<?php echo $get_the_id; ?> form   
      {position: relative;}
</style>
 <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='block';document.getElementById('fade<?php echo $get_the_id; ?>').style.display='block'">Edit</a></p>
	<div id="light<?php echo $get_the_id; ?>" class="white_content hyu<?php echo $get_the_id; ?>"><?php echo $get_the_id; ?>
	<?php $post_to_edit = get_post($get_the_id); 
//print_r($post_to_edit); ?>
<!-- edit Post Form -->
<div id="postbox">

<form id="new_post" name="new_post" method="post" action="<?php echo site_url(); ?>/thanks/"  enctype="multipart/form-data">
<p><label for="title">Title</label><br />
<input type="text" id="title" value="<?php echo $post_to_edit->post_title; ?>" tabindex="1" size="20" name="title" />
</p>  <input type="file" name="image">
<p><label for="description">Description</label><br />
<textarea id="description" tabindex="3" name="description" cols="50" rows="6"><?php echo $post_to_edit->post_content; ?></textarea>
</p>

<p align="right"><input type="submit" value="submit" tabindex="6" id="submit" name="submited" /></p>
<input type="hidden" name="action" value="f_edit_post" />
<input type="hidden" name="pid" value="<?php echo $post_to_edit->ID; ?>" />
<?php wp_nonce_field( 'new-post' ); ?>
</form>
</div>

   
<!--// edit Post Form -->

	
	<a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='none';  document.getElementById('fade<?php echo $get_the_id; ?>').style.display='none'" >X</a></div>
    <div id="fade<?php echo $get_the_id; ?>" class="black_overlay"></div>
					 
					 
			
						
                        <div class="section-third-title 123"><? echo the_title(); ?></div>
                        <div class="section-third-content"><? echo the_content(); ?></div>
                     </div>
					 
                     <?php    endwhile; 
		} else {
                echo  "No Post found";
              } ?> 
		


                  </div>
               </div>
<?php
   //  echo get_avatar( $current_user->user_email, 32 );
   }
   
 }
?>
       <div id="contact" class="tabcontent">
                  <div class="third-section">
                     <div class="third-section-authorname">Contact <?php $user_name;  ?></div>
                     <form name="contactusartist" method="post">
                        <input  class="input_uk" type="text" name="ftext" placeholder="First name*" required />
                         <input class="input_us" type="text" name="ltext" placeholder="Surname*" required />
                        <textarea rows="4" cols="50" name="ymessage" placeholder="Your message*" required>
</textarea>
                        <input type="hidden" name="currentuser" value=" <?php  echo  $current_user->display_name;  ?>" />
                      <input class="input_submit" type="submit" name="submit" value="Send" />
                        <p>You can only contact the artist,If you have a buyer account,If you don't,please <a href="<?php  echo site_url(); ?>/ukarts/uk-artists-collection/">signup</a> for free </p>
                     </form>
                     <?php  
                        if (isset($_POST["submit"])) {
                           //echo "Yes";
                        $firstname = $_POST['ftext'];
                        $surname = $_POST['ltext'];
                        $message = $_POST['ymessage'];
                        $currentuser = $_POST['currentuser'];
                        
                        global $wpdb;
                        //$wpdb->query($sql);
                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser);
                        $wpdb->query($sql);   
                        
                        
                        
                        }else{  
                         //  echo "N0";
                        }
                        
                        
                        global $wpdb;
                          //print_r($current_user);
                           $current_user_name= $current_user->user_login;
                        $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
                        //print_r($curentuser_contact);
                        foreach ( $curentuser_contact as $curentuser_contacted )
                        {
                         $current_first= $curentuser_contacted->firstname; 
                         $current_surename= $curentuser_contacted->surname;
                         $current_message= $curentuser_contacted->message; 
                        
                        }
                        ?>
                  </div>
               </div>

               <div id="review" class="tabcontent">
                  <div class="review-main-section">
                     <h2>Artist Reviews</h2>


                    <div class="review-overall">
 <?php
$row = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 5");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row[0]->aggregate_rating;
   $err = round($aggregate_rating);
  $total_reviews = $row[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
  if($err == "5") { ?>
   <div class="row first_reviews"> 
<div class="first_line">  
 <div class="side">
    <div>5 star</div></div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-5"></div>
    </div>
    </div> 
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
  
   <div class="review-inner-part"><span>Seller Communication: </span>  <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(sellercomm) as seller FROM artistreviews where artistlist= '$user_id'"); 
                           //print_r($avgsell); 
                           $average_sell=  $avgsell[0]->seller;
                           $average_sell= round($average_sell);
                           if($average_sell=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_sell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_sell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_sell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_sell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           ?> </div>  </div> 
               
               
                <div class="review-inner-part macrto"><span class="ght">Overall</span>  
                          <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(artistid) as artseller FROM artistreviews where artistlist= '$user_id'");  
                           $avg_artseller= $avgsell[0]->artseller;
                           $avg_artseller= round($avg_artseller);
                           if($avg_artseller=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($avg_artseller=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($avg_artseller=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($avg_artseller=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($avg_artseller=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
  
<!--div class="middle">
    <div class="bar-container">
      <div class="bar-5"></div>
    </div>
    </div> 
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div---> 
  
     
<?php } else {
  
  echo "No Record Found";
}




$row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 4");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row1[0]->aggregate_rating;
   $erro = round($aggregate_rating);
  $total_reviews = $row1[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
if ($erro == "4") { ?>
<div class="first_line">
 <div class="side">
    <div>4 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-4"></div>
    </div></div>
  <div class="side right">
     <div><?php echo $total_reviews; ?></div>
  </div>
  
  <div class="review-inner-part"><span>Delivery and Packing: </span>  <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(deliverynpaint) as deliseller FROM artistreviews where artistlist= '$user_id'");  
                           $average_delisell= $avgsell[0]->deliseller;
                           $average_delisell= round($average_delisell);
                           if($average_delisell=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_delisell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_delisell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_delisell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_delisell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
  </div>

  <?php 
 } 
 ?>
 <?php 
$row2 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 3");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row2[0]->aggregate_rating;
   $err3 = round($aggregate_rating);
  $total_reviews = $row2[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
 if ($err3 == "3") { ?> 
    <div class="first_line">
 <div class="side">
   <div>3 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-3"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
 <div class="review-inner-part"><span>Listing Accuracy: </span>  <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(listingacc) as listseller FROM artistreviews where artistlist= '$user_id'");  
                           $average_listsell= $avgsell[0]->listseller;
                           $average_listsell= round($average_listsell);
                           if($average_listsell=="1")
                           { ?>
                            <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_listsell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_listsell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_listsell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_listsell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
  
</div>
  <?php 
 }
 $row3 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 2");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row3[0]->aggregate_rating;
   $err4 = round($aggregate_rating);
  $total_reviews = $row3[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
 if ($err4 == "2") { ?> 
   <div class="first_line"> 
 <div class="side">
   <div>2 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-2"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div></div>


  <?php 
 }
$row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 1");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row1[0]->aggregate_rating;
   $err5 = round($aggregate_rating);
  $total_reviews = $row1[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
  if ($err5 == "1") { ?>
    <div class="first_line">     
 <div class="side">
   <div>1 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-1"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div></div>

 <?php 
 }
  ?>                             
  </div><div class="review-inner-left">
                        <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $findID = $wpdb->get_results("SELECT * FROM artistreviews where artistlist= '$user_id'  ORDER BY sno DESC"); 
                           //print_r($findID); 
                           foreach ( $findID as $fivesdraft ) 
                           {
                           ?> 
                           <div class="outer-review">
                           <div class="review-inner-main-left">

                              <div class="review-inner-part"><span>Delivery and Packing </span><?php  $deliverynpaint_pic= $fivesdraft->deliverynpaint; 
                                 if($deliverynpaint_pic=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($deliverynpaint_pic=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($deliverynpaint_pic=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($deliverynpaint_pic=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($deliverynpaint_pic=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                              <div class="review-inner-part"><span>Listing Accuracy </span><?php  $listingacc_pic= $fivesdraft->listingacc; 
                                 if($listingacc_pic=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($listingacc_pic=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($listingacc_pic=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($listingacc_pic=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($listingacc_pic=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                              <div class="review-inner-part"><span><b>Overall Rating</b> </span><?php  $overall_rating_con= $fivesdraft->artistid; 
                                 $overall_rating_con= round($overall_rating_con);
                                 
                                 if($overall_rating_con=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($overall_rating_con=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($overall_rating_con=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($overall_rating_con=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($overall_rating_con=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                           </div>
                           
                            <div class="review-inner-main-right">
  <div class="review-inner-part">
<?php
$wp_user_search = $wpdb->get_results("SELECT ID, user_nicename FROM $wpdb->users WHERE ID='$fivesdraft->username'");
//print_r($wp_user_search);
foreach ( $wp_user_search as $page ) {
    $page->user_nicename; 
   //echo $page['user_nicename']; 
  ?><h2><?php echo  $page->user_nicename;   ?></h2>
<?php    }  ?><span>
<?php  $date_publish= $fivesdraft->date; ?>
<?php   echo $newDate = date("d/m/Y", strtotime($date_publish)); ?>
<?php //echo $fivesdraft->date; ?></span>
<p><?php echo $fivesdraft->discription; ?></p>
<?php    $reviewid = $fivesdraft->sno; 
 //  $userss = wp_get_current_user();
  //echo $userss->ID.'jjjjj';
  //echo $user_id.'kkkk';  
  ?>    
<?php
if ( is_user_logged_in() && $userss->ID =='$user_id') { ?>
 <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Reply</button>
<?php } ?>
<div class="container">
  <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $user_name; ?> reply</h4>
        </div>
        <div class="modal-body">
         
<form name="artist-inert-table" method="post">
<input type="hidden" class="reviewid<?php echo $i;?>" value="<?php echo $reviewid; ?>" name="reviewid">
 <div class="two-section-table">
  <div class="two-section-tableinner">
  <input type="text" class="reply<?php echo $i;?>" name="reply" placeholder="Reply"/>
  </div>
  </div>      
</form>
<style>
.modal-dialog {
  padding-top: 201px;
}
</style>
</div>
        <div class="modal-footer">

          <?php echo $fivesdraft->reply;?>
           <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm<?php echo $i;?>()">SUBMIT</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script>
function submitContactForm<?php echo $i;?>(){
    var artist_id = $('.reviewid<?php echo $i;?>').val();
    //alert(artist_id);
    var reply = $('.reply<?php echo $i;?>').val();
   /// alert(reply);
        $.ajax({
        type: "POST",
        url: "<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php",
        data: "artist_id="+artist_id+"&reply="+reply, 
        cache: false,
        success: function(html) {   
        ///alert("sdjkffhdkj"); 
        location.reload();
        },
    error: function(error){
    //alert(error);
    }
    });
   }

 </script>
<?php ++$i; ?> 

<p class="reply-message"><strong><?php echo $user_name; ?> Reply:</strong></p>
<?php echo $fivesdraft->reply;  ?>  </div>
</div>
                      </div>
          <?php } ?>           
          </div>  
           </div> 
                                   
         
               <?php
                  } else {
                    global $current_user;
                     wp_get_current_user(); 
                  ?>
               <div id="artist" class="tabcontent">
                  <a class="add-product-button" href="<?php echo site_url(); ?>/add-product/">Add New Artwork</a>
      <?php  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $args = array(          
      'posts_per_page'   =>8,
      'post_type'        => 'download',
      'author' => $user_id, 
      'post_status'      => 'publish',  
      'paged'            => $paged
    );
    $wp_query = new WP_Query( $args );
    if( $wp_query->have_posts()):while($wp_query->have_posts()):$wp_query->the_post();

  ?>
                  <?php //echo the_title(); ?>
                  <div class="download-current-user-inner">
				  	<? $get_the_id= get_the_id(); 
			 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) {   }  else {  ?><span  class="onsale-product">Sale</span><?php  }  ?> 
              <div class="main-tbl-blf-dd">
    <div class="hover-img"><a href="<?php echo the_permalink(); ?>">VIEW DETAILS</a></div> 
      
      <?php 
        if ( has_post_thumbnail() ) { 
          echo '<p class="thumbnail_custome">';
          the_post_thumbnail("small");
          echo '</p>';
        }
      ?>
    </div> 
                     
                     <div class="download-current-user-name">    <a href="<?php the_permalink(); ?>" title="<?php //the_title_attribute(); ?>"><?php //the_title(); ?></a> </div>
  


                     <div class="basic-info-product ">



                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>  </h2>
                        <p>By <?php  the_author(); ?></p>
                        <p class="price_symbol">
						   <?php  $get_the_id= get_the_id(); 

 if( ! get_post_meta( $get_the_id, 'edd_sale_price', true ) ) {   echo $price = get_post_meta($get_the_id, 'edd_price', true); }  else {  ?>  <strong class="item-price"><span><del>£<? echo $price = get_post_meta($get_the_id, 'edd_price', true); ?></del>  £<?php echo $sale_price= get_post_meta($get_the_id, 'edd_sale_price', true); ?></span></strong> <?php  } ?>


                           <span class="add-new-btnd">
    <?php  

   
$current_date = strtotime(date("d-m-Y"));
//$datet = strtotime($datet);

  $past_date1= date("d-m-Y", strtotime('-10 days'));
 
  $past_date=  strtotime($past_date1);
    $post_date1= get_the_date();
 
//echo $post_date=strtotime($post_date1);

 
  $post_date2 = str_replace('/', '-', $post_date1);
  $todays_date = date("d-m-Y");
  $post_date = strtotime($post_date2);
  
  //echo $expiration_date.' | '.$today.'<br>';

if (($post_date > $past_date) && ($post_date <= $current_date))
    {
      echo "New";
    }
    

           ?>      </span> 
                        </p>
                     </div>

  
                     <div class="comlex-info-product">
                        <?php //dynamic_sidebar('reviews'); ?>
            
            
                        <form id="edd_purchase_<?php echo get_the_id(); ?>" class="edd_download_purchase_form edd_purchase_<?php echo get_the_id(); ?>" method="post">
                           <a href="#" class="edd-wl-button  before edd-wl-action edd-wl-open-modal glyph-left " data-action="edd_wl_open_modal" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single"><i class="glyphicon glyphicon-heart"></i></i><span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>       
                           <div class="edd_purchase_submit_wrapper">
                              <input class="edd-add-to-cart edd-no-js button  edd-submit" name="edd_purchase_download" value="£ <?php echo $price; ?>&nbsp;–&nbsp;Purchase" data-action="edd_add_to_cart" data-download-id="<?php echo get_the_id(); ?>" data-variable-price="no" data-price-mode="single" style="display: none;" type="submit"><a href="<?php echo site_url(); ?>/checkout/" class="edd_go_to_checkout button  edd-submit" style="display:none;"></a>
                           </div>
                           <!--end .edd_purchase_submit_wrapper-->
                        </form>
                 
             <style>
    .black_overlay{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;z-index:9998;
        background: rgba(0,0,0,0.8);
       
    }
    .white_content<?php echo $get_the_id; ?> {
        display: none;
        position: fixed;
        top: 25%;
        left: 50%;
        margin-left: -400px;
        width: 800px;
        /*height: 50%;*/
        padding: 20px;
        /*border: 16px solid orange;*/
        background-color: white;
        z-index:9999;
        
        box-shadow: 0 0 10px rgba(0,0,0,0.3);
    } .white_content<?php echo $get_the_id; ?> > a
    {position: absolute; right: -15px; top: -15px; width:30px; height: 30px;
      font:700 12px/30px arial; text-align: center; display: block;color: #FFF;
background: #70C0EF;
border-radius: 100px;}   

      .white_content<?php echo $get_the_id; ?> input[type="text"]
      {height:50px; width: 100%; border:1px solid #ccc;}

      .white_content<?php echo $get_the_id; ?> input[type="submit"]
      {position: absolute; right:0; top: 0; height: 50px;}

      .white_content<?php echo $get_the_id; ?> form
      {position: relative;}
</style>
 <p class="flex-caption"> <a href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $get_the_id; ?>').style.display='block';document.getElementById('fade').style.display='block'"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/star.png" /></a></p>
  <div id="light<?php echo $get_the_id; ?>" class="white_content<?php echo $get_the_id; ?>">
   <a class="close_icon" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">X</a>
  
  <?php
    $current_user = wp_get_current_user();
       $current_user_id= $current_user->ID;
?>

    <form name="review_table" class="review_table_form" method="post" action="<?php echo site_url(); ?>/review-thanks/">
            <input type="text" name="review_title" placeholder="Review Title*" required/>
            <select name="review_starts">
                <option name="0">Select Review star:</option>
                <option name="1">1</option>
                <option name="2">2</option>
                <option name="3">3</option>
                <option name="4">4</option>
                <option name="5">5</option>
              </select>
              <input type="text" name="review_description" placeholder="Review Description*" required/>
         <input type="hidden" name="product_id" value="<?php echo $get_the_id; ?>" />
         <input type="hidden" name="user_id" value="<?php echo $current_user_id; ?>">
             <input type="submit" name="submit" value="submit" />
       </form>
</div>


         
            
            
                        <a href="<?php echo site_url(); ?>/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_id(); ?>" class="checkout-custome-button"></a>
                     </div>
                  </div>
                   <?php 
    endwhile;
    endif;
  ?>     
  
    <div class="post-nav-container">
    <?php //previous_posts_link( __('&rarr; Older Posts',$wp_query->max_num_pages)); ?>
    <?php
    $big = 999999999; // need an unlikely integer

echo paginate_links( array(
  'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
  'format' => '?paged=%#%',
  'current' => max( 1, get_query_var('paged') ),
  'total' => $wp_query->max_num_pages
) );
     
    //next_posts_link( __('Newer Posts &larr; ')); ?>
  </div> </div>

  <?php //get_footer(); ?>

  <?php wp_reset_query(); ?>
   </div>
<?php 
}
                  if ( !is_user_logged_in() ){
  $user = get_user_by('ID', $_GET['user_id'] ); ?>
 <div id="aboutme" class="tabcontent">
                  <div class="section-second">
                     
                     <? $author_queryb = array('post_type' => 'about_us',
                        'posts_per_page' => '1','author' => $user_name);
                           $author_postsb = new WP_Query($author_queryb);
                           while($author_postsb->have_posts()) : $author_postsb->the_post();
                        ?>

                    <div class="second-section-authorname"><h2>About  <?php echo the_title();  ?></div>
                       
  
 <strong>  <div class="section-second-title">  <?php echo  get_post_meta( get_the_ID(), 'about_artist', true ); ?></div></strong>
                     <div class="section-second-content"><? echo the_content(); ?></div>
                     <?         endwhile; ?>
 </div>
               </div>
<?php  }  ?>         
               <?php
         if ( !is_user_logged_in() ){

          global $wpdb;                 
                  //print_r($author_postsa);
          $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

          $limit = 6; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "SELECT COUNT(`id`) FROM wp_posts where author=".$user_id );
$num_of_pages = ceil( $total / $limit );
        

  $user = get_user_by('ID', $_GET['user_id'] ); ?>
 <div id="myblog" class="tabcontent">
                  <div class="third-section">
                     <div class="third-section-authorname">Blog <?php  $user_name;  ?></div>
                     <?
                        $author_queryc = array('posts_per_page' => $limit,'author' => $user_id,'paged' => $pagenum);


                           $author_postsc = new WP_Query($author_queryc);
						 
                 while($author_postsc->have_posts()) : $author_postsc->the_post();
                     ?>
                     <div class="section-third-inner aaa">
                        <div class="section-third-thumb"><a href="<?php echo the_permalink(); ?>"><? echo the_post_thumbnail();  ?></a></div>
                        <div class="section-third-date"> <? echo get_the_date(); ?></div>
                        <div class="section-third-title"><? echo the_title(); ?></div>


                        <div class="section-third-content"><? echo the_content(); ?></div>
                     </div>
  <?php         endwhile;    ?>		






                  </div>
               </div>

                 <div class="pagination-compition">
<?php

 $page_links = paginate_links( array(
    'base' => add_query_arg( 'pagenum', '%#%' ),
    'format' => '',
    'prev_text' => __( '&laquo;', 'text-domain' ),
    'next_text' => __( '&raquo;', 'text-domain' ),
    'total' => $num_of_pages,
    'current' => $pagenum
) );

if ( $page_links ) {
    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
}
?>
  </div>
<?php  }  ?> 






<?php
         if ( !is_user_logged_in() ){
  $user = get_user_by('ID', $_GET['user_id'] ); ?>
<div id="contact" class="tabcontent">
                  <div class="third-section">
                     <div class="third-section-authorname">Contact  <?php  echo  $user_name;  ?></div>
                     <form name="contactusartist" method="post">
                        <input  class="input_uk" type="text" name="ftext" placeholder="First name" required />
                         <input class="input_us" type="text" name="ltext" placeholder="Surname" required />
                        <textarea rows="4" cols="50" name="ymessage" placeholder="Your message" required>
</textarea>
                        <input type="hidden" name="currentuser" value=" <?php  echo  $user->display_name;  ?>" />
                  <input class="input_submit" type="submit" name="submit" value="Send" />
                   <p>You can only contact the artist,If you have a buyer account,If you don't,please <a href="<?php  echo site_url(); ?>/ukarts/uk-artists-collection/">signup</a> for free </p>
                   </form>
                     <?php  
                        if (isset($_POST["submit"])) {
                           //echo "Yes";
                        $firstname = $_POST['ftext'];
                        $surname = $_POST['ltext'];
                        $message = $_POST['ymessage'];
                        $currentuser = $_POST['currentuser'];
                        
                        global $wpdb;
                        //$wpdb->query($sql);
                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser);
                        $wpdb->query($sql);   
                        
                        
                        
                        }else{  
                         //  echo "N0";
                        }
                        
                        
                        global $wpdb;
                          //print_r($current_user);
                           $current_user_name= $user->user_login;
                        $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
                        //print_r($curentuser_contact);
                        foreach ( $curentuser_contact as $curentuser_contacted )
                        {
                         $current_first= $curentuser_contacted->firstname; 
                         $current_surename= $curentuser_contacted->surname;
                         $current_message= $curentuser_contacted->message; 
                        
                        }
                        ?>
                  </div>
               </div>
<?php  }  ?>

               <div id="contact" class="tabcontent">
                  <div class="third-section">
                     <div class="third-section-authorname">Contact <?php  echo   $user_name;  ?></div>
                     <form name="contactusartist" method="post">
                        <input  class="input_uk" type="text" name="ftext" placeholder="First name*" required />
                         <input class="input_us" type="text" name="ltext" placeholder="Surname*" required />
                        <textarea rows="4" cols="50" name="ymessage" placeholder="Your message*" required>
</textarea>
                        <input type="hidden" name="currentuser" value=" <?php  echo  $current_user->display_name;  ?>" />
                  <input class="input_submit" type="submit" name="submit" value="Send" />
                   <p>You can only contact the artist,If you have a buyer account,If you don't,please <a href="#">signup</a> here </p>
                   </form>
                     <?php  
                        if (isset($_POST["submit"])) {
                           //echo "Yes";
                        $firstname = $_POST['ftext'];
                        $surname = $_POST['ltext'];
                        $message = $_POST['ymessage'];
                        $currentuser = $_POST['currentuser'];
                        
                        global $wpdb;
                        //$wpdb->query($sql);
                        $sql = $wpdb->prepare("INSERT INTO artistcontact(firstname,surname,message,currentlogin) values (%s,%s,%s,%s)", $firstname,$surname,$message,$currentuser);
                        $wpdb->query($sql);   
                        
                        
                        
                        }else{  
                         //  echo "N0";
                        }
                        
                        
                        global $wpdb;
                          //print_r($current_user);
                           $current_user_name= $current_user->user_login;
                        $curentuser_contact = $wpdb->get_results("SELECT DISTINCT author FROM artistcontact where currentlogin= '$current_user_name'");
                        //print_r($curentuser_contact);
                        foreach ( $curentuser_contact as $curentuser_contacted )
                        {
                         $current_first= $curentuser_contacted->firstname; 
                         $current_surename= $curentuser_contacted->surname;
                         $current_message= $curentuser_contacted->message; 
                        
                        }
                        ?>
                  </div>
               </div>
              <?php if ( !is_user_logged_in() ){
             $user = get_user_by('ID', $_GET['user_id'] ); ?>
               <div id="review" class="tabcontent">
                  <div class="review-main-section">
                     <h2>Artist Reviews</h2>
                     <div class="review-overall">
                        <?php
$row = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 5");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row[0]->aggregate_rating;
   $err = round($aggregate_rating);
  $total_reviews = $row[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();  
  if($err == "5") { ?>
<div class="row first_reviews">
<div class="first_line">     
 <div class="side">
    <div>5 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-5"></div>
    </div>
    </div> 
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
  
  <div class="review-inner-part"><span>Seller Communication </span>  <?php 

                        global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(sellercomm) as seller FROM artistreviews where artistlist= '$user_id'");  
                           $average_sell=  $avgsell[0]->seller;
                           $average_sell= round($average_sell);
                           if($average_sell=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_sell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_sell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_sell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_sell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else {
//echo "fff";

                            }
                           ?> </div>

  </div>
  
  
   <div class="review-inner-part macrto"><span class="ght">Overall</span> <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(artistid) as artseller FROM artistreviews where artistlist= '$user_id'");  
                           $avg_artseller= $avgsell[0]->artseller;
                           $avg_artseller= round($avg_artseller);
                           if($avg_artseller=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($avg_artseller=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($avg_artseller=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($avg_artseller=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($avg_artseller=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
  
     
<?php } 
else { }
$row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 4");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row1[0]->aggregate_rating;
   $erro = round($aggregate_rating);
  $total_reviews = $row1[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
if ($erro == "4") { ?>
<div class="first_line">  
 <div class="side">
    <div>4 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-4"></div>
    </div></div>
  <div class="side right">
     <div><?php echo $total_reviews; ?></div>
  </div>
  <div class="review-inner-part"><span>Delivery and Packing </span>  <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(deliverynpaint) as deliseller FROM artistreviews where artistlist= '$user_id'");  
                           $average_delisell= $avgsell[0]->deliseller;
                           $average_delisell= round($average_delisell);
                           if($average_delisell=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_delisell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_delisell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_delisell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_delisell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
  
  </div>

  <?php 
 } 
$row2 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 3");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row2[0]->aggregate_rating;
   $err3 = round($aggregate_rating);
  $total_reviews = $row2[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
 if ($err3 == "3") { ?> 
   <div class="first_line">   
 <div class="side">
   <div>3 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-3"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
  
  <div class="review-inner-part"><span>Listing Accuracy </span>  <?php global $wpdb;
                           //print_r($current_user);
                            $current_user_name= $user_name;
                           $avgsell = $wpdb->get_results("SELECT AVG(listingacc) as listseller FROM artistreviews where artistlist= '$user_id'");  
                           $average_listsell= $avgsell[0]->listseller;
                           $average_listsell= round($average_listsell);
                           
                           if($average_listsell=="1")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                           elseif($average_listsell=="2")
                            { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                           elseif($average_listsell=="3")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                           elseif($average_listsell=="4")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                           elseif($average_listsell=="5")
                           { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                           else { }
                           
                           ?> </div>
</div>

  <?php 
 }
 $row3 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 2");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row3[0]->aggregate_rating;
   $err4 = round($aggregate_rating);
  $total_reviews = $row3[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
 if ($err4 == "2") { ?> 
  <div class="first_line">    
 <div class="side">
   <div>2 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-2"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
</div>

  <?php 
 }
$row1 = $wpdb->get_results("SELECT COUNT(*) AS `total`, AVG(artistid) AS `aggregate_rating` FROM artistreviews WHERE `artistlist`= $user_id AND artistid = 1");
  $max_rating = $row[0]->max_rating; 
  $aggregate_rating = $row1[0]->aggregate_rating;
   $err5 = round($aggregate_rating);
  $total_reviews = $row1[0]->total; 
  $totl = $aggregate_rating * 20; 
  $wpdb->flush();
  if ($err5 == "1") { ?>
   <div class="first_line">        
 <div class="side">
   <div>1 star</div>
  </div>
   <div class="middle">
    <div class="bar-container">
      <div class="bar-1"></div>
    </div></div>
  <div class="side right">
    <div><?php echo $total_reviews; ?></div>
  </div>
</div>

  <?php  }  ?>
   
      <?php  $err; ?>                      
      <?php  $erro; ?>                      
      <?php  $err3; ?>                       
      <?php  $err4; ?>                       
      <?php  $err5; ?>    
			  <?php if($err=="0" && $erro=="" && $err3=="" && $err4=="" && $err5=="")  
			  { ?><div class="no-record-found"><? echo "No Record Found"; ?></div><? } else { //echo "no";
		  } ?>
	  
                      
                        
                        
                       
                     </div>
                     <div class="review-inner-left">
                      <?php $i=1; ?>
                        <?php global $wpdb;
                           //print_r($current_user);
                           //echo $current_user_name= $current_user->display_name;
                           $findID = $wpdb->get_results("SELECT * FROM artistreviews where artistlist= '$user_id' ORDER BY sno DESC"); 
                           //print_r($findID); 
                           foreach ( $findID as $fivesdraft ) 
                           {
                           ?> 
                        <div class="outer-review 1111">
                           <div class="review-inner-main-left">

                              <div class="review-inner-part"><span>Delivery and Packing </span><?php  $deliverynpaint_pic= $fivesdraft->deliverynpaint; 
                                 if($deliverynpaint_pic=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($deliverynpaint_pic=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($deliverynpaint_pic=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($deliverynpaint_pic=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($deliverynpaint_pic=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                              <div class="review-inner-part"><span>Listing Accuracy </span><?php  $listingacc_pic= $fivesdraft->listingacc; 
                                 if($listingacc_pic=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($listingacc_pic=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($listingacc_pic=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($listingacc_pic=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($listingacc_pic=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                              <div class="review-inner-part"><span><b>Overall Rating</b> </span><?php  $overall_rating_con= $fivesdraft->artistid; 
                                 $overall_rating_con= round($overall_rating_con);
                                 
                                 if($overall_rating_con=="1")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/1star.png" /><? }
                                 elseif($overall_rating_con=="2")
                                  { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/2star.png" /><? }
                                 elseif($overall_rating_con=="3")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/3star.png" /><? }
                                 elseif($overall_rating_con=="4")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/4star.png" /><? }
                                 elseif($overall_rating_con=="5")
                                 { ?><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/02/5star.png" /><? }
                                 else { } ?>
                              </div>
                           </div>
                         
                           <div class="review-inner-main-right">
                              <div class="review-inner-part">
<?php
$wp_user_search = $wpdb->get_results("SELECT ID, user_nicename FROM $wpdb->users WHERE ID='$fivesdraft->username'");
//print_r($wp_user_search);
foreach ( $wp_user_search as $page ) {
    $page->user_nicename; 
   //echo $page['user_nicename']; 



  ?>
                                  <h2><?php echo  $page->user_nicename;   ?></h2>
                             <?php    }  ?>
                                
                                 <span>
                 <?php  $date_publish= $fivesdraft->date; ?>
                <?php   echo $newDate = date("d/m/Y", strtotime($date_publish)); ?>
                 <?php //echo $fivesdraft->date; ?></span>
                                 <p><?php echo $fivesdraft->discription; ?></p>
  <?php    $reviewid = $fivesdraft->sno; 
 //  $userss = wp_get_current_user();
  //echo $userss->ID.'jjjjj';
  //echo $user_id.'kkkk';  
  ?>    
<?php

if ( is_user_logged_in() && $userss->ID =='$user_id') { ?>
 <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Reply</button>
<?php } ?>

<div class="container">
  <!--h2>Modal Example</h2-->
  <!-- Trigger the modal with a button -->
 
  <!-- Modal -->
  <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $user_name; ?> reply</h4>
        </div>


        <div class="modal-body">
         
<form name="artist-inert-table" method="post">
<input type="hidden" class="reviewid<?php echo $i;?>" value="<?php echo $reviewid; ?>" name="reviewid">
 <div class="two-section-table">
  <div class="two-section-tableinner">
  <input type="text" class="reply<?php echo $i;?>" name="reply" placeholder="Reply"/>
  </div>
  </div>      

</form>
<style>
.modal-dialog {
  padding-top: 201px;
}
</style>

        </div>
        <div class="modal-footer">

          <?php echo $fivesdraft->reply;?>
           <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm<?php echo $i;?>()">SUBMIT</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script>

 function submitContactForm<?php echo $i;?>(){
    var artist_id = $('.reviewid<?php echo $i;?>').val();
    //alert(artist_id);
    var reply = $('.reply<?php echo $i;?>').val();
   /// alert(reply);
        $.ajax({
        type: "POST",
        url: "<?php echo site_url(); ?>/wp-content/themes/marketify-child/ajax.php",
        data: "artist_id="+artist_id+"&reply="+reply, 
        cache: false,
        success: function(html) {   
        ///alert("sdjkffhdkj"); 
        location.reload();
        },
    error: function(error){
    //alert(error);
    }
    });
   }

 </script>
<?php ++$i; ?> 

<p class="reply-message"><strong><?php echo $user_name; ?> Reply:</strong></p>
<?php echo $fivesdraft->reply;  ?>  </div>
          </div>
               </div>
                     <hr>
                        </hr>

                        <?php  } ?>
                     </div>
                     <div class="review-inner-right">
                        <div class="review-inner-part">
                        </div>
                     </div>
                  </div>
               </div>
			    </div>
        <?php } 

?>
	 		
			
     
        


<style>
   .download-current-user-inner {
   width: 30%;
   float: left;
   margin-right: 2%;
   /* height: 300px; */
   overflow: hidden;
   margin-bottom: 3%;
   }
   .download-current-thumbnail { height: 300px; }
   .artist-work-button {
   background-color: #dedc00;
   width: 550px;
   padding: 8px 19px 1px 12px;
   margin-right: 43px;
   }
   .section-third-inner {
   width: 50%;
   float: left;
   } 
   div#artist {
   display: block;  
   }
   
   .tab button.active {
  background-color: #dedc00;
}
#aboutme a.linkw {
  color: #dedc00;
  width: 100%;
}
}
</style>

<div id="myModal" class="modal myModal container">
 <div class="modal-content">
    <span class="close">&times;</span>
    <div class="col-sm-12" ><?php 
$paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypalID = 'munishgup-facilitator@gmail.com'; //Business Email
    ?>
     <form action="<?php echo $paypalURL; ?>" method="post">
      <label>Amount Agreed</label>
     <input type="text" name="agree_budgt" id="agree_budgt" value="">
     <label>Amount Pending</label>
     <input type="text" name="first_milestone" id="first_budget" value="" readonly>
     <label>Amount Deposite</label>
     <input type="text" name="second_milestone" id="second_budget" value="" readonly>
      <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
        
        <!-- Specify a Buy Now button. -->
        <input type="hidden" name="cmd" value="_xclick">
        
        <input type="hidden" name="item_name" id="refrence_id" value="">
        <input type="hidden" name="item_number" id="item_number" value="">
        <input type="hidden" name="amount" id="amount_main" value="">
        <input type="hidden" name="currency_code" value="USD">
        
        <!-- Specify URLs -->
        <input type='hidden' name='cancel_return' value='<?php  echo site_url(); ?>/ukarts/'>
        <input type='hidden' name='return' value='<?php  echo site_url(); ?>/ukarts/confirm-payment'>
        <!--<input type="image" name="submit" border="0"
        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
        <!-- Display the payment button. -->
        
        <input type="submit" name="hire" value="confirm Hire">
      </form>
    
                </div>
     



     </div>
</div> 






    

<script>
  $(".myBtn").click(function(){
    
     $(".myModal").css("display","block");
     var amount=$(this).attr('data-amount');
     var half= amount/2;
 
     var ref_id=$(this).attr("data-refid");
     $("#agree_budgt").val(amount);
     $("#first_budget").val(half);
     $("#second_budget").val(half);
     $("#refrence_id").val(ref_id);
     $("#amount_main").val(half);
     var author=$(this).attr('data-author');
    var item_name= amount+'_'+half+'_<?php echo get_current_user_id(); ?>_'+author;
   $("#item_number").val(item_name);
     });
    
    $("#agree_budgt").blur(function(){
          var aaaaa=$(this).val();
          var bbb= aaaaa/2;
           $("#first_budget").val(bbb);
           $("#second_budget").val(bbb);
           $("#amount_main").val(bbb);
          
    });

// Get the modal

var modal = document.getElementsByClassName('myModal')[0];

// Get the button that opens the modal
var btn = document.getElementsByClassName("myBtn")[0];

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
   //  alert

}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>  </main>
         <!-- #main -->
      </div>
      <!-- #primary -->
   </div>

</div>
</div>
</div>
</div>

</div> 
<style>
  .new-msg{
    display:none !important;
  }
  
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.container2 {
    border: 2px solid #dedede;
    background-color: #f1f1f1;
    border-radius: 5px;
    padding: 10px;
    margin: 10px 0;
}

.darker {
    border-color: #ccc;
    background-color: #ddd;
}

.container2::after {
    content: "";
    clear: both;
    display: table;
}

.container2 img {
    float: left;
    max-width: 60px;
    width: 100%;
    margin-right: 20px;
    border-radius: 50%;
}

.container2 img.right {
    float: right;
    margin-left: 20px;
    margin-right:0;
}

.time-right {
    float: right;
    color: #aaa;
}

.time-left {
    float: left;
    color: #999;
}
</style>  

<?php get_footer(); ?>  