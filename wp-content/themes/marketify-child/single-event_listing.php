<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Marketify
 */

get_header(); ?>

    <?php do_action( 'marketify_entry_before' ); ?>
    <div class="titl-dat-mn">
<h2 class="title-cont-clf"><?php echo 'Events & Exhibitions'; ?></h2>

</div> <div class="full-bak">
    <div class="container">
        <div id="content" class="site-content row">

            <div role="main" id="primary" class="col-xs-12 col-md-12">
                <main id="main" class="site-main event_sig" role="main">
  
                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); 
                       display_event_banner(); ?>
 <div class="event-all">	 				   
					   <div class="event-title-sig"><?php echo the_title(); ?></div>
					
                   <date><?php $timestamp = strtotime(get_event_start_date()); if($timestamp!= null): echo date("d F Y",$timestamp); endif;?></date> <span class="mid-bar">- </span><date><?php $timestamp = strtotime(get_event_end_date()); if($timestamp!= null): echo date("d F",$timestamp); endif;?></date> 
				   <div class="gallery-name"> <?php echo get_post_meta( get_the_id(),'_event_venue_name', true );  
					 ?> </div>
                     <div class="exc-name"><?php display_event_category(); ?> Artists: <?php echo get_post_meta( get_the_id(),'_organizer_name', true );?></div> 
					<p> <?php echo get_post_meta( get_the_id(),'_event_description', true );
					 ?></p>
					  <div class="sigle-address"><h2>Address:</h2>
					  <span class="add-sing"><?php echo get_post_meta( get_the_id(),'_event_location', true );?>, <?php echo get_post_meta( get_the_id(),'_event_address', true );?>, <?php echo get_post_meta( get_the_id(),'_city/town', true );?>, <?php echo get_post_meta( get_the_id(),'_event_venue_name', true );?>, <?php echo get_post_meta( get_the_id(),'_event_pincode', true );?></span> 
					 <span class="tel-sing"><b>Tel: </b><?php echo get_post_meta( get_the_id(),'_telephone_no', true );?></span>
					 <span class="email-sing"><b>Email:</b> <?php echo get_post_meta( get_the_id(),'_registration', true );?></span>
					 <?php
						if(get_post_meta( get_the_id(),'_organizer_website', true ) == ''){
							echo "</br>";
						}else{
					 ?>
					 <span class="web-sing"><b>Website:</b> <?php echo get_post_meta( get_the_id(),'_organizer_website', true );?></span></div> 
						<?php }?>
					  <div class="sigle-time"><h2>Opening times</h2> <?php
					
       $from_date =get_event_start_date();
	   //echo "<p class='time-day1'><b>Start Date : </b>".$newDate = date("d-m-Y", strtotime($from_date))."</p>";
	   
$to_date1 =get_event_end_date();

	   
$from_date = new DateTime($from_date);
$to_date = new DateTime($to_date1);

$d = $newDate1 - $newDate;

$f = 0;

for ($date = $from_date; $date <= $to_date; $date->modify('+1 day')) {
	
	if($f >= '7'){
		break;
	}
  echo '<p class="time-day"><b>'.$date->format('l') . ":</b>\n".get_event_start_time()." - ".get_event_end_time().'</p>';
  $f++;
}  

//echo "<p class='time-day1'><b>End Date : </b>". $newDate1 = date("d-m-Y", strtotime($to_date1))."</p>";

  $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
					  </div> 
	<div class="socil_share">				
<div class="footer-social">
 <h2>Share</h2>
<a class="btn-share twitter" href="https://twitter.com/share?u=<?php echo $current_url; ?>"><span class="screen-reader-text">Twitter</span></a>

<a class="btn-share instagram" href="https://instagram.com"><span class="screen-reader-text">Instagram</span></a>

<a class="btn-share facebook" href="https://www.facebook.com/sharer.php?u=<?php echo $current_url; ?>&t=<?php echo the_title(); ?>" title="Facebook share button" target="blank"><span class="screen-reader-text">Facebook</span></a>

<a href="https://pinterest.com/pin/create/button/?url=<?php echo $current_url; ?>&description=<?php the_title(); ?>" ><span class="screen-reader-text">Pinterest</span></a>

<a href="mailto:?subject=I wanted you to see this event&amp;body=Check out this event <?php echo $current_url; ?>"
   title="Share by Email"><i class="fa fa-envelope" aria-hidden="true"></i></a>
 
    
</div>      
					  
		 </div>   </div> 		


					  
					  
					 
					<?php endwhile; ?>      
 
         
                <?php endif; ?>  
				
				
			
                </main><!-- #main -->
            </div><!-- #primary -->

            <?php //get_sidebar(); ?>

        </div>
    
    
	
	
<div class="page-event-pre-next">
<?php
$prev_post = get_previous_post();
if($prev_post) {
   $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
   echo "\t" . '<a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class=" "><i class="fa fa-angle-left"></i>Back</a>' . "<span class='bar_side'>|<span>";
}

$next_post = get_next_post();
if($next_post) {
   $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
   echo "\t" . '<a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" ">More<br /><i class="fa fa-angle-right"></i></a>' . "\n";
}
?>
</div>
</div>
 </div>
	  <script>
jQuery(document).ready(function(){
var lol = jQuery(".page-event-pre-next a").length;
if(lol == '1'){
	jQuery(".bar_side").css("display","none");
}else{	
jQuery(".bar_side").css("display","inline-block");
}
});
</script>
<?php get_footer(); ?>
