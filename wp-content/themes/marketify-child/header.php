<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package Marketify
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri('custom.css'); ?>">
        <link rel="stylesheet" href="http://beta.uk-arts.com/wp-content/themes/marketify-child/custom.css">
        <!-- jQuery library -->

        <!-- Latest compiled JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> 
        <script>
            function toggleIcon(e) {
                $(e.target)
                        .prev('.panel-heading')
                        .find(".more-less")
                        .toggleClass('glyphicon-plus glyphicon-minus');
            }
            $('.panel-group').on('hidden.bs.collapse', toggleIcon);
            $('.panel-group').on('shown.bs.collapse', toggleIcon);

        </script>
        <script>
            $(document).ready(function () {
                $("#menu-item-771").hover(function () {
                    $(".sub-menu").css("background-color", "#F2F2F2");
                    $(".sub-menu").css("opacity", "0.98");
                });


                $(".fes-price-value").attr("placeholder", "0.00");


                $(".wpcf7-list-item-label").before("<label for='radio' class='radio-custom-label'></label>");
                $('.wpcf7-form input:radio').addClass("radio-custom");

                $('#event_start_date, #event_end_date').attr('readonly', true);

                $('#telephone_no').on('keydown', function (e) {
                    if (!((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39)) {
                        return false;
                    }
                    var value = $(this).val().length;
                    if (value > 15) {
                        alert('Please enter maximum 15 number');

                    }
                });
                $('#event_pincode').on('keydown', function (e) {
                    if ((e.keyCode == 187 || e.keyCode == 220)) {
                        return false;
                    }
                    var value = $(this).val().length;
                    if (value > 10) {
                        alert('Please enter maximum 10 number');
                    }
                });

            });
        </script>



        <?php wp_head(); ?> 
    </head> 

    <body <?php body_class(); ?>>
        <div id="load"></div>
        <div id="page" class="hfeed site">
            <?php
            /*  $current_user = wp_get_current_user();
              echo "<pre>";
              print_r($current_user);
              echo "</pre>"; */
            $user_id = get_current_user_id();
            $user_meta = get_userdata($user_id);
            $user_roles = $user_meta->roles;
            $rol = $user_roles[0];
            if ($rol == administrator) {
                ?>
                <style>
                    #complex-information {
                        display: none;
                    }
                </style>
            <?php } ?>
            <header id="masthead" class="site-header" role="banner">
                <div class="container">

                    <div class="site-header-inner">

                        <div class="site-branding">
                            <?php $header_image = get_header_image(); ?>
                            <?php if (!empty($header_image)) : ?>
                                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="custom-header"><img src="<?php echo esc_url($header_image); ?>" alt=""></a>
                            <?php endif; ?>

                            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                            <h2 class="site-description screen-reader-text"><?php bloginfo('description'); ?></h2>
                        </div>

                        <button class="js-toggle-nav-menu--primary nav-menu--primary-toggle"><span class="screen-reader-text"><?php _e('Menu', 'marketify'); ?></span></button>
                        <div class="right-nav">
                            <div class="top-header"><?php dynamic_sidebar('headerer'); ?>
                                <li class="nav-menu-search top-src"><a href="#" class="js-toggle-search"><span class="screen-reader-text">Search</span></a>

                                    <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
                                        <button type="submit" class="search-submit"><span class="screen-reader-text"><?php _e('Submit', 'marketify'); ?></span></button>
                                        <label>
                                            <span class="screen-reader-text"><?php _ex('Search for:', 'label', 'marketify'); ?></span>
                                            <input type="search" class="search-field" placeholder="<?php echo esc_attr__('search art', 'marketify'); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" required title="<?php echo esc_attr__('Search for:', 'marketify'); ?>">
                                        </label>

                                        <input type="hidden" name="post_type" value="<?php echo esc_attr($type); ?>" />
                                    </form>

                                <li class="current-cart menu-item menu-item-has-children asdfghjkl">
                                    <a href="<?php echo edd_get_checkout_uri(); ?>"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/05/cartd.png" /> <span class="header-cart edd-cart-quantity"><?php echo edd_get_cart_quantity(); ?></span>
                                    </a></li>

                                <?php
                                global $current_user;
                                wp_get_current_user();
                                ?>
                                <?php if (is_user_logged_in()) { ?>
                                    <li>
                                        <?php
                                        echo '<a href="' . wp_logout_url() . '" title="Logout">Logout</a>';
//echo 'Username: ' . $current_user->user_login . "\n";  
                                        ?> 
                                    </li>
                                    <?php
                                } else {
                                    ?><li class="sign-cls"><img class="sign-ibn" src="<?php echo site_url(); ?>/wp-content/uploads/2018/03/mantwo12.png"><a href="<?php echo site_url(); ?>/login/">sign in</a></li><?php }
                                ?>



                                <?php //echo do_shortcode('[yobro_chat_notification]');   ?>
                                </li>

                                <style>
                                    .page-id-1124 li.wpuf-el.post_title {
                                        display: none;
                                    }
                                </style>

                            </div>


                            <?php
                            $get_the_id = get_the_id();
                            if ($get_the_id == "167" || $get_the_id == "697" || $get_the_id == "717" || $get_the_id == "727" || $get_the_id == "736" || $get_the_id == "748" || $get_the_id == "742" || $get_the_id == "754" || $get_the_id == "182") {
                                ?>
                                <style>

                                    .right-nav #menu-menunew #menu-item-771 .sub-menu{ 
                                        opacity:0.98 !important;
                                        transform: none !important;
                                        display: block;
                                    }
                                    .right-nav #menu-menunew #menu-item-771:hover .sub-menu{ 
                                        display: block !important;
                                    }
                                    #menu-item-1408:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                    #menu-item-1262:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                    #menu-item-1786:hover .profile-custome {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }

                                </style> 
                            <?php } elseif ($get_the_id == "117" || $get_the_id == "2024") {
                                ?>

                                <style>
                                    .right-nav #menu-menunew #menu-item-1262 .sub-menu{ 
                                        opacity:0.98 !important;
                                        transform: none !important;
                                    }

                                    #menu-item-771:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                    #menu-item-1408:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                    #menu-item-1786:hover .profile-custome {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }

                                </style>
                            <? } elseif ($get_the_id == "565") { ?>
                                <style>
                                    .right-nav #menu-menunew #menu-item-1408 .sub-menu{ 
                                        opacity:0.98 !important;
                                        transform: none !important;
                                    }
                                    #menu-item-771:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                    #menu-item-1262:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }

                                    #menu-item-1786:hover .profile-custome {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }
                                </style> <?php
                            } elseif ($get_the_id == "629" || $get_the_id == "890" || $get_the_id == "661" || $get_the_id == "1124" || $get_the_id == "685" || $get_the_id == "645" || $get_the_id == "1241" || $get_the_id == "890") {
                                ?>
                                <style>
                                    .mydashboard_li.profile-custome { 
                                        opacity:0.98 !important;
                                        transform: none !important;
                                    }
                                    #menu-item-1262:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }

                                    #menu-item-771:hover .sub-menu {
                                        position: absolute;
                                        z-index: 99999 !important;
                                    }

                                </style> <?php
                            } else {
                                
                            }
                            ?> 

                            <?php
                            $args = array(
                                'theme_location' => 'primary'
                            );

                            if (has_nav_menu('primary')) {
                                $args['container_class'] = 'nav-menu nav-menu--primary';
                            } else {
                                $args['menu_class'] = 'nav-menu nav-menu--primary';
                            }

                            wp_nav_menu($args);
                            ?>


                        </div>

                    </div>

                </div>


            </header><!-- #masthead -->
            <?php if (is_front_page() || is_home()) { ?>
                <style>
                    .page-header.page-header--singular.container {
                        display: none;
                    }
                </style>
                <div class="first-sl">
                    <?php echo do_shortcode('[rev_slider alias="home-slider"]'); ?>
                </div>

                <!-- <div class="banner">
                <h1>The very best of UK Arts</h1>
                <div class="ban-anchor"><a class="artist" href="http://gloanaya.com/ukarts/join-uk-arts-family/" rel="noopener">artist</a>
                <a class="artist2" href="http://gloanaya.com/ukarts/uk-artists-collection/" rel="noopener">art lover</a>
                <a class="artist3" href="http://gloanaya.com/ukarts/directory-index/" rel="noopener">art supplier</a></div>
                <p>Rewarding artists, art buyers and art lovers with more than just a gallery</p>
                
                </div> -->

                <?php
            } else {

//everything else
            }
            ?>

            <style>
                .page-header.page-header--singular.container {
                    display: none;
                }
                .banner {
                    background: #dfdc01 none repeat scroll 0 0;
                    padding-bottom: 30px;
                }
                .banner p {
                    color: #000;
                    font-size: 24px;
                }
                .banner p {
                    padding-top: 21px;
                }
            </style>
            <!--div class="search-form-overlay">
            <?php
            // add_filter( 'get_search_form', array( marketify()->template->header, 'search_form' ) );
            //  get_search_form();
            //  remove_filter( 'get_search_form', array( marketify()->template->header, 'search_form' ) );
            ?>
            </div-->
            <div <?php // echo apply_filters( 'marketify_page_header', array() );                                                                                                                       ?>>


                <script>

                    $('#menu-menunew li').hover(function () {
                        //$(this).children('.sub-menu').show();//hover on
                    }, function () {
                        //$(this).children('.sub-menu').hide();//hover off
                    });
                </script>

                <?php if (is_user_logged_in()) { ?>
                    <style>
                        li.current-cart.menu-item.menu-item-has-children.xzvcxvcx {
                            margin-right: 10%;
                            margin-top: -4.5% !important;
                        }
                        li.current-cart.menu-item.menu-item-has-children.xzvcxvcx {
                            display: block;
                        }
                        @media screen and (min-width: 992px) and (max-width: 1220px) {
                            li.current-cart.menu-item.menu-item-has-children.xzvcxvcx {
                                margin-top: -6% !important;
                                margin-right: 11% !important;
                            }
                            #menu-item-1786 .profile-custome {
                                right: -180px !important;
                            }
                            #menu-item-771 .sub-menu {
                                top: 100%;
                            }
                            #menu-item-1408 .sub-menu {
                                top: 100%;
                            }
                            #menu-item-1786 .profile-custome {
                                height: 40px;
                                top: 100% !important;
                            }
                            #menu-item-1786 .profile-custome li {
                                padding-top: 10px;
                            }
                            #menu-item-1408 .sub-menu {
                                right: -391px !important;
                                width: 1576px !important;
                            }
                        }
                        @media screen and (min-width: 320px) and (max-width: 375px) {
                            .right-nav .top-header li a {
                                margin-left: 34% !important;
                            }

                        }
                        @media screen and (min-width: 320px) and (max-width: 499px) {
                            .current-cart.menu-item.menu-item-has-children.asdfghjkl {
                                width: 28%;
                            }
                            .top-header li a {
                                margin-left: 53% !important;
                            }
                            #wprmenu_bar {
                                top: 0 !important;
                            }
                        }
                        @media screen and (min-width: 500px) and (max-width: 615px) {
                            .top-header li a {
                                margin-left: 56% !important;
                            }
                            .current-cart.menu-item.menu-item-has-children.asdfghjkl {
                                right: 60px;
                            }
                            #wprmenu_bar {
                                top: 0 !important;
                            }
                        }
                        @media screen and (min-width: 616px) and (max-width: 767px) {
                            .top-header li a {
                                margin-left: 65% !important;
                                margin-top: 6% !important;
                            }
                            #wprmenu_bar {
                                top: 0 !important;
                            }
                            .current-cart.menu-item.menu-item-has-children.asdfghjkl {
                                top: 102px;
                                right: 50px;
                                width: 25%;
                            }
                        }
                        @media screen and (min-width: 768px) and (max-width: 991px) {
                            #wprmenu_bar {
                                top: 0 !important;
                            }
                            .top-header li a {
                                margin-left: 17% !important;
                                margin-top: 1.3% !important;
                            }

                            .current-cart.menu-item.menu-item-has-children.asdfghjkl {
                                position: absolute;
                                top: 74%;
                                right: 64px;
                                z-index: 999999999 !important;
                                float: right;
                                text-align: right;
                                width: 9%;
                                display: block !important;
                            }
                        }   
                    </style>
                    <?
                } else {
                    
                }
                ?>