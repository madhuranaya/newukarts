<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Marketify
 */

get_header(); ?>
  
    <?php do_action( 'marketify_entry_before' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
<div class="single page-mainnnn">
    <div class="container">
    <div class="after-container-class-start">
	
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
	<!--div class="singal-page-allevents">
	 <a href="<?php //site_url().'/events-and-exhibitions/'; ?>">All events</a>
</div-->


 <div class="singal-page-title">
	 <h1> <?php echo get_the_title(); ?>
</h1></div>
<div class="singal-page-date">
	  <?php echo get_the_date(); ?>
</div>
    

       <div class="col-sm-8">
      <div class="singal-page-image"> 
	  <span class="imagessss"><?php the_post_thumbnail(); ?></span>
</div>
</div>     

<div class="col-sm-12">

<div class="singal-page-content">
 <?php the_content(); ?>
</div>

</div>
      
 
        <span class="mand-saab"> <?php echo do_shortcode('[feather_share]'); ?>
        <?php
            wp_link_pages( array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'marketify' ),
                'after'  => '</div>',
            ) );
        ?>
</span>
        <div class="entry-meta entry-meta--hentry entry-meta--footer">
            <?php the_tags( '<span class="entry-tags">', ', ', '</span>' ); ?>
            <span class="entry-categories"><?php the_category( ', ' ); ?></span>
            <?php //edit_post_link( __( 'Edit', 'marketify' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-meta -->
    <!-- .entry-content -->
</article><!-- #post-## --> 
    

        <?php do_action( 'marketify_single_download_after' ); ?>
    </div>
    </div>
    </div>

    <?php endwhile; ?>

<?php get_footer(); ?>
